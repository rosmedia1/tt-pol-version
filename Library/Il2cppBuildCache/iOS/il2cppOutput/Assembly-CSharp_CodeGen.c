﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AdMob::Start()
extern void AdMob_Start_m57EC0FB42DA5A695EC8E4C11E5D090AE6C5E0000 (void);
// 0x00000002 System.Void AdMob::RequestInterstitialAd()
extern void AdMob_RequestInterstitialAd_mDB3FA8710CD2A4EA3F422BC1AE5E7F25855E328A (void);
// 0x00000003 System.Void AdMob::ShowInterstitialAd()
extern void AdMob_ShowInterstitialAd_m0229EB7E07C5AF7CC1AA1C33A905574DE266D795 (void);
// 0x00000004 System.Void AdMob::DestroyInterstitialAd()
extern void AdMob_DestroyInterstitialAd_m416BA52C8B72CFD20E1D1969F2ACA722E2DE66F4 (void);
// 0x00000005 System.Void AdMob::HandleOnAdLoaded(System.Object,System.EventArgs)
extern void AdMob_HandleOnAdLoaded_m424636419BF546EA518C8A191BD47233B8FF5530 (void);
// 0x00000006 System.Void AdMob::HandleOnAdOpening(System.Object,System.EventArgs)
extern void AdMob_HandleOnAdOpening_mCC6315FCBBDDE48EFA010374B3907DADF12232DE (void);
// 0x00000007 System.Void AdMob::HandleOnAdClosed(System.Object,System.EventArgs)
extern void AdMob_HandleOnAdClosed_mCC1BA65008B2F588BBCC275876EFF9BCC32DDF33 (void);
// 0x00000008 GoogleMobileAds.Api.AdRequest AdMob::AdRequestBuild()
extern void AdMob_AdRequestBuild_mAE6957133C5FECA4E32036F8F588F653075F619D (void);
// 0x00000009 System.Void AdMob::OnDestroy()
extern void AdMob_OnDestroy_m0DFD2DFA7292422B91CCCF7B90F1252453DFC891 (void);
// 0x0000000A System.Void AdMob::.ctor()
extern void AdMob__ctor_m3AF6D0DE3D81B4D4BD12A1D4AC4BD5A05A9CADAB (void);
// 0x0000000B System.Void AdMob/<>c::.cctor()
extern void U3CU3Ec__cctor_mA913151AFE1DA1C09137E8800A264AAF7AEABD59 (void);
// 0x0000000C System.Void AdMob/<>c::.ctor()
extern void U3CU3Ec__ctor_m99E41F298DB640BCD88C6CA722086CB50BC9A950 (void);
// 0x0000000D System.Void AdMob/<>c::<Start>b__3_0(GoogleMobileAds.Api.InitializationStatus)
extern void U3CU3Ec_U3CStartU3Eb__3_0_mA3BDBAFDED17F7AB3D8EE0800929654FEDF94E14 (void);
// 0x0000000E System.Void CanvasScalee::Awake()
extern void CanvasScalee_Awake_mE23A5037C45D9ABAAF18F19829C6A58CD8136B68 (void);
// 0x0000000F System.Void CanvasScalee::Update()
extern void CanvasScalee_Update_mA6C8D0459A45CD766D372708650B6593E049293F (void);
// 0x00000010 System.Void CanvasScalee::HorizontalDevice()
extern void CanvasScalee_HorizontalDevice_mC3ADFD21A9D7FF3BB0FF72922E05EAFFB8A4BB5F (void);
// 0x00000011 System.Void CanvasScalee::VerticalDevice()
extern void CanvasScalee_VerticalDevice_m53BC16D7FB10DA55EBE442B421639DBDF787BE29 (void);
// 0x00000012 System.Void CanvasScalee::.ctor()
extern void CanvasScalee__ctor_m97823671FEEA8622BA9A267617576988F62742BB (void);
// 0x00000013 System.Void Ukryj_Przyciski::Start()
extern void Ukryj_Przyciski_Start_mCA379C17946B72EC4177A151ABCF5203F7FE4671 (void);
// 0x00000014 System.Void Ukryj_Przyciski::Update()
extern void Ukryj_Przyciski_Update_m7225AE8D5F4BF914AAA4A77C1CBA6EB40F3E92BF (void);
// 0x00000015 System.Void Ukryj_Przyciski::Ukryj_Przyciskii()
extern void Ukryj_Przyciski_Ukryj_Przyciskii_m43C6F498FCD342C0187DB1BBAC5635A8DD7D87C2 (void);
// 0x00000016 System.Void Ukryj_Przyciski::Pokaz_przyciski()
extern void Ukryj_Przyciski_Pokaz_przyciski_m2374FB68550094D7DFF04B075F7F884DAFD22DA8 (void);
// 0x00000017 System.Void Ukryj_Przyciski::Ukryj_buttony()
extern void Ukryj_Przyciski_Ukryj_buttony_mCEE1B086BA08F2619F36BBE442744FCE99474ED9 (void);
// 0x00000018 System.Void Ukryj_Przyciski::Ukryj_dzwiek()
extern void Ukryj_Przyciski_Ukryj_dzwiek_mA09F3F3241FB901A4909098E915D72BB4BA625F2 (void);
// 0x00000019 System.Void Ukryj_Przyciski::.ctor()
extern void Ukryj_Przyciski__ctor_m51003C1E4FF70C7329F848CA9C26F786E2CB7410 (void);
// 0x0000001A System.Void Blokuj::Start()
extern void Blokuj_Start_m7D9999C88CD393050B9DD976A6D5109FFF6663ED (void);
// 0x0000001B System.Void Blokuj::Update()
extern void Blokuj_Update_mECC3730E9BA541D2934866205F91DCCB173D4E89 (void);
// 0x0000001C System.Void Blokuj::.ctor()
extern void Blokuj__ctor_mAA8A588A6155BDBC1B16B0E2C2E9A90538EA5B8B (void);
// 0x0000001D System.Void chmurka_skrypt::Start()
extern void chmurka_skrypt_Start_m4001D6630924851E60BA9DD37009F90852CB4CEE (void);
// 0x0000001E System.Void chmurka_skrypt::Update()
extern void chmurka_skrypt_Update_m30A172E4B46FED9E08FC8B5DB4602E412163DC73 (void);
// 0x0000001F System.Void chmurka_skrypt::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void chmurka_skrypt_OnTriggerEnter2D_m3E3BAFC6E80DA30D6A7A2DE20F62CD338603FC9F (void);
// 0x00000020 System.Void chmurka_skrypt::Lec_chmuro()
extern void chmurka_skrypt_Lec_chmuro_mA23DB4E9F5BAF0D1E5BF9FD3CFB83BF7040B9485 (void);
// 0x00000021 System.Void chmurka_skrypt::.ctor()
extern void chmurka_skrypt__ctor_m277D3B06BF8A5B8290D1C6A04259D8A767F19F42 (void);
// 0x00000022 System.Void Cykl_dnia_i_nocy_KD::Start()
extern void Cykl_dnia_i_nocy_KD_Start_m4A89FD7E4EB155A374CA31D5BC5C84D0849A29AC (void);
// 0x00000023 System.Void Cykl_dnia_i_nocy_KD::Update()
extern void Cykl_dnia_i_nocy_KD_Update_m40AFA75EAE41C2ABB54CB374442FD0590BE2A9CD (void);
// 0x00000024 System.Void Cykl_dnia_i_nocy_KD::KaczkaDuza_spac()
extern void Cykl_dnia_i_nocy_KD_KaczkaDuza_spac_mAFEF227FEEE0C4373A55E1BBA516F237AA82ABD5 (void);
// 0x00000025 System.Void Cykl_dnia_i_nocy_KD::KaczkaDuza_wstawac()
extern void Cykl_dnia_i_nocy_KD_KaczkaDuza_wstawac_mC31FF962C404C9F28ED19304626ABADA7941CDA6 (void);
// 0x00000026 System.Void Cykl_dnia_i_nocy_KD::.ctor()
extern void Cykl_dnia_i_nocy_KD__ctor_mD183901B2ABAFB052E14F7DEFFC26106D12AE8D9 (void);
// 0x00000027 System.Void Cykl_dnia_i_nocy_KM::Start()
extern void Cykl_dnia_i_nocy_KM_Start_m9642C935DFBA0292001F0C9C415EC4599A46A119 (void);
// 0x00000028 System.Void Cykl_dnia_i_nocy_KM::Update()
extern void Cykl_dnia_i_nocy_KM_Update_m4940E8E120460DC109FA29904FBD0AB739765692 (void);
// 0x00000029 System.Void Cykl_dnia_i_nocy_KM::KaczkaMala_spac()
extern void Cykl_dnia_i_nocy_KM_KaczkaMala_spac_mF69A78953AF5F86462E912D009F20B2A195DB835 (void);
// 0x0000002A System.Void Cykl_dnia_i_nocy_KM::KaczkaMala_wstawac()
extern void Cykl_dnia_i_nocy_KM_KaczkaMala_wstawac_m1777D034017CBCAE997DC698EE4583FEF1C3A017 (void);
// 0x0000002B System.Void Cykl_dnia_i_nocy_KM::.ctor()
extern void Cykl_dnia_i_nocy_KM__ctor_m83E63B19C0E09301F79AF8349CD0D28FFE0CEE3F (void);
// 0x0000002C System.Void Czekin::Start()
extern void Czekin_Start_m979E185466F7036186E9B51AA09B6DCEB8B52B96 (void);
// 0x0000002D System.Void Czekin::Update()
extern void Czekin_Update_mB76A692AB4DA4667A859B1ECB3A15A881B540C00 (void);
// 0x0000002E System.Void Czekin::.ctor()
extern void Czekin__ctor_m447DA073AD40D5B0DB597F4C6F771BC86F46891F (void);
// 0x0000002F System.Void dzwieki_kota::Start()
extern void dzwieki_kota_Start_m9953A186D820281F17A275795D2FD1FEBBF472BC (void);
// 0x00000030 System.Void dzwieki_kota::Update()
extern void dzwieki_kota_Update_m12FFCB5F7EF6EAE912A19FCA6DC54FF3A3B37460 (void);
// 0x00000031 System.Void dzwieki_kota::NoZaryczNOoo()
extern void dzwieki_kota_NoZaryczNOoo_m0F941904849768D81A962B31C81882667C9BD8BE (void);
// 0x00000032 System.Void dzwieki_kota::.ctor()
extern void dzwieki_kota__ctor_mCD7E125AF4F9C459EAE8C92E24B06878412F6732 (void);
// 0x00000033 System.Void Dzwiek_jedzenia::Start()
extern void Dzwiek_jedzenia_Start_mACBF243CD6926E7091D1831C47EA44B24CCA25CE (void);
// 0x00000034 System.Void Dzwiek_jedzenia::Update()
extern void Dzwiek_jedzenia_Update_m8838CA76DD3596EAAAB8E4F0C3F257B14F16D7A3 (void);
// 0x00000035 System.Void Dzwiek_jedzenia::Glosno_Jedz()
extern void Dzwiek_jedzenia_Glosno_Jedz_m9B6B266BD465DAB5D246DEA0D092C49617206322 (void);
// 0x00000036 System.Void Dzwiek_jedzenia::.ctor()
extern void Dzwiek_jedzenia__ctor_m75EDADB5464A1D91D14AB3862771CAA1BF012826 (void);
// 0x00000037 System.Void Fidget_spinner::Start()
extern void Fidget_spinner_Start_m82FC572DBB71D9E45B624E7A15493647528EF9A8 (void);
// 0x00000038 System.Void Fidget_spinner::Update()
extern void Fidget_spinner_Update_m30E5BBD8686298607DDDF6C191776B5B98DD4E18 (void);
// 0x00000039 System.Void Fidget_spinner::Odpal_Animacje()
extern void Fidget_spinner_Odpal_Animacje_m0E18A73032913AC9AA5F3B8A6EDB386267C0819B (void);
// 0x0000003A System.Void Fidget_spinner::Odpal_muze()
extern void Fidget_spinner_Odpal_muze_m1DA52C93A3A595629CC8DF65A7BAAA0C3DC462F9 (void);
// 0x0000003B System.Void Fidget_spinner::blyskOn()
extern void Fidget_spinner_blyskOn_mFD5EA1DC284D62551E7951DAFA4525DDC6EF9ECD (void);
// 0x0000003C System.Void Fidget_spinner::blyskOff()
extern void Fidget_spinner_blyskOff_mDC644C7D8303697F4FD13D29624FFB8B53AD3F7C (void);
// 0x0000003D System.Void Fidget_spinner::Inny()
extern void Fidget_spinner_Inny_mBA83C5F4199B54AD7D4A22AF22764A5E04D72A77 (void);
// 0x0000003E System.Collections.IEnumerator Fidget_spinner::Zmien_Skina()
extern void Fidget_spinner_Zmien_Skina_m34940A073AB7D236A063046589ACDE0B07B1FF8B (void);
// 0x0000003F System.Void Fidget_spinner::.ctor()
extern void Fidget_spinner__ctor_mF9BE1C87B163D78B174D68C097D11556A9C4E835 (void);
// 0x00000040 System.Void Fidget_spinner/<Zmien_Skina>d__15::.ctor(System.Int32)
extern void U3CZmien_SkinaU3Ed__15__ctor_mC970868DD676D16509FFC48057ABFDE9819811C7 (void);
// 0x00000041 System.Void Fidget_spinner/<Zmien_Skina>d__15::System.IDisposable.Dispose()
extern void U3CZmien_SkinaU3Ed__15_System_IDisposable_Dispose_m0A3B931E488D319B38530986189548785A72377E (void);
// 0x00000042 System.Boolean Fidget_spinner/<Zmien_Skina>d__15::MoveNext()
extern void U3CZmien_SkinaU3Ed__15_MoveNext_mEEB84C2ACA9E0DA7DC48C19141EE6AB00BE326D4 (void);
// 0x00000043 System.Object Fidget_spinner/<Zmien_Skina>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZmien_SkinaU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2501D0C9911D023218134F55A083E5B1238EF26D (void);
// 0x00000044 System.Void Fidget_spinner/<Zmien_Skina>d__15::System.Collections.IEnumerator.Reset()
extern void U3CZmien_SkinaU3Ed__15_System_Collections_IEnumerator_Reset_mDDFE5FD4BD94657F7E70F0421C44DCB3F97B725F (void);
// 0x00000045 System.Object Fidget_spinner/<Zmien_Skina>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CZmien_SkinaU3Ed__15_System_Collections_IEnumerator_get_Current_mBFF4369F424BA1E174C2DFE83ABFF4102C1F91A6 (void);
// 0x00000046 System.Void gumka_do_mazania::Start()
extern void gumka_do_mazania_Start_mB05A17244FBE71EF09BF4BA15627CC47181602D9 (void);
// 0x00000047 System.Void gumka_do_mazania::Update()
extern void gumka_do_mazania_Update_m9B29A404D9C6D53799FF2BF7DD18082CA8975DDB (void);
// 0x00000048 System.Void gumka_do_mazania::.ctor()
extern void gumka_do_mazania__ctor_m803139ACAF1464AF15846F2DA3ADAEE5F1F42827 (void);
// 0x00000049 System.Void Idle_czy_nie::Start()
extern void Idle_czy_nie_Start_m3BFD1BF99DF00A3AB86C5B8A044BB3145F525761 (void);
// 0x0000004A System.Void Idle_czy_nie::Update()
extern void Idle_czy_nie_Update_mDAE5A4504796CF242C6AD76F41350F16FDDF80E6 (void);
// 0x0000004B System.Void Idle_czy_nie::.ctor()
extern void Idle_czy_nie__ctor_m730FC0AA2B06CA0D73CFA5DB2D3A56964FD7200A (void);
// 0x0000004C System.Void Informacje_od_Kaczek::Start()
extern void Informacje_od_Kaczek_Start_m805A59A9E5C16D177BF8508C2B8AA639FFCEFF5C (void);
// 0x0000004D System.Void Informacje_od_Kaczek::Update()
extern void Informacje_od_Kaczek_Update_mC92FBAFD4E96E5C9C84A4F728358AA682EBD5B2E (void);
// 0x0000004E System.Void Informacje_od_Kaczek::.ctor()
extern void Informacje_od_Kaczek__ctor_mA53E38BF33186AA93D74524959BD43AC9F605CFF (void);
// 0x0000004F System.Void kaczka_chodzi_dzwiek::Start()
extern void kaczka_chodzi_dzwiek_Start_mA1D99DE56DD4B075FE1B410C0D722A0CDE276EDB (void);
// 0x00000050 System.Void kaczka_chodzi_dzwiek::Update()
extern void kaczka_chodzi_dzwiek_Update_mCEF6AF162A6D70CCAFF5AC93492CDE29BED9FCF3 (void);
// 0x00000051 System.Void kaczka_chodzi_dzwiek::Kaczka_Do_Wody()
extern void kaczka_chodzi_dzwiek_Kaczka_Do_Wody_mCB741966F8D5DC735CA52F5C4B83DE6BDE61F4DC (void);
// 0x00000052 System.Void kaczka_chodzi_dzwiek::Kaczka_z_wody()
extern void kaczka_chodzi_dzwiek_Kaczka_z_wody_m69C94ED5C38547FE30597E08A42CEAB7860B64FA (void);
// 0x00000053 System.Void kaczka_chodzi_dzwiek::.ctor()
extern void kaczka_chodzi_dzwiek__ctor_mF72367B29A337E6CA759F5866DEBC6924843B712 (void);
// 0x00000054 System.Void Kaczka_Moze_Spac::Start()
extern void Kaczka_Moze_Spac_Start_mCD49C7DE18F65B11AD9F6FFC4DA31A41892AB48C (void);
// 0x00000055 System.Void Kaczka_Moze_Spac::Update()
extern void Kaczka_Moze_Spac_Update_mF24CF1AF481507BD588411196B71E51442DDC0B6 (void);
// 0x00000056 System.Void Kaczka_Moze_Spac::MozeszSpacAnim_KaczkaMala()
extern void Kaczka_Moze_Spac_MozeszSpacAnim_KaczkaMala_mD846E250ECE171F1D18EB3C0167B4CD6E45C11C0 (void);
// 0x00000057 System.Void Kaczka_Moze_Spac::MozeszSpacAnim_KaczkaDuza()
extern void Kaczka_Moze_Spac_MozeszSpacAnim_KaczkaDuza_m9DACFC38741EF220D9B24991CF0CCDF0139597E2 (void);
// 0x00000058 System.Void Kaczka_Moze_Spac::NieMozeszSpacAnim_KaczkaMala()
extern void Kaczka_Moze_Spac_NieMozeszSpacAnim_KaczkaMala_m52D3F7A87490E6E137A1341D1E69564233E65751 (void);
// 0x00000059 System.Void Kaczka_Moze_Spac::NieMozeszSpacAnim_KaczkaDuza()
extern void Kaczka_Moze_Spac_NieMozeszSpacAnim_KaczkaDuza_mEAB6B467DE415818D071230D64004568C81F6369 (void);
// 0x0000005A System.Void Kaczka_Moze_Spac::KaczkiSen()
extern void Kaczka_Moze_Spac_KaczkiSen_m3C864CFB9100999A64DB664467482E1E8E31EDEC (void);
// 0x0000005B System.Void Kaczka_Moze_Spac::KaczkaDuzaIdzieSpac()
extern void Kaczka_Moze_Spac_KaczkaDuzaIdzieSpac_m7BACC40FD0E8C95226D1263700AF381E174FB51D (void);
// 0x0000005C System.Void Kaczka_Moze_Spac::KaczkaMalaIdzieSpac()
extern void Kaczka_Moze_Spac_KaczkaMalaIdzieSpac_mD06283125C7A5C3D8ACCEB473BD5A0FC87021B60 (void);
// 0x0000005D System.Void Kaczka_Moze_Spac::.ctor()
extern void Kaczka_Moze_Spac__ctor_m72C98BDF8A6A5F5B535053AE398A86296CB0F92A (void);
// 0x0000005E System.Void kwakaj::Start()
extern void kwakaj_Start_mC4819124482D9C62613C44B2971599AED6808D30 (void);
// 0x0000005F System.Void kwakaj::Update()
extern void kwakaj_Update_mC0E895FDB40F6C31E519D182C51A1089CDAF6672 (void);
// 0x00000060 System.Void kwakaj::Kwaki()
extern void kwakaj_Kwaki_mB9B6BC5049338D4D8399189B296E32157FD222A2 (void);
// 0x00000061 System.Void kwakaj::.ctor()
extern void kwakaj__ctor_m66B55FAF07B5F3BB9720D3AC9E77437F7B50A054 (void);
// 0x00000062 UnityEngine.Color32 ColourToggle::get_Colour()
extern void ColourToggle_get_Colour_m646B5EFA0029FF0C178614485F333FFCE1FCE07D (void);
// 0x00000063 System.Boolean ColourToggle::get_IsEraser()
extern void ColourToggle_get_IsEraser_mD8670D6FE9CBC4044BF4C2A52F2E0CB16715BDE4 (void);
// 0x00000064 System.Void ColourToggle::OnValidate()
extern void ColourToggle_OnValidate_m3564D71886A3F14C7AB49907461F27E6C416BC98 (void);
// 0x00000065 System.Void ColourToggle::.ctor()
extern void ColourToggle__ctor_mA779BD33C616ECAB81E92F4F63AE419DA52BE4E6 (void);
// 0x00000066 System.Boolean MouseDraw::get_IsInFocus()
extern void MouseDraw_get_IsInFocus_m9DF3761CB2B9B5BD4FF2A3D69F652E242C882C1F (void);
// 0x00000067 System.Void MouseDraw::set_IsInFocus(System.Boolean)
extern void MouseDraw_set_IsInFocus_mD3579AB4670C1838E5A05692F5D1A0AB98870480 (void);
// 0x00000068 System.Void MouseDraw::Start()
extern void MouseDraw_Start_m462E17E9DDF33F63B0847AB489E746EF1EC1B5E8 (void);
// 0x00000069 System.Void MouseDraw::Losuj_Kolorki_LR()
extern void MouseDraw_Losuj_Kolorki_LR_mC3E47934AF34FBA2CA0A27788737A0E4A74C2B59 (void);
// 0x0000006A System.Void MouseDraw::Losuj_Rozmiar_Pena()
extern void MouseDraw_Losuj_Rozmiar_Pena_m7B242C8724DEE179C920AF0ABEA60944F7566428 (void);
// 0x0000006B System.Void MouseDraw::OnEnable()
extern void MouseDraw_OnEnable_m751585397ABB391EDB442C3D93D9A28761FE1260 (void);
// 0x0000006C System.Void MouseDraw::SliderValueDidChange(System.Single)
extern void MouseDraw_SliderValueDidChange_m3FF26499C58666275DF01805A506BFCEE4B0C7A4 (void);
// 0x0000006D System.Void MouseDraw::Update()
extern void MouseDraw_Update_m58672FBD99BE2250AD9501BE8E184D4BDD476557 (void);
// 0x0000006E System.Boolean MouseDraw::CzyTrzyma()
extern void MouseDraw_CzyTrzyma_mA46649EC4944BE79ADA0E539646F14FABFC8C355 (void);
// 0x0000006F System.Void MouseDraw::Init()
extern void MouseDraw_Init_mE5C868DF1E378476A40D68863EB12E9F5912BE90 (void);
// 0x00000070 System.Void MouseDraw::WritePixels(UnityEngine.Vector2,System.Int32)
extern void MouseDraw_WritePixels_m83421E4CBF155C7155192EF3AA7C94A01F3132C3 (void);
// 0x00000071 System.Void MouseDraw::WritePixelsV2(System.Single)
extern void MouseDraw_WritePixelsV2_mD1758A33693F8270FDA115B6B37DD12917317606 (void);
// 0x00000072 System.Void MouseDraw::ClearTexture()
extern void MouseDraw_ClearTexture_m3521795A523C2790042B5102F47F2FC0D33EE4A2 (void);
// 0x00000073 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw::GetNeighbouringPixels(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw_GetNeighbouringPixels_m2E3359641FB7C7B05F3D520F0EA5EAE520D2ADB4 (void);
// 0x00000074 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw::GetNeighbouringPixelsV2(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Int32)
extern void MouseDraw_GetNeighbouringPixelsV2_m3AD33DC50F5C0B0A946880BA8CF115B6A2885492 (void);
// 0x00000075 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw::GetLinearPositions(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw_GetLinearPositions_m8BA2A27046A5AA9A94D485AD6C7D258A51A36DDA (void);
// 0x00000076 System.Void MouseDraw::SetPenColour(UnityEngine.Color32)
extern void MouseDraw_SetPenColour_m6E50FC62C7B583E898CACAAFEA3759EE0FEBD115 (void);
// 0x00000077 System.Void MouseDraw::SetPenRadius(System.Int32)
extern void MouseDraw_SetPenRadius_m3DBDF3D9D1BFDF0A14C85842E56ACF41648470AC (void);
// 0x00000078 System.Void MouseDraw::SetPenPointerSize()
extern void MouseDraw_SetPenPointerSize_m0B3FB5B716DB2D55D78A78276C291B9E0EBF4FEE (void);
// 0x00000079 System.Void MouseDraw::TogglePenPointerVisibility(System.Boolean)
extern void MouseDraw_TogglePenPointerVisibility_m62E7F1B1A05B7CAF28FD7704BB8C9BD10B399E24 (void);
// 0x0000007A System.Void MouseDraw::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw_OnPointerEnter_mC78F84D022050BDE2B18BC2FABBA3F0CE30FFEE6 (void);
// 0x0000007B System.Void MouseDraw::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw_OnPointerExit_mE3159B0A1D5EC9E8777F121DC7E7C008D3BA6625 (void);
// 0x0000007C System.Void MouseDraw::.ctor()
extern void MouseDraw__ctor_m9B651CF30415AD30664A77F220E83AD976AA0C20 (void);
// 0x0000007D System.Void MainUI::Start()
extern void MainUI_Start_m1ACA1116EEE959FA317F44DAB4C292C67F61D218 (void);
// 0x0000007E System.Void MainUI::Update()
extern void MainUI_Update_m7D1D4AEB499AAFEBE3230CA860D9A2722A817D74 (void);
// 0x0000007F System.Void MainUI::PoziomInstrumenty()
extern void MainUI_PoziomInstrumenty_mF2508799305B595D3459FBD3C23F924C8E3BC456 (void);
// 0x00000080 System.Void MainUI::PoziomFarma()
extern void MainUI_PoziomFarma_mE729B033115D1F0919A0D08FE97E4801028F737B (void);
// 0x00000081 System.Void MainUI::PoziomElektronika()
extern void MainUI_PoziomElektronika_m0669CF91A7F17E5ECAA2CE41779B81C26AA730D1 (void);
// 0x00000082 System.Void MainUI::PoziomZabawki()
extern void MainUI_PoziomZabawki_m724709A3D9DC9957D86B4056E0D36FD1158186B0 (void);
// 0x00000083 System.Void MainUI::PoziomKolorowanka()
extern void MainUI_PoziomKolorowanka_mE8B9513F70EB43434609E2105DD4CEF29ACAE904 (void);
// 0x00000084 System.Void MainUI::PoziomCzajnik()
extern void MainUI_PoziomCzajnik_m3D9BB39687EF7E20E77598519D5358FC2E88E496 (void);
// 0x00000085 System.Void MainUI::Ekran_dla_rodzicow()
extern void MainUI_Ekran_dla_rodzicow_mB27C9C63A19437FDFF1BFB4634C87D9C1908D0AF (void);
// 0x00000086 System.Void MainUI::BackButton()
extern void MainUI_BackButton_mDBA1E197379A13116B3E06F6745D438D65B9653B (void);
// 0x00000087 System.Collections.IEnumerator MainUI::zaladuj_poziom(System.Int32)
extern void MainUI_zaladuj_poziom_m0B69BC63CE2A02185ACA7704EB19FF52333CF639 (void);
// 0x00000088 System.Collections.IEnumerator MainUI::zaladuj_poziom_dla_rodzicow(System.Int32)
extern void MainUI_zaladuj_poziom_dla_rodzicow_m7F479949F8F29BDF4B7C6FB200F1DFAC2035BEA0 (void);
// 0x00000089 System.Void MainUI::DzwiekOnOff()
extern void MainUI_DzwiekOnOff_m11BE615043A28C9A604C77D35ACA0028DB14A4DA (void);
// 0x0000008A System.Void MainUI::.ctor()
extern void MainUI__ctor_m7F9F45AD02C9A3C4DD9347331F0F4B5B37343CF5 (void);
// 0x0000008B System.Void MainUI/<zaladuj_poziom>d__19::.ctor(System.Int32)
extern void U3Czaladuj_poziomU3Ed__19__ctor_m4C144F3AFC90240B3DBD5DF17E574C5D6F9C5FE7 (void);
// 0x0000008C System.Void MainUI/<zaladuj_poziom>d__19::System.IDisposable.Dispose()
extern void U3Czaladuj_poziomU3Ed__19_System_IDisposable_Dispose_m105E9B7BEF85B7124DEBEB6FA6DFA26267EC689D (void);
// 0x0000008D System.Boolean MainUI/<zaladuj_poziom>d__19::MoveNext()
extern void U3Czaladuj_poziomU3Ed__19_MoveNext_mA82F68CC22E4AB67AECE99B7CBD413DBFFB97865 (void);
// 0x0000008E System.Object MainUI/<zaladuj_poziom>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Czaladuj_poziomU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FE179B31C136993BEF610209FB5C15132E0D023 (void);
// 0x0000008F System.Void MainUI/<zaladuj_poziom>d__19::System.Collections.IEnumerator.Reset()
extern void U3Czaladuj_poziomU3Ed__19_System_Collections_IEnumerator_Reset_m747FE02A4C158D1095DE853BD89308BEA9C77BB1 (void);
// 0x00000090 System.Object MainUI/<zaladuj_poziom>d__19::System.Collections.IEnumerator.get_Current()
extern void U3Czaladuj_poziomU3Ed__19_System_Collections_IEnumerator_get_Current_m377015ED983F849BD2EB31F4D5C08C1C760D12B9 (void);
// 0x00000091 System.Void MainUI/<zaladuj_poziom_dla_rodzicow>d__20::.ctor(System.Int32)
extern void U3Czaladuj_poziom_dla_rodzicowU3Ed__20__ctor_m6A6B8ABCB8DA20E736A3D6E9F05752149DF76DC1 (void);
// 0x00000092 System.Void MainUI/<zaladuj_poziom_dla_rodzicow>d__20::System.IDisposable.Dispose()
extern void U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_IDisposable_Dispose_m7A15C2897ABB45066496ED779CEE7725451FA328 (void);
// 0x00000093 System.Boolean MainUI/<zaladuj_poziom_dla_rodzicow>d__20::MoveNext()
extern void U3Czaladuj_poziom_dla_rodzicowU3Ed__20_MoveNext_mFB7D855DD718454AA580883D18EBA80B2C29F404 (void);
// 0x00000094 System.Object MainUI/<zaladuj_poziom_dla_rodzicow>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4F34244896F484CA187CB8D4A193DC3EAFB6619 (void);
// 0x00000095 System.Void MainUI/<zaladuj_poziom_dla_rodzicow>d__20::System.Collections.IEnumerator.Reset()
extern void U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_Collections_IEnumerator_Reset_m6BEAEBDAA1BFD474B8873E37D2AB5CA3EEFB3418 (void);
// 0x00000096 System.Object MainUI/<zaladuj_poziom_dla_rodzicow>d__20::System.Collections.IEnumerator.get_Current()
extern void U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_Collections_IEnumerator_get_Current_m40FF36F8AC9785F77F68A7D3BF7FA432A2D2B4F1 (void);
// 0x00000097 System.Boolean MouseDraw2RD::get_IsInFocus()
extern void MouseDraw2RD_get_IsInFocus_mC044E4CFEF9380B7E2427142CD3F98FD903DEF29 (void);
// 0x00000098 System.Void MouseDraw2RD::set_IsInFocus(System.Boolean)
extern void MouseDraw2RD_set_IsInFocus_m3630CC7B7A23BCB0A3D2CF47AEC0CBFBA9C14219 (void);
// 0x00000099 System.Void MouseDraw2RD::Start()
extern void MouseDraw2RD_Start_m965221C706E54F098F54746AD9C73DB5DACED5B1 (void);
// 0x0000009A System.Void MouseDraw2RD::OnEnable()
extern void MouseDraw2RD_OnEnable_m197C3B23B55B7597EC4E0E42F7085CE9897BD628 (void);
// 0x0000009B System.Void MouseDraw2RD::Update()
extern void MouseDraw2RD_Update_m8BBF45A1CB4E9A0B8AD1E268EC5C768248AFB743 (void);
// 0x0000009C System.Void MouseDraw2RD::Init()
extern void MouseDraw2RD_Init_mE1D47BAC13F75B51F2C2CDB5223472822F544728 (void);
// 0x0000009D System.Void MouseDraw2RD::Losuj_Kolorki_LR()
extern void MouseDraw2RD_Losuj_Kolorki_LR_mF2084DDF61109EE276C6DAE34E5AA6A5204245A5 (void);
// 0x0000009E System.Void MouseDraw2RD::Losuj_Rozmiar_Pena()
extern void MouseDraw2RD_Losuj_Rozmiar_Pena_mDC947F7C7A2EDAFBB87A1FA0C12496ED77AD7BDA (void);
// 0x0000009F System.Void MouseDraw2RD::WritePixels(UnityEngine.Vector2)
extern void MouseDraw2RD_WritePixels_mCA6F8B55308E01056AC62B4AAE392FFE3CCD4379 (void);
// 0x000000A0 System.Void MouseDraw2RD::ClearTexture()
extern void MouseDraw2RD_ClearTexture_mA095D2E6CDF0EF71B1A36F8467147C727D3E69E3 (void);
// 0x000000A1 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw2RD::GetNeighbouringPixels(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw2RD_GetNeighbouringPixels_m6391C6B0C908D382F117AA2FC438C3DA97D9CDC2 (void);
// 0x000000A2 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw2RD::GetLinearPositions(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw2RD_GetLinearPositions_m2DDA79349495416AF082692A348A073761343748 (void);
// 0x000000A3 System.Void MouseDraw2RD::SetPenColour(UnityEngine.Color32)
extern void MouseDraw2RD_SetPenColour_mF4BECF9996BEB881710EF4D53AE42F8D32D27265 (void);
// 0x000000A4 System.Void MouseDraw2RD::SetPenRadius(System.Int32)
extern void MouseDraw2RD_SetPenRadius_m02610ED7D0D4EFF15C8C2D4754329FE38D8BC4E1 (void);
// 0x000000A5 System.Void MouseDraw2RD::SetPenPointerSize()
extern void MouseDraw2RD_SetPenPointerSize_m79CC868B78C96F834F336BF791EECAB2C120183A (void);
// 0x000000A6 System.Void MouseDraw2RD::SetPenPointerPosition(UnityEngine.Vector2)
extern void MouseDraw2RD_SetPenPointerPosition_mEFB6234801AB6C47FBBDE8EE5048E7116D22625D (void);
// 0x000000A7 System.Void MouseDraw2RD::TogglePenPointerVisibility(System.Boolean)
extern void MouseDraw2RD_TogglePenPointerVisibility_m4B2221C0E3BC9519F82C22DBA23DD32CF6C658B2 (void);
// 0x000000A8 System.Void MouseDraw2RD::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw2RD_OnPointerEnter_mD16EC1ABAECB59AB0170E4CBA6BF50871C25C00A (void);
// 0x000000A9 System.Void MouseDraw2RD::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw2RD_OnPointerExit_mBDBFCB1C6EFE62BAAD1BDF18867786D37546D7D3 (void);
// 0x000000AA System.Void MouseDraw2RD::ExportSketch(System.String,System.String)
extern void MouseDraw2RD_ExportSketch_mF45A0DA841B1DB18940DF84BED4368B9A14A4A50 (void);
// 0x000000AB System.Void MouseDraw2RD::.ctor()
extern void MouseDraw2RD__ctor_mBCA21F080D3AEA3B1B2627C990E44A1F4967E0A2 (void);
// 0x000000AC System.Boolean MouseDraw_ForMap3::get_IsInFocus()
extern void MouseDraw_ForMap3_get_IsInFocus_mFEB5CE66C73EF17A9BF8AB37F8EDCA1C1052E84A (void);
// 0x000000AD System.Void MouseDraw_ForMap3::set_IsInFocus(System.Boolean)
extern void MouseDraw_ForMap3_set_IsInFocus_m5B97688C9CF9C8D7667356CF7B6EA6FEF5C34CAD (void);
// 0x000000AE System.Void MouseDraw_ForMap3::Start()
extern void MouseDraw_ForMap3_Start_mA15A2480DBF291BCF1ED32EC47734CB66396010E (void);
// 0x000000AF System.Void MouseDraw_ForMap3::Losuj_Kolorki_LR()
extern void MouseDraw_ForMap3_Losuj_Kolorki_LR_m45D52C4B6BD174B543ADA3C251C42735B8581FE7 (void);
// 0x000000B0 System.Void MouseDraw_ForMap3::OnEnable()
extern void MouseDraw_ForMap3_OnEnable_mFE10C63E9C7A2CA71DF5024F1177F9095C8CAF4E (void);
// 0x000000B1 System.Void MouseDraw_ForMap3::Update()
extern void MouseDraw_ForMap3_Update_m4DBCE060B6B439E1188F467D1F86B67D9B12576F (void);
// 0x000000B2 System.Void MouseDraw_ForMap3::Init()
extern void MouseDraw_ForMap3_Init_mB1C5DA17FEB3AE09831A705B93809ECE04D673C8 (void);
// 0x000000B3 System.Void MouseDraw_ForMap3::WritePixels(UnityEngine.Vector2)
extern void MouseDraw_ForMap3_WritePixels_mA928F3C590878DF582615161FA4FAC192AB8154A (void);
// 0x000000B4 System.Void MouseDraw_ForMap3::ClearTexture()
extern void MouseDraw_ForMap3_ClearTexture_mCEB54F33C29D15EC9888F0D52DCF3E07D7FFF05D (void);
// 0x000000B5 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw_ForMap3::GetNeighbouringPixels(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw_ForMap3_GetNeighbouringPixels_mA321A1AF01B620ADC354CD1BFEBB28FE9ADCC7EC (void);
// 0x000000B6 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw_ForMap3::GetLinearPositions(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw_ForMap3_GetLinearPositions_m09549495AF2EFC1D9A3085C3E857DDAEFAA42845 (void);
// 0x000000B7 System.Void MouseDraw_ForMap3::SetPenColour(UnityEngine.Color32)
extern void MouseDraw_ForMap3_SetPenColour_m5E93F6BCBF7A59C4657F62DE81A44CDD517C4CC8 (void);
// 0x000000B8 System.Void MouseDraw_ForMap3::SetPenRadius(System.Int32)
extern void MouseDraw_ForMap3_SetPenRadius_m0ECEFE154DD74B484A29278BC74DC97BCDF27D1F (void);
// 0x000000B9 System.Void MouseDraw_ForMap3::SetGumSensor(System.Int32)
extern void MouseDraw_ForMap3_SetGumSensor_mE29F159362C0259B4E475DDE11D9A415234D53AE (void);
// 0x000000BA System.Void MouseDraw_ForMap3::SetGumSensorV2(System.Int32)
extern void MouseDraw_ForMap3_SetGumSensorV2_m239149196DDC5F8E9A6DCC4CAD5ADAB671A578C8 (void);
// 0x000000BB System.Void MouseDraw_ForMap3::PositionOfGum(UnityEngine.Vector2)
extern void MouseDraw_ForMap3_PositionOfGum_m38AE773B38215CD6EC230647E13EA07E0718895F (void);
// 0x000000BC System.Void MouseDraw_ForMap3::TogglePenPointerVisibility(System.Boolean)
extern void MouseDraw_ForMap3_TogglePenPointerVisibility_m254B51A14EC22A35CB558FDA0CED8CB96BB3BFFE (void);
// 0x000000BD UnityEngine.Vector2 MouseDraw_ForMap3::Correct(UnityEngine.Vector2)
extern void MouseDraw_ForMap3_Correct_m77A30D7DBCCCC1A485F9EFEF4A76902E98B90D66 (void);
// 0x000000BE System.Void MouseDraw_ForMap3::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw_ForMap3_OnPointerEnter_mC5E595BFF45266759314A76041990D8C71730673 (void);
// 0x000000BF System.Void MouseDraw_ForMap3::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw_ForMap3_OnPointerExit_m773C1624CB04AC0B562449760B55702939F238D0 (void);
// 0x000000C0 System.Void MouseDraw_ForMap3::.ctor()
extern void MouseDraw_ForMap3__ctor_mAE2D06B125C43341021E4C5A479F19B47033B0E0 (void);
// 0x000000C1 System.Boolean MouseDraw_Orginal::get_IsInFocus()
extern void MouseDraw_Orginal_get_IsInFocus_mB87C9A8FC97F249A000A20E60818D713D0D68CE2 (void);
// 0x000000C2 System.Void MouseDraw_Orginal::set_IsInFocus(System.Boolean)
extern void MouseDraw_Orginal_set_IsInFocus_m693C00134801A08D9063F5D20EADAE96172CD215 (void);
// 0x000000C3 System.Void MouseDraw_Orginal::Start()
extern void MouseDraw_Orginal_Start_mBC1DBE625705C15BCCC7E5E8F4B2587F14D613C4 (void);
// 0x000000C4 System.Void MouseDraw_Orginal::OnEnable()
extern void MouseDraw_Orginal_OnEnable_mBDB36B6C0593AE62B018BCB03DFF846061802E70 (void);
// 0x000000C5 System.Void MouseDraw_Orginal::Update()
extern void MouseDraw_Orginal_Update_mE11B94AFEB30318C74AD81DA3C3FDE1D0CB729E2 (void);
// 0x000000C6 System.Void MouseDraw_Orginal::Init()
extern void MouseDraw_Orginal_Init_m2599664D2B2DAC2545F97F5840E8B10521DAD9F1 (void);
// 0x000000C7 System.Void MouseDraw_Orginal::WritePixels(UnityEngine.Vector2)
extern void MouseDraw_Orginal_WritePixels_mAEAEC5238B23833882B6E0153792B4E154B22CFE (void);
// 0x000000C8 System.Void MouseDraw_Orginal::ClearTexture()
extern void MouseDraw_Orginal_ClearTexture_m954BEC4167A94C46A4624DB74AE0FC52D0FE64CF (void);
// 0x000000C9 System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw_Orginal::GetNeighbouringPixels(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw_Orginal_GetNeighbouringPixels_mDFEF1ABD69B49EE4039FEE0E715B5499AD395719 (void);
// 0x000000CA System.Collections.Generic.List`1<UnityEngine.Vector2> MouseDraw_Orginal::GetLinearPositions(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void MouseDraw_Orginal_GetLinearPositions_m4CB2CF3DD8C7BC1A4BD408F9E90007AFF13189DE (void);
// 0x000000CB System.Void MouseDraw_Orginal::SetPenColour(UnityEngine.Color32)
extern void MouseDraw_Orginal_SetPenColour_m166D61356E05D1E5F222575D5CEB98E6F64C1BF2 (void);
// 0x000000CC System.Void MouseDraw_Orginal::SetPenRadius(System.Int32)
extern void MouseDraw_Orginal_SetPenRadius_mAF081B4C286D663E434460FB54EA79A54F430A13 (void);
// 0x000000CD System.Void MouseDraw_Orginal::SetPenPointerSize()
extern void MouseDraw_Orginal_SetPenPointerSize_m09CF86F6D00FDBA67E7FDA8ED4B7FC927790D4DE (void);
// 0x000000CE System.Void MouseDraw_Orginal::SetPenPointerPosition(UnityEngine.Vector2)
extern void MouseDraw_Orginal_SetPenPointerPosition_m327D8BFB2BAC71F05851951B8A75A4EF3580C4BA (void);
// 0x000000CF System.Void MouseDraw_Orginal::TogglePenPointerVisibility(System.Boolean)
extern void MouseDraw_Orginal_TogglePenPointerVisibility_mFA8C3B85AB8F80F6C30C380FEE107C4986427251 (void);
// 0x000000D0 System.Void MouseDraw_Orginal::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw_Orginal_OnPointerEnter_m6E176199BAF3EDE8049E1425E3D03EA86A976F93 (void);
// 0x000000D1 System.Void MouseDraw_Orginal::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MouseDraw_Orginal_OnPointerExit_m143A7348063A0873D8F2D75228E7DA16B0FD0B4E (void);
// 0x000000D2 System.Void MouseDraw_Orginal::ExportSketch(System.String,System.String)
extern void MouseDraw_Orginal_ExportSketch_mA6EDFA86C4C0CAAD8E30D91FC539ED3CBE48BD37 (void);
// 0x000000D3 System.Void MouseDraw_Orginal::.ctor()
extern void MouseDraw_Orginal__ctor_mECBF8AC9D35594F653DD130965F2027E1A50353A (void);
// 0x000000D4 System.Void Mrugnij::Start()
extern void Mrugnij_Start_mC895AB92BC52A740BF94704F9A1F01542A5DF6CB (void);
// 0x000000D5 System.Void Mrugnij::Update()
extern void Mrugnij_Update_m8944445094170F07F3907FF6966BC835FA55C256 (void);
// 0x000000D6 System.Void Mrugnij::Mruganie_dzwiek()
extern void Mrugnij_Mruganie_dzwiek_mA488316D6BA1FDAAD0935FF4AD261DA603422723 (void);
// 0x000000D7 System.Void Mrugnij::.ctor()
extern void Mrugnij__ctor_m9DEFBA78ACA55A12331D061444C0B5BEA428D14B (void);
// 0x000000D8 System.Void Odliczanie_do_snu::Start()
extern void Odliczanie_do_snu_Start_m365CF8B1C70B6AA1D3B21CB6A04C5216494BB84B (void);
// 0x000000D9 System.Void Odliczanie_do_snu::Update()
extern void Odliczanie_do_snu_Update_m36E5F9FC532CE9074E4EC1196F69F3E83F30BE0C (void);
// 0x000000DA System.Void Odliczanie_do_snu::.ctor()
extern void Odliczanie_do_snu__ctor_mF8614D57B26A21FE946BFD6FCA02A9D35622D8B1 (void);
// 0x000000DB System.Void odpal_czujnik::Start()
extern void odpal_czujnik_Start_mE8496497B5D257A55DAEA9B1BBF588F7E3C3965D (void);
// 0x000000DC System.Void odpal_czujnik::Update()
extern void odpal_czujnik_Update_m9AB4D632F764F54EA2A8C9CD348BB366E1FF7A3F (void);
// 0x000000DD System.Void odpal_czujnik::Odpal_Off()
extern void odpal_czujnik_Odpal_Off_mC819C8918EF83AF5C940F619601A5D341E69B128 (void);
// 0x000000DE System.Void odpal_czujnik::Odpal()
extern void odpal_czujnik_Odpal_m8907823A27BC7AEF5B0B6C54F270DE876FDE176B (void);
// 0x000000DF System.Void odpal_czujnik::.ctor()
extern void odpal_czujnik__ctor_m23BE8BA19A1EC86D0E1C31DEBB791D7691DB40C5 (void);
// 0x000000E0 System.Void odpal_parental_gate::Start()
extern void odpal_parental_gate_Start_mB125B830B9E05C4AB2139B8D8F98D546A05A874B (void);
// 0x000000E1 System.Void odpal_parental_gate::Update()
extern void odpal_parental_gate_Update_mE09361899D1E9519DE74F3E58B8D07BEB8053E8A (void);
// 0x000000E2 System.Void odpal_parental_gate::klik_ads()
extern void odpal_parental_gate_klik_ads_m43A1F63168D1B6C7EDD4B703BE8D3073DFC67C5F (void);
// 0x000000E3 System.Void odpal_parental_gate::.ctor()
extern void odpal_parental_gate__ctor_m2178D6AA6575B778BA6D7A3A0FDA7FCED05AF5E1 (void);
// 0x000000E4 System.Void podazaj::Start()
extern void podazaj_Start_m64814F17F4096CCD20AA3F8C83A1F01AAE97FB97 (void);
// 0x000000E5 System.Void podazaj::Update()
extern void podazaj_Update_m053F75491DD3F3CC68E68CBB1A94B4F2A75C5B91 (void);
// 0x000000E6 System.Void podazaj::Podaza()
extern void podazaj_Podaza_mF5032C960493A9B0C4BDBC0B90AC876CDB5C5E20 (void);
// 0x000000E7 System.Void podazaj::.ctor()
extern void podazaj__ctor_m21EB51714CD99BFADBDAEF85AA926C0B4D820C7E (void);
// 0x000000E8 System.Void poziom5_przyciski::Start()
extern void poziom5_przyciski_Start_mC442F85750DAA4A102531D363F501957798DF4BE (void);
// 0x000000E9 System.Void poziom5_przyciski::Update()
extern void poziom5_przyciski_Update_mAC7E4B5ABB597AA93AD9355CBEEE04BBBA2A7237 (void);
// 0x000000EA System.Void poziom5_przyciski::PrzyciskClick(System.Int32)
extern void poziom5_przyciski_PrzyciskClick_mF0ACB9C965EFAB517CF3F76326FF42C1D7778822 (void);
// 0x000000EB System.Collections.IEnumerator poziom5_przyciski::blok()
extern void poziom5_przyciski_blok_mA5FD912FB76F1927A6987D29E2B18F8A2A3F393E (void);
// 0x000000EC System.Void poziom5_przyciski::.ctor()
extern void poziom5_przyciski__ctor_m88D6FCF77454A6A1B7FE352EBFCABE6F2560008E (void);
// 0x000000ED System.Void poziom5_przyciski::<Start>b__8_0()
extern void poziom5_przyciski_U3CStartU3Eb__8_0_mF7EE41B9AB53B5EC2D40FDA7C816A9C742EE525E (void);
// 0x000000EE System.Void poziom5_przyciski::<Start>b__8_1()
extern void poziom5_przyciski_U3CStartU3Eb__8_1_m9B7B19BC96AA9A68CB483ACD5635AE303BA87F07 (void);
// 0x000000EF System.Void poziom5_przyciski/<blok>d__11::.ctor(System.Int32)
extern void U3CblokU3Ed__11__ctor_mAD1F00CC885658A3776AA2CEA83D7271E4133DDE (void);
// 0x000000F0 System.Void poziom5_przyciski/<blok>d__11::System.IDisposable.Dispose()
extern void U3CblokU3Ed__11_System_IDisposable_Dispose_mD6E9D7803B094474D174D784D2D60FDAB6E7B3C5 (void);
// 0x000000F1 System.Boolean poziom5_przyciski/<blok>d__11::MoveNext()
extern void U3CblokU3Ed__11_MoveNext_mF4E06FFA91FE1EB02650CDC679FC2FB5B7EF910D (void);
// 0x000000F2 System.Object poziom5_przyciski/<blok>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CblokU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB635648C6D7F408238C904192738C5495DB1224 (void);
// 0x000000F3 System.Void poziom5_przyciski/<blok>d__11::System.Collections.IEnumerator.Reset()
extern void U3CblokU3Ed__11_System_Collections_IEnumerator_Reset_mFD4FE135DE33993F7DBBDD1BF84874331E733824 (void);
// 0x000000F4 System.Object poziom5_przyciski/<blok>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CblokU3Ed__11_System_Collections_IEnumerator_get_Current_m58F74BDF9ABDF88D86DB327DFA8268EF76BFCCD4 (void);
// 0x000000F5 System.Void prysznic_dzwiek::Start()
extern void prysznic_dzwiek_Start_m5380493C9BDA5713A3B407A3BC9D723B0DF6CC16 (void);
// 0x000000F6 System.Void prysznic_dzwiek::Update()
extern void prysznic_dzwiek_Update_m983DB67F1798B813D1BDBFE07C11E242E03E00A8 (void);
// 0x000000F7 System.Void prysznic_dzwiek::Prysznic()
extern void prysznic_dzwiek_Prysznic_m6E8B29FF42696B85B9E6BA6B9CE8AF5C7D492A43 (void);
// 0x000000F8 System.Void prysznic_dzwiek::.ctor()
extern void prysznic_dzwiek__ctor_m583E179B569F1ED2E6D0B93408C80414AABC6543 (void);
// 0x000000F9 System.Void Przerwij_zzzz_TEST::Start()
extern void Przerwij_zzzz_TEST_Start_m7ACFA688B45EF7CCA862272E53608BDB53AE88BD (void);
// 0x000000FA System.Void Przerwij_zzzz_TEST::Update()
extern void Przerwij_zzzz_TEST_Update_mD39886FB13AE3CCF2C0957466DAE8E9ACD7EF03C (void);
// 0x000000FB System.Void Przerwij_zzzz_TEST::.ctor()
extern void Przerwij_zzzz_TEST__ctor_mEDC3EDF093CA1854D5FC43530FDDF159735040A3 (void);
// 0x000000FC System.Void cymbalek::Start()
extern void cymbalek_Start_m31A72E36C0C6855BDBE90B3D3029D01C19361435 (void);
// 0x000000FD System.Void cymbalek::Update()
extern void cymbalek_Update_m4BE874CD5A2A20A928CC290D85231B46D09BC258 (void);
// 0x000000FE System.Void cymbalek::IdleSound()
extern void cymbalek_IdleSound_m7FD62002FA8617D80453409F7DE7EC646BD61A7F (void);
// 0x000000FF System.Void cymbalek::CymbRandom()
extern void cymbalek_CymbRandom_m0B0D518900143EC73D1625D46044B3588953D87E (void);
// 0x00000100 System.Collections.IEnumerator cymbalek::CymbIdleOn()
extern void cymbalek_CymbIdleOn_m12A675EB6D6069553C7E37926CE72ABD6E6C7AD4 (void);
// 0x00000101 System.Void cymbalek::.ctor()
extern void cymbalek__ctor_mE6CAAD0D4A7414BFB5715A2B28F9232D1A4E3B55 (void);
// 0x00000102 System.Void cymbalek/<CymbIdleOn>d__10::.ctor(System.Int32)
extern void U3CCymbIdleOnU3Ed__10__ctor_mCCFEB7A25A236FAF215F4FA7FEADB09460850D2E (void);
// 0x00000103 System.Void cymbalek/<CymbIdleOn>d__10::System.IDisposable.Dispose()
extern void U3CCymbIdleOnU3Ed__10_System_IDisposable_Dispose_m014D9954CCE0A3D3FC0FA9CD757ABA9762E68E6F (void);
// 0x00000104 System.Boolean cymbalek/<CymbIdleOn>d__10::MoveNext()
extern void U3CCymbIdleOnU3Ed__10_MoveNext_mBF3755AAC73973834C84FC66F3CDFD56294F88EA (void);
// 0x00000105 System.Object cymbalek/<CymbIdleOn>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCymbIdleOnU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B7DB9DA250E09DB80E1CA30769A7B9900DCB9EF (void);
// 0x00000106 System.Void cymbalek/<CymbIdleOn>d__10::System.Collections.IEnumerator.Reset()
extern void U3CCymbIdleOnU3Ed__10_System_Collections_IEnumerator_Reset_mFE0766D93497113019B51C6550CB8BE25795DA63 (void);
// 0x00000107 System.Object cymbalek/<CymbIdleOn>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CCymbIdleOnU3Ed__10_System_Collections_IEnumerator_get_Current_m5C523C448DF976AEE2DD1AF0478D3A2E4A0FFC7E (void);
// 0x00000108 System.Void Instrumenty::Start()
extern void Instrumenty_Start_m961A3509EEF03E92E238D90334974F1625153ECA (void);
// 0x00000109 System.Void Instrumenty::UstawPaleczki()
extern void Instrumenty_UstawPaleczki_m1CAE59DDD89E43D1C7A7560B04EBC27293FCCDAF (void);
// 0x0000010A System.Void Instrumenty::UstawBanke()
extern void Instrumenty_UstawBanke_m7DD7DC59A734467B56C9579453A092FD00C42D60 (void);
// 0x0000010B System.Void Instrumenty::Update()
extern void Instrumenty_Update_mF3D0ACCB5B78B113FDCB33E422DF0AB9D7965C54 (void);
// 0x0000010C System.Collections.IEnumerator Instrumenty::ZbijBanke()
extern void Instrumenty_ZbijBanke_mB6730411FDAEFA3A30E531B62CC1FE5DCB864AC7 (void);
// 0x0000010D System.Collections.IEnumerator Instrumenty::AutoZbijanieBaniek()
extern void Instrumenty_AutoZbijanieBaniek_mB352284CFECB7C4B07E66E25095F272B9E7B09A1 (void);
// 0x0000010E System.Void Instrumenty::SoundGlitchFix()
extern void Instrumenty_SoundGlitchFix_m45A12BD40A9C00B191D79745C7F28117C62A28B1 (void);
// 0x0000010F System.Collections.IEnumerator Instrumenty::PlaySoundAndAnimation()
extern void Instrumenty_PlaySoundAndAnimation_mBAE61AF82D82C657718C4FB94B2AC85D03736443 (void);
// 0x00000110 System.Void Instrumenty::PlayAnimation()
extern void Instrumenty_PlayAnimation_m3A03B2B4827B713AA5FFAE4378FCE28D97194543 (void);
// 0x00000111 System.Void Instrumenty::EmiterOn()
extern void Instrumenty_EmiterOn_m63A7A46EE984C4DC144F9DB94E6B2D57147E85AE (void);
// 0x00000112 System.Void Instrumenty::EmiterOff()
extern void Instrumenty_EmiterOff_m7475CE0E02AC08FC83D24CC6F59318442705C982 (void);
// 0x00000113 System.Collections.IEnumerator Instrumenty::WylaczIdle()
extern void Instrumenty_WylaczIdle_m286FA4CDC390D351DCD13CC2F084ABABEFF60BBF (void);
// 0x00000114 System.Collections.IEnumerator Instrumenty::SpawnerBaniek()
extern void Instrumenty_SpawnerBaniek_mC6239D96E84D3765C1A0A76DF4521A860D6A57D5 (void);
// 0x00000115 System.Collections.IEnumerator Instrumenty::IdleAnimations()
extern void Instrumenty_IdleAnimations_mC4A5DBCA106BE2058285492E8812EDC3AE9A5D0F (void);
// 0x00000116 System.Collections.IEnumerator Instrumenty::CymbIdleAnimation()
extern void Instrumenty_CymbIdleAnimation_mEFA7F7BDBE9B85D825D821ED65F99313BF4BA84B (void);
// 0x00000117 System.Void Instrumenty::.ctor()
extern void Instrumenty__ctor_m26A5A9E146608FEA9E31E169A692D9FC0795BCE9 (void);
// 0x00000118 System.Void Instrumenty/<ZbijBanke>d__44::.ctor(System.Int32)
extern void U3CZbijBankeU3Ed__44__ctor_mFDA83268ED6064BB49C8919092A8FA03893B9C09 (void);
// 0x00000119 System.Void Instrumenty/<ZbijBanke>d__44::System.IDisposable.Dispose()
extern void U3CZbijBankeU3Ed__44_System_IDisposable_Dispose_m8D7C2107653C0BE5C58B203BE8DACF8512E2D914 (void);
// 0x0000011A System.Boolean Instrumenty/<ZbijBanke>d__44::MoveNext()
extern void U3CZbijBankeU3Ed__44_MoveNext_mB2450FA54BFAE63858BC1BE663D0234658D18703 (void);
// 0x0000011B System.Object Instrumenty/<ZbijBanke>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZbijBankeU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m577AB1FEAFEE9AB5BA055C9D3DF6F06564712406 (void);
// 0x0000011C System.Void Instrumenty/<ZbijBanke>d__44::System.Collections.IEnumerator.Reset()
extern void U3CZbijBankeU3Ed__44_System_Collections_IEnumerator_Reset_mBEB1543D8C80F64C29CD36B46A13DD49F7D5ECC3 (void);
// 0x0000011D System.Object Instrumenty/<ZbijBanke>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CZbijBankeU3Ed__44_System_Collections_IEnumerator_get_Current_m67250C0D771A0B56CA4324E9364C291472973FAF (void);
// 0x0000011E System.Void Instrumenty/<AutoZbijanieBaniek>d__45::.ctor(System.Int32)
extern void U3CAutoZbijanieBaniekU3Ed__45__ctor_mB07514C6927FECE20C31797A1F340B5BBD44B1D6 (void);
// 0x0000011F System.Void Instrumenty/<AutoZbijanieBaniek>d__45::System.IDisposable.Dispose()
extern void U3CAutoZbijanieBaniekU3Ed__45_System_IDisposable_Dispose_mB385DE98CB1FA08D071A4DBF87BB4C634CF8FBAF (void);
// 0x00000120 System.Boolean Instrumenty/<AutoZbijanieBaniek>d__45::MoveNext()
extern void U3CAutoZbijanieBaniekU3Ed__45_MoveNext_m1C179B6BAC3E6FEA6D1BDE5EBAD23C28B09909C2 (void);
// 0x00000121 System.Object Instrumenty/<AutoZbijanieBaniek>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAutoZbijanieBaniekU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09167645D22A2724FAA7184316162FEF2F8480BE (void);
// 0x00000122 System.Void Instrumenty/<AutoZbijanieBaniek>d__45::System.Collections.IEnumerator.Reset()
extern void U3CAutoZbijanieBaniekU3Ed__45_System_Collections_IEnumerator_Reset_m2CC79D3D90B28AA7E467EB9A068AB86B9E8A8B14 (void);
// 0x00000123 System.Object Instrumenty/<AutoZbijanieBaniek>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CAutoZbijanieBaniekU3Ed__45_System_Collections_IEnumerator_get_Current_m8BDBC492AA54D87C641B223E860FA4236BA2EEB8 (void);
// 0x00000124 System.Void Instrumenty/<PlaySoundAndAnimation>d__47::.ctor(System.Int32)
extern void U3CPlaySoundAndAnimationU3Ed__47__ctor_m049698453D802A9BB9BF500F0845DB0F806B2E99 (void);
// 0x00000125 System.Void Instrumenty/<PlaySoundAndAnimation>d__47::System.IDisposable.Dispose()
extern void U3CPlaySoundAndAnimationU3Ed__47_System_IDisposable_Dispose_m07A11E5AA11CD45464E48495F1A2411CA0712404 (void);
// 0x00000126 System.Boolean Instrumenty/<PlaySoundAndAnimation>d__47::MoveNext()
extern void U3CPlaySoundAndAnimationU3Ed__47_MoveNext_mD23447A81CF024F31BB6B688F79A1E31F44870E7 (void);
// 0x00000127 System.Object Instrumenty/<PlaySoundAndAnimation>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlaySoundAndAnimationU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m56EEFFC82058628460C785914378166313FB9A25 (void);
// 0x00000128 System.Void Instrumenty/<PlaySoundAndAnimation>d__47::System.Collections.IEnumerator.Reset()
extern void U3CPlaySoundAndAnimationU3Ed__47_System_Collections_IEnumerator_Reset_m6059A72EE10F5F32CAA4BF645E60858DF02678EA (void);
// 0x00000129 System.Object Instrumenty/<PlaySoundAndAnimation>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CPlaySoundAndAnimationU3Ed__47_System_Collections_IEnumerator_get_Current_m5CF458634EB8949E3D43D133C223291ABEEC5AC8 (void);
// 0x0000012A System.Void Instrumenty/<WylaczIdle>d__51::.ctor(System.Int32)
extern void U3CWylaczIdleU3Ed__51__ctor_m125650A38CBDA281E9540CF9F992D8B2CC76DEAC (void);
// 0x0000012B System.Void Instrumenty/<WylaczIdle>d__51::System.IDisposable.Dispose()
extern void U3CWylaczIdleU3Ed__51_System_IDisposable_Dispose_m02CDD9E7E2777159BB207406A28F226B60050727 (void);
// 0x0000012C System.Boolean Instrumenty/<WylaczIdle>d__51::MoveNext()
extern void U3CWylaczIdleU3Ed__51_MoveNext_m167A81BDD96392D12D827BD86ECD279ACBF028AD (void);
// 0x0000012D System.Object Instrumenty/<WylaczIdle>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWylaczIdleU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7479C062879082548C3B431B9859E2887202FB8 (void);
// 0x0000012E System.Void Instrumenty/<WylaczIdle>d__51::System.Collections.IEnumerator.Reset()
extern void U3CWylaczIdleU3Ed__51_System_Collections_IEnumerator_Reset_mF20DE93E9417B44EFE8523EE1D3EB4FD5FCDC9E4 (void);
// 0x0000012F System.Object Instrumenty/<WylaczIdle>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CWylaczIdleU3Ed__51_System_Collections_IEnumerator_get_Current_mDCE77657FAA20320B8337D99300A3B0C97366FE9 (void);
// 0x00000130 System.Void Instrumenty/<SpawnerBaniek>d__52::.ctor(System.Int32)
extern void U3CSpawnerBaniekU3Ed__52__ctor_m1F69A419C8E271873E40B6A53BC04F4C0E634638 (void);
// 0x00000131 System.Void Instrumenty/<SpawnerBaniek>d__52::System.IDisposable.Dispose()
extern void U3CSpawnerBaniekU3Ed__52_System_IDisposable_Dispose_m08851CEAD69B29ED612C6AFDCF81B57601251AB1 (void);
// 0x00000132 System.Boolean Instrumenty/<SpawnerBaniek>d__52::MoveNext()
extern void U3CSpawnerBaniekU3Ed__52_MoveNext_m868AAA3F277353F92E3694EB364BEECAD8EB2CE8 (void);
// 0x00000133 System.Object Instrumenty/<SpawnerBaniek>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnerBaniekU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0AA3891A86E729B473EA153A5A59FD856C6A89F (void);
// 0x00000134 System.Void Instrumenty/<SpawnerBaniek>d__52::System.Collections.IEnumerator.Reset()
extern void U3CSpawnerBaniekU3Ed__52_System_Collections_IEnumerator_Reset_mB0EB80D288A48F2167351DF6A75A265FD19C5774 (void);
// 0x00000135 System.Object Instrumenty/<SpawnerBaniek>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnerBaniekU3Ed__52_System_Collections_IEnumerator_get_Current_mAC0AD90DC0D364BCBE4B4A68185CC9ADE67B3118 (void);
// 0x00000136 System.Void Instrumenty/<IdleAnimations>d__53::.ctor(System.Int32)
extern void U3CIdleAnimationsU3Ed__53__ctor_m02CF8A3CAEBCD1720EC6DAB5352DA7B92D0E116B (void);
// 0x00000137 System.Void Instrumenty/<IdleAnimations>d__53::System.IDisposable.Dispose()
extern void U3CIdleAnimationsU3Ed__53_System_IDisposable_Dispose_m28574FFE3475280DF9A5DEF09F4F9C5314980CE5 (void);
// 0x00000138 System.Boolean Instrumenty/<IdleAnimations>d__53::MoveNext()
extern void U3CIdleAnimationsU3Ed__53_MoveNext_m1628ED6A2753204DA1D2FA96C2F234D2B9A67DBC (void);
// 0x00000139 System.Object Instrumenty/<IdleAnimations>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIdleAnimationsU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C4EFC6B5C8372F5A540D93F81F18928E8D49237 (void);
// 0x0000013A System.Void Instrumenty/<IdleAnimations>d__53::System.Collections.IEnumerator.Reset()
extern void U3CIdleAnimationsU3Ed__53_System_Collections_IEnumerator_Reset_m9B2616177EDBE2DCDA711FC34119DA35D2451DDD (void);
// 0x0000013B System.Object Instrumenty/<IdleAnimations>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CIdleAnimationsU3Ed__53_System_Collections_IEnumerator_get_Current_m80D6CFD5FEC8C1AC9544D6D3A90DBFAE61447D80 (void);
// 0x0000013C System.Void Instrumenty/<CymbIdleAnimation>d__54::.ctor(System.Int32)
extern void U3CCymbIdleAnimationU3Ed__54__ctor_m7F602FD42069132239DD69A1286AB116F8C64DC7 (void);
// 0x0000013D System.Void Instrumenty/<CymbIdleAnimation>d__54::System.IDisposable.Dispose()
extern void U3CCymbIdleAnimationU3Ed__54_System_IDisposable_Dispose_m047B119A13896DF7CE99DAB9C3784676ACEE76DC (void);
// 0x0000013E System.Boolean Instrumenty/<CymbIdleAnimation>d__54::MoveNext()
extern void U3CCymbIdleAnimationU3Ed__54_MoveNext_mB0C0630770C913AB3340AA36F735507BB9A90757 (void);
// 0x0000013F System.Object Instrumenty/<CymbIdleAnimation>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCymbIdleAnimationU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m353F07D42254FA93BABB412392B1E7A12392941B (void);
// 0x00000140 System.Void Instrumenty/<CymbIdleAnimation>d__54::System.Collections.IEnumerator.Reset()
extern void U3CCymbIdleAnimationU3Ed__54_System_Collections_IEnumerator_Reset_mCC3C1B88F8E01D9312CC5730849B7AA10596F558 (void);
// 0x00000141 System.Object Instrumenty/<CymbIdleAnimation>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CCymbIdleAnimationU3Ed__54_System_Collections_IEnumerator_get_Current_mB336531C1EA44A294EF35F5E1307B4807E28A927 (void);
// 0x00000142 System.Void Farma::Start()
extern void Farma_Start_m071523F2157EE0E5F535B394A61D77E57020A38D (void);
// 0x00000143 System.Void Farma::Ubrudz_sie()
extern void Farma_Ubrudz_sie_m354215D67CDC101A91531E745DD20EC69EE74EA5 (void);
// 0x00000144 System.Collections.IEnumerator Farma::GetPigDirty()
extern void Farma_GetPigDirty_mD538913E4E12DA67DC444260615559EEC38A0FEF (void);
// 0x00000145 System.Void Farma::Odpal_coroutynke()
extern void Farma_Odpal_coroutynke_m0E43A7DC61EDEB7381B07DAF9C4BAAB19B926A56 (void);
// 0x00000146 System.Void Farma::Zatrzymaj_coroutynke()
extern void Farma_Zatrzymaj_coroutynke_mE5FCE251B2047BC52487E560837A29F88C627155 (void);
// 0x00000147 System.Void Farma::newPigDirty()
extern void Farma_newPigDirty_m3E12A50BE250C6D309CDFFAA457F3DB753C038D7 (void);
// 0x00000148 System.Void Farma::PigClickEnd()
extern void Farma_PigClickEnd_mA11A766C02343ECCA9A75AADB0FB8C6DB73613A1 (void);
// 0x00000149 System.Void Farma::MakePigClean()
extern void Farma_MakePigClean_m1AAA4F0643AA303CAB0692C9AE0E2220E1A12758 (void);
// 0x0000014A System.Void Farma::ShowerOffPig()
extern void Farma_ShowerOffPig_mA578FAA512564B7D6E4C0585E9D5661370AB466F (void);
// 0x0000014B System.Void Farma::TurnShowerOn()
extern void Farma_TurnShowerOn_m82C93B6F0E091FA157D47FB4F1A8180DD2967824 (void);
// 0x0000014C System.Void Farma::TurnShowerOff()
extern void Farma_TurnShowerOff_m7AAC5AE5A0899ECEA0D757E8663E2360DF33A6C0 (void);
// 0x0000014D System.Void Farma::TurnPigDirtOn()
extern void Farma_TurnPigDirtOn_m9B0D09575AD692A957DC8ABCCB788C32FF677E15 (void);
// 0x0000014E System.Void Farma::TurnPigDirtOff()
extern void Farma_TurnPigDirtOff_mA99955E6B3EF85EECDF25287C3B662871D9D04BA (void);
// 0x0000014F System.Collections.IEnumerator Farma::KaczaAnimacja()
extern void Farma_KaczaAnimacja_m240091D5932C0DA0985007E210BE0FFCE706001F (void);
// 0x00000150 System.Collections.IEnumerator Farma::KaczkaQuack()
extern void Farma_KaczkaQuack_m8E7552523F566E0DDBA5C300567D06AF01AE93E2 (void);
// 0x00000151 System.Collections.IEnumerator Farma::KaczkaQuack2()
extern void Farma_KaczkaQuack2_mA74132066F37577D6717643F46FA862E7973B4B3 (void);
// 0x00000152 System.Void Farma::Clouds()
extern void Farma_Clouds_m211D72CCB4BBDA5F2BCBE095564968D204B8D74A (void);
// 0x00000153 System.Void Farma::PoopPhy()
extern void Farma_PoopPhy_m5596C7CACB887A1CD2394F83C181F6218005A7CF (void);
// 0x00000154 System.Void Farma::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Farma_OnTriggerEnter2D_mE6EBB9DDD373152663399B4D1BE0E3BAB48DB3AD (void);
// 0x00000155 System.Void Farma::Update()
extern void Farma_Update_mE5ADC9DDCE236660BD7E6D0E6EEAFD4BC3AB8468 (void);
// 0x00000156 System.Collections.IEnumerator Farma::OnlyQuack()
extern void Farma_OnlyQuack_m64617D10BE84CA0BD330DB034C60E97CEF4E5CDD (void);
// 0x00000157 System.Collections.IEnumerator Farma::IdleGuard()
extern void Farma_IdleGuard_m7FE81EEF419077DB512A02A1C929DB7CFD354392 (void);
// 0x00000158 System.Collections.IEnumerator Farma::Sranie()
extern void Farma_Sranie_mB8784AD64B56C2978ABEAD793C18B762B0CFA0D2 (void);
// 0x00000159 System.Void Farma::DuckBeWalking()
extern void Farma_DuckBeWalking_m9267693C46EDF887550281B9393C93F20262C01C (void);
// 0x0000015A System.Void Farma::StopWalking()
extern void Farma_StopWalking_m22FFBFAA9475313770639A9072EC1E6A8607058F (void);
// 0x0000015B System.Void Farma::PlayAnimationAndSound()
extern void Farma_PlayAnimationAndSound_mF77C3227CC13B622C0277CF3D4C770165BA0F8D7 (void);
// 0x0000015C System.Void Farma::Playsound()
extern void Farma_Playsound_mDA608EEE996AD8F9EB3045B8AD739EF0FFAA3C77 (void);
// 0x0000015D System.Collections.IEnumerator Farma::CloudSpawner()
extern void Farma_CloudSpawner_m9A36F2032BF5EF949E92374094DB46BF7E064914 (void);
// 0x0000015E System.Void Farma::.ctor()
extern void Farma__ctor_m043AF65FBE7FB0F6CAA9A002F5B8411359601EAC (void);
// 0x0000015F System.Void Farma/<GetPigDirty>d__42::.ctor(System.Int32)
extern void U3CGetPigDirtyU3Ed__42__ctor_mB49FE40B445452922AAAE3B714BDC89340A102D5 (void);
// 0x00000160 System.Void Farma/<GetPigDirty>d__42::System.IDisposable.Dispose()
extern void U3CGetPigDirtyU3Ed__42_System_IDisposable_Dispose_m74817D9B14D305A0BEE2902797434982D76F1FE2 (void);
// 0x00000161 System.Boolean Farma/<GetPigDirty>d__42::MoveNext()
extern void U3CGetPigDirtyU3Ed__42_MoveNext_m8AF86C3347C4D73DAE689E3A6DA2A7B80C16DAB9 (void);
// 0x00000162 System.Object Farma/<GetPigDirty>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetPigDirtyU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m146B1E212CF415CD7F6A674A37C049BC50D61B81 (void);
// 0x00000163 System.Void Farma/<GetPigDirty>d__42::System.Collections.IEnumerator.Reset()
extern void U3CGetPigDirtyU3Ed__42_System_Collections_IEnumerator_Reset_m7B8953424C740DFC61B6A67557C35EE8F9308C79 (void);
// 0x00000164 System.Object Farma/<GetPigDirty>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CGetPigDirtyU3Ed__42_System_Collections_IEnumerator_get_Current_m73E75654464F3C11502E5F0D7D9D2947BF388324 (void);
// 0x00000165 System.Void Farma/<KaczaAnimacja>d__53::.ctor(System.Int32)
extern void U3CKaczaAnimacjaU3Ed__53__ctor_m2617176B3502C128E92242C8CF36025E875469AA (void);
// 0x00000166 System.Void Farma/<KaczaAnimacja>d__53::System.IDisposable.Dispose()
extern void U3CKaczaAnimacjaU3Ed__53_System_IDisposable_Dispose_m5F75ECF68302A0D5F018D8D8F929B8C254E756B7 (void);
// 0x00000167 System.Boolean Farma/<KaczaAnimacja>d__53::MoveNext()
extern void U3CKaczaAnimacjaU3Ed__53_MoveNext_m5D1526DB5677BBE91274B617ED508D846481ACF6 (void);
// 0x00000168 System.Object Farma/<KaczaAnimacja>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKaczaAnimacjaU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F7303792186D2AA16F3F9B725339CF496B1550D (void);
// 0x00000169 System.Void Farma/<KaczaAnimacja>d__53::System.Collections.IEnumerator.Reset()
extern void U3CKaczaAnimacjaU3Ed__53_System_Collections_IEnumerator_Reset_m3B805F853BBB9CED96BA52C7E0438EED685E3941 (void);
// 0x0000016A System.Object Farma/<KaczaAnimacja>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CKaczaAnimacjaU3Ed__53_System_Collections_IEnumerator_get_Current_mC80061F54ACEF822CABBFAA4F754560586674B95 (void);
// 0x0000016B System.Void Farma/<KaczkaQuack>d__54::.ctor(System.Int32)
extern void U3CKaczkaQuackU3Ed__54__ctor_m2D6B4E2DD8A84767208082996B50BCD8ADEE5B74 (void);
// 0x0000016C System.Void Farma/<KaczkaQuack>d__54::System.IDisposable.Dispose()
extern void U3CKaczkaQuackU3Ed__54_System_IDisposable_Dispose_m9855F27E3A68FD419A3EA384F6E96A19149B6456 (void);
// 0x0000016D System.Boolean Farma/<KaczkaQuack>d__54::MoveNext()
extern void U3CKaczkaQuackU3Ed__54_MoveNext_m702E25D8076A3C15AED98149D08F2C2E2E30054F (void);
// 0x0000016E System.Object Farma/<KaczkaQuack>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKaczkaQuackU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AAC50BB26A054A222D22B6C437A91E44639C962 (void);
// 0x0000016F System.Void Farma/<KaczkaQuack>d__54::System.Collections.IEnumerator.Reset()
extern void U3CKaczkaQuackU3Ed__54_System_Collections_IEnumerator_Reset_mC868E63351019AC63F228991A9C8F3A3A3E36B51 (void);
// 0x00000170 System.Object Farma/<KaczkaQuack>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CKaczkaQuackU3Ed__54_System_Collections_IEnumerator_get_Current_m6A59328FC5803F7E3C8C754AEDABE7C2DB1AE034 (void);
// 0x00000171 System.Void Farma/<KaczkaQuack2>d__55::.ctor(System.Int32)
extern void U3CKaczkaQuack2U3Ed__55__ctor_m93DF2E5960FE4A96BC70A4E4D4F85DF74A860CCB (void);
// 0x00000172 System.Void Farma/<KaczkaQuack2>d__55::System.IDisposable.Dispose()
extern void U3CKaczkaQuack2U3Ed__55_System_IDisposable_Dispose_mEFFEAD3B0FA0695C95A0BF13FFE3C2077FE129EC (void);
// 0x00000173 System.Boolean Farma/<KaczkaQuack2>d__55::MoveNext()
extern void U3CKaczkaQuack2U3Ed__55_MoveNext_m75535C6AD7DB7D85EA2505FE9CAFAD5F567BCA1F (void);
// 0x00000174 System.Object Farma/<KaczkaQuack2>d__55::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKaczkaQuack2U3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m789E7837277DBC7FE6CA2A63444011AF983681C5 (void);
// 0x00000175 System.Void Farma/<KaczkaQuack2>d__55::System.Collections.IEnumerator.Reset()
extern void U3CKaczkaQuack2U3Ed__55_System_Collections_IEnumerator_Reset_mC2C047054D7D0BBC6692381DB558BE11C2F55D1F (void);
// 0x00000176 System.Object Farma/<KaczkaQuack2>d__55::System.Collections.IEnumerator.get_Current()
extern void U3CKaczkaQuack2U3Ed__55_System_Collections_IEnumerator_get_Current_m018BB9FA96CF9121B630EF4AC79BFD8EA523F584 (void);
// 0x00000177 System.Void Farma/<OnlyQuack>d__60::.ctor(System.Int32)
extern void U3COnlyQuackU3Ed__60__ctor_mEBBEE5966C566E3501B1D1D339957745A6354FDE (void);
// 0x00000178 System.Void Farma/<OnlyQuack>d__60::System.IDisposable.Dispose()
extern void U3COnlyQuackU3Ed__60_System_IDisposable_Dispose_m310A8233FC40C468E60D21D976ABFF0AC8AA32E4 (void);
// 0x00000179 System.Boolean Farma/<OnlyQuack>d__60::MoveNext()
extern void U3COnlyQuackU3Ed__60_MoveNext_mABCAA2CADD6FD8BD1694A99970E08D38BB18B2E0 (void);
// 0x0000017A System.Object Farma/<OnlyQuack>d__60::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnlyQuackU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB304C4C84F152712B1AC7DEE0C9FF2294B8457CD (void);
// 0x0000017B System.Void Farma/<OnlyQuack>d__60::System.Collections.IEnumerator.Reset()
extern void U3COnlyQuackU3Ed__60_System_Collections_IEnumerator_Reset_mFF4F9CECC9638ACAA858F1F531C3EBAB4BEADDE6 (void);
// 0x0000017C System.Object Farma/<OnlyQuack>d__60::System.Collections.IEnumerator.get_Current()
extern void U3COnlyQuackU3Ed__60_System_Collections_IEnumerator_get_Current_m15A923DDAA38F3C5B56DD661FDC65B0EF594881E (void);
// 0x0000017D System.Void Farma/<IdleGuard>d__61::.ctor(System.Int32)
extern void U3CIdleGuardU3Ed__61__ctor_m96487948FEF28306398FC3DA95B5EFC27339DB97 (void);
// 0x0000017E System.Void Farma/<IdleGuard>d__61::System.IDisposable.Dispose()
extern void U3CIdleGuardU3Ed__61_System_IDisposable_Dispose_mC7111443E3DE4454DD5BB5C8718318D6B1AAC215 (void);
// 0x0000017F System.Boolean Farma/<IdleGuard>d__61::MoveNext()
extern void U3CIdleGuardU3Ed__61_MoveNext_mF09EC4E2019D2C4F45B6820C8B00D96E372E1641 (void);
// 0x00000180 System.Object Farma/<IdleGuard>d__61::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIdleGuardU3Ed__61_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97A5B60076B74F47DCE50F086D960EAAD0255182 (void);
// 0x00000181 System.Void Farma/<IdleGuard>d__61::System.Collections.IEnumerator.Reset()
extern void U3CIdleGuardU3Ed__61_System_Collections_IEnumerator_Reset_m972BACDB7FB4A07471B40088797815F4A784086A (void);
// 0x00000182 System.Object Farma/<IdleGuard>d__61::System.Collections.IEnumerator.get_Current()
extern void U3CIdleGuardU3Ed__61_System_Collections_IEnumerator_get_Current_m681B247E1222B7AE834CCE89910B5EAB5BDFA32D (void);
// 0x00000183 System.Void Farma/<Sranie>d__62::.ctor(System.Int32)
extern void U3CSranieU3Ed__62__ctor_m64C94E581069D50EE1AB0493F17400FB48FBE50B (void);
// 0x00000184 System.Void Farma/<Sranie>d__62::System.IDisposable.Dispose()
extern void U3CSranieU3Ed__62_System_IDisposable_Dispose_m0BB17614988D6D763C7CAB272EC07EB3695FBE18 (void);
// 0x00000185 System.Boolean Farma/<Sranie>d__62::MoveNext()
extern void U3CSranieU3Ed__62_MoveNext_mFB00CEDE10A81946C3682375177ECF30A3DCF0FD (void);
// 0x00000186 System.Object Farma/<Sranie>d__62::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSranieU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30E708A9B8C1B3F6C8B2480AE1D5A9C994DD9DC4 (void);
// 0x00000187 System.Void Farma/<Sranie>d__62::System.Collections.IEnumerator.Reset()
extern void U3CSranieU3Ed__62_System_Collections_IEnumerator_Reset_m343B9367562C0FB0FEE40088FD29B0151B55BB52 (void);
// 0x00000188 System.Object Farma/<Sranie>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CSranieU3Ed__62_System_Collections_IEnumerator_get_Current_mB6C2F38E44676BFE3ED013709925532BD114CBF5 (void);
// 0x00000189 System.Void Farma/<CloudSpawner>d__67::.ctor(System.Int32)
extern void U3CCloudSpawnerU3Ed__67__ctor_mCE351A97FAEC7EF96F238BC273C3E286DBBAA359 (void);
// 0x0000018A System.Void Farma/<CloudSpawner>d__67::System.IDisposable.Dispose()
extern void U3CCloudSpawnerU3Ed__67_System_IDisposable_Dispose_mF5FD5607C1D0C4D8EA875AEACAC2B1FE0C5FC239 (void);
// 0x0000018B System.Boolean Farma/<CloudSpawner>d__67::MoveNext()
extern void U3CCloudSpawnerU3Ed__67_MoveNext_mCEE8B007D64AA96F4E7A37A8FDF488EA4EC501FC (void);
// 0x0000018C System.Object Farma/<CloudSpawner>d__67::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCloudSpawnerU3Ed__67_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m69F2956311883A8CCD3E6ED4E749B35E184FBD12 (void);
// 0x0000018D System.Void Farma/<CloudSpawner>d__67::System.Collections.IEnumerator.Reset()
extern void U3CCloudSpawnerU3Ed__67_System_Collections_IEnumerator_Reset_mA0D3934CA811780333417B9FE7FD07C741BADD06 (void);
// 0x0000018E System.Object Farma/<CloudSpawner>d__67::System.Collections.IEnumerator.get_Current()
extern void U3CCloudSpawnerU3Ed__67_System_Collections_IEnumerator_get_Current_mA313638C0C638285D0A001731DEDAE82D4B9C187 (void);
// 0x0000018F System.Void Shower::Start()
extern void Shower_Start_m6727CAC664411179BB97C3924FEC6FB8C6EA68A5 (void);
// 0x00000190 System.Void Shower::Update()
extern void Shower_Update_m8B3EC134998A117F5E6572780947BADB0914DEAD (void);
// 0x00000191 System.Void Shower::.ctor()
extern void Shower__ctor_m415A59D003485490B4B5FCAE13B14E8E33E23306 (void);
// 0x00000192 System.Void dzwiek_menu::Start()
extern void dzwiek_menu_Start_m080567101E4266A79E0C7650B48C60733D91FA17 (void);
// 0x00000193 System.Void dzwiek_menu::Update()
extern void dzwiek_menu_Update_m3823500A86447E3BBA1574C68D2ABDBA67E78154 (void);
// 0x00000194 System.Void dzwiek_menu::Dzwiek()
extern void dzwiek_menu_Dzwiek_m70327F1D6A3CB3856CC6976BAD0EEB6A3C2B4EAD (void);
// 0x00000195 System.Void dzwiek_menu::.ctor()
extern void dzwiek_menu__ctor_m2DBEB5735DF5D3366E2948FCA8F55775D41176FB (void);
// 0x00000196 System.Void houseFly::Start()
extern void houseFly_Start_m2F3A71307BE8510E4B45B1A29E735DF2BF2BD9B8 (void);
// 0x00000197 System.Void houseFly::Update()
extern void houseFly_Update_m0D9969256F61795FB6452B99F7D7680247A1438A (void);
// 0x00000198 System.Collections.IEnumerator houseFly::Respawn()
extern void houseFly_Respawn_m1FE140CE12C84DA513B02342F949E86D47049B72 (void);
// 0x00000199 System.Void houseFly::.ctor()
extern void houseFly__ctor_m1A29F0AA59758D0B80AFAE1E09B429E131501735 (void);
// 0x0000019A System.Void houseFly/<Respawn>d__10::.ctor(System.Int32)
extern void U3CRespawnU3Ed__10__ctor_m3B0C579967F2A710EE1E1FE7502DD1F641818781 (void);
// 0x0000019B System.Void houseFly/<Respawn>d__10::System.IDisposable.Dispose()
extern void U3CRespawnU3Ed__10_System_IDisposable_Dispose_m289F0A8221BBB97B2A8E31B05D7E01CB1EE85056 (void);
// 0x0000019C System.Boolean houseFly/<Respawn>d__10::MoveNext()
extern void U3CRespawnU3Ed__10_MoveNext_m8EA08FF7682539BF0CE424278F156AB34131BC69 (void);
// 0x0000019D System.Object houseFly/<Respawn>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9282D8642B3AA4971C0C9F500195DC60FF2CD3E (void);
// 0x0000019E System.Void houseFly/<Respawn>d__10::System.Collections.IEnumerator.Reset()
extern void U3CRespawnU3Ed__10_System_Collections_IEnumerator_Reset_m9C7A142C08189AB774409AD9C34644188B9AD566 (void);
// 0x0000019F System.Object houseFly/<Respawn>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnU3Ed__10_System_Collections_IEnumerator_get_Current_m883D53CDF879576825485BE030596F20B140F45B (void);
// 0x000001A0 System.Void Kredka_podazaj_za_rysowaniem::Start()
extern void Kredka_podazaj_za_rysowaniem_Start_m499745688AD2CA31C64792FCE7030C93E3350832 (void);
// 0x000001A1 System.Void Kredka_podazaj_za_rysowaniem::Update()
extern void Kredka_podazaj_za_rysowaniem_Update_mBCAFADA4133363090F3308071FE0DDA106056E2A (void);
// 0x000001A2 System.Void Kredka_podazaj_za_rysowaniem::.ctor()
extern void Kredka_podazaj_za_rysowaniem__ctor_mF9ED1F5B70B1CB8F0F32B47AB7C4BF5F5D1959B6 (void);
// 0x000001A3 System.Void btn_back::Start()
extern void btn_back_Start_mCA753175302041C6B62F5D21022C92C47F90C152 (void);
// 0x000001A4 System.Void btn_back::Update()
extern void btn_back_Update_m96C0273968EBD0E1143A7EFD03CB7C5D28ED3DFE (void);
// 0x000001A5 System.Collections.IEnumerator btn_back::UnlockBackButton()
extern void btn_back_UnlockBackButton_m0A8AFF7FF28FF433C3CB9F71BD251B3B1DD453EC (void);
// 0x000001A6 System.Void btn_back::Back()
extern void btn_back_Back_m55FC207B7DD99952915A74AB497A78256D4D36D3 (void);
// 0x000001A7 System.Collections.IEnumerator btn_back::Backk()
extern void btn_back_Backk_m57EA631C6647DB1D9B22CFB8F01BB6D96F19C41C (void);
// 0x000001A8 System.Void btn_back::.ctor()
extern void btn_back__ctor_m99372E6D017FBF32F4E4D221F90085ADBC64FFED (void);
// 0x000001A9 System.Void btn_back/<UnlockBackButton>d__35::.ctor(System.Int32)
extern void U3CUnlockBackButtonU3Ed__35__ctor_m4EB1F52D51129405DD86FEC2A022B100E38FA31B (void);
// 0x000001AA System.Void btn_back/<UnlockBackButton>d__35::System.IDisposable.Dispose()
extern void U3CUnlockBackButtonU3Ed__35_System_IDisposable_Dispose_mDA546CDA479C54E92879D8962BC95CF28D1EC090 (void);
// 0x000001AB System.Boolean btn_back/<UnlockBackButton>d__35::MoveNext()
extern void U3CUnlockBackButtonU3Ed__35_MoveNext_m32423314E54EE9ECA93184FA722CC9A21BF43144 (void);
// 0x000001AC System.Object btn_back/<UnlockBackButton>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUnlockBackButtonU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC7C2BFA0C367BE6F6FAE6DA6FD3B5ED13BD51BB (void);
// 0x000001AD System.Void btn_back/<UnlockBackButton>d__35::System.Collections.IEnumerator.Reset()
extern void U3CUnlockBackButtonU3Ed__35_System_Collections_IEnumerator_Reset_mFD30FB596079DC4545E5F4926B012EE74FF392B1 (void);
// 0x000001AE System.Object btn_back/<UnlockBackButton>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CUnlockBackButtonU3Ed__35_System_Collections_IEnumerator_get_Current_m68499FAC498A1537CB9F7B2AEB3A39B04A49A6AD (void);
// 0x000001AF System.Void btn_back/<Backk>d__37::.ctor(System.Int32)
extern void U3CBackkU3Ed__37__ctor_m1B6054C2B5FF3892C177104A90BF18DEAF8827E8 (void);
// 0x000001B0 System.Void btn_back/<Backk>d__37::System.IDisposable.Dispose()
extern void U3CBackkU3Ed__37_System_IDisposable_Dispose_m66B10252C0AAD9167A71708C66277E04EE5A9A3F (void);
// 0x000001B1 System.Boolean btn_back/<Backk>d__37::MoveNext()
extern void U3CBackkU3Ed__37_MoveNext_m28D4E8364E0B67043BB26F151C7F3259B5BE6E26 (void);
// 0x000001B2 System.Object btn_back/<Backk>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackkU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A6024A86E7FC401931E49B45E16B587BA73D963 (void);
// 0x000001B3 System.Void btn_back/<Backk>d__37::System.Collections.IEnumerator.Reset()
extern void U3CBackkU3Ed__37_System_Collections_IEnumerator_Reset_mACA247585B71C1B32B6BAE7EAAD0A6161CC058A3 (void);
// 0x000001B4 System.Object btn_back/<Backk>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CBackkU3Ed__37_System_Collections_IEnumerator_get_Current_mEE4AFE53D0DE2E61FAAEAEB53BE4701BB287692B (void);
// 0x000001B5 System.Void buttony_lvl5::Start()
extern void buttony_lvl5_Start_mF678A8599187804E0C2B3356DEEB1173DD6F3BAD (void);
// 0x000001B6 System.Void buttony_lvl5::Update()
extern void buttony_lvl5_Update_m4929CC1D8D9D625B35E8F3297B8E5B8C4E0F327D (void);
// 0x000001B7 System.Void buttony_lvl5::Sprawdz(System.Int32)
extern void buttony_lvl5_Sprawdz_m2B26891B1288221F3854FE9E2C57775F1FD2A176 (void);
// 0x000001B8 System.Void buttony_lvl5::.ctor()
extern void buttony_lvl5__ctor_mD55963CD823166CC578D76F6AA26740F5E80A137 (void);
// 0x000001B9 System.Void CanvasScale::Start()
extern void CanvasScale_Start_mB05218DAB56F7A8B6DDCD32846F4F1BD40385831 (void);
// 0x000001BA System.Void CanvasScale::Update()
extern void CanvasScale_Update_m236EB5A668AC0E8A79C6BFEBABD37D21FD234D4A (void);
// 0x000001BB System.Void CanvasScale::HorizontalDevice()
extern void CanvasScale_HorizontalDevice_m6FE1990C8EA28C0B0CE084DBDB647894EDE94638 (void);
// 0x000001BC System.Void CanvasScale::VerticalDevice()
extern void CanvasScale_VerticalDevice_mA777EECCEC960E58AD4AE8F77FEEBD13C64C9F60 (void);
// 0x000001BD System.Void CanvasScale::.ctor()
extern void CanvasScale__ctor_mE22BED1D3D4A7211F12A8FACDF29567CD9D0606E (void);
// 0x000001BE System.Void cykl_dnia_i_nocy::Start()
extern void cykl_dnia_i_nocy_Start_m8B253FC445E432DBE39CFED7B3799C330F8357FE (void);
// 0x000001BF System.Void cykl_dnia_i_nocy::idz_spac()
extern void cykl_dnia_i_nocy_idz_spac_mB38FA7E7E31CB5CD706089FEE677F1224C7576A1 (void);
// 0x000001C0 System.Collections.IEnumerator cykl_dnia_i_nocy::idz_spac_enum()
extern void cykl_dnia_i_nocy_idz_spac_enum_mFEAFB775F41CFA25B7B05C999CD3CAEB2D080167 (void);
// 0x000001C1 System.Void cykl_dnia_i_nocy::swinka_idzie_spac_raz_jescze()
extern void cykl_dnia_i_nocy_swinka_idzie_spac_raz_jescze_m4664894337E08B10F61474478BCFF6B31186D90C (void);
// 0x000001C2 System.Void cykl_dnia_i_nocy::Wybudz_sie()
extern void cykl_dnia_i_nocy_Wybudz_sie_mECF206A0C21761DE39580D24A1B7ABAC2431EA8A (void);
// 0x000001C3 System.Void cykl_dnia_i_nocy::Update()
extern void cykl_dnia_i_nocy_Update_mE7DA20E82E5B8E495CD45981152F9684B80CC8FD (void);
// 0x000001C4 System.Void cykl_dnia_i_nocy::.ctor()
extern void cykl_dnia_i_nocy__ctor_m24D3DAA21FB11F1E1056FC1CFF5C28A4ADD7D91D (void);
// 0x000001C5 System.Void cykl_dnia_i_nocy/<idz_spac_enum>d__7::.ctor(System.Int32)
extern void U3Cidz_spac_enumU3Ed__7__ctor_m5D6B838209E22637BBE8544686F70C27A5A417B8 (void);
// 0x000001C6 System.Void cykl_dnia_i_nocy/<idz_spac_enum>d__7::System.IDisposable.Dispose()
extern void U3Cidz_spac_enumU3Ed__7_System_IDisposable_Dispose_m003C84EAF32133715C41D41D4A32EA2AD24FAA39 (void);
// 0x000001C7 System.Boolean cykl_dnia_i_nocy/<idz_spac_enum>d__7::MoveNext()
extern void U3Cidz_spac_enumU3Ed__7_MoveNext_m48562FB96E85EE77EB61A0DC519F96749B9C515C (void);
// 0x000001C8 System.Object cykl_dnia_i_nocy/<idz_spac_enum>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cidz_spac_enumU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCF5ADFBCBB8424E6B17F3E44E4FBAC2DC6A02C5 (void);
// 0x000001C9 System.Void cykl_dnia_i_nocy/<idz_spac_enum>d__7::System.Collections.IEnumerator.Reset()
extern void U3Cidz_spac_enumU3Ed__7_System_Collections_IEnumerator_Reset_mC93127A9B79956FD2D9AB1BC03B5A8DE3919C7DA (void);
// 0x000001CA System.Object cykl_dnia_i_nocy/<idz_spac_enum>d__7::System.Collections.IEnumerator.get_Current()
extern void U3Cidz_spac_enumU3Ed__7_System_Collections_IEnumerator_get_Current_mF753B3994F9FF1FF11305D356A3950A94AEE9A02 (void);
// 0x000001CB System.Void czujnik_zmazywania::Start()
extern void czujnik_zmazywania_Start_m3C2E94F8702D01E63AB653F9E39972926045E13B (void);
// 0x000001CC System.Void czujnik_zmazywania::Update()
extern void czujnik_zmazywania_Update_mB87AF31AB7B7C5AE4B07441A9D3600F7F1579AE5 (void);
// 0x000001CD System.Void czujnik_zmazywania::Rusz_Sie()
extern void czujnik_zmazywania_Rusz_Sie_m2B7D864D9D152E2783DD82A5355A89E761B944D2 (void);
// 0x000001CE System.Void czujnik_zmazywania::.ctor()
extern void czujnik_zmazywania__ctor_mA5A687031A79F96E207538B38465EDBF2DBEF7FF (void);
// 0x000001CF UnityEngine.Quaternion DeviceRotation::Get()
extern void DeviceRotation_Get_mB00C047B42EC3CC1E86A59A5B1124A4E746BB463 (void);
// 0x000001D0 System.Void DeviceRotation::InitGyro()
extern void DeviceRotation_InitGyro_m8DF021A40F51C8D846091391C5D9F81A36F15963 (void);
// 0x000001D1 UnityEngine.Quaternion DeviceRotation::ReadGyroscopeRotation()
extern void DeviceRotation_ReadGyroscopeRotation_m35716EB5855BE7AC162C38EF02927A45DCABEE90 (void);
// 0x000001D2 System.Void drzewo::Start()
extern void drzewo_Start_m0E372651CE5C25C82CBC4B765F2755BFB626A329 (void);
// 0x000001D3 System.Void drzewo::Update()
extern void drzewo_Update_m2618F36F36CE694B68C27F79B7F950000FFD07A8 (void);
// 0x000001D4 System.Void drzewo::Zabrano_jablko()
extern void drzewo_Zabrano_jablko_mC15473398DCEB793AEAD2FBDB7F79472281F747E (void);
// 0x000001D5 System.Collections.IEnumerator drzewo::Odlicz()
extern void drzewo_Odlicz_m638CBE07B044AF09BEF6E2FC402A87F60F9BC4C4 (void);
// 0x000001D6 System.Collections.IEnumerator drzewo::Animacja_drzewo()
extern void drzewo_Animacja_drzewo_m2DAA79739E7C73E73D8E65043AA2CA2BD1878325 (void);
// 0x000001D7 System.Void drzewo::Spawnuj_jablko()
extern void drzewo_Spawnuj_jablko_mCD0064A2F038DB03C21CD2905F77412AB8775105 (void);
// 0x000001D8 System.Collections.IEnumerator drzewo::SpawnMoreApples()
extern void drzewo_SpawnMoreApples_m5C23DF1DDA1CE5D2DB0F36C79CABF65826EF0B12 (void);
// 0x000001D9 System.Void drzewo::.ctor()
extern void drzewo__ctor_m0ED45DCB4771306392AD2AFB63A429BFEFFA8883 (void);
// 0x000001DA System.Void drzewo/<Odlicz>d__8::.ctor(System.Int32)
extern void U3COdliczU3Ed__8__ctor_m3CB3BAD1830F2FC16E422581F028FD6B83D59561 (void);
// 0x000001DB System.Void drzewo/<Odlicz>d__8::System.IDisposable.Dispose()
extern void U3COdliczU3Ed__8_System_IDisposable_Dispose_mB868BEF777A34127A58F395D450797E602BF8083 (void);
// 0x000001DC System.Boolean drzewo/<Odlicz>d__8::MoveNext()
extern void U3COdliczU3Ed__8_MoveNext_m6E48206A6EDE13B6BF33A969B5C5FE7025A7FA99 (void);
// 0x000001DD System.Object drzewo/<Odlicz>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COdliczU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28C38F7F11AB6624F6AA5BE0DBFD0492C8847C9E (void);
// 0x000001DE System.Void drzewo/<Odlicz>d__8::System.Collections.IEnumerator.Reset()
extern void U3COdliczU3Ed__8_System_Collections_IEnumerator_Reset_mEF0E56934779B56E5A90545C192B318312393AE6 (void);
// 0x000001DF System.Object drzewo/<Odlicz>d__8::System.Collections.IEnumerator.get_Current()
extern void U3COdliczU3Ed__8_System_Collections_IEnumerator_get_Current_m8EC19BE3A0D7FEA75738411F4B75F359200AA728 (void);
// 0x000001E0 System.Void drzewo/<Animacja_drzewo>d__9::.ctor(System.Int32)
extern void U3CAnimacja_drzewoU3Ed__9__ctor_mB28D52DDC6BD99AC264AD98133836A45F4C97A44 (void);
// 0x000001E1 System.Void drzewo/<Animacja_drzewo>d__9::System.IDisposable.Dispose()
extern void U3CAnimacja_drzewoU3Ed__9_System_IDisposable_Dispose_m6FD1BAF7E322064A45C8CCB0B64EC1DD35943166 (void);
// 0x000001E2 System.Boolean drzewo/<Animacja_drzewo>d__9::MoveNext()
extern void U3CAnimacja_drzewoU3Ed__9_MoveNext_mEF5D7FF7AF756EC3B5F3F18118B400845CC7FA3A (void);
// 0x000001E3 System.Object drzewo/<Animacja_drzewo>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimacja_drzewoU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79B8127C8BFDB84596128BD00761AB38BB50C3BC (void);
// 0x000001E4 System.Void drzewo/<Animacja_drzewo>d__9::System.Collections.IEnumerator.Reset()
extern void U3CAnimacja_drzewoU3Ed__9_System_Collections_IEnumerator_Reset_mC5858A9BD1F3FEB9CC2F0A70B8D56B7E05F93380 (void);
// 0x000001E5 System.Object drzewo/<Animacja_drzewo>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CAnimacja_drzewoU3Ed__9_System_Collections_IEnumerator_get_Current_m970CEC3D0B5B06CF60762AEA3040CBB314D60A59 (void);
// 0x000001E6 System.Void drzewo/<SpawnMoreApples>d__11::.ctor(System.Int32)
extern void U3CSpawnMoreApplesU3Ed__11__ctor_mF18D21763E6CDB8163566637F4D73969C6529E03 (void);
// 0x000001E7 System.Void drzewo/<SpawnMoreApples>d__11::System.IDisposable.Dispose()
extern void U3CSpawnMoreApplesU3Ed__11_System_IDisposable_Dispose_mF7BE9DFDEF778BDAF01FBE2ABEA5AF05EEC92C0D (void);
// 0x000001E8 System.Boolean drzewo/<SpawnMoreApples>d__11::MoveNext()
extern void U3CSpawnMoreApplesU3Ed__11_MoveNext_mEE0489B7FF74872AC95C7FA3B65546499D399BA1 (void);
// 0x000001E9 System.Object drzewo/<SpawnMoreApples>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnMoreApplesU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m744BDF6BA756F32DE94D272FC56C628D59F5F95D (void);
// 0x000001EA System.Void drzewo/<SpawnMoreApples>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSpawnMoreApplesU3Ed__11_System_Collections_IEnumerator_Reset_m371A9A8EF3B7A7FB439E709E42342CA14E0B212D (void);
// 0x000001EB System.Object drzewo/<SpawnMoreApples>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnMoreApplesU3Ed__11_System_Collections_IEnumerator_get_Current_m68BE5A46D79CCFAF55F26B341540AF0687FA2966 (void);
// 0x000001EC System.Void dzwieki_idle::Start()
extern void dzwieki_idle_Start_m54A32573B51758F51B62ED88FCB286206014F510 (void);
// 0x000001ED System.Void dzwieki_idle::Update()
extern void dzwieki_idle_Update_mE4B59C161969ACD94EAD85AEE3B827270C14C715 (void);
// 0x000001EE System.Void dzwieki_idle::Odtworz_idle()
extern void dzwieki_idle_Odtworz_idle_mB5527B85A55D03A58A1CDAB381712795F8EA016F (void);
// 0x000001EF System.Void dzwieki_idle::OdtworzTrzaski()
extern void dzwieki_idle_OdtworzTrzaski_m26EB58216E65B28A60675FBED37C42148F031476 (void);
// 0x000001F0 System.Void dzwieki_idle::Wyldziw()
extern void dzwieki_idle_Wyldziw_m7424D26DFF9F4D44F5DCC4FDDA2AF97FE8EF36C5 (void);
// 0x000001F1 System.Void dzwieki_idle::WlaDziw()
extern void dzwieki_idle_WlaDziw_m6FFF88CD1E54E75278D63DC0E02638AD1A137400 (void);
// 0x000001F2 System.Void dzwieki_idle::.ctor()
extern void dzwieki_idle__ctor_m639F6DEE1233C719E8FD70C80205AC0C8CE11048 (void);
// 0x000001F3 System.Void dzwiek_do_telefonu1::Start()
extern void dzwiek_do_telefonu1_Start_m04C7EB70A85B225F5E4EA3B13E860C4D671E3998 (void);
// 0x000001F4 System.Void dzwiek_do_telefonu1::Update()
extern void dzwiek_do_telefonu1_Update_mEEA48A16E948F576CA9AD48CC4BEF96018AEAA68 (void);
// 0x000001F5 System.Void dzwiek_do_telefonu1::Odtworz_idla1()
extern void dzwiek_do_telefonu1_Odtworz_idla1_mE145109E09E957626483299F6759537571BE91B6 (void);
// 0x000001F6 System.Void dzwiek_do_telefonu1::Odtworz_idla2()
extern void dzwiek_do_telefonu1_Odtworz_idla2_mCECFD4C47E83D0011F751C06AEF139341D1837E3 (void);
// 0x000001F7 System.Void dzwiek_do_telefonu1::Odtworz_idla3()
extern void dzwiek_do_telefonu1_Odtworz_idla3_m8262D2E4CF7795E6A6DD3B770DE6CABB35EA84B1 (void);
// 0x000001F8 System.Void dzwiek_do_telefonu1::Odtworz_idla4()
extern void dzwiek_do_telefonu1_Odtworz_idla4_m690B33B1469609B059AB393037A323959CCF3EE6 (void);
// 0x000001F9 System.Void dzwiek_do_telefonu1::Odtworz_idla5()
extern void dzwiek_do_telefonu1_Odtworz_idla5_mAD9F20321B6316797AE74EAE80DD78F954BD3F2F (void);
// 0x000001FA System.Void dzwiek_do_telefonu1::Odtworz_idla6()
extern void dzwiek_do_telefonu1_Odtworz_idla6_mB03B4655038AA4E7B68DDE58A6B0125CCC8FC350 (void);
// 0x000001FB System.Void dzwiek_do_telefonu1::.ctor()
extern void dzwiek_do_telefonu1__ctor_m2E4986FCD2BACF4BF4347A674856D1433DC73E15 (void);
// 0x000001FC System.Void GameHandler::Start()
extern void GameHandler_Start_mBACFF72C246BA6AC459E0F1BD27C84580141098A (void);
// 0x000001FD System.Void GameHandler::Update()
extern void GameHandler_Update_mB7AE5E1CDAE1D6BE50875EEA0CC1B20BB06708C3 (void);
// 0x000001FE System.Void GameHandler::.ctor()
extern void GameHandler__ctor_m838290D17C46D189D8A77B3D7CE6CE4C6D9EC485 (void);
// 0x000001FF System.Void gyroscope_to_lvl_5::Update()
extern void gyroscope_to_lvl_5_Update_mC2A831EDECC6970D761FAFA670EB4825CFAE7EDE (void);
// 0x00000200 System.Void gyroscope_to_lvl_5::.ctor()
extern void gyroscope_to_lvl_5__ctor_mB575CF9E24EBA48DC6E0890F86394F6F8DB5A984 (void);
// 0x00000201 System.Void Spaceship::Start()
extern void Spaceship_Start_m0A9F253FF8660210DD091A63D498DBA96B1F1D63 (void);
// 0x00000202 System.Void Spaceship::Update()
extern void Spaceship_Update_m00663D91B04315825C8BCEB18C07E338356EE4CD (void);
// 0x00000203 System.Void Spaceship::FixedUpdate()
extern void Spaceship_FixedUpdate_m278D0E4BB58DF1FB28C63C1A008B751F5EDED093 (void);
// 0x00000204 System.Void Spaceship::.ctor()
extern void Spaceship__ctor_mBC65712F83F75C0B669EC6FE9A3A34F5A5C45C81 (void);
// 0x00000205 System.Void jedzenie_dla_zwierzatek::Start()
extern void jedzenie_dla_zwierzatek_Start_mFDF2B9D5C972BDA3953E1CE6E3B9AD93D7D4560C (void);
// 0x00000206 System.Void jedzenie_dla_zwierzatek::Update()
extern void jedzenie_dla_zwierzatek_Update_mEB1024B50D57F8001B9DA698D10D19218B40E7AA (void);
// 0x00000207 System.Void jedzenie_dla_zwierzatek::.ctor()
extern void jedzenie_dla_zwierzatek__ctor_m91419CC850F5E265FF87C92D3B1AA9895B92FB82 (void);
// 0x00000208 System.Void kaczki::Start()
extern void kaczki_Start_m4B33E10DF979E0B0DC64B58CB0EE9BCE89CB7E7D (void);
// 0x00000209 System.Void kaczki::Update()
extern void kaczki_Update_m8FCDCA00125D1EC79359C3BBB64112F0DC3F75EE (void);
// 0x0000020A System.Void kaczki::NieAktywny()
extern void kaczki_NieAktywny_mBF5B604B682FEFAC24C4617F35B31C8EC61EF93D (void);
// 0x0000020B System.Void kaczki::Aktywny()
extern void kaczki_Aktywny_mEAA8E59BC9082285D35BE5E4B101742B548BD268 (void);
// 0x0000020C System.Void kaczki::.ctor()
extern void kaczki__ctor_mCA35DF1BF0C3185CC4F19D4AA9752271DB8A0A45 (void);
// 0x0000020D System.Void karmienie_konia::Start()
extern void karmienie_konia_Start_mBF4571755E0C4EF381DE771522EDFB065EA300F0 (void);
// 0x0000020E System.Void karmienie_konia::Update()
extern void karmienie_konia_Update_m70C5BFEACFAA326C55905DB58C23DA900DD37284 (void);
// 0x0000020F System.Void karmienie_konia::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void karmienie_konia_OnTriggerEnter2D_mA6D7D6366E1F319098D798527208A57C6E445EC6 (void);
// 0x00000210 System.Void karmienie_konia::.ctor()
extern void karmienie_konia__ctor_m4A914A60DF05107701AD71BC73FF9C271D31DC70 (void);
// 0x00000211 System.Void klikanie_prefab::Start()
extern void klikanie_prefab_Start_mDB2C4EFBF580686164F417AE81C98733864F5D32 (void);
// 0x00000212 System.Void klikanie_prefab::Update()
extern void klikanie_prefab_Update_mBFFA9AE0D94D097E24116E73755677DAAA5EDB94 (void);
// 0x00000213 System.Void klikanie_prefab::OnCollisionEnter(UnityEngine.Collision)
extern void klikanie_prefab_OnCollisionEnter_mAEE3D42E1DC5D61C1ED2ED36BE790A2627362A29 (void);
// 0x00000214 System.Collections.IEnumerator klikanie_prefab::Zniszcz_po_czasie()
extern void klikanie_prefab_Zniszcz_po_czasie_m5A84361AFF638AA0A7F0A2E204246406F69CFF22 (void);
// 0x00000215 System.Void klikanie_prefab::Spadaj()
extern void klikanie_prefab_Spadaj_m183EC520E543210598B0B95A85321E1CC39B77D4 (void);
// 0x00000216 System.Void klikanie_prefab::.ctor()
extern void klikanie_prefab__ctor_m4345397C101CF5515983837D03902603BA6050C3 (void);
// 0x00000217 System.Void klikanie_prefab/<Zniszcz_po_czasie>d__15::.ctor(System.Int32)
extern void U3CZniszcz_po_czasieU3Ed__15__ctor_mA37948393F40A74B0EF2E78D422B4FD88EAACF04 (void);
// 0x00000218 System.Void klikanie_prefab/<Zniszcz_po_czasie>d__15::System.IDisposable.Dispose()
extern void U3CZniszcz_po_czasieU3Ed__15_System_IDisposable_Dispose_m90FEA5AEF589C77FAB08418CEA02DE7538991746 (void);
// 0x00000219 System.Boolean klikanie_prefab/<Zniszcz_po_czasie>d__15::MoveNext()
extern void U3CZniszcz_po_czasieU3Ed__15_MoveNext_mE3CE8D95782EBF0BE79068292554959F189B90C4 (void);
// 0x0000021A System.Object klikanie_prefab/<Zniszcz_po_czasie>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZniszcz_po_czasieU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4509787946F6A68D99797A120ECBE6C4B9CA91BA (void);
// 0x0000021B System.Void klikanie_prefab/<Zniszcz_po_czasie>d__15::System.Collections.IEnumerator.Reset()
extern void U3CZniszcz_po_czasieU3Ed__15_System_Collections_IEnumerator_Reset_m1950218FAC29F61E4640C88FB29E29806CCA0C3F (void);
// 0x0000021C System.Object klikanie_prefab/<Zniszcz_po_czasie>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CZniszcz_po_czasieU3Ed__15_System_Collections_IEnumerator_get_Current_mF644E4A7DA1FCA9132FE8C9C6378E1CA0A81BB18 (void);
// 0x0000021D System.Void Klocki::Start()
extern void Klocki_Start_mFE5C63F826BF7980B47FB19B35742B6A13005932 (void);
// 0x0000021E System.Void Klocki::Update()
extern void Klocki_Update_m2174422984EAE5B10621384D902E551CA7858045 (void);
// 0x0000021F System.Void Klocki::KlocekClick(System.Int32)
extern void Klocki_KlocekClick_m66797DB873A881C8034219AFEC4981BDB996F1A4 (void);
// 0x00000220 System.Void Klocki::.ctor()
extern void Klocki__ctor_mF727C462145C8D9F96CF5ABEDD8A10154A8F0E18 (void);
// 0x00000221 System.Void Klocki::<Start>b__5_0()
extern void Klocki_U3CStartU3Eb__5_0_mBCC2E3875DBBC0A3B17587FF5C8E96C94AF64448 (void);
// 0x00000222 System.Void Klocki::<Start>b__5_1()
extern void Klocki_U3CStartU3Eb__5_1_m6791B4CA555C977E470452126DD609E16B1EF76F (void);
// 0x00000223 System.Void krawedzie_collider::Start()
extern void krawedzie_collider_Start_mFF66ED379E6047D3E42B983FA930155BD80B51CB (void);
// 0x00000224 System.Collections.IEnumerator krawedzie_collider::opoznienie_krawedzi()
extern void krawedzie_collider_opoznienie_krawedzi_mB327CEE591C7626D2F2A4C9A59D92A05ADD9251E (void);
// 0x00000225 System.Void krawedzie_collider::Update()
extern void krawedzie_collider_Update_m23DB5201B2AADD28F8D5359CB5EF4F796C753ECA (void);
// 0x00000226 System.Void krawedzie_collider::.ctor()
extern void krawedzie_collider__ctor_mB1FB7EC78EE2F1BCEDEA44439199D04EC0851FF2 (void);
// 0x00000227 System.Void krawedzie_collider/<opoznienie_krawedzi>d__6::.ctor(System.Int32)
extern void U3Copoznienie_krawedziU3Ed__6__ctor_mA0D594EC936CDE5E8A05B1BB83D0BFBC75E7F244 (void);
// 0x00000228 System.Void krawedzie_collider/<opoznienie_krawedzi>d__6::System.IDisposable.Dispose()
extern void U3Copoznienie_krawedziU3Ed__6_System_IDisposable_Dispose_m179E47E8B0BEC540A4FAA81120D65E5AE14D9C6A (void);
// 0x00000229 System.Boolean krawedzie_collider/<opoznienie_krawedzi>d__6::MoveNext()
extern void U3Copoznienie_krawedziU3Ed__6_MoveNext_m3064BF9CBCCFE305BAE43460994F9B837CA697AB (void);
// 0x0000022A System.Object krawedzie_collider/<opoznienie_krawedzi>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Copoznienie_krawedziU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2FD2D66EB7D0607D7E89ED4830C83B17B7663D5E (void);
// 0x0000022B System.Void krawedzie_collider/<opoznienie_krawedzi>d__6::System.Collections.IEnumerator.Reset()
extern void U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_Reset_m3FD560B8195725C674BAA897E94E2644A8DB9A74 (void);
// 0x0000022C System.Object krawedzie_collider/<opoznienie_krawedzi>d__6::System.Collections.IEnumerator.get_Current()
extern void U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_get_Current_m073E59EB7950B6C6356A0444DFE912964FCD3644 (void);
// 0x0000022D System.Void Krawedzie_collider_V2::Start()
extern void Krawedzie_collider_V2_Start_mCDD688F67CF3EEC82B152DC156F42D3A88C355E5 (void);
// 0x0000022E System.Collections.IEnumerator Krawedzie_collider_V2::opoznienie_krawedzi()
extern void Krawedzie_collider_V2_opoznienie_krawedzi_m8385487AC27E8D0955CA7F8AA71E748BC8D51748 (void);
// 0x0000022F System.Void Krawedzie_collider_V2::Update()
extern void Krawedzie_collider_V2_Update_mB48818F4D3E942B37A8072D30454FDB04F1B236E (void);
// 0x00000230 System.Void Krawedzie_collider_V2::.ctor()
extern void Krawedzie_collider_V2__ctor_m02C4BCBA2E71113ABE3599AAF394A144FA4A5C41 (void);
// 0x00000231 System.Void Krawedzie_collider_V2/<opoznienie_krawedzi>d__6::.ctor(System.Int32)
extern void U3Copoznienie_krawedziU3Ed__6__ctor_m7BD0AB0A278448657BE58BEDE265B817421633E7 (void);
// 0x00000232 System.Void Krawedzie_collider_V2/<opoznienie_krawedzi>d__6::System.IDisposable.Dispose()
extern void U3Copoznienie_krawedziU3Ed__6_System_IDisposable_Dispose_mEA55E47720ADFC2FDCB7213AC2D96D0CBA5AF2E6 (void);
// 0x00000233 System.Boolean Krawedzie_collider_V2/<opoznienie_krawedzi>d__6::MoveNext()
extern void U3Copoznienie_krawedziU3Ed__6_MoveNext_mABB5A4EAA9944882774A32BD5EF9F413DB1AB50A (void);
// 0x00000234 System.Object Krawedzie_collider_V2/<opoznienie_krawedzi>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Copoznienie_krawedziU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DADFF4BBF599E77610CD2DA833BD8AFF52D0F12 (void);
// 0x00000235 System.Void Krawedzie_collider_V2/<opoznienie_krawedzi>d__6::System.Collections.IEnumerator.Reset()
extern void U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_Reset_m33B09EE0B676F23A07A74E40E98FFEFBF010BAD2 (void);
// 0x00000236 System.Object Krawedzie_collider_V2/<opoznienie_krawedzi>d__6::System.Collections.IEnumerator.get_Current()
extern void U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_get_Current_mD092C451B9A65857F30FA48379079E7B8B681EF4 (void);
// 0x00000237 System.Void Link::Go_To_www()
extern void Link_Go_To_www_m2F5B8F0296E7C6D2FEDD9C11CBA8ED985E682C33 (void);
// 0x00000238 System.Void Link::Go_To_youtube()
extern void Link_Go_To_youtube_m90432F0F072DF32A82747AB2A947B856E9E81CE4 (void);
// 0x00000239 System.Void Link::Go_To_facebook()
extern void Link_Go_To_facebook_mFE113FED7BFA76296767D43F297CE9726C2A9104 (void);
// 0x0000023A System.Void Link::Go_To_email()
extern void Link_Go_To_email_m55E776E59440F040482DFDF872F86872B5493C6D (void);
// 0x0000023B System.Void Link::Go_To_opinion()
extern void Link_Go_To_opinion_m1A370C0A319A01DCDEE27B4DD6AB7F946C7808C4 (void);
// 0x0000023C System.Void Link::SendEmail()
extern void Link_SendEmail_mC65FA6744F4ACFA24446F6AEAB3793219D7346BA (void);
// 0x0000023D System.String Link::MyEscapeURL(System.String)
extern void Link_MyEscapeURL_m5C129C77D493C95A78BB926EBA53D7D23E2DBEAE (void);
// 0x0000023E System.Void Link::.ctor()
extern void Link__ctor_m63C9F1B9DEE13922D1EE2C3508D9BAE07D8324F8 (void);
// 0x0000023F System.Void Menu_music::Start()
extern void Menu_music_Start_mB0D13B5C193F378443C8DA867BE0BFAA79E0603A (void);
// 0x00000240 System.Void Menu_music::Update()
extern void Menu_music_Update_m27F976A9C8F6AD1694BC252F3367185D6C326223 (void);
// 0x00000241 System.Void Menu_music::.ctor()
extern void Menu_music__ctor_mDA83BB47DA56FF302B57142C9FDC0F00AB8C46D8 (void);
// 0x00000242 System.Void mucha::Start()
extern void mucha_Start_m5E26A863861EDC0B4ADD78DDC568C936DC24C0BE (void);
// 0x00000243 System.Void mucha::Update()
extern void mucha_Update_m3B333A86D636C8B5AE66D1B6F679A42A0BA7B07D (void);
// 0x00000244 System.Void mucha::.ctor()
extern void mucha__ctor_mE3D432B24527738D07FD450ED18D3E6C25A32BBE (void);
// 0x00000245 System.Void mucha2::Start()
extern void mucha2_Start_m3776ECE66C9454E12E225630D3825B95A9634C7F (void);
// 0x00000246 System.Void mucha2::Update()
extern void mucha2_Update_mBCBEE966AD42C0936E8C459427CC6AD8051E92BC (void);
// 0x00000247 System.Collections.IEnumerator mucha2::Respawn()
extern void mucha2_Respawn_mB982A6A723153C512A5AC7ABB3BE31037B39CA5C (void);
// 0x00000248 System.Void mucha2::Lataj_dalej_z_tym_tematem()
extern void mucha2_Lataj_dalej_z_tym_tematem_m4DD568B7AD73E19ACC6DA4AC5D09CCE42903ED31 (void);
// 0x00000249 System.Collections.IEnumerator mucha2::MoveObject(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void mucha2_MoveObject_mF3E870A971D0CBBFB394F3320FB4CC5B9476836F (void);
// 0x0000024A System.Collections.IEnumerator mucha2::RotateObjectWithAngle(System.Single,UnityEngine.GameObject,UnityEngine.Quaternion)
extern void mucha2_RotateObjectWithAngle_mFEDEAF40863E79D357A5029D8E3FB9AA8698AD8B (void);
// 0x0000024B System.Void mucha2::.ctor()
extern void mucha2__ctor_mEFDA35A5567F13A4A30302AB121CD9E4887CF827 (void);
// 0x0000024C System.Void mucha2/<Respawn>d__13::.ctor(System.Int32)
extern void U3CRespawnU3Ed__13__ctor_m7221B2DBEB8EA5074B7B4008F53CBD5898709740 (void);
// 0x0000024D System.Void mucha2/<Respawn>d__13::System.IDisposable.Dispose()
extern void U3CRespawnU3Ed__13_System_IDisposable_Dispose_m739940A8C5E54BDCD776891CF1C02618D4762F83 (void);
// 0x0000024E System.Boolean mucha2/<Respawn>d__13::MoveNext()
extern void U3CRespawnU3Ed__13_MoveNext_m9AEDBBD7B1AD7D3F738A495221522907435C41E5 (void);
// 0x0000024F System.Object mucha2/<Respawn>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D8044D9D1FB01E236B2AC83160B0135010A100 (void);
// 0x00000250 System.Void mucha2/<Respawn>d__13::System.Collections.IEnumerator.Reset()
extern void U3CRespawnU3Ed__13_System_Collections_IEnumerator_Reset_m48B8F484BF3A17C6554F390FA3A695DA56F16BC8 (void);
// 0x00000251 System.Object mucha2/<Respawn>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnU3Ed__13_System_Collections_IEnumerator_get_Current_m1175602C6FDA4538F35192EE4F750D7E36A75780 (void);
// 0x00000252 System.Void mucha2/<MoveObject>d__15::.ctor(System.Int32)
extern void U3CMoveObjectU3Ed__15__ctor_m6A73DD809FF82ECC55E2A4731EAF19A9AFFC1814 (void);
// 0x00000253 System.Void mucha2/<MoveObject>d__15::System.IDisposable.Dispose()
extern void U3CMoveObjectU3Ed__15_System_IDisposable_Dispose_m06186EC61F3A73E5DD9061E549C50861051AD395 (void);
// 0x00000254 System.Boolean mucha2/<MoveObject>d__15::MoveNext()
extern void U3CMoveObjectU3Ed__15_MoveNext_m013031E3D7543ABABC724D293776D81721E7573B (void);
// 0x00000255 System.Object mucha2/<MoveObject>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveObjectU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF76C1B5E7291B43E48384D4823CC7493EED3A30A (void);
// 0x00000256 System.Void mucha2/<MoveObject>d__15::System.Collections.IEnumerator.Reset()
extern void U3CMoveObjectU3Ed__15_System_Collections_IEnumerator_Reset_m0D1F7A484A4B2166F54D8085B374435BCB58CD3B (void);
// 0x00000257 System.Object mucha2/<MoveObject>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CMoveObjectU3Ed__15_System_Collections_IEnumerator_get_Current_mD6415A53F1E2DC3371A9244D31732825B4D75BC8 (void);
// 0x00000258 System.Void mucha2/<RotateObjectWithAngle>d__16::.ctor(System.Int32)
extern void U3CRotateObjectWithAngleU3Ed__16__ctor_m5A39C35445F1E2DA003E28390B4142F54341C526 (void);
// 0x00000259 System.Void mucha2/<RotateObjectWithAngle>d__16::System.IDisposable.Dispose()
extern void U3CRotateObjectWithAngleU3Ed__16_System_IDisposable_Dispose_m8C1234B8DAF1A6F86C336AC7CA01FE2819E1E4DA (void);
// 0x0000025A System.Boolean mucha2/<RotateObjectWithAngle>d__16::MoveNext()
extern void U3CRotateObjectWithAngleU3Ed__16_MoveNext_m79615F5A8AD858417A57150AFD416615DAF26EE3 (void);
// 0x0000025B System.Object mucha2/<RotateObjectWithAngle>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotateObjectWithAngleU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99B208132F4FF6CEFE5C0046C6DF71F835D9D319 (void);
// 0x0000025C System.Void mucha2/<RotateObjectWithAngle>d__16::System.Collections.IEnumerator.Reset()
extern void U3CRotateObjectWithAngleU3Ed__16_System_Collections_IEnumerator_Reset_mC67DF33635760FD8DD19570DFF420DAA974590B8 (void);
// 0x0000025D System.Object mucha2/<RotateObjectWithAngle>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CRotateObjectWithAngleU3Ed__16_System_Collections_IEnumerator_get_Current_m047E2EBF9CEF077172A650A3F857ABBCE19FEE99 (void);
// 0x0000025E System.Void myszka_animacja::Start()
extern void myszka_animacja_Start_mB09E99364416FCF770F7AC2B4266CA2838E87E01 (void);
// 0x0000025F System.Void myszka_animacja::Update()
extern void myszka_animacja_Update_m4C9457DC5D9F3B86F760943864B66D988F6092BE (void);
// 0x00000260 System.Void myszka_animacja::PlayAnim()
extern void myszka_animacja_PlayAnim_mD0ACF7694703CF7A1DD26D93CAF703C5B1014B59 (void);
// 0x00000261 System.Void myszka_animacja::.ctor()
extern void myszka_animacja__ctor_mB544EC87FB8ACCB1CB4210EC6360F790E1930C57 (void);
// 0x00000262 System.Void Objects_from_lvl5::Start()
extern void Objects_from_lvl5_Start_m3B24B276A540B24992DEE6571AA6F20B0BB36D94 (void);
// 0x00000263 System.Void Objects_from_lvl5::Update()
extern void Objects_from_lvl5_Update_mF38EA7F048B8C6A71D6E08FE25FBD31E6847E413 (void);
// 0x00000264 System.Void Objects_from_lvl5::.ctor()
extern void Objects_from_lvl5__ctor_m8BEEDC913201C15BFFAE293BD1989B7AE8F6A77D (void);
// 0x00000265 System.Void odpal_plansze::Start()
extern void odpal_plansze_Start_mA45F8DB80F99F1D942116B3FBD5C4286E11F3332 (void);
// 0x00000266 System.Void odpal_plansze::Update()
extern void odpal_plansze_Update_m547550AB30E3B9B79A3B987ECAA39A4F597C2F96 (void);
// 0x00000267 System.Void odpal_plansze::Uruchom_plansze()
extern void odpal_plansze_Uruchom_plansze_m0CF289E2C3534E8BFF21BAB04E4FA50B9CC9D476 (void);
// 0x00000268 System.Collections.IEnumerator odpal_plansze::Odlicz(System.Int32)
extern void odpal_plansze_Odlicz_mDE0C9ED7C368B363D84031A2B104F815DC09CC5B (void);
// 0x00000269 System.Void odpal_plansze::.ctor()
extern void odpal_plansze__ctor_m120478E6D52E3636D7D26FFF898569BEB7F2A0E4 (void);
// 0x0000026A System.Void odpal_plansze/<Odlicz>d__8::.ctor(System.Int32)
extern void U3COdliczU3Ed__8__ctor_m6B3113967CC6B2934131BA7B975046F21E7A07B6 (void);
// 0x0000026B System.Void odpal_plansze/<Odlicz>d__8::System.IDisposable.Dispose()
extern void U3COdliczU3Ed__8_System_IDisposable_Dispose_mA5D3EC9CC6E82406A9A96A6616C1973FC638AA6A (void);
// 0x0000026C System.Boolean odpal_plansze/<Odlicz>d__8::MoveNext()
extern void U3COdliczU3Ed__8_MoveNext_m3A3E55714DFAEF4B1D759B3EC396ABD7E1E25967 (void);
// 0x0000026D System.Object odpal_plansze/<Odlicz>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COdliczU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2FDECA5879495986AABD650447D669E7F3F426AB (void);
// 0x0000026E System.Void odpal_plansze/<Odlicz>d__8::System.Collections.IEnumerator.Reset()
extern void U3COdliczU3Ed__8_System_Collections_IEnumerator_Reset_m4004F3FAB175E8CA1E6104A209D9F0E6CB473A47 (void);
// 0x0000026F System.Object odpal_plansze/<Odlicz>d__8::System.Collections.IEnumerator.get_Current()
extern void U3COdliczU3Ed__8_System_Collections_IEnumerator_get_Current_mDDDFBC256E8C081F5FBBBA63393C505E992FBCEC (void);
// 0x00000270 System.Void Odpal_planszeV2::Update()
extern void Odpal_planszeV2_Update_m03E02873B5F5241B29F2AE3A08A9928DB8FA277A (void);
// 0x00000271 System.Void Odpal_planszeV2::LoadNextLevel()
extern void Odpal_planszeV2_LoadNextLevel_m26004B29A1959AE53BD8B154E45CA8809A07A71A (void);
// 0x00000272 System.Collections.IEnumerator Odpal_planszeV2::LoadLevel()
extern void Odpal_planszeV2_LoadLevel_m6DB69C8017FD18963F72BA197C0D15E3435EF0B8 (void);
// 0x00000273 System.Void Odpal_planszeV2::.ctor()
extern void Odpal_planszeV2__ctor_mACC99602A624D2F56ED7C6B98625CCBB50D6B2FE (void);
// 0x00000274 System.Void Odpal_planszeV2/<LoadLevel>d__8::.ctor(System.Int32)
extern void U3CLoadLevelU3Ed__8__ctor_mB596DAD6212B617D1715851A8ACA0E70A3FE0BE7 (void);
// 0x00000275 System.Void Odpal_planszeV2/<LoadLevel>d__8::System.IDisposable.Dispose()
extern void U3CLoadLevelU3Ed__8_System_IDisposable_Dispose_mA30C6506EAE92BFB3BA5691D88852C2D6A374F2F (void);
// 0x00000276 System.Boolean Odpal_planszeV2/<LoadLevel>d__8::MoveNext()
extern void U3CLoadLevelU3Ed__8_MoveNext_mAF404D286477FB537CEFA01CE147007AFF12751B (void);
// 0x00000277 System.Object Odpal_planszeV2/<LoadLevel>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F6580E9BAF52F06020214F4046829B7D2F13F25 (void);
// 0x00000278 System.Void Odpal_planszeV2/<LoadLevel>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelU3Ed__8_System_Collections_IEnumerator_Reset_mED99FC4B401A2366F8930DE4962D449070D74476 (void);
// 0x00000279 System.Object Odpal_planszeV2/<LoadLevel>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelU3Ed__8_System_Collections_IEnumerator_get_Current_m1ABABDA75A7A1F857BFDDC216E22987C64CF13B5 (void);
// 0x0000027A System.Void odtworzAnimacje::Update()
extern void odtworzAnimacje_Update_m910AEFCAE66412001CC9D84062C4B811032AA066 (void);
// 0x0000027B System.Void odtworzAnimacje::Animacja()
extern void odtworzAnimacje_Animacja_m7A144BB50B07B08FB0541CA3368E3FEB4E0034C9 (void);
// 0x0000027C System.Collections.IEnumerator odtworzAnimacje::powrot_do_idle()
extern void odtworzAnimacje_powrot_do_idle_mF431517CA96619460CB2C0AE656EA64A5C2FAED0 (void);
// 0x0000027D System.Void odtworzAnimacje::.ctor()
extern void odtworzAnimacje__ctor_m2A7437ADC31F1C462D68CAC4B48A0303B1BC97C1 (void);
// 0x0000027E System.Void odtworzAnimacje/<powrot_do_idle>d__10::.ctor(System.Int32)
extern void U3Cpowrot_do_idleU3Ed__10__ctor_m5CEB621A9AE536C33AB16C17EA2B27F72B02EB0B (void);
// 0x0000027F System.Void odtworzAnimacje/<powrot_do_idle>d__10::System.IDisposable.Dispose()
extern void U3Cpowrot_do_idleU3Ed__10_System_IDisposable_Dispose_mB2F97F7947FACBD5567E6961ED7B9181C0F2456B (void);
// 0x00000280 System.Boolean odtworzAnimacje/<powrot_do_idle>d__10::MoveNext()
extern void U3Cpowrot_do_idleU3Ed__10_MoveNext_mC8F8871F706F26F4599851C8AECAB5889B92E84B (void);
// 0x00000281 System.Object odtworzAnimacje/<powrot_do_idle>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cpowrot_do_idleU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F41DE2279C2CDA1894F5AD423463BE47FC6B38B (void);
// 0x00000282 System.Void odtworzAnimacje/<powrot_do_idle>d__10::System.Collections.IEnumerator.Reset()
extern void U3Cpowrot_do_idleU3Ed__10_System_Collections_IEnumerator_Reset_m06D3A3A3C026141B5EBB96B3BD2BCAAA861CE22A (void);
// 0x00000283 System.Object odtworzAnimacje/<powrot_do_idle>d__10::System.Collections.IEnumerator.get_Current()
extern void U3Cpowrot_do_idleU3Ed__10_System_Collections_IEnumerator_get_Current_m47744200FAD3336CEE8E36BD126F239BA8C84B9D (void);
// 0x00000284 System.Void OdtworzAnimacje2::Start()
extern void OdtworzAnimacje2_Start_mC082560229BCB25D7C5461F8FCCE81EA1D0D5A25 (void);
// 0x00000285 System.Void OdtworzAnimacje2::Update()
extern void OdtworzAnimacje2_Update_m24BEBD55E0DF87B081DA41813CB674FAD610BBF5 (void);
// 0x00000286 System.Void OdtworzAnimacje2::OdtworzAnimacje()
extern void OdtworzAnimacje2_OdtworzAnimacje_m7E1B5C0534D386489AD98CAFDAFDB4BBA5610788 (void);
// 0x00000287 System.Void OdtworzAnimacje2::.ctor()
extern void OdtworzAnimacje2__ctor_mE0CF5BD3C0C0D3261DE29AA6DEB443E32550E3DC (void);
// 0x00000288 System.Void okienka::Start()
extern void okienka_Start_mA9DB420C76B0DBF9FB4B6764D824175863320F0B (void);
// 0x00000289 System.Void okienka::Update()
extern void okienka_Update_m5565A626C967B3BE8662C2C60D4BFE100654D658 (void);
// 0x0000028A System.Void okienka::Wlacz_okno()
extern void okienka_Wlacz_okno_m6299C96405851DB683C2B7403E1105BA04536E4B (void);
// 0x0000028B System.Void okienka::.ctor()
extern void okienka__ctor_mCA6E61DBEE32CFF5F56264E1AD57F10993B9B36E (void);
// 0x0000028C System.Void parental_gate::Start()
extern void parental_gate_Start_m96E8320A1BD9D5AF5531DA5FD0C30EEDA635F2AC (void);
// 0x0000028D System.Void parental_gate::Update()
extern void parental_gate_Update_mAB8C66B2B514C1BE5F7E498B8DEA52000D1DFB96 (void);
// 0x0000028E System.Void parental_gate::Zamknij()
extern void parental_gate_Zamknij_mE594118D08C3EBD69DB07474987CAC6A1EE6500F (void);
// 0x0000028F System.Void parental_gate::Otworz()
extern void parental_gate_Otworz_mD7AB6FAB9EE97A52CF5ECE5B3B782AB27386448A (void);
// 0x00000290 System.Void parental_gate::Sprawdz_wynik(System.Int32)
extern void parental_gate_Sprawdz_wynik_m669DC6CD8B293F42BD173FE8BA1B7E7CB081406C (void);
// 0x00000291 System.Void parental_gate::.ctor()
extern void parental_gate__ctor_mB00F2D4CA08CA2E5B391279101E0D020DC9780A7 (void);
// 0x00000292 System.Void parental_gate::<Start>b__22_0()
extern void parental_gate_U3CStartU3Eb__22_0_m6B487015790BE472B0848A94C9C3660D347EF120 (void);
// 0x00000293 System.Void parental_gate::<Start>b__22_1()
extern void parental_gate_U3CStartU3Eb__22_1_m1D80F37BAC457C070A164484B1311765353B71BF (void);
// 0x00000294 System.Void parental_gate::<Start>b__22_2()
extern void parental_gate_U3CStartU3Eb__22_2_mC848EAD591EDCF810559D3F9168C7E430985AB38 (void);
// 0x00000295 System.Void parental_gate::<Otworz>b__25_0()
extern void parental_gate_U3COtworzU3Eb__25_0_m3C33A3956E2B1F4A2E897C947AF647B5B2A02EC6 (void);
// 0x00000296 System.Void PlaySoundOnClick::Start()
extern void PlaySoundOnClick_Start_m6B8273A1DB35FA786419812A67A57444344D40C0 (void);
// 0x00000297 System.Void PlaySoundOnClick::Update()
extern void PlaySoundOnClick_Update_mC24C2DC8E5E79344214B1366617DDA182B013721 (void);
// 0x00000298 System.Void PlaySoundOnClick::PlaySound(System.String)
extern void PlaySoundOnClick_PlaySound_m52AF3B9B5A9678202851B90C3CD50525A5B5C767 (void);
// 0x00000299 System.Void PlaySoundOnClick::PrzyciskPilota_lub_back_btn()
extern void PlaySoundOnClick_PrzyciskPilota_lub_back_btn_mB881DEDB02FB66427DAC9575696A0CAB8B298030 (void);
// 0x0000029A System.Void PlaySoundOnClick::.ctor()
extern void PlaySoundOnClick__ctor_m7C021FAC19D375FBB148D9C8AD73686C1FCD430A (void);
// 0x0000029B System.Void podnies::Start()
extern void podnies_Start_m21CFFA4EE32C10476E3EDCAE367434A554D8DE9A (void);
// 0x0000029C System.Void podnies::Update()
extern void podnies_Update_mD87A8028FE4EC32A4F087E8CA835229B6F41AD14 (void);
// 0x0000029D System.Void podnies::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void podnies_OnTriggerEnter2D_mA6F02D218EF77C0F4092C136F994665E26C19CBE (void);
// 0x0000029E System.Collections.IEnumerator podnies::Restart_warzywa()
extern void podnies_Restart_warzywa_mE10955312F516A60988D9936C276BF437C17AC24 (void);
// 0x0000029F System.Void podnies::.ctor()
extern void podnies__ctor_m3EEBD85DC44A123E0738C43D9113C3B3E26783D9 (void);
// 0x000002A0 System.Void podnies/<Restart_warzywa>d__12::.ctor(System.Int32)
extern void U3CRestart_warzywaU3Ed__12__ctor_mF1AB9E5C6DC52B68883CBBCAA408D6E1DF780457 (void);
// 0x000002A1 System.Void podnies/<Restart_warzywa>d__12::System.IDisposable.Dispose()
extern void U3CRestart_warzywaU3Ed__12_System_IDisposable_Dispose_m1D8CD81E218EDEE9B6F07DFA95F556D3ED2FF263 (void);
// 0x000002A2 System.Boolean podnies/<Restart_warzywa>d__12::MoveNext()
extern void U3CRestart_warzywaU3Ed__12_MoveNext_mBB2F6CD7CFF1CA678119E615DDFC959833EB5B46 (void);
// 0x000002A3 System.Object podnies/<Restart_warzywa>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestart_warzywaU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m766E446A1CF6C35AC51B51985D90B73521834644 (void);
// 0x000002A4 System.Void podnies/<Restart_warzywa>d__12::System.Collections.IEnumerator.Reset()
extern void U3CRestart_warzywaU3Ed__12_System_Collections_IEnumerator_Reset_mBDEEFB3CA734E98B28939C798F28A4AA0DDD13D8 (void);
// 0x000002A5 System.Object podnies/<Restart_warzywa>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CRestart_warzywaU3Ed__12_System_Collections_IEnumerator_get_Current_m5000C188344D11B0DE46C54A46A6DA9A3146DF0F (void);
// 0x000002A6 System.Void pokazz_przyciskiOnStart::Start()
extern void pokazz_przyciskiOnStart_Start_m7A924BCF4289BECD4F6990B29F902447CCDC9625 (void);
// 0x000002A7 System.Void pokazz_przyciskiOnStart::Update()
extern void pokazz_przyciskiOnStart_Update_m61195435C4DC26E60382D950768F69FCB0636636 (void);
// 0x000002A8 System.Void pokazz_przyciskiOnStart::pokaz()
extern void pokazz_przyciskiOnStart_pokaz_m68DFA91ADA8A4A26B0DC63110C6F1BFC42A1B82A (void);
// 0x000002A9 System.Void pokazz_przyciskiOnStart::pokaz_raw_image()
extern void pokazz_przyciskiOnStart_pokaz_raw_image_m6112D162DA29C9173F7CE2D62F640CB8EAFC3C88 (void);
// 0x000002AA System.Void pokazz_przyciskiOnStart::.ctor()
extern void pokazz_przyciskiOnStart__ctor_m45A621E0A9DF1DCEAC7EFD85EE44B6B1CEC594D5 (void);
// 0x000002AB System.Void pokaz_przyciski_Menu::Start()
extern void pokaz_przyciski_Menu_Start_m5263EECD2A3DDFF5781A3015C41A27ED610BC5C6 (void);
// 0x000002AC System.Void pokaz_przyciski_Menu::Update()
extern void pokaz_przyciski_Menu_Update_m6079C483A245726783F7458B2E0CFA879618752B (void);
// 0x000002AD System.Void pokaz_przyciski_Menu::.ctor()
extern void pokaz_przyciski_Menu__ctor_m3F144D8BB36E4606C5FC17CCF21A49D0312A4436 (void);
// 0x000002AE System.Void PressHandler::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PressHandler_OnPointerDown_mE4A3AB7C45DF4246DD501D809A6EBB6AE2E41FD5 (void);
// 0x000002AF System.Void PressHandler::.ctor()
extern void PressHandler__ctor_m0CD7508CE3B1FFF4EFF0733C5212B9B305274F95 (void);
// 0x000002B0 System.Void PressHandler/ButtonPressEvent::.ctor()
extern void ButtonPressEvent__ctor_m516CD67C95D008E473BDF03C87FB59B4A44ED431 (void);
// 0x000002B1 System.Void przejscieV2::Animuj()
extern void przejscieV2_Animuj_m2C96E4CEA4F32BDC81C9B3B46B396F93B8EE61FA (void);
// 0x000002B2 System.Void przejscieV2::Start()
extern void przejscieV2_Start_mEB40F3B7E6C59FFF69DE12BE49E2B3FEF90D334F (void);
// 0x000002B3 System.Void przejscieV2::Update()
extern void przejscieV2_Update_mC6A5CC69DA9261742CEB1D0345C2A513D243FCA6 (void);
// 0x000002B4 System.Void przejscieV2::.ctor()
extern void przejscieV2__ctor_m50E4A812D98AC7FCD9D9DEC2D45C45763DFA0B1A (void);
// 0x000002B5 System.Void przerwij_zzzz::Start()
extern void przerwij_zzzz_Start_mAD1B60F89FA817C3F9730910E5DBC576D36A856D (void);
// 0x000002B6 System.Void przerwij_zzzz::Update()
extern void przerwij_zzzz_Update_m08500400F96D7A46D7893BF2A6DF1BCC851238FC (void);
// 0x000002B7 System.Void przerwij_zzzz::ZwierzeJe()
extern void przerwij_zzzz_ZwierzeJe_m2F0E8F4221EF6BFC88CD1C090E9CD7420615EDA1 (void);
// 0x000002B8 System.Void przerwij_zzzz::.ctor()
extern void przerwij_zzzz__ctor_m705011F5C1120DF9CD15834FDDB7C3526D9AA813 (void);
// 0x000002B9 System.Void Przerwij_zzzz_dla_kaczek::Start()
extern void Przerwij_zzzz_dla_kaczek_Start_m7A39D7F6646ABC39E22EFA56741083738D2E70B7 (void);
// 0x000002BA System.Void Przerwij_zzzz_dla_kaczek::Update()
extern void Przerwij_zzzz_dla_kaczek_Update_m65A73BDD3C87D2EC7103FD3C762EA79047FB5ED0 (void);
// 0x000002BB System.Void Przerwij_zzzz_dla_kaczek::Kaczki_Wyszly_na_Zer()
extern void Przerwij_zzzz_dla_kaczek_Kaczki_Wyszly_na_Zer_m777E9150AE287C2814857F95835DE98B6DD27804 (void);
// 0x000002BC System.Void Przerwij_zzzz_dla_kaczek::Kaczki_wrocily_spac()
extern void Przerwij_zzzz_dla_kaczek_Kaczki_wrocily_spac_m580624FB77F787658A521F2C4D603B0E935CC227 (void);
// 0x000002BD System.Void Przerwij_zzzz_dla_kaczek::ZwierzeJe()
extern void Przerwij_zzzz_dla_kaczek_ZwierzeJe_m4EE985D0244809797BBAEFA107ACFFB971520BA7 (void);
// 0x000002BE System.Void Przerwij_zzzz_dla_kaczek::.ctor()
extern void Przerwij_zzzz_dla_kaczek__ctor_m2532B4FC4B674EE7F0038F49DE0F84951C374232 (void);
// 0x000002BF System.Void przesuwanie_suwaka::Start()
extern void przesuwanie_suwaka_Start_m1CAA1F55F303576A4C3E1034CD8F567AA64A3629 (void);
// 0x000002C0 System.Void przesuwanie_suwaka::Update()
extern void przesuwanie_suwaka_Update_m6406A5E7D3A49B6A25EFC7879655801945C22547 (void);
// 0x000002C1 System.Void przesuwanie_suwaka::OnTriggerEnter(UnityEngine.Collider)
extern void przesuwanie_suwaka_OnTriggerEnter_m342A2246A14A30561E14B75EA0A4E117528866F2 (void);
// 0x000002C2 System.Void przesuwanie_suwaka::OnTriggerExit(UnityEngine.Collider)
extern void przesuwanie_suwaka_OnTriggerExit_m0C5E6F6C6A1185D033B7D4213CAA9F45AA340715 (void);
// 0x000002C3 System.Void przesuwanie_suwaka::.ctor()
extern void przesuwanie_suwaka__ctor_m89C755292CD045B6E5AF21CFA666A9A4F884BAEA (void);
// 0x000002C4 System.Void przyciaganie_jedzenia::Start()
extern void przyciaganie_jedzenia_Start_m43407BC9D457AF23A0BE897FBEE8227DB815918E (void);
// 0x000002C5 System.Collections.IEnumerator przyciaganie_jedzenia::Pozycja_startowa()
extern void przyciaganie_jedzenia_Pozycja_startowa_mD6CB6314ACA01403F369749CA4E631DA0D90BEDE (void);
// 0x000002C6 System.Void przyciaganie_jedzenia::Update()
extern void przyciaganie_jedzenia_Update_m0A1F301953FD7918D89F832C4EDBF00E86B8BEBE (void);
// 0x000002C7 System.Void przyciaganie_jedzenia::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void przyciaganie_jedzenia_OnTriggerEnter2D_mA7EF16C871E568D73CBB09FB70D41516EED8361F (void);
// 0x000002C8 System.Collections.IEnumerator przyciaganie_jedzenia::Zniszcz_po_czasie(UnityEngine.GameObject)
extern void przyciaganie_jedzenia_Zniszcz_po_czasie_m82FB0FD1D93ED0CEAB424CC957AC2A6DD395FE7E (void);
// 0x000002C9 System.Void przyciaganie_jedzenia::.ctor()
extern void przyciaganie_jedzenia__ctor_m0036C2100756BD58ACD6B163C87DC3D1710B6934 (void);
// 0x000002CA System.Void przyciaganie_jedzenia/<Pozycja_startowa>d__11::.ctor(System.Int32)
extern void U3CPozycja_startowaU3Ed__11__ctor_m201ECCAB449ABB89C7931F314DD1D6BCBD28F30E (void);
// 0x000002CB System.Void przyciaganie_jedzenia/<Pozycja_startowa>d__11::System.IDisposable.Dispose()
extern void U3CPozycja_startowaU3Ed__11_System_IDisposable_Dispose_m6C63FC14096F35CA6455BE94E2A0DDB664FB55C2 (void);
// 0x000002CC System.Boolean przyciaganie_jedzenia/<Pozycja_startowa>d__11::MoveNext()
extern void U3CPozycja_startowaU3Ed__11_MoveNext_mDDE893C2B1A9584F01A4109E08CE2471BED9A5DC (void);
// 0x000002CD System.Object przyciaganie_jedzenia/<Pozycja_startowa>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPozycja_startowaU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DB4FB19D82AE415A9F5192AADB1D16CAE4601D3 (void);
// 0x000002CE System.Void przyciaganie_jedzenia/<Pozycja_startowa>d__11::System.Collections.IEnumerator.Reset()
extern void U3CPozycja_startowaU3Ed__11_System_Collections_IEnumerator_Reset_m1485F63ACE5D941308FD469715D2A91681405A1B (void);
// 0x000002CF System.Object przyciaganie_jedzenia/<Pozycja_startowa>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CPozycja_startowaU3Ed__11_System_Collections_IEnumerator_get_Current_m853BD97B3D6FC858A2C4B5A0357FD9134B0AB30E (void);
// 0x000002D0 System.Void przyciaganie_jedzenia/<Zniszcz_po_czasie>d__14::.ctor(System.Int32)
extern void U3CZniszcz_po_czasieU3Ed__14__ctor_m1B56EC847353AA29A252B447926251A2CA09B9FE (void);
// 0x000002D1 System.Void przyciaganie_jedzenia/<Zniszcz_po_czasie>d__14::System.IDisposable.Dispose()
extern void U3CZniszcz_po_czasieU3Ed__14_System_IDisposable_Dispose_m5BE45F6D01D80EE040BEB6A79038639E58942156 (void);
// 0x000002D2 System.Boolean przyciaganie_jedzenia/<Zniszcz_po_czasie>d__14::MoveNext()
extern void U3CZniszcz_po_czasieU3Ed__14_MoveNext_m209AC42C341B4A20EC8A05CAE6458C5CD6EA3106 (void);
// 0x000002D3 System.Object przyciaganie_jedzenia/<Zniszcz_po_czasie>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZniszcz_po_czasieU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BC10E8ABCBA1701B7128D26756630D84CAA3940 (void);
// 0x000002D4 System.Void przyciaganie_jedzenia/<Zniszcz_po_czasie>d__14::System.Collections.IEnumerator.Reset()
extern void U3CZniszcz_po_czasieU3Ed__14_System_Collections_IEnumerator_Reset_m0B0CFC02FE002B2B81806AC0EA61E4FC46DD0352 (void);
// 0x000002D5 System.Object przyciaganie_jedzenia/<Zniszcz_po_czasie>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CZniszcz_po_czasieU3Ed__14_System_Collections_IEnumerator_get_Current_m120436A80C0C99D662F02B24DAF984C651E33645 (void);
// 0x000002D6 System.Void przycisk_dla_rodzicow::Start()
extern void przycisk_dla_rodzicow_Start_m1105D82BEC6D25E1BED410D40B523A6B090E1DB7 (void);
// 0x000002D7 System.Void przycisk_dla_rodzicow::Update()
extern void przycisk_dla_rodzicow_Update_m9F21BD4FC0E86FFA9853185B01B655BE8AE387E4 (void);
// 0x000002D8 System.Void przycisk_dla_rodzicow::Ekran_dla_rodzicow()
extern void przycisk_dla_rodzicow_Ekran_dla_rodzicow_m3A3050B88584A51D853531707A983A79C3552BE8 (void);
// 0x000002D9 System.Void przycisk_dla_rodzicow::.ctor()
extern void przycisk_dla_rodzicow__ctor_m65C38A4ABE14040399D6E1AD7BC529F8B660133A (void);
// 0x000002DA System.Void Raycast::Start()
extern void Raycast_Start_mE545E5462FCA8ED37857AAFB304E217D4D240D14 (void);
// 0x000002DB System.Void Raycast::Update()
extern void Raycast_Update_m9805032A4E75826B7AD913258F679C4B0DE38914 (void);
// 0x000002DC System.Void Raycast::.ctor()
extern void Raycast__ctor_m87D1EE0F8A0C8F66112EC24E52E8AD161EA6D132 (void);
// 0x000002DD System.Void RenderPieczatek::Start()
extern void RenderPieczatek_Start_mDE9BB65BA5099FDB49EC8800AA9848A1CB0B42FC (void);
// 0x000002DE System.Void RenderPieczatek::Zmiana_pref(UnityEngine.GameObject,System.Int32)
extern void RenderPieczatek_Zmiana_pref_m1FF8F5E8E4172968EFCE4E9986C96C9327324615 (void);
// 0x000002DF System.Void RenderPieczatek::Update()
extern void RenderPieczatek_Update_m84F0790CF1B63300F04AF64E3DF717AB88D29BF3 (void);
// 0x000002E0 System.Void RenderPieczatek::.ctor()
extern void RenderPieczatek__ctor_m6B16A63DE582E9DBEEC47F78361CC0BCC266C3B4 (void);
// 0x000002E1 System.Void RobienieKupy::Start()
extern void RobienieKupy_Start_m273DD513AFFB4CCCE4919A41019106F88B60087C (void);
// 0x000002E2 System.Void RobienieKupy::Update()
extern void RobienieKupy_Update_m06D85811AFBCD2831A36FB5E66403B5F81650CFE (void);
// 0x000002E3 System.Void RobienieKupy::zrobKupe()
extern void RobienieKupy_zrobKupe_mCBAEB4F7BEDA0B5EBB340D62C942F74E2C76A836 (void);
// 0x000002E4 System.Void RobienieKupy::.ctor()
extern void RobienieKupy__ctor_m10A4B4748FB7E60FB914EAFD227402961D612BB9 (void);
// 0x000002E5 System.Void Rysowanie_poziom5::Start()
extern void Rysowanie_poziom5_Start_m0FAEDB1DD7308E9FDD1FA3C0437B4F6E081C24F5 (void);
// 0x000002E6 System.Void Rysowanie_poziom5::Update()
extern void Rysowanie_poziom5_Update_m3EB7B53AB13FF136451B535512A744F7EB037886 (void);
// 0x000002E7 System.Void Rysowanie_poziom5::Drawing(UnityEngine.Touch,UnityEngine.Vector3)
extern void Rysowanie_poziom5_Drawing_m19C391FC8409F27EEBCC96DEE06B3EAA1F10A636 (void);
// 0x000002E8 UnityEngine.Vector2 Rysowanie_poziom5::getTouchPosition(UnityEngine.Vector2)
extern void Rysowanie_poziom5_getTouchPosition_m7E719A60DE29ECCF2C4D0E469EB63FFFC001B9D1 (void);
// 0x000002E9 UnityEngine.LineRenderer Rysowanie_poziom5::CreateBrush(UnityEngine.Vector3)
extern void Rysowanie_poziom5_CreateBrush_m730BB2CB81D7D04802555CE7AF12188FE4BEAB6C (void);
// 0x000002EA System.Void Rysowanie_poziom5::AddAPoint(UnityEngine.LineRenderer,UnityEngine.Vector3)
extern void Rysowanie_poziom5_AddAPoint_m8DC88EB2331ADC16A0D08E84BD838412B1D5F4B8 (void);
// 0x000002EB System.Void Rysowanie_poziom5::PointToMousePos(UnityEngine.LineRenderer,UnityEngine.Vector3)
extern void Rysowanie_poziom5_PointToMousePos_m5DE581DD45B43DE98DFE33406F161EA32CD379AE (void);
// 0x000002EC System.Void Rysowanie_poziom5::.ctor()
extern void Rysowanie_poziom5__ctor_m02CC022753BB104B41B798A533C52B16DD540B85 (void);
// 0x000002ED System.Void Rysuj::Start()
extern void Rysuj_Start_m210B6E4324C65F166944122E800B5D7579940E0C (void);
// 0x000002EE System.Void Rysuj::Update()
extern void Rysuj_Update_m28409F3D76D891626BF73E7B215BD94C12D7376E (void);
// 0x000002EF System.Void Rysuj::Kredka()
extern void Rysuj_Kredka_mF67EE5358C3AEF24052E8E298D93650CD3842CAF (void);
// 0x000002F0 System.Void Rysuj::Drawing()
extern void Rysuj_Drawing_mD1F6D8E4E7F14FA7DCD592830C3E6577EB5AF01F (void);
// 0x000002F1 System.Void Rysuj::CreateBrush()
extern void Rysuj_CreateBrush_mDA5FA0B4268B888B344756CC0BA6EA0D01269115 (void);
// 0x000002F2 System.Void Rysuj::AddAPoint(UnityEngine.Vector3)
extern void Rysuj_AddAPoint_m2240512A229DCDBCDC68A7832AB17863709CB229 (void);
// 0x000002F3 System.Void Rysuj::PointToMousePos()
extern void Rysuj_PointToMousePos_m6070B7EED37A9FA7B61456ECFC8E69D4B4DA2E54 (void);
// 0x000002F4 System.Void Rysuj::.ctor()
extern void Rysuj__ctor_m6417571E25B522ADF6714CACBAE1839F015197F3 (void);
// 0x000002F5 System.Void skaluj::Start()
extern void skaluj_Start_m003D25E3543CBBEA36EF2B07CEF91B98BB4B3C98 (void);
// 0x000002F6 System.Void skaluj::Update()
extern void skaluj_Update_mBF4739894E7771428147DB23645F003E3C2CA361 (void);
// 0x000002F7 System.Void skaluj::Skaluj()
extern void skaluj_Skaluj_m59E3FF240A724D38E388FFD55E3533FBE5B23D32 (void);
// 0x000002F8 System.Void skaluj::.ctor()
extern void skaluj__ctor_m2D7FBEA2118CB8FF9BF3A4AAB72A14E30C7BD12D (void);
// 0x000002F9 System.Void spawn_sianka::Start()
extern void spawn_sianka_Start_mF2B11F9FFC60065E480E0B616EA2CB6ACA541495 (void);
// 0x000002FA System.Void spawn_sianka::Update()
extern void spawn_sianka_Update_m61523C7443678D731A81770BD00BB4C50524E006 (void);
// 0x000002FB System.Void spawn_sianka::.ctor()
extern void spawn_sianka__ctor_m060D0CCAE2AD96B78FD4E6C645CABB7E4681483D (void);
// 0x000002FC System.Void spawn_zabawek::Start()
extern void spawn_zabawek_Start_m55BB95401BD69D9EC8123B8A6F6D6B2D237D863B (void);
// 0x000002FD System.Void spawn_zabawek::Update()
extern void spawn_zabawek_Update_m713F08A133F1A3B7AB12137955F7CA0375C585C4 (void);
// 0x000002FE System.Collections.IEnumerator spawn_zabawek::blok()
extern void spawn_zabawek_blok_mE55B64FB44B1D7255C5CC2871DCDDB30CD0171B9 (void);
// 0x000002FF System.Void spawn_zabawek::.ctor()
extern void spawn_zabawek__ctor_m4176AC077DFF0860A2951701E0BC4A775B547BC6 (void);
// 0x00000300 System.Void spawn_zabawek/<blok>d__12::.ctor(System.Int32)
extern void U3CblokU3Ed__12__ctor_mEEF57704106999A5868EEB2081F4BC545D1D31DD (void);
// 0x00000301 System.Void spawn_zabawek/<blok>d__12::System.IDisposable.Dispose()
extern void U3CblokU3Ed__12_System_IDisposable_Dispose_mCD97BD183F706538471BEFC07E50E9A0FE34F090 (void);
// 0x00000302 System.Boolean spawn_zabawek/<blok>d__12::MoveNext()
extern void U3CblokU3Ed__12_MoveNext_m5AFC52827D0A1551A242F06E03E458FC35BA7B68 (void);
// 0x00000303 System.Object spawn_zabawek/<blok>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CblokU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EE1C377915AFD287F6AC9AE18AE09554C083796 (void);
// 0x00000304 System.Void spawn_zabawek/<blok>d__12::System.Collections.IEnumerator.Reset()
extern void U3CblokU3Ed__12_System_Collections_IEnumerator_Reset_mE00A4C890B5554FB12FE68213C0BECFC41ABC971 (void);
// 0x00000305 System.Object spawn_zabawek/<blok>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CblokU3Ed__12_System_Collections_IEnumerator_get_Current_m670445573D229171F539E1D44FBCD433F0508D53 (void);
// 0x00000306 System.Void sprawdz_punkt::Start()
extern void sprawdz_punkt_Start_mD6EEF994281E5C9F56892AC48016B2D4697651F7 (void);
// 0x00000307 System.Void sprawdz_punkt::Update()
extern void sprawdz_punkt_Update_m26FEAD7C1FD05DF5746C956F0175FB8CF0BAD5C4 (void);
// 0x00000308 System.Void sprawdz_punkt::.ctor()
extern void sprawdz_punkt__ctor_mCEAFFEAD154CDA5483126BDF98A036A0D24D139F (void);
// 0x00000309 System.Void szerokosc_suwaka::Start()
extern void szerokosc_suwaka_Start_mFB92E347350CDD5DCB3B1378CD03160E76627C41 (void);
// 0x0000030A System.Collections.IEnumerator szerokosc_suwaka::Dostosuj_suwak()
extern void szerokosc_suwaka_Dostosuj_suwak_m0EE7615816CE51C56C3D09CD5DEC91386A46A7D7 (void);
// 0x0000030B System.Void szerokosc_suwaka::Update()
extern void szerokosc_suwaka_Update_m363FA591CB89464773BA485449C722D7067E2033 (void);
// 0x0000030C System.Void szerokosc_suwaka::.ctor()
extern void szerokosc_suwaka__ctor_m5E5D127607852BA7C3C2B098DCCBC4A930292B43 (void);
// 0x0000030D System.Void szerokosc_suwaka/<Dostosuj_suwak>d__3::.ctor(System.Int32)
extern void U3CDostosuj_suwakU3Ed__3__ctor_m8D6BAFD5A6E536E4F0855C517C9CD20223F0B4A9 (void);
// 0x0000030E System.Void szerokosc_suwaka/<Dostosuj_suwak>d__3::System.IDisposable.Dispose()
extern void U3CDostosuj_suwakU3Ed__3_System_IDisposable_Dispose_mC854D49ADDE4FABEECC7BDB10D0380667921A811 (void);
// 0x0000030F System.Boolean szerokosc_suwaka/<Dostosuj_suwak>d__3::MoveNext()
extern void U3CDostosuj_suwakU3Ed__3_MoveNext_m0E9E366EEF5B128B00DBC5F3BE0FE191634FEADE (void);
// 0x00000310 System.Object szerokosc_suwaka/<Dostosuj_suwak>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDostosuj_suwakU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA913F87A0A2AB2C76624A3DA597D2BF71360522 (void);
// 0x00000311 System.Void szerokosc_suwaka/<Dostosuj_suwak>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDostosuj_suwakU3Ed__3_System_Collections_IEnumerator_Reset_mB729429FEB48F3309EA3A9AA74CF4F32134B7F59 (void);
// 0x00000312 System.Object szerokosc_suwaka/<Dostosuj_suwak>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDostosuj_suwakU3Ed__3_System_Collections_IEnumerator_get_Current_m7DC24AA604A916BCDDE34757A97AB64B4DC8A7D8 (void);
// 0x00000313 System.Void TakeAScreenShot::Awake()
extern void TakeAScreenShot_Awake_m1088F0723B694ECB0648DC63A5B9C97B0801ABE5 (void);
// 0x00000314 System.Void TakeAScreenShot::OnPostRender()
extern void TakeAScreenShot_OnPostRender_mCC9AF65CAB1F5F725A88636B870C4510324895D5 (void);
// 0x00000315 System.Void TakeAScreenShot::TakeScreen(System.Int32,System.Int32)
extern void TakeAScreenShot_TakeScreen_m7420B106EAD5A17087DF534315597E256CE73186 (void);
// 0x00000316 System.Void TakeAScreenShot::TakeScreen_static(System.Int32,System.Int32)
extern void TakeAScreenShot_TakeScreen_static_m1E6557581B62CAA4A2B1DC937A014A6BB8C46DAC (void);
// 0x00000317 System.Void TakeAScreenShot::.ctor()
extern void TakeAScreenShot__ctor_m9A7A4221124EFB3A34094B2F0D319D0AE958C0F0 (void);
// 0x00000318 System.Void Touch_location::.ctor(System.Int32,UnityEngine.GameObject)
extern void Touch_location__ctor_m83019BE5FC11ACA4CC0832ECA5719AE8E7490E4C (void);
// 0x00000319 System.Void traktor_dym::Start()
extern void traktor_dym_Start_m2D86E19283C223FAE9A083293FA64F0F38B6163F (void);
// 0x0000031A System.Void traktor_dym::Update()
extern void traktor_dym_Update_m51BF7D024046ADDF8235AE6266A305FD2B53FBE6 (void);
// 0x0000031B System.Void traktor_dym::Pusci_dymek()
extern void traktor_dym_Pusci_dymek_mADF10ADA27BE81FD43D558241F2E5119A3084603 (void);
// 0x0000031C System.Void traktor_dym::.ctor()
extern void traktor_dym__ctor_mEF6E18F4BFA24D4DE8E85CC67A5FD0B4588A74F8 (void);
// 0x0000031D System.Void Transition::Start()
extern void Transition_Start_m56FB6AAED27A2560AA91288897726B384F7E4F96 (void);
// 0x0000031E System.Void Transition::Update()
extern void Transition_Update_m92BA0BCCA1DEB843C9840CFB87F43A9C59A9CEEA (void);
// 0x0000031F System.Void Transition::.ctor()
extern void Transition__ctor_m641CBEB2CAC3DD257BAC609DE294488F9BF64768 (void);
// 0x00000320 System.Void wroc::Powrot()
extern void wroc_Powrot_m2B4B71ACA21995EEA4DDB2B922B5A7B16E6D5168 (void);
// 0x00000321 System.Collections.IEnumerator wroc::Odlicz()
extern void wroc_Odlicz_mEBE6125750C8C1400ABD2283EE5031CF0506B293 (void);
// 0x00000322 System.Void wroc::.ctor()
extern void wroc__ctor_m92B904FB29036A448A2D9647BAF58EB50D9D94AD (void);
// 0x00000323 System.Void wroc/<Odlicz>d__1::.ctor(System.Int32)
extern void U3COdliczU3Ed__1__ctor_m600C88D9950FE2F3214CC0EAE4221167E256D2A4 (void);
// 0x00000324 System.Void wroc/<Odlicz>d__1::System.IDisposable.Dispose()
extern void U3COdliczU3Ed__1_System_IDisposable_Dispose_m3619036035C71A8688B8C4FCCCEA57A58100EE1B (void);
// 0x00000325 System.Boolean wroc/<Odlicz>d__1::MoveNext()
extern void U3COdliczU3Ed__1_MoveNext_m32E60ECFB657D878E3674ABAECAADB6A7974CA2A (void);
// 0x00000326 System.Object wroc/<Odlicz>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COdliczU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC64AE7DD542B6D7E938689FE085A3AC04FD71D54 (void);
// 0x00000327 System.Void wroc/<Odlicz>d__1::System.Collections.IEnumerator.Reset()
extern void U3COdliczU3Ed__1_System_Collections_IEnumerator_Reset_mB5218F820D5771067492D69800AA2DB828E5CA42 (void);
// 0x00000328 System.Object wroc/<Odlicz>d__1::System.Collections.IEnumerator.get_Current()
extern void U3COdliczU3Ed__1_System_Collections_IEnumerator_get_Current_m6F8ACE4BD09D4FE82706F9B06A7D8A5E5AE1A2AE (void);
// 0x00000329 System.Void wylacz_cien::Start()
extern void wylacz_cien_Start_mBC4A433CE43FAA08A01E22FD08BFBEFAE59171A4 (void);
// 0x0000032A System.Void wylacz_cien::Update()
extern void wylacz_cien_Update_m3D836B492D6BC54ADA1596920283DDBFF0B536D8 (void);
// 0x0000032B System.Void wylacz_cien::Graj_cien()
extern void wylacz_cien_Graj_cien_mA07B2A1F074FCF3126D17CB6F6A59835A6531B1A (void);
// 0x0000032C System.Void wylacz_cien::Wylacz_cien()
extern void wylacz_cien_Wylacz_cien_m822FA9347DB569B1D0BDA27DF48DD33FF4EC75A3 (void);
// 0x0000032D System.Void wylacz_cien::.ctor()
extern void wylacz_cien__ctor_m93B9674CC40322D5B1D588113EF9A0A10D2AA693 (void);
// 0x0000032E System.Void wylacz_lub_wlacz_buttony::Start()
extern void wylacz_lub_wlacz_buttony_Start_mDF2A5BEC7B73322FC71553215B546953C68BA6D5 (void);
// 0x0000032F System.Void wylacz_lub_wlacz_buttony::Update()
extern void wylacz_lub_wlacz_buttony_Update_m5FE20971DF75EDF2583445E460BD6EF23AEBC4A8 (void);
// 0x00000330 System.Void wylacz_lub_wlacz_buttony::Wyl_wla_button()
extern void wylacz_lub_wlacz_buttony_Wyl_wla_button_mCCBA61197A59E9D88F739A49BCF299E447D18AFB (void);
// 0x00000331 System.Void wylacz_lub_wlacz_buttony::.ctor()
extern void wylacz_lub_wlacz_buttony__ctor_m01EAAD91FD4F4F9DD95FA0AEAB92A8B81315DC35 (void);
// 0x00000332 System.Void zagraj::Start()
extern void zagraj_Start_m926BD229FA0C673DAABCA9AAC6A3D23E4642C36B (void);
// 0x00000333 System.Void zagraj::Update()
extern void zagraj_Update_mBD83BA642B11DF0E42604311E816A0FE160E8CC6 (void);
// 0x00000334 System.Void zagraj::GrajIdle()
extern void zagraj_GrajIdle_m5007A58C4AA2EAEA6A6375FE836376D3B0282058 (void);
// 0x00000335 System.Void zagraj::.ctor()
extern void zagraj__ctor_mB311BEDFD4C9B0E61A573A62D0D7AB7AAB27BC8F (void);
// 0x00000336 System.Void zejscie::Start()
extern void zejscie_Start_mFAEF3C6E9287809AE0EEE31BAADC43AF21C21A03 (void);
// 0x00000337 System.Void zejscie::Update()
extern void zejscie_Update_m1AA297BC8207A333DA8E593064142D24D42CB7E6 (void);
// 0x00000338 System.Void zejscie::.ctor()
extern void zejscie__ctor_m14C4081DCE699A86E6D8BE244DEDC68931FD14CB (void);
// 0x00000339 System.Void ZmianaPrzycisku::Start()
extern void ZmianaPrzycisku_Start_m86F749E809A7F35F2CAEC85A3C5CB754E2D8AEFE (void);
// 0x0000033A System.Void ZmianaPrzycisku::Update()
extern void ZmianaPrzycisku_Update_m8199F06F5CCECF695ADC21708E07B526EAFEC172 (void);
// 0x0000033B System.Void ZmianaPrzycisku::Zmiana()
extern void ZmianaPrzycisku_Zmiana_mA60241BFF61ED5568C80A35E8F091BA362B786D5 (void);
// 0x0000033C System.Collections.IEnumerator ZmianaPrzycisku::Odliczanie()
extern void ZmianaPrzycisku_Odliczanie_mB2F8305180AA582DBBD72BE95D11DFAFEF83316F (void);
// 0x0000033D System.Void ZmianaPrzycisku::.ctor()
extern void ZmianaPrzycisku__ctor_m671B7C0EDFA1631DAE485FE6058E511C6153ED3D (void);
// 0x0000033E System.Void ZmianaPrzycisku/<Odliczanie>d__11::.ctor(System.Int32)
extern void U3COdliczanieU3Ed__11__ctor_mC0D9EA1295F165FA2D1E44723C2DBE8F8FFBA706 (void);
// 0x0000033F System.Void ZmianaPrzycisku/<Odliczanie>d__11::System.IDisposable.Dispose()
extern void U3COdliczanieU3Ed__11_System_IDisposable_Dispose_m732660A62B8168391623594CD82C6853E47D70DA (void);
// 0x00000340 System.Boolean ZmianaPrzycisku/<Odliczanie>d__11::MoveNext()
extern void U3COdliczanieU3Ed__11_MoveNext_m4F5A90664DE8472A1F70D254ADA4D35F9B5CBA3B (void);
// 0x00000341 System.Object ZmianaPrzycisku/<Odliczanie>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COdliczanieU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m999F90ED8624C784874695892F05DB704F545185 (void);
// 0x00000342 System.Void ZmianaPrzycisku/<Odliczanie>d__11::System.Collections.IEnumerator.Reset()
extern void U3COdliczanieU3Ed__11_System_Collections_IEnumerator_Reset_m393805EB9CB81ED0838535B600510AE4FE0B9901 (void);
// 0x00000343 System.Object ZmianaPrzycisku/<Odliczanie>d__11::System.Collections.IEnumerator.get_Current()
extern void U3COdliczanieU3Ed__11_System_Collections_IEnumerator_get_Current_mFE9A283E8255893D062F5EE63F1DC34DC0396106 (void);
// 0x00000344 System.Void Znikani::Start()
extern void Znikani_Start_m00812A56028C48163DB66EA5B27C2B2CA267363F (void);
// 0x00000345 System.Void Znikani::Update()
extern void Znikani_Update_mAB317D3CC7FE832254D141177CF3A24BF6951AE3 (void);
// 0x00000346 System.Collections.IEnumerator Znikani::przezroczystosc()
extern void Znikani_przezroczystosc_m9E8D40473E6E36BFB114F54CF725EFF7C6A867B4 (void);
// 0x00000347 System.Void Znikani::.ctor()
extern void Znikani__ctor_m269A8A059734AF5A9E0D85F9A90DAE867497530F (void);
// 0x00000348 System.Void Znikani/<przezroczystosc>d__2::.ctor(System.Int32)
extern void U3CprzezroczystoscU3Ed__2__ctor_m32ECD20462DCE0CD1D15143A43014DBE7B5C5025 (void);
// 0x00000349 System.Void Znikani/<przezroczystosc>d__2::System.IDisposable.Dispose()
extern void U3CprzezroczystoscU3Ed__2_System_IDisposable_Dispose_m81EFC0D8E0D3A18F78DB5EFFB0D92F3F7A629401 (void);
// 0x0000034A System.Boolean Znikani/<przezroczystosc>d__2::MoveNext()
extern void U3CprzezroczystoscU3Ed__2_MoveNext_mFFFAB37772D9E246F719E39E12042E02D439F556 (void);
// 0x0000034B System.Object Znikani/<przezroczystosc>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CprzezroczystoscU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7117A9D0D326CD4CE242BA6C218D266681663C1 (void);
// 0x0000034C System.Void Znikani/<przezroczystosc>d__2::System.Collections.IEnumerator.Reset()
extern void U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_Reset_m795D7A922600D17DE3132D0316F0FE2C0239D85F (void);
// 0x0000034D System.Object Znikani/<przezroczystosc>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_get_Current_m1F4EB8DDE83437BC7B277C7092A162373C3B5866 (void);
// 0x0000034E System.Void Znikanie::Start()
extern void Znikanie_Start_mB2FB1E65F42A75B815B7BB2218CEC11F76A7EFA1 (void);
// 0x0000034F System.Void Znikanie::Update()
extern void Znikanie_Update_m6E710580AEDF97228D6422A41F125ADFC08467B6 (void);
// 0x00000350 System.Collections.IEnumerator Znikanie::przezroczystosc()
extern void Znikanie_przezroczystosc_m6B297CAC7142D53A5A12D842744ADC2C02217915 (void);
// 0x00000351 System.Void Znikanie::.ctor()
extern void Znikanie__ctor_m2FB112A57A993C9B6D1716DB55810E50EA461DA6 (void);
// 0x00000352 System.Void Znikanie/<przezroczystosc>d__2::.ctor(System.Int32)
extern void U3CprzezroczystoscU3Ed__2__ctor_mF16A534B6C0C6734C19A077669F63BBBEB8229D6 (void);
// 0x00000353 System.Void Znikanie/<przezroczystosc>d__2::System.IDisposable.Dispose()
extern void U3CprzezroczystoscU3Ed__2_System_IDisposable_Dispose_m5748E576F7C0979A909503A27B6BFB22F9751627 (void);
// 0x00000354 System.Boolean Znikanie/<przezroczystosc>d__2::MoveNext()
extern void U3CprzezroczystoscU3Ed__2_MoveNext_m2CBF728B027D1FD0B6BFCF004BF81681480ACFCE (void);
// 0x00000355 System.Object Znikanie/<przezroczystosc>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CprzezroczystoscU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19B571A6CAB62B237348DF9C682ED78CE0767D68 (void);
// 0x00000356 System.Void Znikanie/<przezroczystosc>d__2::System.Collections.IEnumerator.Reset()
extern void U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_Reset_m2C02BD0B6D460CC5352DD7D4AD35DA72C6945E16 (void);
// 0x00000357 System.Object Znikanie/<przezroczystosc>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_get_Current_mF87BC6D8C828846D82C293637B08F30E33D289F6 (void);
// 0x00000358 System.Void zwierze_spi::Start()
extern void zwierze_spi_Start_m4DA5D1D0EE4393BE70F0EB940B53676B26A6EEBB (void);
// 0x00000359 System.Void zwierze_spi::Update()
extern void zwierze_spi_Update_m3B30539EA9C931332E388A9BCF40B99120A2F592 (void);
// 0x0000035A System.Void zwierze_spi::Spanie()
extern void zwierze_spi_Spanie_m26A0CB3C821699D960B9F499A7F0EC04062A9A70 (void);
// 0x0000035B System.Void zwierze_spi::sprawdz_emiter()
extern void zwierze_spi_sprawdz_emiter_mB6278DBAD89A731EDB1EFBADD6F12C78BCC1DB52 (void);
// 0x0000035C System.Void zwierze_spi::Wybudzanie()
extern void zwierze_spi_Wybudzanie_m95E6A8F5FF5E664C5AF174538CDE35FE7E4E48DA (void);
// 0x0000035D System.Void zwierze_spi::Chrapanie()
extern void zwierze_spi_Chrapanie_m9B5980A584118DCC680D9899A0DED142C5CB4F19 (void);
// 0x0000035E System.Void zwierze_spi::Sen()
extern void zwierze_spi_Sen_mE7778609E47C0CDE6E4DE0CD7C0F4FB88C335CE4 (void);
// 0x0000035F System.Void zwierze_spi::WybudzanieDlaKaczek()
extern void zwierze_spi_WybudzanieDlaKaczek_m5A73F354BA8AE354AF57D269711DC66B8019E3B4 (void);
// 0x00000360 System.Void zwierze_spi::.ctor()
extern void zwierze_spi__ctor_mCD89971D947C414E2D71FA0ED65C59D0BD6CB8A8 (void);
// 0x00000361 System.Void Zyroskop::Start()
extern void Zyroskop_Start_m3A45A7CC7C643FE714DD8F454C486C00FEEB8B42 (void);
// 0x00000362 System.Void Zyroskop::Update()
extern void Zyroskop_Update_m66FDA0B3B2B0463AF0584FE59483ADF28E3599FE (void);
// 0x00000363 System.Void Zyroskop::.ctor()
extern void Zyroskop__ctor_m3BF6F0A20643F1888337252F2B77967AD93DE9BF (void);
// 0x00000364 System.Void Zzzzz::Start()
extern void Zzzzz_Start_m13CAE368DCB57CCD8E5654C4E4C89B2631D63C58 (void);
// 0x00000365 System.Void Zzzzz::Update()
extern void Zzzzz_Update_m388D91D4AE2D641BFBEDEE001B72D93EC5C25E3F (void);
// 0x00000366 System.Void Zzzzz::Stworz_Zzzz()
extern void Zzzzz_Stworz_Zzzz_m156AEF224519D0703F46B8541CF3FE540193521B (void);
// 0x00000367 System.Void Zzzzz::stworz_zzz_dla_swinki()
extern void Zzzzz_stworz_zzz_dla_swinki_mE73AB85C4ECFE8A731646EA3F3889D5E5FCED3CF (void);
// 0x00000368 System.Void Zzzzz::Przerwij_zzzz(UnityEngine.GameObject)
extern void Zzzzz_Przerwij_zzzz_mE4416E25C6031A1815D0325E9511F862A3B44FC8 (void);
// 0x00000369 System.Void Zzzzz::Przerwij_zzzzV2()
extern void Zzzzz_Przerwij_zzzzV2_m6B66158B641F875FC8FC3FAFE657A907E90F8DCA (void);
// 0x0000036A System.Void Zzzzz::.ctor()
extern void Zzzzz__ctor_mF7376CE734839F2188C23ECA4715E6F1D66EAD25 (void);
// 0x0000036B System.Void Zzzz_dla_kaczek::Start()
extern void Zzzz_dla_kaczek_Start_m1A90BF0F56C205F71254280D4E281C056894F50E (void);
// 0x0000036C System.Void Zzzz_dla_kaczek::Update()
extern void Zzzz_dla_kaczek_Update_mA9AC9C39863330D378949FA3722F2F0502B22B60 (void);
// 0x0000036D System.Void Zzzz_dla_kaczek::Stworz_Zzzz_dla_kaczki_malej()
extern void Zzzz_dla_kaczek_Stworz_Zzzz_dla_kaczki_malej_mAA21425C3D9A9C9FA2B29337EBE4101A9D4E1CAA (void);
// 0x0000036E System.Void Zzzz_dla_kaczek::Stworz_zzzzz_dla_kaczki_duzej()
extern void Zzzz_dla_kaczek_Stworz_zzzzz_dla_kaczki_duzej_m83119E279C12514CFF01DFD04747A07109A0AB61 (void);
// 0x0000036F System.Void Zzzz_dla_kaczek::Przerwij_zzzz_kaczki_malej()
extern void Zzzz_dla_kaczek_Przerwij_zzzz_kaczki_malej_m5195871D99E201AF06DC8064C2B42DD59BCC5CD3 (void);
// 0x00000370 System.Void Zzzz_dla_kaczek::Przerwij_zzzz_kaczki_duzej()
extern void Zzzz_dla_kaczek_Przerwij_zzzz_kaczki_duzej_m28499A8F7A9DEDB043460B20A6196EDE6F18A8F7 (void);
// 0x00000371 System.Void Zzzz_dla_kaczek::.ctor()
extern void Zzzz_dla_kaczek__ctor_m6BF57E2DE7823B8858E476331E868A2173AEEABD (void);
// 0x00000372 System.Void dzwieki_idle_fixed::Start()
extern void dzwieki_idle_fixed_Start_m179CC4E38AFEE78FE8719FEDF1B587D291E69FE2 (void);
// 0x00000373 System.Void dzwieki_idle_fixed::Update()
extern void dzwieki_idle_fixed_Update_m44E5F0F823005B509BB4F8EAE7839C9A3B7C9126 (void);
// 0x00000374 System.Void dzwieki_idle_fixed::Odtworz_idle()
extern void dzwieki_idle_fixed_Odtworz_idle_mD3736D2144695360B56A0A0CDF5F4C12F9543816 (void);
// 0x00000375 System.Void dzwieki_idle_fixed::OdtworzTrzaski()
extern void dzwieki_idle_fixed_OdtworzTrzaski_m30683568EC653A8D5D4F81E4DA2EB898AB6AFC13 (void);
// 0x00000376 System.Void dzwieki_idle_fixed::Wyldziw()
extern void dzwieki_idle_fixed_Wyldziw_m5CD57401F20FFE4BF3D37FA3C0967EC449F4BEC1 (void);
// 0x00000377 System.Void dzwieki_idle_fixed::WlaDziw()
extern void dzwieki_idle_fixed_WlaDziw_m5EBB05B1BDF6CA6708E815BD18DF286BFB654EA4 (void);
// 0x00000378 System.Void dzwieki_idle_fixed::.ctor()
extern void dzwieki_idle_fixed__ctor_mE7E61E42573BE0BA2E1AD21ECC796DFD5BE1CF1E (void);
// 0x00000379 System.Void odtworzAnimacje_fixed::Update()
extern void odtworzAnimacje_fixed_Update_m31DF34FCA105107F084559A232F89193EEA64427 (void);
// 0x0000037A System.Void odtworzAnimacje_fixed::Animacja()
extern void odtworzAnimacje_fixed_Animacja_m2E4D8C71E7FDB943E81BEC919042B4BF9D233D6A (void);
// 0x0000037B System.Collections.IEnumerator odtworzAnimacje_fixed::powrot_do_idle()
extern void odtworzAnimacje_fixed_powrot_do_idle_m52B32050EE0D05096576178B7414852C5BC9ED5C (void);
// 0x0000037C System.Void odtworzAnimacje_fixed::.ctor()
extern void odtworzAnimacje_fixed__ctor_mB821E52581215DF9E46E50E491BAF9C86B38A911 (void);
// 0x0000037D System.Void odtworzAnimacje_fixed/<powrot_do_idle>d__9::.ctor(System.Int32)
extern void U3Cpowrot_do_idleU3Ed__9__ctor_m73BEA6E206BF878AC9CBBB729D0CDCD7256D6C8C (void);
// 0x0000037E System.Void odtworzAnimacje_fixed/<powrot_do_idle>d__9::System.IDisposable.Dispose()
extern void U3Cpowrot_do_idleU3Ed__9_System_IDisposable_Dispose_mC004C3FCB7F217A052262DE89974A8EE80DBC965 (void);
// 0x0000037F System.Boolean odtworzAnimacje_fixed/<powrot_do_idle>d__9::MoveNext()
extern void U3Cpowrot_do_idleU3Ed__9_MoveNext_m42DF93AAB8620133118E369D32B2F0A2D812E09B (void);
// 0x00000380 System.Object odtworzAnimacje_fixed/<powrot_do_idle>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cpowrot_do_idleU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4CBC54993A77BAB390622647BCBB591BC5CD62DC (void);
// 0x00000381 System.Void odtworzAnimacje_fixed/<powrot_do_idle>d__9::System.Collections.IEnumerator.Reset()
extern void U3Cpowrot_do_idleU3Ed__9_System_Collections_IEnumerator_Reset_m8E0935BF14395A32D56834F86E92216CF3DFEEBB (void);
// 0x00000382 System.Object odtworzAnimacje_fixed/<powrot_do_idle>d__9::System.Collections.IEnumerator.get_Current()
extern void U3Cpowrot_do_idleU3Ed__9_System_Collections_IEnumerator_get_Current_mEF3553D76545FA212736C05978FD01400467E9B4 (void);
// 0x00000383 System.Void PlaySoundOnClick_fixed::Start()
extern void PlaySoundOnClick_fixed_Start_mC1E2C9CAB22E8253684B699C290527A0590A0234 (void);
// 0x00000384 System.Void PlaySoundOnClick_fixed::Update()
extern void PlaySoundOnClick_fixed_Update_mCC2853910E12605BDE866192D492C03656186699 (void);
// 0x00000385 System.Void PlaySoundOnClick_fixed::PlaySound(System.String)
extern void PlaySoundOnClick_fixed_PlaySound_mECE51CDE6ED852F6B8601572F02C214C74489B98 (void);
// 0x00000386 System.Void PlaySoundOnClick_fixed::PrzyciskPilota_lub_back_btn()
extern void PlaySoundOnClick_fixed_PrzyciskPilota_lub_back_btn_mCCF121997B1738C89DC4B2D6A379865AC609272C (void);
// 0x00000387 System.Void PlaySoundOnClick_fixed::.ctor()
extern void PlaySoundOnClick_fixed__ctor_m906F3AE27CE88094A15BB8D462834C1F2939DC98 (void);
// 0x00000388 System.Void Spawnuj_chmury::Start()
extern void Spawnuj_chmury_Start_m3EA55A67E6A349C6A7F5A9F474C2EEFF92E813F6 (void);
// 0x00000389 System.Void Spawnuj_chmury::Update()
extern void Spawnuj_chmury_Update_mF1034A90847EA30876ED839DC0805F7D109F00C3 (void);
// 0x0000038A System.Collections.IEnumerator Spawnuj_chmury::Spawnuj_chmurki()
extern void Spawnuj_chmury_Spawnuj_chmurki_m8B44BD0E14F18446C1BA5BB3B0D9B070875D8614 (void);
// 0x0000038B System.Void Spawnuj_chmury::Zablokuj_chwyt(UnityEngine.GameObject)
extern void Spawnuj_chmury_Zablokuj_chwyt_mAEB6B79C210C23D7EEBFF4B53DC0047945343A7D (void);
// 0x0000038C System.Void Spawnuj_chmury::Odblokuj_chwyt()
extern void Spawnuj_chmury_Odblokuj_chwyt_m6B232A7A7160FC57094387480DCA3464808E87D4 (void);
// 0x0000038D System.Void Spawnuj_chmury::ZniszczonoChmurke(UnityEngine.GameObject)
extern void Spawnuj_chmury_ZniszczonoChmurke_mFEE65A7FE4389EE964D97074FA3B75CF9620A322 (void);
// 0x0000038E System.Void Spawnuj_chmury::.ctor()
extern void Spawnuj_chmury__ctor_mE688EA3D0D04DB7D407507027491D982C7924AFE (void);
// 0x0000038F System.Void Spawnuj_chmury/<Spawnuj_chmurki>d__7::.ctor(System.Int32)
extern void U3CSpawnuj_chmurkiU3Ed__7__ctor_m81CB08DB0CBE3D56B532C1C354A71ED85CD8B091 (void);
// 0x00000390 System.Void Spawnuj_chmury/<Spawnuj_chmurki>d__7::System.IDisposable.Dispose()
extern void U3CSpawnuj_chmurkiU3Ed__7_System_IDisposable_Dispose_mF0C85F588F4EA061591136B39E913032C0FE7242 (void);
// 0x00000391 System.Boolean Spawnuj_chmury/<Spawnuj_chmurki>d__7::MoveNext()
extern void U3CSpawnuj_chmurkiU3Ed__7_MoveNext_mAEF2ABB53FC502E877DABFD530D494632CAD6340 (void);
// 0x00000392 System.Object Spawnuj_chmury/<Spawnuj_chmurki>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnuj_chmurkiU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E3803F6CC7750BBACAC301463BB4AD1CC72AFBC (void);
// 0x00000393 System.Void Spawnuj_chmury/<Spawnuj_chmurki>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSpawnuj_chmurkiU3Ed__7_System_Collections_IEnumerator_Reset_m28243D9B29400E893CF1DD9E9F76C2EFB1182AFB (void);
// 0x00000394 System.Object Spawnuj_chmury/<Spawnuj_chmurki>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnuj_chmurkiU3Ed__7_System_Collections_IEnumerator_get_Current_m5B9719A6B90956FAD3BF7BFA42E471657D197018 (void);
// 0x00000395 System.Void sprawdz_punkt_gumka::Start()
extern void sprawdz_punkt_gumka_Start_mD953E440045AAF24A3C86040EFEF071F72ED2297 (void);
// 0x00000396 System.Void sprawdz_punkt_gumka::Update()
extern void sprawdz_punkt_gumka_Update_m41FED98C40E01051304A5930E974B9775969E1F3 (void);
// 0x00000397 System.Void sprawdz_punkt_gumka::.ctor()
extern void sprawdz_punkt_gumka__ctor_mEBC08CC2E3FC517B543232ECA28DE5D2A6F61085 (void);
// 0x00000398 System.Void Sprawdz_punkt_improve::Start()
extern void Sprawdz_punkt_improve_Start_mD475491C01A92EB1D25BD915B245499F6AE3A41F (void);
// 0x00000399 System.Void Sprawdz_punkt_improve::Update()
extern void Sprawdz_punkt_improve_Update_m53EDBC51C7EFB0E42382198DD73631947524D96C (void);
// 0x0000039A System.Void Sprawdz_punkt_improve::WritePixels(UnityEngine.Vector2)
extern void Sprawdz_punkt_improve_WritePixels_mFA6DA5C64E21D227A8F755CDC0F9343B656623F1 (void);
// 0x0000039B System.Collections.Generic.List`1<UnityEngine.Vector2> Sprawdz_punkt_improve::GetLinearPositions(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void Sprawdz_punkt_improve_GetLinearPositions_m504751DA30F8124B98CAB98BDB22EAF13050C731 (void);
// 0x0000039C System.Collections.Generic.List`1<UnityEngine.Vector2> Sprawdz_punkt_improve::GetNeighbouringPixels(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void Sprawdz_punkt_improve_GetNeighbouringPixels_mCABAED56FAA522F8C79018752C0D1399858EC0FF (void);
// 0x0000039D System.Void Sprawdz_punkt_improve::.ctor()
extern void Sprawdz_punkt_improve__ctor_m8CC57DD4458EDAC8C6022C5F5D3C5765D99C871E (void);
// 0x0000039E System.Void stuknij::Start()
extern void stuknij_Start_m5D73E59C1FC20BEC5D239A38684A890AFF436C3B (void);
// 0x0000039F System.Void stuknij::Update()
extern void stuknij_Update_m4374848631C43AD31D1356DCF41856755737A367 (void);
// 0x000003A0 System.Void stuknij::Zgniec()
extern void stuknij_Zgniec_m61A0F033FF57E9E8A45BA83B86DC119ABCFE86C4 (void);
// 0x000003A1 System.Collections.IEnumerator stuknij::Stukniecie()
extern void stuknij_Stukniecie_mA8438645708C849BF62496E9709B2A0344477A64 (void);
// 0x000003A2 System.Void stuknij::.ctor()
extern void stuknij__ctor_mBE3EDF9FD88A2D1862C0C07DE01CBA874C9A516C (void);
// 0x000003A3 System.Void stuknij/<Stukniecie>d__8::.ctor(System.Int32)
extern void U3CStukniecieU3Ed__8__ctor_mB548264EB2695355A669C0BA2CDAB9A68517776E (void);
// 0x000003A4 System.Void stuknij/<Stukniecie>d__8::System.IDisposable.Dispose()
extern void U3CStukniecieU3Ed__8_System_IDisposable_Dispose_mB61818D70BFDAD55CAA82570C29F41600C28EABE (void);
// 0x000003A5 System.Boolean stuknij/<Stukniecie>d__8::MoveNext()
extern void U3CStukniecieU3Ed__8_MoveNext_mE46E4617B20C9D03745F987E2685F6C0066FF83C (void);
// 0x000003A6 System.Object stuknij/<Stukniecie>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStukniecieU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47C7829991E6D9F0F8330780BDF1EEC15A617534 (void);
// 0x000003A7 System.Void stuknij/<Stukniecie>d__8::System.Collections.IEnumerator.Reset()
extern void U3CStukniecieU3Ed__8_System_Collections_IEnumerator_Reset_m6501C7C0C12337CE0A8CB145757F66C50597B525 (void);
// 0x000003A8 System.Object stuknij/<Stukniecie>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CStukniecieU3Ed__8_System_Collections_IEnumerator_get_Current_mCFD297B22EAA80D0C44C6FA251FA1C88156F278B (void);
// 0x000003A9 System.Void swinka_script::Start()
extern void swinka_script_Start_mA70C1E2B94357CC3CCFEE5EB5A12A38825A46B91 (void);
// 0x000003AA System.Void swinka_script::Update()
extern void swinka_script_Update_m7EDF8E5D41348BBB7A38FB0AD2493D81040E7925 (void);
// 0x000003AB System.Void swinka_script::Wylacz_swinie()
extern void swinka_script_Wylacz_swinie_m394F26D3C6E629D079C378BFD4C62DB6FA4A5EA1 (void);
// 0x000003AC System.Void swinka_script::.ctor()
extern void swinka_script__ctor_mDBAD611E7FF09F11F1ECEAD5545190328A354FCD (void);
// 0x000003AD System.Void traktor_jezdzi::Start()
extern void traktor_jezdzi_Start_mDD20F518445B21915B77D7BB8534C01FE67C24D6 (void);
// 0x000003AE System.Void traktor_jezdzi::Update()
extern void traktor_jezdzi_Update_m11930AB708CE5653EFBA0ABE3774AD0C12F57E04 (void);
// 0x000003AF System.Void traktor_jezdzi::Traktorek_Dzwiek()
extern void traktor_jezdzi_Traktorek_Dzwiek_m1D0240067656723F147C02866FD36C2F3200AE34 (void);
// 0x000003B0 System.Void traktor_jezdzi::.ctor()
extern void traktor_jezdzi__ctor_m6BB21CA1DC9E503E0A2CF1907A76550AF49D4F57 (void);
// 0x000003B1 System.Void wroc_z_reklama::Start()
extern void wroc_z_reklama_Start_m05B041D05C372D967E2B00CD676518B77DDE8F62 (void);
// 0x000003B2 System.Void wroc_z_reklama::Update()
extern void wroc_z_reklama_Update_m07C231B4F117B822DB673F3DBC15C65FA9DB91F0 (void);
// 0x000003B3 System.Void wroc_z_reklama::Wroc_z_reklama()
extern void wroc_z_reklama_Wroc_z_reklama_mE80D53471FDF5B3ECF1A3E0CD632D0A45E027D3B (void);
// 0x000003B4 System.Collections.IEnumerator wroc_z_reklama::Powrot()
extern void wroc_z_reklama_Powrot_m9F27345F71A99573806678AADC9DD2260B29BADF (void);
// 0x000003B5 System.Void wroc_z_reklama::.ctor()
extern void wroc_z_reklama__ctor_mE35135F47CB8E75AF49C509DCCC21A61347004B8 (void);
// 0x000003B6 System.Void wroc_z_reklama/<Powrot>d__5::.ctor(System.Int32)
extern void U3CPowrotU3Ed__5__ctor_mC3EB9791D5BC6115D18C74C32B2025F0DC97219C (void);
// 0x000003B7 System.Void wroc_z_reklama/<Powrot>d__5::System.IDisposable.Dispose()
extern void U3CPowrotU3Ed__5_System_IDisposable_Dispose_m0C3C7E51BB55E222299EC9C15143BBD40CACC31C (void);
// 0x000003B8 System.Boolean wroc_z_reklama/<Powrot>d__5::MoveNext()
extern void U3CPowrotU3Ed__5_MoveNext_m58446E1C88D3AC84DCEDB5CF4C2CFB9346A2FCA4 (void);
// 0x000003B9 System.Object wroc_z_reklama/<Powrot>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPowrotU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF77C938FB3D991CF41694FD5C2D1EBD323D16A0F (void);
// 0x000003BA System.Void wroc_z_reklama/<Powrot>d__5::System.Collections.IEnumerator.Reset()
extern void U3CPowrotU3Ed__5_System_Collections_IEnumerator_Reset_mAC1F92D10DF7AEFBECFD3E4AF2BB5E2382B9813D (void);
// 0x000003BB System.Object wroc_z_reklama/<Powrot>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CPowrotU3Ed__5_System_Collections_IEnumerator_get_Current_m52FAD4421C3210F66E3F528C4BD787BDD7D5167B (void);
// 0x000003BC System.Void wylacz_animator::Start()
extern void wylacz_animator_Start_m4EE55120820BB556725A5E4BBF915475D54448F7 (void);
// 0x000003BD System.Void wylacz_animator::Update()
extern void wylacz_animator_Update_m0007AB1B35CE35AA66DE4FC965926C81FBE5474D (void);
// 0x000003BE System.Void wylacz_animator::Wylacz()
extern void wylacz_animator_Wylacz_mCC5B5B5E7167400CD500FA7867E67C91CFEA31D0 (void);
// 0x000003BF System.Void wylacz_animator::.ctor()
extern void wylacz_animator__ctor_m95083924EAAD4876A4740A34F67D76092F1BF527 (void);
// 0x000003C0 System.Void zwierze_spi_KaczkaDuza::Start()
extern void zwierze_spi_KaczkaDuza_Start_mFDDBA884ADE3697F0CF8A6BBDD33CB06056969CC (void);
// 0x000003C1 System.Void zwierze_spi_KaczkaDuza::Update()
extern void zwierze_spi_KaczkaDuza_Update_m8CA8F60D8FEA32FAB8AF783C65872A4D5D0FE8A6 (void);
// 0x000003C2 System.Void zwierze_spi_KaczkaDuza::SpanieDlaKaczki_Duzej()
extern void zwierze_spi_KaczkaDuza_SpanieDlaKaczki_Duzej_mDD3E15A9E4043912B1CA1D025D77C72D88E8C96D (void);
// 0x000003C3 System.Void zwierze_spi_KaczkaDuza::.ctor()
extern void zwierze_spi_KaczkaDuza__ctor_m030D96580EBCCDD6678D35511E1F139510D033E4 (void);
// 0x000003C4 System.Void zwierze_spi_kaczkaMala::Start()
extern void zwierze_spi_kaczkaMala_Start_mEE76B18893B824B7D45399E8CD2201F9D3B52DF9 (void);
// 0x000003C5 System.Void zwierze_spi_kaczkaMala::Update()
extern void zwierze_spi_kaczkaMala_Update_m9780C896CF0E59FA414790342F81B75FA3CBAAE8 (void);
// 0x000003C6 System.Void zwierze_spi_kaczkaMala::SpanieDlaKaczki_Malej()
extern void zwierze_spi_kaczkaMala_SpanieDlaKaczki_Malej_mC718E70B9AAAF2B38E35CC06C4963A7B99606E01 (void);
// 0x000003C7 System.Void zwierze_spi_kaczkaMala::.ctor()
extern void zwierze_spi_kaczkaMala__ctor_m8E00CE07B78A97DFCE81F5FB10F1CEAF74C9110E (void);
// 0x000003C8 System.Void zzz_turn_off::Start()
extern void zzz_turn_off_Start_m70350E2A9B70F4D1131159FD8353F4C127E00438 (void);
// 0x000003C9 System.Void zzz_turn_off::Update()
extern void zzz_turn_off_Update_m4C21DA7B54C8026AC0F09E5DDD6A1ACDB02F5E21 (void);
// 0x000003CA System.Void zzz_turn_off::Wylacz_zzz()
extern void zzz_turn_off_Wylacz_zzz_mBE14900961EF78575CAEBA4A4530973F85598658 (void);
// 0x000003CB System.Void zzz_turn_off::.ctor()
extern void zzz_turn_off__ctor_m3AA3157DC7C075DE406F28C2DB9C7EE2AAF078B3 (void);
// 0x000003CC System.Void CompleteProject.INAP::Start()
extern void INAP_Start_m458A93023033C82A65F4ABFEFD5AE4CFA7A827B5 (void);
// 0x000003CD System.Void CompleteProject.INAP::Update()
extern void INAP_Update_m172CFA4DD2CC4776952627D96AF11E9503005F93 (void);
// 0x000003CE System.Void CompleteProject.INAP::InitializePurchasing()
extern void INAP_InitializePurchasing_m5C36F0C1734D376E3FB23D308E2CF772F0CAD765 (void);
// 0x000003CF System.Void CompleteProject.INAP::AddInitializationCallback(System.Action`1<System.Boolean>)
extern void INAP_AddInitializationCallback_m30E33C6F21146CE6C849FB0996FFBA293E184B62 (void);
// 0x000003D0 System.Boolean CompleteProject.INAP::IsInitialized()
extern void INAP_IsInitialized_mA1AFA91DBE86C5F14B51201A5C084560B390D113 (void);
// 0x000003D1 System.Void CompleteProject.INAP::BuyConsumable()
extern void INAP_BuyConsumable_m8D51D13539E23862ED9FF68D4AC17FAFD18C8B56 (void);
// 0x000003D2 System.Void CompleteProject.INAP::BuyNonConsumable()
extern void INAP_BuyNonConsumable_m01D7159CC2FA6CD3D37E782C4E3B35CCD00EBAFE (void);
// 0x000003D3 System.Void CompleteProject.INAP::BuySubscription()
extern void INAP_BuySubscription_m51CF307D523A9EC2722FE274E850F2E65D831F5B (void);
// 0x000003D4 System.Void CompleteProject.INAP::BuyProductID(System.String)
extern void INAP_BuyProductID_m55F570790AAB86E1F6463241106051D13076131C (void);
// 0x000003D5 System.Void CompleteProject.INAP::RestorePurchases()
extern void INAP_RestorePurchases_mEF0D5E04B4B79F12ECB62734B1705E7DC18AB30C (void);
// 0x000003D6 System.Void CompleteProject.INAP::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void INAP_OnInitialized_mB02D4AEB2132E4F9CC24D1DCC806CA1E540409EB (void);
// 0x000003D7 System.Void CompleteProject.INAP::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void INAP_OnInitializeFailed_mD39D64E543F58C6133450338066AB8224E046245 (void);
// 0x000003D8 UnityEngine.Purchasing.PurchaseProcessingResult CompleteProject.INAP::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void INAP_ProcessPurchase_m50F99EEE6107A71BBD3EA0F7BDF0B98A1D1794FD (void);
// 0x000003D9 System.Void CompleteProject.INAP::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void INAP_OnPurchaseFailed_m903C9AF7644D89E6EC9754C536AB3F279499DABB (void);
// 0x000003DA System.Void CompleteProject.INAP::LoadBack()
extern void INAP_LoadBack_mCD67344F7280D4B4336B136C410A0ACA5E3D0258 (void);
// 0x000003DB System.Void CompleteProject.INAP::LoadKupno()
extern void INAP_LoadKupno_m4FFF1DC22EBC6C335CD8DE489AAFCB9FBB914B43 (void);
// 0x000003DC System.Void CompleteProject.INAP::ResetPlayerPrefs()
extern void INAP_ResetPlayerPrefs_mB94478DAA9F239D04550897ACBFBABF4CD0AE85B (void);
// 0x000003DD System.Collections.IEnumerator CompleteProject.INAP::Back()
extern void INAP_Back_m8962B05F750B6F3BA6A75E964480917AAEC50703 (void);
// 0x000003DE System.Void CompleteProject.INAP::.ctor()
extern void INAP__ctor_mF5670AEAE40AFCF07412E7B9B61AB72E4C717873 (void);
// 0x000003DF System.Void CompleteProject.INAP::.cctor()
extern void INAP__cctor_mC8193CC81E3BB77BB2F0E94D9D4B4CB86857064E (void);
// 0x000003E0 System.Void CompleteProject.INAP/<>c::.cctor()
extern void U3CU3Ec__cctor_m9D744397B1D39386F10D0133E10E0D91B57A57E7 (void);
// 0x000003E1 System.Void CompleteProject.INAP/<>c::.ctor()
extern void U3CU3Ec__ctor_m3C7D04ED84B3BE4EC6C635EA0BA0318C81AC0792 (void);
// 0x000003E2 System.Void CompleteProject.INAP/<>c::<RestorePurchases>b__28_0(System.Boolean)
extern void U3CU3Ec_U3CRestorePurchasesU3Eb__28_0_m688EBF758C1CF5775A36008B6EFA7BD599899577 (void);
// 0x000003E3 System.Void CompleteProject.INAP/<Back>d__36::.ctor(System.Int32)
extern void U3CBackU3Ed__36__ctor_m983E4CDDC53D9212D9962D0DAF4376B81649CC9A (void);
// 0x000003E4 System.Void CompleteProject.INAP/<Back>d__36::System.IDisposable.Dispose()
extern void U3CBackU3Ed__36_System_IDisposable_Dispose_mA27D1ACEF0E1B01AFAAF29B75E441BDADF6912D1 (void);
// 0x000003E5 System.Boolean CompleteProject.INAP/<Back>d__36::MoveNext()
extern void U3CBackU3Ed__36_MoveNext_mD44802A47EF259FC9109C2B0505FBDD3C9F3ED54 (void);
// 0x000003E6 System.Object CompleteProject.INAP/<Back>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92249BBBE2458A0013DA464BCB108D8C713FD627 (void);
// 0x000003E7 System.Void CompleteProject.INAP/<Back>d__36::System.Collections.IEnumerator.Reset()
extern void U3CBackU3Ed__36_System_Collections_IEnumerator_Reset_mCB694A95BB856557716A9D2FF8BBDA3E31195708 (void);
// 0x000003E8 System.Object CompleteProject.INAP/<Back>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CBackU3Ed__36_System_Collections_IEnumerator_get_Current_mB447F3D30B6BA0BF98CE7D27A2C9CEE8C532C03B (void);
static Il2CppMethodPointer s_methodPointers[1000] = 
{
	AdMob_Start_m57EC0FB42DA5A695EC8E4C11E5D090AE6C5E0000,
	AdMob_RequestInterstitialAd_mDB3FA8710CD2A4EA3F422BC1AE5E7F25855E328A,
	AdMob_ShowInterstitialAd_m0229EB7E07C5AF7CC1AA1C33A905574DE266D795,
	AdMob_DestroyInterstitialAd_m416BA52C8B72CFD20E1D1969F2ACA722E2DE66F4,
	AdMob_HandleOnAdLoaded_m424636419BF546EA518C8A191BD47233B8FF5530,
	AdMob_HandleOnAdOpening_mCC6315FCBBDDE48EFA010374B3907DADF12232DE,
	AdMob_HandleOnAdClosed_mCC1BA65008B2F588BBCC275876EFF9BCC32DDF33,
	AdMob_AdRequestBuild_mAE6957133C5FECA4E32036F8F588F653075F619D,
	AdMob_OnDestroy_m0DFD2DFA7292422B91CCCF7B90F1252453DFC891,
	AdMob__ctor_m3AF6D0DE3D81B4D4BD12A1D4AC4BD5A05A9CADAB,
	U3CU3Ec__cctor_mA913151AFE1DA1C09137E8800A264AAF7AEABD59,
	U3CU3Ec__ctor_m99E41F298DB640BCD88C6CA722086CB50BC9A950,
	U3CU3Ec_U3CStartU3Eb__3_0_mA3BDBAFDED17F7AB3D8EE0800929654FEDF94E14,
	CanvasScalee_Awake_mE23A5037C45D9ABAAF18F19829C6A58CD8136B68,
	CanvasScalee_Update_mA6C8D0459A45CD766D372708650B6593E049293F,
	CanvasScalee_HorizontalDevice_mC3ADFD21A9D7FF3BB0FF72922E05EAFFB8A4BB5F,
	CanvasScalee_VerticalDevice_m53BC16D7FB10DA55EBE442B421639DBDF787BE29,
	CanvasScalee__ctor_m97823671FEEA8622BA9A267617576988F62742BB,
	Ukryj_Przyciski_Start_mCA379C17946B72EC4177A151ABCF5203F7FE4671,
	Ukryj_Przyciski_Update_m7225AE8D5F4BF914AAA4A77C1CBA6EB40F3E92BF,
	Ukryj_Przyciski_Ukryj_Przyciskii_m43C6F498FCD342C0187DB1BBAC5635A8DD7D87C2,
	Ukryj_Przyciski_Pokaz_przyciski_m2374FB68550094D7DFF04B075F7F884DAFD22DA8,
	Ukryj_Przyciski_Ukryj_buttony_mCEE1B086BA08F2619F36BBE442744FCE99474ED9,
	Ukryj_Przyciski_Ukryj_dzwiek_mA09F3F3241FB901A4909098E915D72BB4BA625F2,
	Ukryj_Przyciski__ctor_m51003C1E4FF70C7329F848CA9C26F786E2CB7410,
	Blokuj_Start_m7D9999C88CD393050B9DD976A6D5109FFF6663ED,
	Blokuj_Update_mECC3730E9BA541D2934866205F91DCCB173D4E89,
	Blokuj__ctor_mAA8A588A6155BDBC1B16B0E2C2E9A90538EA5B8B,
	chmurka_skrypt_Start_m4001D6630924851E60BA9DD37009F90852CB4CEE,
	chmurka_skrypt_Update_m30A172E4B46FED9E08FC8B5DB4602E412163DC73,
	chmurka_skrypt_OnTriggerEnter2D_m3E3BAFC6E80DA30D6A7A2DE20F62CD338603FC9F,
	chmurka_skrypt_Lec_chmuro_mA23DB4E9F5BAF0D1E5BF9FD3CFB83BF7040B9485,
	chmurka_skrypt__ctor_m277D3B06BF8A5B8290D1C6A04259D8A767F19F42,
	Cykl_dnia_i_nocy_KD_Start_m4A89FD7E4EB155A374CA31D5BC5C84D0849A29AC,
	Cykl_dnia_i_nocy_KD_Update_m40AFA75EAE41C2ABB54CB374442FD0590BE2A9CD,
	Cykl_dnia_i_nocy_KD_KaczkaDuza_spac_mAFEF227FEEE0C4373A55E1BBA516F237AA82ABD5,
	Cykl_dnia_i_nocy_KD_KaczkaDuza_wstawac_mC31FF962C404C9F28ED19304626ABADA7941CDA6,
	Cykl_dnia_i_nocy_KD__ctor_mD183901B2ABAFB052E14F7DEFFC26106D12AE8D9,
	Cykl_dnia_i_nocy_KM_Start_m9642C935DFBA0292001F0C9C415EC4599A46A119,
	Cykl_dnia_i_nocy_KM_Update_m4940E8E120460DC109FA29904FBD0AB739765692,
	Cykl_dnia_i_nocy_KM_KaczkaMala_spac_mF69A78953AF5F86462E912D009F20B2A195DB835,
	Cykl_dnia_i_nocy_KM_KaczkaMala_wstawac_m1777D034017CBCAE997DC698EE4583FEF1C3A017,
	Cykl_dnia_i_nocy_KM__ctor_m83E63B19C0E09301F79AF8349CD0D28FFE0CEE3F,
	Czekin_Start_m979E185466F7036186E9B51AA09B6DCEB8B52B96,
	Czekin_Update_mB76A692AB4DA4667A859B1ECB3A15A881B540C00,
	Czekin__ctor_m447DA073AD40D5B0DB597F4C6F771BC86F46891F,
	dzwieki_kota_Start_m9953A186D820281F17A275795D2FD1FEBBF472BC,
	dzwieki_kota_Update_m12FFCB5F7EF6EAE912A19FCA6DC54FF3A3B37460,
	dzwieki_kota_NoZaryczNOoo_m0F941904849768D81A962B31C81882667C9BD8BE,
	dzwieki_kota__ctor_mCD7E125AF4F9C459EAE8C92E24B06878412F6732,
	Dzwiek_jedzenia_Start_mACBF243CD6926E7091D1831C47EA44B24CCA25CE,
	Dzwiek_jedzenia_Update_m8838CA76DD3596EAAAB8E4F0C3F257B14F16D7A3,
	Dzwiek_jedzenia_Glosno_Jedz_m9B6B266BD465DAB5D246DEA0D092C49617206322,
	Dzwiek_jedzenia__ctor_m75EDADB5464A1D91D14AB3862771CAA1BF012826,
	Fidget_spinner_Start_m82FC572DBB71D9E45B624E7A15493647528EF9A8,
	Fidget_spinner_Update_m30E5BBD8686298607DDDF6C191776B5B98DD4E18,
	Fidget_spinner_Odpal_Animacje_m0E18A73032913AC9AA5F3B8A6EDB386267C0819B,
	Fidget_spinner_Odpal_muze_m1DA52C93A3A595629CC8DF65A7BAAA0C3DC462F9,
	Fidget_spinner_blyskOn_mFD5EA1DC284D62551E7951DAFA4525DDC6EF9ECD,
	Fidget_spinner_blyskOff_mDC644C7D8303697F4FD13D29624FFB8B53AD3F7C,
	Fidget_spinner_Inny_mBA83C5F4199B54AD7D4A22AF22764A5E04D72A77,
	Fidget_spinner_Zmien_Skina_m34940A073AB7D236A063046589ACDE0B07B1FF8B,
	Fidget_spinner__ctor_mF9BE1C87B163D78B174D68C097D11556A9C4E835,
	U3CZmien_SkinaU3Ed__15__ctor_mC970868DD676D16509FFC48057ABFDE9819811C7,
	U3CZmien_SkinaU3Ed__15_System_IDisposable_Dispose_m0A3B931E488D319B38530986189548785A72377E,
	U3CZmien_SkinaU3Ed__15_MoveNext_mEEB84C2ACA9E0DA7DC48C19141EE6AB00BE326D4,
	U3CZmien_SkinaU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2501D0C9911D023218134F55A083E5B1238EF26D,
	U3CZmien_SkinaU3Ed__15_System_Collections_IEnumerator_Reset_mDDFE5FD4BD94657F7E70F0421C44DCB3F97B725F,
	U3CZmien_SkinaU3Ed__15_System_Collections_IEnumerator_get_Current_mBFF4369F424BA1E174C2DFE83ABFF4102C1F91A6,
	gumka_do_mazania_Start_mB05A17244FBE71EF09BF4BA15627CC47181602D9,
	gumka_do_mazania_Update_m9B29A404D9C6D53799FF2BF7DD18082CA8975DDB,
	gumka_do_mazania__ctor_m803139ACAF1464AF15846F2DA3ADAEE5F1F42827,
	Idle_czy_nie_Start_m3BFD1BF99DF00A3AB86C5B8A044BB3145F525761,
	Idle_czy_nie_Update_mDAE5A4504796CF242C6AD76F41350F16FDDF80E6,
	Idle_czy_nie__ctor_m730FC0AA2B06CA0D73CFA5DB2D3A56964FD7200A,
	Informacje_od_Kaczek_Start_m805A59A9E5C16D177BF8508C2B8AA639FFCEFF5C,
	Informacje_od_Kaczek_Update_mC92FBAFD4E96E5C9C84A4F728358AA682EBD5B2E,
	Informacje_od_Kaczek__ctor_mA53E38BF33186AA93D74524959BD43AC9F605CFF,
	kaczka_chodzi_dzwiek_Start_mA1D99DE56DD4B075FE1B410C0D722A0CDE276EDB,
	kaczka_chodzi_dzwiek_Update_mCEF6AF162A6D70CCAFF5AC93492CDE29BED9FCF3,
	kaczka_chodzi_dzwiek_Kaczka_Do_Wody_mCB741966F8D5DC735CA52F5C4B83DE6BDE61F4DC,
	kaczka_chodzi_dzwiek_Kaczka_z_wody_m69C94ED5C38547FE30597E08A42CEAB7860B64FA,
	kaczka_chodzi_dzwiek__ctor_mF72367B29A337E6CA759F5866DEBC6924843B712,
	Kaczka_Moze_Spac_Start_mCD49C7DE18F65B11AD9F6FFC4DA31A41892AB48C,
	Kaczka_Moze_Spac_Update_mF24CF1AF481507BD588411196B71E51442DDC0B6,
	Kaczka_Moze_Spac_MozeszSpacAnim_KaczkaMala_mD846E250ECE171F1D18EB3C0167B4CD6E45C11C0,
	Kaczka_Moze_Spac_MozeszSpacAnim_KaczkaDuza_m9DACFC38741EF220D9B24991CF0CCDF0139597E2,
	Kaczka_Moze_Spac_NieMozeszSpacAnim_KaczkaMala_m52D3F7A87490E6E137A1341D1E69564233E65751,
	Kaczka_Moze_Spac_NieMozeszSpacAnim_KaczkaDuza_mEAB6B467DE415818D071230D64004568C81F6369,
	Kaczka_Moze_Spac_KaczkiSen_m3C864CFB9100999A64DB664467482E1E8E31EDEC,
	Kaczka_Moze_Spac_KaczkaDuzaIdzieSpac_m7BACC40FD0E8C95226D1263700AF381E174FB51D,
	Kaczka_Moze_Spac_KaczkaMalaIdzieSpac_mD06283125C7A5C3D8ACCEB473BD5A0FC87021B60,
	Kaczka_Moze_Spac__ctor_m72C98BDF8A6A5F5B535053AE398A86296CB0F92A,
	kwakaj_Start_mC4819124482D9C62613C44B2971599AED6808D30,
	kwakaj_Update_mC0E895FDB40F6C31E519D182C51A1089CDAF6672,
	kwakaj_Kwaki_mB9B6BC5049338D4D8399189B296E32157FD222A2,
	kwakaj__ctor_m66B55FAF07B5F3BB9720D3AC9E77437F7B50A054,
	ColourToggle_get_Colour_m646B5EFA0029FF0C178614485F333FFCE1FCE07D,
	ColourToggle_get_IsEraser_mD8670D6FE9CBC4044BF4C2A52F2E0CB16715BDE4,
	ColourToggle_OnValidate_m3564D71886A3F14C7AB49907461F27E6C416BC98,
	ColourToggle__ctor_mA779BD33C616ECAB81E92F4F63AE419DA52BE4E6,
	MouseDraw_get_IsInFocus_m9DF3761CB2B9B5BD4FF2A3D69F652E242C882C1F,
	MouseDraw_set_IsInFocus_mD3579AB4670C1838E5A05692F5D1A0AB98870480,
	MouseDraw_Start_m462E17E9DDF33F63B0847AB489E746EF1EC1B5E8,
	MouseDraw_Losuj_Kolorki_LR_mC3E47934AF34FBA2CA0A27788737A0E4A74C2B59,
	MouseDraw_Losuj_Rozmiar_Pena_m7B242C8724DEE179C920AF0ABEA60944F7566428,
	MouseDraw_OnEnable_m751585397ABB391EDB442C3D93D9A28761FE1260,
	MouseDraw_SliderValueDidChange_m3FF26499C58666275DF01805A506BFCEE4B0C7A4,
	MouseDraw_Update_m58672FBD99BE2250AD9501BE8E184D4BDD476557,
	MouseDraw_CzyTrzyma_mA46649EC4944BE79ADA0E539646F14FABFC8C355,
	MouseDraw_Init_mE5C868DF1E378476A40D68863EB12E9F5912BE90,
	MouseDraw_WritePixels_m83421E4CBF155C7155192EF3AA7C94A01F3132C3,
	MouseDraw_WritePixelsV2_mD1758A33693F8270FDA115B6B37DD12917317606,
	MouseDraw_ClearTexture_m3521795A523C2790042B5102F47F2FC0D33EE4A2,
	MouseDraw_GetNeighbouringPixels_m2E3359641FB7C7B05F3D520F0EA5EAE520D2ADB4,
	MouseDraw_GetNeighbouringPixelsV2_m3AD33DC50F5C0B0A946880BA8CF115B6A2885492,
	MouseDraw_GetLinearPositions_m8BA2A27046A5AA9A94D485AD6C7D258A51A36DDA,
	MouseDraw_SetPenColour_m6E50FC62C7B583E898CACAAFEA3759EE0FEBD115,
	MouseDraw_SetPenRadius_m3DBDF3D9D1BFDF0A14C85842E56ACF41648470AC,
	MouseDraw_SetPenPointerSize_m0B3FB5B716DB2D55D78A78276C291B9E0EBF4FEE,
	MouseDraw_TogglePenPointerVisibility_m62E7F1B1A05B7CAF28FD7704BB8C9BD10B399E24,
	MouseDraw_OnPointerEnter_mC78F84D022050BDE2B18BC2FABBA3F0CE30FFEE6,
	MouseDraw_OnPointerExit_mE3159B0A1D5EC9E8777F121DC7E7C008D3BA6625,
	MouseDraw__ctor_m9B651CF30415AD30664A77F220E83AD976AA0C20,
	MainUI_Start_m1ACA1116EEE959FA317F44DAB4C292C67F61D218,
	MainUI_Update_m7D1D4AEB499AAFEBE3230CA860D9A2722A817D74,
	MainUI_PoziomInstrumenty_mF2508799305B595D3459FBD3C23F924C8E3BC456,
	MainUI_PoziomFarma_mE729B033115D1F0919A0D08FE97E4801028F737B,
	MainUI_PoziomElektronika_m0669CF91A7F17E5ECAA2CE41779B81C26AA730D1,
	MainUI_PoziomZabawki_m724709A3D9DC9957D86B4056E0D36FD1158186B0,
	MainUI_PoziomKolorowanka_mE8B9513F70EB43434609E2105DD4CEF29ACAE904,
	MainUI_PoziomCzajnik_m3D9BB39687EF7E20E77598519D5358FC2E88E496,
	MainUI_Ekran_dla_rodzicow_mB27C9C63A19437FDFF1BFB4634C87D9C1908D0AF,
	MainUI_BackButton_mDBA1E197379A13116B3E06F6745D438D65B9653B,
	MainUI_zaladuj_poziom_m0B69BC63CE2A02185ACA7704EB19FF52333CF639,
	MainUI_zaladuj_poziom_dla_rodzicow_m7F479949F8F29BDF4B7C6FB200F1DFAC2035BEA0,
	MainUI_DzwiekOnOff_m11BE615043A28C9A604C77D35ACA0028DB14A4DA,
	MainUI__ctor_m7F9F45AD02C9A3C4DD9347331F0F4B5B37343CF5,
	U3Czaladuj_poziomU3Ed__19__ctor_m4C144F3AFC90240B3DBD5DF17E574C5D6F9C5FE7,
	U3Czaladuj_poziomU3Ed__19_System_IDisposable_Dispose_m105E9B7BEF85B7124DEBEB6FA6DFA26267EC689D,
	U3Czaladuj_poziomU3Ed__19_MoveNext_mA82F68CC22E4AB67AECE99B7CBD413DBFFB97865,
	U3Czaladuj_poziomU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FE179B31C136993BEF610209FB5C15132E0D023,
	U3Czaladuj_poziomU3Ed__19_System_Collections_IEnumerator_Reset_m747FE02A4C158D1095DE853BD89308BEA9C77BB1,
	U3Czaladuj_poziomU3Ed__19_System_Collections_IEnumerator_get_Current_m377015ED983F849BD2EB31F4D5C08C1C760D12B9,
	U3Czaladuj_poziom_dla_rodzicowU3Ed__20__ctor_m6A6B8ABCB8DA20E736A3D6E9F05752149DF76DC1,
	U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_IDisposable_Dispose_m7A15C2897ABB45066496ED779CEE7725451FA328,
	U3Czaladuj_poziom_dla_rodzicowU3Ed__20_MoveNext_mFB7D855DD718454AA580883D18EBA80B2C29F404,
	U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4F34244896F484CA187CB8D4A193DC3EAFB6619,
	U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_Collections_IEnumerator_Reset_m6BEAEBDAA1BFD474B8873E37D2AB5CA3EEFB3418,
	U3Czaladuj_poziom_dla_rodzicowU3Ed__20_System_Collections_IEnumerator_get_Current_m40FF36F8AC9785F77F68A7D3BF7FA432A2D2B4F1,
	MouseDraw2RD_get_IsInFocus_mC044E4CFEF9380B7E2427142CD3F98FD903DEF29,
	MouseDraw2RD_set_IsInFocus_m3630CC7B7A23BCB0A3D2CF47AEC0CBFBA9C14219,
	MouseDraw2RD_Start_m965221C706E54F098F54746AD9C73DB5DACED5B1,
	MouseDraw2RD_OnEnable_m197C3B23B55B7597EC4E0E42F7085CE9897BD628,
	MouseDraw2RD_Update_m8BBF45A1CB4E9A0B8AD1E268EC5C768248AFB743,
	MouseDraw2RD_Init_mE1D47BAC13F75B51F2C2CDB5223472822F544728,
	MouseDraw2RD_Losuj_Kolorki_LR_mF2084DDF61109EE276C6DAE34E5AA6A5204245A5,
	MouseDraw2RD_Losuj_Rozmiar_Pena_mDC947F7C7A2EDAFBB87A1FA0C12496ED77AD7BDA,
	MouseDraw2RD_WritePixels_mCA6F8B55308E01056AC62B4AAE392FFE3CCD4379,
	MouseDraw2RD_ClearTexture_mA095D2E6CDF0EF71B1A36F8467147C727D3E69E3,
	MouseDraw2RD_GetNeighbouringPixels_m6391C6B0C908D382F117AA2FC438C3DA97D9CDC2,
	MouseDraw2RD_GetLinearPositions_m2DDA79349495416AF082692A348A073761343748,
	MouseDraw2RD_SetPenColour_mF4BECF9996BEB881710EF4D53AE42F8D32D27265,
	MouseDraw2RD_SetPenRadius_m02610ED7D0D4EFF15C8C2D4754329FE38D8BC4E1,
	MouseDraw2RD_SetPenPointerSize_m79CC868B78C96F834F336BF791EECAB2C120183A,
	MouseDraw2RD_SetPenPointerPosition_mEFB6234801AB6C47FBBDE8EE5048E7116D22625D,
	MouseDraw2RD_TogglePenPointerVisibility_m4B2221C0E3BC9519F82C22DBA23DD32CF6C658B2,
	MouseDraw2RD_OnPointerEnter_mD16EC1ABAECB59AB0170E4CBA6BF50871C25C00A,
	MouseDraw2RD_OnPointerExit_mBDBFCB1C6EFE62BAAD1BDF18867786D37546D7D3,
	MouseDraw2RD_ExportSketch_mF45A0DA841B1DB18940DF84BED4368B9A14A4A50,
	MouseDraw2RD__ctor_mBCA21F080D3AEA3B1B2627C990E44A1F4967E0A2,
	MouseDraw_ForMap3_get_IsInFocus_mFEB5CE66C73EF17A9BF8AB37F8EDCA1C1052E84A,
	MouseDraw_ForMap3_set_IsInFocus_m5B97688C9CF9C8D7667356CF7B6EA6FEF5C34CAD,
	MouseDraw_ForMap3_Start_mA15A2480DBF291BCF1ED32EC47734CB66396010E,
	MouseDraw_ForMap3_Losuj_Kolorki_LR_m45D52C4B6BD174B543ADA3C251C42735B8581FE7,
	MouseDraw_ForMap3_OnEnable_mFE10C63E9C7A2CA71DF5024F1177F9095C8CAF4E,
	MouseDraw_ForMap3_Update_m4DBCE060B6B439E1188F467D1F86B67D9B12576F,
	MouseDraw_ForMap3_Init_mB1C5DA17FEB3AE09831A705B93809ECE04D673C8,
	MouseDraw_ForMap3_WritePixels_mA928F3C590878DF582615161FA4FAC192AB8154A,
	MouseDraw_ForMap3_ClearTexture_mCEB54F33C29D15EC9888F0D52DCF3E07D7FFF05D,
	MouseDraw_ForMap3_GetNeighbouringPixels_mA321A1AF01B620ADC354CD1BFEBB28FE9ADCC7EC,
	MouseDraw_ForMap3_GetLinearPositions_m09549495AF2EFC1D9A3085C3E857DDAEFAA42845,
	MouseDraw_ForMap3_SetPenColour_m5E93F6BCBF7A59C4657F62DE81A44CDD517C4CC8,
	MouseDraw_ForMap3_SetPenRadius_m0ECEFE154DD74B484A29278BC74DC97BCDF27D1F,
	MouseDraw_ForMap3_SetGumSensor_mE29F159362C0259B4E475DDE11D9A415234D53AE,
	MouseDraw_ForMap3_SetGumSensorV2_m239149196DDC5F8E9A6DCC4CAD5ADAB671A578C8,
	MouseDraw_ForMap3_PositionOfGum_m38AE773B38215CD6EC230647E13EA07E0718895F,
	MouseDraw_ForMap3_TogglePenPointerVisibility_m254B51A14EC22A35CB558FDA0CED8CB96BB3BFFE,
	MouseDraw_ForMap3_Correct_m77A30D7DBCCCC1A485F9EFEF4A76902E98B90D66,
	MouseDraw_ForMap3_OnPointerEnter_mC5E595BFF45266759314A76041990D8C71730673,
	MouseDraw_ForMap3_OnPointerExit_m773C1624CB04AC0B562449760B55702939F238D0,
	MouseDraw_ForMap3__ctor_mAE2D06B125C43341021E4C5A479F19B47033B0E0,
	MouseDraw_Orginal_get_IsInFocus_mB87C9A8FC97F249A000A20E60818D713D0D68CE2,
	MouseDraw_Orginal_set_IsInFocus_m693C00134801A08D9063F5D20EADAE96172CD215,
	MouseDraw_Orginal_Start_mBC1DBE625705C15BCCC7E5E8F4B2587F14D613C4,
	MouseDraw_Orginal_OnEnable_mBDB36B6C0593AE62B018BCB03DFF846061802E70,
	MouseDraw_Orginal_Update_mE11B94AFEB30318C74AD81DA3C3FDE1D0CB729E2,
	MouseDraw_Orginal_Init_m2599664D2B2DAC2545F97F5840E8B10521DAD9F1,
	MouseDraw_Orginal_WritePixels_mAEAEC5238B23833882B6E0153792B4E154B22CFE,
	MouseDraw_Orginal_ClearTexture_m954BEC4167A94C46A4624DB74AE0FC52D0FE64CF,
	MouseDraw_Orginal_GetNeighbouringPixels_mDFEF1ABD69B49EE4039FEE0E715B5499AD395719,
	MouseDraw_Orginal_GetLinearPositions_m4CB2CF3DD8C7BC1A4BD408F9E90007AFF13189DE,
	MouseDraw_Orginal_SetPenColour_m166D61356E05D1E5F222575D5CEB98E6F64C1BF2,
	MouseDraw_Orginal_SetPenRadius_mAF081B4C286D663E434460FB54EA79A54F430A13,
	MouseDraw_Orginal_SetPenPointerSize_m09CF86F6D00FDBA67E7FDA8ED4B7FC927790D4DE,
	MouseDraw_Orginal_SetPenPointerPosition_m327D8BFB2BAC71F05851951B8A75A4EF3580C4BA,
	MouseDraw_Orginal_TogglePenPointerVisibility_mFA8C3B85AB8F80F6C30C380FEE107C4986427251,
	MouseDraw_Orginal_OnPointerEnter_m6E176199BAF3EDE8049E1425E3D03EA86A976F93,
	MouseDraw_Orginal_OnPointerExit_m143A7348063A0873D8F2D75228E7DA16B0FD0B4E,
	MouseDraw_Orginal_ExportSketch_mA6EDFA86C4C0CAAD8E30D91FC539ED3CBE48BD37,
	MouseDraw_Orginal__ctor_mECBF8AC9D35594F653DD130965F2027E1A50353A,
	Mrugnij_Start_mC895AB92BC52A740BF94704F9A1F01542A5DF6CB,
	Mrugnij_Update_m8944445094170F07F3907FF6966BC835FA55C256,
	Mrugnij_Mruganie_dzwiek_mA488316D6BA1FDAAD0935FF4AD261DA603422723,
	Mrugnij__ctor_m9DEFBA78ACA55A12331D061444C0B5BEA428D14B,
	Odliczanie_do_snu_Start_m365CF8B1C70B6AA1D3B21CB6A04C5216494BB84B,
	Odliczanie_do_snu_Update_m36E5F9FC532CE9074E4EC1196F69F3E83F30BE0C,
	Odliczanie_do_snu__ctor_mF8614D57B26A21FE946BFD6FCA02A9D35622D8B1,
	odpal_czujnik_Start_mE8496497B5D257A55DAEA9B1BBF588F7E3C3965D,
	odpal_czujnik_Update_m9AB4D632F764F54EA2A8C9CD348BB366E1FF7A3F,
	odpal_czujnik_Odpal_Off_mC819C8918EF83AF5C940F619601A5D341E69B128,
	odpal_czujnik_Odpal_m8907823A27BC7AEF5B0B6C54F270DE876FDE176B,
	odpal_czujnik__ctor_m23BE8BA19A1EC86D0E1C31DEBB791D7691DB40C5,
	odpal_parental_gate_Start_mB125B830B9E05C4AB2139B8D8F98D546A05A874B,
	odpal_parental_gate_Update_mE09361899D1E9519DE74F3E58B8D07BEB8053E8A,
	odpal_parental_gate_klik_ads_m43A1F63168D1B6C7EDD4B703BE8D3073DFC67C5F,
	odpal_parental_gate__ctor_m2178D6AA6575B778BA6D7A3A0FDA7FCED05AF5E1,
	podazaj_Start_m64814F17F4096CCD20AA3F8C83A1F01AAE97FB97,
	podazaj_Update_m053F75491DD3F3CC68E68CBB1A94B4F2A75C5B91,
	podazaj_Podaza_mF5032C960493A9B0C4BDBC0B90AC876CDB5C5E20,
	podazaj__ctor_m21EB51714CD99BFADBDAEF85AA926C0B4D820C7E,
	poziom5_przyciski_Start_mC442F85750DAA4A102531D363F501957798DF4BE,
	poziom5_przyciski_Update_mAC7E4B5ABB597AA93AD9355CBEEE04BBBA2A7237,
	poziom5_przyciski_PrzyciskClick_mF0ACB9C965EFAB517CF3F76326FF42C1D7778822,
	poziom5_przyciski_blok_mA5FD912FB76F1927A6987D29E2B18F8A2A3F393E,
	poziom5_przyciski__ctor_m88D6FCF77454A6A1B7FE352EBFCABE6F2560008E,
	poziom5_przyciski_U3CStartU3Eb__8_0_mF7EE41B9AB53B5EC2D40FDA7C816A9C742EE525E,
	poziom5_przyciski_U3CStartU3Eb__8_1_m9B7B19BC96AA9A68CB483ACD5635AE303BA87F07,
	U3CblokU3Ed__11__ctor_mAD1F00CC885658A3776AA2CEA83D7271E4133DDE,
	U3CblokU3Ed__11_System_IDisposable_Dispose_mD6E9D7803B094474D174D784D2D60FDAB6E7B3C5,
	U3CblokU3Ed__11_MoveNext_mF4E06FFA91FE1EB02650CDC679FC2FB5B7EF910D,
	U3CblokU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB635648C6D7F408238C904192738C5495DB1224,
	U3CblokU3Ed__11_System_Collections_IEnumerator_Reset_mFD4FE135DE33993F7DBBDD1BF84874331E733824,
	U3CblokU3Ed__11_System_Collections_IEnumerator_get_Current_m58F74BDF9ABDF88D86DB327DFA8268EF76BFCCD4,
	prysznic_dzwiek_Start_m5380493C9BDA5713A3B407A3BC9D723B0DF6CC16,
	prysznic_dzwiek_Update_m983DB67F1798B813D1BDBFE07C11E242E03E00A8,
	prysznic_dzwiek_Prysznic_m6E8B29FF42696B85B9E6BA6B9CE8AF5C7D492A43,
	prysznic_dzwiek__ctor_m583E179B569F1ED2E6D0B93408C80414AABC6543,
	Przerwij_zzzz_TEST_Start_m7ACFA688B45EF7CCA862272E53608BDB53AE88BD,
	Przerwij_zzzz_TEST_Update_mD39886FB13AE3CCF2C0957466DAE8E9ACD7EF03C,
	Przerwij_zzzz_TEST__ctor_mEDC3EDF093CA1854D5FC43530FDDF159735040A3,
	cymbalek_Start_m31A72E36C0C6855BDBE90B3D3029D01C19361435,
	cymbalek_Update_m4BE874CD5A2A20A928CC290D85231B46D09BC258,
	cymbalek_IdleSound_m7FD62002FA8617D80453409F7DE7EC646BD61A7F,
	cymbalek_CymbRandom_m0B0D518900143EC73D1625D46044B3588953D87E,
	cymbalek_CymbIdleOn_m12A675EB6D6069553C7E37926CE72ABD6E6C7AD4,
	cymbalek__ctor_mE6CAAD0D4A7414BFB5715A2B28F9232D1A4E3B55,
	U3CCymbIdleOnU3Ed__10__ctor_mCCFEB7A25A236FAF215F4FA7FEADB09460850D2E,
	U3CCymbIdleOnU3Ed__10_System_IDisposable_Dispose_m014D9954CCE0A3D3FC0FA9CD757ABA9762E68E6F,
	U3CCymbIdleOnU3Ed__10_MoveNext_mBF3755AAC73973834C84FC66F3CDFD56294F88EA,
	U3CCymbIdleOnU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B7DB9DA250E09DB80E1CA30769A7B9900DCB9EF,
	U3CCymbIdleOnU3Ed__10_System_Collections_IEnumerator_Reset_mFE0766D93497113019B51C6550CB8BE25795DA63,
	U3CCymbIdleOnU3Ed__10_System_Collections_IEnumerator_get_Current_m5C523C448DF976AEE2DD1AF0478D3A2E4A0FFC7E,
	Instrumenty_Start_m961A3509EEF03E92E238D90334974F1625153ECA,
	Instrumenty_UstawPaleczki_m1CAE59DDD89E43D1C7A7560B04EBC27293FCCDAF,
	Instrumenty_UstawBanke_m7DD7DC59A734467B56C9579453A092FD00C42D60,
	Instrumenty_Update_mF3D0ACCB5B78B113FDCB33E422DF0AB9D7965C54,
	Instrumenty_ZbijBanke_mB6730411FDAEFA3A30E531B62CC1FE5DCB864AC7,
	Instrumenty_AutoZbijanieBaniek_mB352284CFECB7C4B07E66E25095F272B9E7B09A1,
	Instrumenty_SoundGlitchFix_m45A12BD40A9C00B191D79745C7F28117C62A28B1,
	Instrumenty_PlaySoundAndAnimation_mBAE61AF82D82C657718C4FB94B2AC85D03736443,
	Instrumenty_PlayAnimation_m3A03B2B4827B713AA5FFAE4378FCE28D97194543,
	Instrumenty_EmiterOn_m63A7A46EE984C4DC144F9DB94E6B2D57147E85AE,
	Instrumenty_EmiterOff_m7475CE0E02AC08FC83D24CC6F59318442705C982,
	Instrumenty_WylaczIdle_m286FA4CDC390D351DCD13CC2F084ABABEFF60BBF,
	Instrumenty_SpawnerBaniek_mC6239D96E84D3765C1A0A76DF4521A860D6A57D5,
	Instrumenty_IdleAnimations_mC4A5DBCA106BE2058285492E8812EDC3AE9A5D0F,
	Instrumenty_CymbIdleAnimation_mEFA7F7BDBE9B85D825D821ED65F99313BF4BA84B,
	Instrumenty__ctor_m26A5A9E146608FEA9E31E169A692D9FC0795BCE9,
	U3CZbijBankeU3Ed__44__ctor_mFDA83268ED6064BB49C8919092A8FA03893B9C09,
	U3CZbijBankeU3Ed__44_System_IDisposable_Dispose_m8D7C2107653C0BE5C58B203BE8DACF8512E2D914,
	U3CZbijBankeU3Ed__44_MoveNext_mB2450FA54BFAE63858BC1BE663D0234658D18703,
	U3CZbijBankeU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m577AB1FEAFEE9AB5BA055C9D3DF6F06564712406,
	U3CZbijBankeU3Ed__44_System_Collections_IEnumerator_Reset_mBEB1543D8C80F64C29CD36B46A13DD49F7D5ECC3,
	U3CZbijBankeU3Ed__44_System_Collections_IEnumerator_get_Current_m67250C0D771A0B56CA4324E9364C291472973FAF,
	U3CAutoZbijanieBaniekU3Ed__45__ctor_mB07514C6927FECE20C31797A1F340B5BBD44B1D6,
	U3CAutoZbijanieBaniekU3Ed__45_System_IDisposable_Dispose_mB385DE98CB1FA08D071A4DBF87BB4C634CF8FBAF,
	U3CAutoZbijanieBaniekU3Ed__45_MoveNext_m1C179B6BAC3E6FEA6D1BDE5EBAD23C28B09909C2,
	U3CAutoZbijanieBaniekU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09167645D22A2724FAA7184316162FEF2F8480BE,
	U3CAutoZbijanieBaniekU3Ed__45_System_Collections_IEnumerator_Reset_m2CC79D3D90B28AA7E467EB9A068AB86B9E8A8B14,
	U3CAutoZbijanieBaniekU3Ed__45_System_Collections_IEnumerator_get_Current_m8BDBC492AA54D87C641B223E860FA4236BA2EEB8,
	U3CPlaySoundAndAnimationU3Ed__47__ctor_m049698453D802A9BB9BF500F0845DB0F806B2E99,
	U3CPlaySoundAndAnimationU3Ed__47_System_IDisposable_Dispose_m07A11E5AA11CD45464E48495F1A2411CA0712404,
	U3CPlaySoundAndAnimationU3Ed__47_MoveNext_mD23447A81CF024F31BB6B688F79A1E31F44870E7,
	U3CPlaySoundAndAnimationU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m56EEFFC82058628460C785914378166313FB9A25,
	U3CPlaySoundAndAnimationU3Ed__47_System_Collections_IEnumerator_Reset_m6059A72EE10F5F32CAA4BF645E60858DF02678EA,
	U3CPlaySoundAndAnimationU3Ed__47_System_Collections_IEnumerator_get_Current_m5CF458634EB8949E3D43D133C223291ABEEC5AC8,
	U3CWylaczIdleU3Ed__51__ctor_m125650A38CBDA281E9540CF9F992D8B2CC76DEAC,
	U3CWylaczIdleU3Ed__51_System_IDisposable_Dispose_m02CDD9E7E2777159BB207406A28F226B60050727,
	U3CWylaczIdleU3Ed__51_MoveNext_m167A81BDD96392D12D827BD86ECD279ACBF028AD,
	U3CWylaczIdleU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7479C062879082548C3B431B9859E2887202FB8,
	U3CWylaczIdleU3Ed__51_System_Collections_IEnumerator_Reset_mF20DE93E9417B44EFE8523EE1D3EB4FD5FCDC9E4,
	U3CWylaczIdleU3Ed__51_System_Collections_IEnumerator_get_Current_mDCE77657FAA20320B8337D99300A3B0C97366FE9,
	U3CSpawnerBaniekU3Ed__52__ctor_m1F69A419C8E271873E40B6A53BC04F4C0E634638,
	U3CSpawnerBaniekU3Ed__52_System_IDisposable_Dispose_m08851CEAD69B29ED612C6AFDCF81B57601251AB1,
	U3CSpawnerBaniekU3Ed__52_MoveNext_m868AAA3F277353F92E3694EB364BEECAD8EB2CE8,
	U3CSpawnerBaniekU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0AA3891A86E729B473EA153A5A59FD856C6A89F,
	U3CSpawnerBaniekU3Ed__52_System_Collections_IEnumerator_Reset_mB0EB80D288A48F2167351DF6A75A265FD19C5774,
	U3CSpawnerBaniekU3Ed__52_System_Collections_IEnumerator_get_Current_mAC0AD90DC0D364BCBE4B4A68185CC9ADE67B3118,
	U3CIdleAnimationsU3Ed__53__ctor_m02CF8A3CAEBCD1720EC6DAB5352DA7B92D0E116B,
	U3CIdleAnimationsU3Ed__53_System_IDisposable_Dispose_m28574FFE3475280DF9A5DEF09F4F9C5314980CE5,
	U3CIdleAnimationsU3Ed__53_MoveNext_m1628ED6A2753204DA1D2FA96C2F234D2B9A67DBC,
	U3CIdleAnimationsU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C4EFC6B5C8372F5A540D93F81F18928E8D49237,
	U3CIdleAnimationsU3Ed__53_System_Collections_IEnumerator_Reset_m9B2616177EDBE2DCDA711FC34119DA35D2451DDD,
	U3CIdleAnimationsU3Ed__53_System_Collections_IEnumerator_get_Current_m80D6CFD5FEC8C1AC9544D6D3A90DBFAE61447D80,
	U3CCymbIdleAnimationU3Ed__54__ctor_m7F602FD42069132239DD69A1286AB116F8C64DC7,
	U3CCymbIdleAnimationU3Ed__54_System_IDisposable_Dispose_m047B119A13896DF7CE99DAB9C3784676ACEE76DC,
	U3CCymbIdleAnimationU3Ed__54_MoveNext_mB0C0630770C913AB3340AA36F735507BB9A90757,
	U3CCymbIdleAnimationU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m353F07D42254FA93BABB412392B1E7A12392941B,
	U3CCymbIdleAnimationU3Ed__54_System_Collections_IEnumerator_Reset_mCC3C1B88F8E01D9312CC5730849B7AA10596F558,
	U3CCymbIdleAnimationU3Ed__54_System_Collections_IEnumerator_get_Current_mB336531C1EA44A294EF35F5E1307B4807E28A927,
	Farma_Start_m071523F2157EE0E5F535B394A61D77E57020A38D,
	Farma_Ubrudz_sie_m354215D67CDC101A91531E745DD20EC69EE74EA5,
	Farma_GetPigDirty_mD538913E4E12DA67DC444260615559EEC38A0FEF,
	Farma_Odpal_coroutynke_m0E43A7DC61EDEB7381B07DAF9C4BAAB19B926A56,
	Farma_Zatrzymaj_coroutynke_mE5FCE251B2047BC52487E560837A29F88C627155,
	Farma_newPigDirty_m3E12A50BE250C6D309CDFFAA457F3DB753C038D7,
	Farma_PigClickEnd_mA11A766C02343ECCA9A75AADB0FB8C6DB73613A1,
	Farma_MakePigClean_m1AAA4F0643AA303CAB0692C9AE0E2220E1A12758,
	Farma_ShowerOffPig_mA578FAA512564B7D6E4C0585E9D5661370AB466F,
	Farma_TurnShowerOn_m82C93B6F0E091FA157D47FB4F1A8180DD2967824,
	Farma_TurnShowerOff_m7AAC5AE5A0899ECEA0D757E8663E2360DF33A6C0,
	Farma_TurnPigDirtOn_m9B0D09575AD692A957DC8ABCCB788C32FF677E15,
	Farma_TurnPigDirtOff_mA99955E6B3EF85EECDF25287C3B662871D9D04BA,
	Farma_KaczaAnimacja_m240091D5932C0DA0985007E210BE0FFCE706001F,
	Farma_KaczkaQuack_m8E7552523F566E0DDBA5C300567D06AF01AE93E2,
	Farma_KaczkaQuack2_mA74132066F37577D6717643F46FA862E7973B4B3,
	Farma_Clouds_m211D72CCB4BBDA5F2BCBE095564968D204B8D74A,
	Farma_PoopPhy_m5596C7CACB887A1CD2394F83C181F6218005A7CF,
	Farma_OnTriggerEnter2D_mE6EBB9DDD373152663399B4D1BE0E3BAB48DB3AD,
	Farma_Update_mE5ADC9DDCE236660BD7E6D0E6EEAFD4BC3AB8468,
	Farma_OnlyQuack_m64617D10BE84CA0BD330DB034C60E97CEF4E5CDD,
	Farma_IdleGuard_m7FE81EEF419077DB512A02A1C929DB7CFD354392,
	Farma_Sranie_mB8784AD64B56C2978ABEAD793C18B762B0CFA0D2,
	Farma_DuckBeWalking_m9267693C46EDF887550281B9393C93F20262C01C,
	Farma_StopWalking_m22FFBFAA9475313770639A9072EC1E6A8607058F,
	Farma_PlayAnimationAndSound_mF77C3227CC13B622C0277CF3D4C770165BA0F8D7,
	Farma_Playsound_mDA608EEE996AD8F9EB3045B8AD739EF0FFAA3C77,
	Farma_CloudSpawner_m9A36F2032BF5EF949E92374094DB46BF7E064914,
	Farma__ctor_m043AF65FBE7FB0F6CAA9A002F5B8411359601EAC,
	U3CGetPigDirtyU3Ed__42__ctor_mB49FE40B445452922AAAE3B714BDC89340A102D5,
	U3CGetPigDirtyU3Ed__42_System_IDisposable_Dispose_m74817D9B14D305A0BEE2902797434982D76F1FE2,
	U3CGetPigDirtyU3Ed__42_MoveNext_m8AF86C3347C4D73DAE689E3A6DA2A7B80C16DAB9,
	U3CGetPigDirtyU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m146B1E212CF415CD7F6A674A37C049BC50D61B81,
	U3CGetPigDirtyU3Ed__42_System_Collections_IEnumerator_Reset_m7B8953424C740DFC61B6A67557C35EE8F9308C79,
	U3CGetPigDirtyU3Ed__42_System_Collections_IEnumerator_get_Current_m73E75654464F3C11502E5F0D7D9D2947BF388324,
	U3CKaczaAnimacjaU3Ed__53__ctor_m2617176B3502C128E92242C8CF36025E875469AA,
	U3CKaczaAnimacjaU3Ed__53_System_IDisposable_Dispose_m5F75ECF68302A0D5F018D8D8F929B8C254E756B7,
	U3CKaczaAnimacjaU3Ed__53_MoveNext_m5D1526DB5677BBE91274B617ED508D846481ACF6,
	U3CKaczaAnimacjaU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F7303792186D2AA16F3F9B725339CF496B1550D,
	U3CKaczaAnimacjaU3Ed__53_System_Collections_IEnumerator_Reset_m3B805F853BBB9CED96BA52C7E0438EED685E3941,
	U3CKaczaAnimacjaU3Ed__53_System_Collections_IEnumerator_get_Current_mC80061F54ACEF822CABBFAA4F754560586674B95,
	U3CKaczkaQuackU3Ed__54__ctor_m2D6B4E2DD8A84767208082996B50BCD8ADEE5B74,
	U3CKaczkaQuackU3Ed__54_System_IDisposable_Dispose_m9855F27E3A68FD419A3EA384F6E96A19149B6456,
	U3CKaczkaQuackU3Ed__54_MoveNext_m702E25D8076A3C15AED98149D08F2C2E2E30054F,
	U3CKaczkaQuackU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AAC50BB26A054A222D22B6C437A91E44639C962,
	U3CKaczkaQuackU3Ed__54_System_Collections_IEnumerator_Reset_mC868E63351019AC63F228991A9C8F3A3A3E36B51,
	U3CKaczkaQuackU3Ed__54_System_Collections_IEnumerator_get_Current_m6A59328FC5803F7E3C8C754AEDABE7C2DB1AE034,
	U3CKaczkaQuack2U3Ed__55__ctor_m93DF2E5960FE4A96BC70A4E4D4F85DF74A860CCB,
	U3CKaczkaQuack2U3Ed__55_System_IDisposable_Dispose_mEFFEAD3B0FA0695C95A0BF13FFE3C2077FE129EC,
	U3CKaczkaQuack2U3Ed__55_MoveNext_m75535C6AD7DB7D85EA2505FE9CAFAD5F567BCA1F,
	U3CKaczkaQuack2U3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m789E7837277DBC7FE6CA2A63444011AF983681C5,
	U3CKaczkaQuack2U3Ed__55_System_Collections_IEnumerator_Reset_mC2C047054D7D0BBC6692381DB558BE11C2F55D1F,
	U3CKaczkaQuack2U3Ed__55_System_Collections_IEnumerator_get_Current_m018BB9FA96CF9121B630EF4AC79BFD8EA523F584,
	U3COnlyQuackU3Ed__60__ctor_mEBBEE5966C566E3501B1D1D339957745A6354FDE,
	U3COnlyQuackU3Ed__60_System_IDisposable_Dispose_m310A8233FC40C468E60D21D976ABFF0AC8AA32E4,
	U3COnlyQuackU3Ed__60_MoveNext_mABCAA2CADD6FD8BD1694A99970E08D38BB18B2E0,
	U3COnlyQuackU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB304C4C84F152712B1AC7DEE0C9FF2294B8457CD,
	U3COnlyQuackU3Ed__60_System_Collections_IEnumerator_Reset_mFF4F9CECC9638ACAA858F1F531C3EBAB4BEADDE6,
	U3COnlyQuackU3Ed__60_System_Collections_IEnumerator_get_Current_m15A923DDAA38F3C5B56DD661FDC65B0EF594881E,
	U3CIdleGuardU3Ed__61__ctor_m96487948FEF28306398FC3DA95B5EFC27339DB97,
	U3CIdleGuardU3Ed__61_System_IDisposable_Dispose_mC7111443E3DE4454DD5BB5C8718318D6B1AAC215,
	U3CIdleGuardU3Ed__61_MoveNext_mF09EC4E2019D2C4F45B6820C8B00D96E372E1641,
	U3CIdleGuardU3Ed__61_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97A5B60076B74F47DCE50F086D960EAAD0255182,
	U3CIdleGuardU3Ed__61_System_Collections_IEnumerator_Reset_m972BACDB7FB4A07471B40088797815F4A784086A,
	U3CIdleGuardU3Ed__61_System_Collections_IEnumerator_get_Current_m681B247E1222B7AE834CCE89910B5EAB5BDFA32D,
	U3CSranieU3Ed__62__ctor_m64C94E581069D50EE1AB0493F17400FB48FBE50B,
	U3CSranieU3Ed__62_System_IDisposable_Dispose_m0BB17614988D6D763C7CAB272EC07EB3695FBE18,
	U3CSranieU3Ed__62_MoveNext_mFB00CEDE10A81946C3682375177ECF30A3DCF0FD,
	U3CSranieU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30E708A9B8C1B3F6C8B2480AE1D5A9C994DD9DC4,
	U3CSranieU3Ed__62_System_Collections_IEnumerator_Reset_m343B9367562C0FB0FEE40088FD29B0151B55BB52,
	U3CSranieU3Ed__62_System_Collections_IEnumerator_get_Current_mB6C2F38E44676BFE3ED013709925532BD114CBF5,
	U3CCloudSpawnerU3Ed__67__ctor_mCE351A97FAEC7EF96F238BC273C3E286DBBAA359,
	U3CCloudSpawnerU3Ed__67_System_IDisposable_Dispose_mF5FD5607C1D0C4D8EA875AEACAC2B1FE0C5FC239,
	U3CCloudSpawnerU3Ed__67_MoveNext_mCEE8B007D64AA96F4E7A37A8FDF488EA4EC501FC,
	U3CCloudSpawnerU3Ed__67_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m69F2956311883A8CCD3E6ED4E749B35E184FBD12,
	U3CCloudSpawnerU3Ed__67_System_Collections_IEnumerator_Reset_mA0D3934CA811780333417B9FE7FD07C741BADD06,
	U3CCloudSpawnerU3Ed__67_System_Collections_IEnumerator_get_Current_mA313638C0C638285D0A001731DEDAE82D4B9C187,
	Shower_Start_m6727CAC664411179BB97C3924FEC6FB8C6EA68A5,
	Shower_Update_m8B3EC134998A117F5E6572780947BADB0914DEAD,
	Shower__ctor_m415A59D003485490B4B5FCAE13B14E8E33E23306,
	dzwiek_menu_Start_m080567101E4266A79E0C7650B48C60733D91FA17,
	dzwiek_menu_Update_m3823500A86447E3BBA1574C68D2ABDBA67E78154,
	dzwiek_menu_Dzwiek_m70327F1D6A3CB3856CC6976BAD0EEB6A3C2B4EAD,
	dzwiek_menu__ctor_m2DBEB5735DF5D3366E2948FCA8F55775D41176FB,
	houseFly_Start_m2F3A71307BE8510E4B45B1A29E735DF2BF2BD9B8,
	houseFly_Update_m0D9969256F61795FB6452B99F7D7680247A1438A,
	houseFly_Respawn_m1FE140CE12C84DA513B02342F949E86D47049B72,
	houseFly__ctor_m1A29F0AA59758D0B80AFAE1E09B429E131501735,
	U3CRespawnU3Ed__10__ctor_m3B0C579967F2A710EE1E1FE7502DD1F641818781,
	U3CRespawnU3Ed__10_System_IDisposable_Dispose_m289F0A8221BBB97B2A8E31B05D7E01CB1EE85056,
	U3CRespawnU3Ed__10_MoveNext_m8EA08FF7682539BF0CE424278F156AB34131BC69,
	U3CRespawnU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9282D8642B3AA4971C0C9F500195DC60FF2CD3E,
	U3CRespawnU3Ed__10_System_Collections_IEnumerator_Reset_m9C7A142C08189AB774409AD9C34644188B9AD566,
	U3CRespawnU3Ed__10_System_Collections_IEnumerator_get_Current_m883D53CDF879576825485BE030596F20B140F45B,
	Kredka_podazaj_za_rysowaniem_Start_m499745688AD2CA31C64792FCE7030C93E3350832,
	Kredka_podazaj_za_rysowaniem_Update_mBCAFADA4133363090F3308071FE0DDA106056E2A,
	Kredka_podazaj_za_rysowaniem__ctor_mF9ED1F5B70B1CB8F0F32B47AB7C4BF5F5D1959B6,
	btn_back_Start_mCA753175302041C6B62F5D21022C92C47F90C152,
	btn_back_Update_m96C0273968EBD0E1143A7EFD03CB7C5D28ED3DFE,
	btn_back_UnlockBackButton_m0A8AFF7FF28FF433C3CB9F71BD251B3B1DD453EC,
	btn_back_Back_m55FC207B7DD99952915A74AB497A78256D4D36D3,
	btn_back_Backk_m57EA631C6647DB1D9B22CFB8F01BB6D96F19C41C,
	btn_back__ctor_m99372E6D017FBF32F4E4D221F90085ADBC64FFED,
	U3CUnlockBackButtonU3Ed__35__ctor_m4EB1F52D51129405DD86FEC2A022B100E38FA31B,
	U3CUnlockBackButtonU3Ed__35_System_IDisposable_Dispose_mDA546CDA479C54E92879D8962BC95CF28D1EC090,
	U3CUnlockBackButtonU3Ed__35_MoveNext_m32423314E54EE9ECA93184FA722CC9A21BF43144,
	U3CUnlockBackButtonU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC7C2BFA0C367BE6F6FAE6DA6FD3B5ED13BD51BB,
	U3CUnlockBackButtonU3Ed__35_System_Collections_IEnumerator_Reset_mFD30FB596079DC4545E5F4926B012EE74FF392B1,
	U3CUnlockBackButtonU3Ed__35_System_Collections_IEnumerator_get_Current_m68499FAC498A1537CB9F7B2AEB3A39B04A49A6AD,
	U3CBackkU3Ed__37__ctor_m1B6054C2B5FF3892C177104A90BF18DEAF8827E8,
	U3CBackkU3Ed__37_System_IDisposable_Dispose_m66B10252C0AAD9167A71708C66277E04EE5A9A3F,
	U3CBackkU3Ed__37_MoveNext_m28D4E8364E0B67043BB26F151C7F3259B5BE6E26,
	U3CBackkU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A6024A86E7FC401931E49B45E16B587BA73D963,
	U3CBackkU3Ed__37_System_Collections_IEnumerator_Reset_mACA247585B71C1B32B6BAE7EAAD0A6161CC058A3,
	U3CBackkU3Ed__37_System_Collections_IEnumerator_get_Current_mEE4AFE53D0DE2E61FAAEAEB53BE4701BB287692B,
	buttony_lvl5_Start_mF678A8599187804E0C2B3356DEEB1173DD6F3BAD,
	buttony_lvl5_Update_m4929CC1D8D9D625B35E8F3297B8E5B8C4E0F327D,
	buttony_lvl5_Sprawdz_m2B26891B1288221F3854FE9E2C57775F1FD2A176,
	buttony_lvl5__ctor_mD55963CD823166CC578D76F6AA26740F5E80A137,
	CanvasScale_Start_mB05218DAB56F7A8B6DDCD32846F4F1BD40385831,
	CanvasScale_Update_m236EB5A668AC0E8A79C6BFEBABD37D21FD234D4A,
	CanvasScale_HorizontalDevice_m6FE1990C8EA28C0B0CE084DBDB647894EDE94638,
	CanvasScale_VerticalDevice_mA777EECCEC960E58AD4AE8F77FEEBD13C64C9F60,
	CanvasScale__ctor_mE22BED1D3D4A7211F12A8FACDF29567CD9D0606E,
	cykl_dnia_i_nocy_Start_m8B253FC445E432DBE39CFED7B3799C330F8357FE,
	cykl_dnia_i_nocy_idz_spac_mB38FA7E7E31CB5CD706089FEE677F1224C7576A1,
	cykl_dnia_i_nocy_idz_spac_enum_mFEAFB775F41CFA25B7B05C999CD3CAEB2D080167,
	cykl_dnia_i_nocy_swinka_idzie_spac_raz_jescze_m4664894337E08B10F61474478BCFF6B31186D90C,
	cykl_dnia_i_nocy_Wybudz_sie_mECF206A0C21761DE39580D24A1B7ABAC2431EA8A,
	cykl_dnia_i_nocy_Update_mE7DA20E82E5B8E495CD45981152F9684B80CC8FD,
	cykl_dnia_i_nocy__ctor_m24D3DAA21FB11F1E1056FC1CFF5C28A4ADD7D91D,
	U3Cidz_spac_enumU3Ed__7__ctor_m5D6B838209E22637BBE8544686F70C27A5A417B8,
	U3Cidz_spac_enumU3Ed__7_System_IDisposable_Dispose_m003C84EAF32133715C41D41D4A32EA2AD24FAA39,
	U3Cidz_spac_enumU3Ed__7_MoveNext_m48562FB96E85EE77EB61A0DC519F96749B9C515C,
	U3Cidz_spac_enumU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCF5ADFBCBB8424E6B17F3E44E4FBAC2DC6A02C5,
	U3Cidz_spac_enumU3Ed__7_System_Collections_IEnumerator_Reset_mC93127A9B79956FD2D9AB1BC03B5A8DE3919C7DA,
	U3Cidz_spac_enumU3Ed__7_System_Collections_IEnumerator_get_Current_mF753B3994F9FF1FF11305D356A3950A94AEE9A02,
	czujnik_zmazywania_Start_m3C2E94F8702D01E63AB653F9E39972926045E13B,
	czujnik_zmazywania_Update_mB87AF31AB7B7C5AE4B07441A9D3600F7F1579AE5,
	czujnik_zmazywania_Rusz_Sie_m2B7D864D9D152E2783DD82A5355A89E761B944D2,
	czujnik_zmazywania__ctor_mA5A687031A79F96E207538B38465EDBF2DBEF7FF,
	DeviceRotation_Get_mB00C047B42EC3CC1E86A59A5B1124A4E746BB463,
	DeviceRotation_InitGyro_m8DF021A40F51C8D846091391C5D9F81A36F15963,
	DeviceRotation_ReadGyroscopeRotation_m35716EB5855BE7AC162C38EF02927A45DCABEE90,
	drzewo_Start_m0E372651CE5C25C82CBC4B765F2755BFB626A329,
	drzewo_Update_m2618F36F36CE694B68C27F79B7F950000FFD07A8,
	drzewo_Zabrano_jablko_mC15473398DCEB793AEAD2FBDB7F79472281F747E,
	drzewo_Odlicz_m638CBE07B044AF09BEF6E2FC402A87F60F9BC4C4,
	drzewo_Animacja_drzewo_m2DAA79739E7C73E73D8E65043AA2CA2BD1878325,
	drzewo_Spawnuj_jablko_mCD0064A2F038DB03C21CD2905F77412AB8775105,
	drzewo_SpawnMoreApples_m5C23DF1DDA1CE5D2DB0F36C79CABF65826EF0B12,
	drzewo__ctor_m0ED45DCB4771306392AD2AFB63A429BFEFFA8883,
	U3COdliczU3Ed__8__ctor_m3CB3BAD1830F2FC16E422581F028FD6B83D59561,
	U3COdliczU3Ed__8_System_IDisposable_Dispose_mB868BEF777A34127A58F395D450797E602BF8083,
	U3COdliczU3Ed__8_MoveNext_m6E48206A6EDE13B6BF33A969B5C5FE7025A7FA99,
	U3COdliczU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28C38F7F11AB6624F6AA5BE0DBFD0492C8847C9E,
	U3COdliczU3Ed__8_System_Collections_IEnumerator_Reset_mEF0E56934779B56E5A90545C192B318312393AE6,
	U3COdliczU3Ed__8_System_Collections_IEnumerator_get_Current_m8EC19BE3A0D7FEA75738411F4B75F359200AA728,
	U3CAnimacja_drzewoU3Ed__9__ctor_mB28D52DDC6BD99AC264AD98133836A45F4C97A44,
	U3CAnimacja_drzewoU3Ed__9_System_IDisposable_Dispose_m6FD1BAF7E322064A45C8CCB0B64EC1DD35943166,
	U3CAnimacja_drzewoU3Ed__9_MoveNext_mEF5D7FF7AF756EC3B5F3F18118B400845CC7FA3A,
	U3CAnimacja_drzewoU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79B8127C8BFDB84596128BD00761AB38BB50C3BC,
	U3CAnimacja_drzewoU3Ed__9_System_Collections_IEnumerator_Reset_mC5858A9BD1F3FEB9CC2F0A70B8D56B7E05F93380,
	U3CAnimacja_drzewoU3Ed__9_System_Collections_IEnumerator_get_Current_m970CEC3D0B5B06CF60762AEA3040CBB314D60A59,
	U3CSpawnMoreApplesU3Ed__11__ctor_mF18D21763E6CDB8163566637F4D73969C6529E03,
	U3CSpawnMoreApplesU3Ed__11_System_IDisposable_Dispose_mF7BE9DFDEF778BDAF01FBE2ABEA5AF05EEC92C0D,
	U3CSpawnMoreApplesU3Ed__11_MoveNext_mEE0489B7FF74872AC95C7FA3B65546499D399BA1,
	U3CSpawnMoreApplesU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m744BDF6BA756F32DE94D272FC56C628D59F5F95D,
	U3CSpawnMoreApplesU3Ed__11_System_Collections_IEnumerator_Reset_m371A9A8EF3B7A7FB439E709E42342CA14E0B212D,
	U3CSpawnMoreApplesU3Ed__11_System_Collections_IEnumerator_get_Current_m68BE5A46D79CCFAF55F26B341540AF0687FA2966,
	dzwieki_idle_Start_m54A32573B51758F51B62ED88FCB286206014F510,
	dzwieki_idle_Update_mE4B59C161969ACD94EAD85AEE3B827270C14C715,
	dzwieki_idle_Odtworz_idle_mB5527B85A55D03A58A1CDAB381712795F8EA016F,
	dzwieki_idle_OdtworzTrzaski_m26EB58216E65B28A60675FBED37C42148F031476,
	dzwieki_idle_Wyldziw_m7424D26DFF9F4D44F5DCC4FDDA2AF97FE8EF36C5,
	dzwieki_idle_WlaDziw_m6FFF88CD1E54E75278D63DC0E02638AD1A137400,
	dzwieki_idle__ctor_m639F6DEE1233C719E8FD70C80205AC0C8CE11048,
	dzwiek_do_telefonu1_Start_m04C7EB70A85B225F5E4EA3B13E860C4D671E3998,
	dzwiek_do_telefonu1_Update_mEEA48A16E948F576CA9AD48CC4BEF96018AEAA68,
	dzwiek_do_telefonu1_Odtworz_idla1_mE145109E09E957626483299F6759537571BE91B6,
	dzwiek_do_telefonu1_Odtworz_idla2_mCECFD4C47E83D0011F751C06AEF139341D1837E3,
	dzwiek_do_telefonu1_Odtworz_idla3_m8262D2E4CF7795E6A6DD3B770DE6CABB35EA84B1,
	dzwiek_do_telefonu1_Odtworz_idla4_m690B33B1469609B059AB393037A323959CCF3EE6,
	dzwiek_do_telefonu1_Odtworz_idla5_mAD9F20321B6316797AE74EAE80DD78F954BD3F2F,
	dzwiek_do_telefonu1_Odtworz_idla6_mB03B4655038AA4E7B68DDE58A6B0125CCC8FC350,
	dzwiek_do_telefonu1__ctor_m2E4986FCD2BACF4BF4347A674856D1433DC73E15,
	GameHandler_Start_mBACFF72C246BA6AC459E0F1BD27C84580141098A,
	GameHandler_Update_mB7AE5E1CDAE1D6BE50875EEA0CC1B20BB06708C3,
	GameHandler__ctor_m838290D17C46D189D8A77B3D7CE6CE4C6D9EC485,
	gyroscope_to_lvl_5_Update_mC2A831EDECC6970D761FAFA670EB4825CFAE7EDE,
	gyroscope_to_lvl_5__ctor_mB575CF9E24EBA48DC6E0890F86394F6F8DB5A984,
	Spaceship_Start_m0A9F253FF8660210DD091A63D498DBA96B1F1D63,
	Spaceship_Update_m00663D91B04315825C8BCEB18C07E338356EE4CD,
	Spaceship_FixedUpdate_m278D0E4BB58DF1FB28C63C1A008B751F5EDED093,
	Spaceship__ctor_mBC65712F83F75C0B669EC6FE9A3A34F5A5C45C81,
	jedzenie_dla_zwierzatek_Start_mFDF2B9D5C972BDA3953E1CE6E3B9AD93D7D4560C,
	jedzenie_dla_zwierzatek_Update_mEB1024B50D57F8001B9DA698D10D19218B40E7AA,
	jedzenie_dla_zwierzatek__ctor_m91419CC850F5E265FF87C92D3B1AA9895B92FB82,
	kaczki_Start_m4B33E10DF979E0B0DC64B58CB0EE9BCE89CB7E7D,
	kaczki_Update_m8FCDCA00125D1EC79359C3BBB64112F0DC3F75EE,
	kaczki_NieAktywny_mBF5B604B682FEFAC24C4617F35B31C8EC61EF93D,
	kaczki_Aktywny_mEAA8E59BC9082285D35BE5E4B101742B548BD268,
	kaczki__ctor_mCA35DF1BF0C3185CC4F19D4AA9752271DB8A0A45,
	karmienie_konia_Start_mBF4571755E0C4EF381DE771522EDFB065EA300F0,
	karmienie_konia_Update_m70C5BFEACFAA326C55905DB58C23DA900DD37284,
	karmienie_konia_OnTriggerEnter2D_mA6D7D6366E1F319098D798527208A57C6E445EC6,
	karmienie_konia__ctor_m4A914A60DF05107701AD71BC73FF9C271D31DC70,
	klikanie_prefab_Start_mDB2C4EFBF580686164F417AE81C98733864F5D32,
	klikanie_prefab_Update_mBFFA9AE0D94D097E24116E73755677DAAA5EDB94,
	klikanie_prefab_OnCollisionEnter_mAEE3D42E1DC5D61C1ED2ED36BE790A2627362A29,
	klikanie_prefab_Zniszcz_po_czasie_m5A84361AFF638AA0A7F0A2E204246406F69CFF22,
	klikanie_prefab_Spadaj_m183EC520E543210598B0B95A85321E1CC39B77D4,
	klikanie_prefab__ctor_m4345397C101CF5515983837D03902603BA6050C3,
	U3CZniszcz_po_czasieU3Ed__15__ctor_mA37948393F40A74B0EF2E78D422B4FD88EAACF04,
	U3CZniszcz_po_czasieU3Ed__15_System_IDisposable_Dispose_m90FEA5AEF589C77FAB08418CEA02DE7538991746,
	U3CZniszcz_po_czasieU3Ed__15_MoveNext_mE3CE8D95782EBF0BE79068292554959F189B90C4,
	U3CZniszcz_po_czasieU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4509787946F6A68D99797A120ECBE6C4B9CA91BA,
	U3CZniszcz_po_czasieU3Ed__15_System_Collections_IEnumerator_Reset_m1950218FAC29F61E4640C88FB29E29806CCA0C3F,
	U3CZniszcz_po_czasieU3Ed__15_System_Collections_IEnumerator_get_Current_mF644E4A7DA1FCA9132FE8C9C6378E1CA0A81BB18,
	Klocki_Start_mFE5C63F826BF7980B47FB19B35742B6A13005932,
	Klocki_Update_m2174422984EAE5B10621384D902E551CA7858045,
	Klocki_KlocekClick_m66797DB873A881C8034219AFEC4981BDB996F1A4,
	Klocki__ctor_mF727C462145C8D9F96CF5ABEDD8A10154A8F0E18,
	Klocki_U3CStartU3Eb__5_0_mBCC2E3875DBBC0A3B17587FF5C8E96C94AF64448,
	Klocki_U3CStartU3Eb__5_1_m6791B4CA555C977E470452126DD609E16B1EF76F,
	krawedzie_collider_Start_mFF66ED379E6047D3E42B983FA930155BD80B51CB,
	krawedzie_collider_opoznienie_krawedzi_mB327CEE591C7626D2F2A4C9A59D92A05ADD9251E,
	krawedzie_collider_Update_m23DB5201B2AADD28F8D5359CB5EF4F796C753ECA,
	krawedzie_collider__ctor_mB1FB7EC78EE2F1BCEDEA44439199D04EC0851FF2,
	U3Copoznienie_krawedziU3Ed__6__ctor_mA0D594EC936CDE5E8A05B1BB83D0BFBC75E7F244,
	U3Copoznienie_krawedziU3Ed__6_System_IDisposable_Dispose_m179E47E8B0BEC540A4FAA81120D65E5AE14D9C6A,
	U3Copoznienie_krawedziU3Ed__6_MoveNext_m3064BF9CBCCFE305BAE43460994F9B837CA697AB,
	U3Copoznienie_krawedziU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2FD2D66EB7D0607D7E89ED4830C83B17B7663D5E,
	U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_Reset_m3FD560B8195725C674BAA897E94E2644A8DB9A74,
	U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_get_Current_m073E59EB7950B6C6356A0444DFE912964FCD3644,
	Krawedzie_collider_V2_Start_mCDD688F67CF3EEC82B152DC156F42D3A88C355E5,
	Krawedzie_collider_V2_opoznienie_krawedzi_m8385487AC27E8D0955CA7F8AA71E748BC8D51748,
	Krawedzie_collider_V2_Update_mB48818F4D3E942B37A8072D30454FDB04F1B236E,
	Krawedzie_collider_V2__ctor_m02C4BCBA2E71113ABE3599AAF394A144FA4A5C41,
	U3Copoznienie_krawedziU3Ed__6__ctor_m7BD0AB0A278448657BE58BEDE265B817421633E7,
	U3Copoznienie_krawedziU3Ed__6_System_IDisposable_Dispose_mEA55E47720ADFC2FDCB7213AC2D96D0CBA5AF2E6,
	U3Copoznienie_krawedziU3Ed__6_MoveNext_mABB5A4EAA9944882774A32BD5EF9F413DB1AB50A,
	U3Copoznienie_krawedziU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DADFF4BBF599E77610CD2DA833BD8AFF52D0F12,
	U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_Reset_m33B09EE0B676F23A07A74E40E98FFEFBF010BAD2,
	U3Copoznienie_krawedziU3Ed__6_System_Collections_IEnumerator_get_Current_mD092C451B9A65857F30FA48379079E7B8B681EF4,
	Link_Go_To_www_m2F5B8F0296E7C6D2FEDD9C11CBA8ED985E682C33,
	Link_Go_To_youtube_m90432F0F072DF32A82747AB2A947B856E9E81CE4,
	Link_Go_To_facebook_mFE113FED7BFA76296767D43F297CE9726C2A9104,
	Link_Go_To_email_m55E776E59440F040482DFDF872F86872B5493C6D,
	Link_Go_To_opinion_m1A370C0A319A01DCDEE27B4DD6AB7F946C7808C4,
	Link_SendEmail_mC65FA6744F4ACFA24446F6AEAB3793219D7346BA,
	Link_MyEscapeURL_m5C129C77D493C95A78BB926EBA53D7D23E2DBEAE,
	Link__ctor_m63C9F1B9DEE13922D1EE2C3508D9BAE07D8324F8,
	Menu_music_Start_mB0D13B5C193F378443C8DA867BE0BFAA79E0603A,
	Menu_music_Update_m27F976A9C8F6AD1694BC252F3367185D6C326223,
	Menu_music__ctor_mDA83BB47DA56FF302B57142C9FDC0F00AB8C46D8,
	mucha_Start_m5E26A863861EDC0B4ADD78DDC568C936DC24C0BE,
	mucha_Update_m3B333A86D636C8B5AE66D1B6F679A42A0BA7B07D,
	mucha__ctor_mE3D432B24527738D07FD450ED18D3E6C25A32BBE,
	mucha2_Start_m3776ECE66C9454E12E225630D3825B95A9634C7F,
	mucha2_Update_mBCBEE966AD42C0936E8C459427CC6AD8051E92BC,
	mucha2_Respawn_mB982A6A723153C512A5AC7ABB3BE31037B39CA5C,
	mucha2_Lataj_dalej_z_tym_tematem_m4DD568B7AD73E19ACC6DA4AC5D09CCE42903ED31,
	mucha2_MoveObject_mF3E870A971D0CBBFB394F3320FB4CC5B9476836F,
	mucha2_RotateObjectWithAngle_mFEDEAF40863E79D357A5029D8E3FB9AA8698AD8B,
	mucha2__ctor_mEFDA35A5567F13A4A30302AB121CD9E4887CF827,
	U3CRespawnU3Ed__13__ctor_m7221B2DBEB8EA5074B7B4008F53CBD5898709740,
	U3CRespawnU3Ed__13_System_IDisposable_Dispose_m739940A8C5E54BDCD776891CF1C02618D4762F83,
	U3CRespawnU3Ed__13_MoveNext_m9AEDBBD7B1AD7D3F738A495221522907435C41E5,
	U3CRespawnU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D8044D9D1FB01E236B2AC83160B0135010A100,
	U3CRespawnU3Ed__13_System_Collections_IEnumerator_Reset_m48B8F484BF3A17C6554F390FA3A695DA56F16BC8,
	U3CRespawnU3Ed__13_System_Collections_IEnumerator_get_Current_m1175602C6FDA4538F35192EE4F750D7E36A75780,
	U3CMoveObjectU3Ed__15__ctor_m6A73DD809FF82ECC55E2A4731EAF19A9AFFC1814,
	U3CMoveObjectU3Ed__15_System_IDisposable_Dispose_m06186EC61F3A73E5DD9061E549C50861051AD395,
	U3CMoveObjectU3Ed__15_MoveNext_m013031E3D7543ABABC724D293776D81721E7573B,
	U3CMoveObjectU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF76C1B5E7291B43E48384D4823CC7493EED3A30A,
	U3CMoveObjectU3Ed__15_System_Collections_IEnumerator_Reset_m0D1F7A484A4B2166F54D8085B374435BCB58CD3B,
	U3CMoveObjectU3Ed__15_System_Collections_IEnumerator_get_Current_mD6415A53F1E2DC3371A9244D31732825B4D75BC8,
	U3CRotateObjectWithAngleU3Ed__16__ctor_m5A39C35445F1E2DA003E28390B4142F54341C526,
	U3CRotateObjectWithAngleU3Ed__16_System_IDisposable_Dispose_m8C1234B8DAF1A6F86C336AC7CA01FE2819E1E4DA,
	U3CRotateObjectWithAngleU3Ed__16_MoveNext_m79615F5A8AD858417A57150AFD416615DAF26EE3,
	U3CRotateObjectWithAngleU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99B208132F4FF6CEFE5C0046C6DF71F835D9D319,
	U3CRotateObjectWithAngleU3Ed__16_System_Collections_IEnumerator_Reset_mC67DF33635760FD8DD19570DFF420DAA974590B8,
	U3CRotateObjectWithAngleU3Ed__16_System_Collections_IEnumerator_get_Current_m047E2EBF9CEF077172A650A3F857ABBCE19FEE99,
	myszka_animacja_Start_mB09E99364416FCF770F7AC2B4266CA2838E87E01,
	myszka_animacja_Update_m4C9457DC5D9F3B86F760943864B66D988F6092BE,
	myszka_animacja_PlayAnim_mD0ACF7694703CF7A1DD26D93CAF703C5B1014B59,
	myszka_animacja__ctor_mB544EC87FB8ACCB1CB4210EC6360F790E1930C57,
	Objects_from_lvl5_Start_m3B24B276A540B24992DEE6571AA6F20B0BB36D94,
	Objects_from_lvl5_Update_mF38EA7F048B8C6A71D6E08FE25FBD31E6847E413,
	Objects_from_lvl5__ctor_m8BEEDC913201C15BFFAE293BD1989B7AE8F6A77D,
	odpal_plansze_Start_mA45F8DB80F99F1D942116B3FBD5C4286E11F3332,
	odpal_plansze_Update_m547550AB30E3B9B79A3B987ECAA39A4F597C2F96,
	odpal_plansze_Uruchom_plansze_m0CF289E2C3534E8BFF21BAB04E4FA50B9CC9D476,
	odpal_plansze_Odlicz_mDE0C9ED7C368B363D84031A2B104F815DC09CC5B,
	odpal_plansze__ctor_m120478E6D52E3636D7D26FFF898569BEB7F2A0E4,
	U3COdliczU3Ed__8__ctor_m6B3113967CC6B2934131BA7B975046F21E7A07B6,
	U3COdliczU3Ed__8_System_IDisposable_Dispose_mA5D3EC9CC6E82406A9A96A6616C1973FC638AA6A,
	U3COdliczU3Ed__8_MoveNext_m3A3E55714DFAEF4B1D759B3EC396ABD7E1E25967,
	U3COdliczU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2FDECA5879495986AABD650447D669E7F3F426AB,
	U3COdliczU3Ed__8_System_Collections_IEnumerator_Reset_m4004F3FAB175E8CA1E6104A209D9F0E6CB473A47,
	U3COdliczU3Ed__8_System_Collections_IEnumerator_get_Current_mDDDFBC256E8C081F5FBBBA63393C505E992FBCEC,
	Odpal_planszeV2_Update_m03E02873B5F5241B29F2AE3A08A9928DB8FA277A,
	Odpal_planszeV2_LoadNextLevel_m26004B29A1959AE53BD8B154E45CA8809A07A71A,
	Odpal_planszeV2_LoadLevel_m6DB69C8017FD18963F72BA197C0D15E3435EF0B8,
	Odpal_planszeV2__ctor_mACC99602A624D2F56ED7C6B98625CCBB50D6B2FE,
	U3CLoadLevelU3Ed__8__ctor_mB596DAD6212B617D1715851A8ACA0E70A3FE0BE7,
	U3CLoadLevelU3Ed__8_System_IDisposable_Dispose_mA30C6506EAE92BFB3BA5691D88852C2D6A374F2F,
	U3CLoadLevelU3Ed__8_MoveNext_mAF404D286477FB537CEFA01CE147007AFF12751B,
	U3CLoadLevelU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F6580E9BAF52F06020214F4046829B7D2F13F25,
	U3CLoadLevelU3Ed__8_System_Collections_IEnumerator_Reset_mED99FC4B401A2366F8930DE4962D449070D74476,
	U3CLoadLevelU3Ed__8_System_Collections_IEnumerator_get_Current_m1ABABDA75A7A1F857BFDDC216E22987C64CF13B5,
	odtworzAnimacje_Update_m910AEFCAE66412001CC9D84062C4B811032AA066,
	odtworzAnimacje_Animacja_m7A144BB50B07B08FB0541CA3368E3FEB4E0034C9,
	odtworzAnimacje_powrot_do_idle_mF431517CA96619460CB2C0AE656EA64A5C2FAED0,
	odtworzAnimacje__ctor_m2A7437ADC31F1C462D68CAC4B48A0303B1BC97C1,
	U3Cpowrot_do_idleU3Ed__10__ctor_m5CEB621A9AE536C33AB16C17EA2B27F72B02EB0B,
	U3Cpowrot_do_idleU3Ed__10_System_IDisposable_Dispose_mB2F97F7947FACBD5567E6961ED7B9181C0F2456B,
	U3Cpowrot_do_idleU3Ed__10_MoveNext_mC8F8871F706F26F4599851C8AECAB5889B92E84B,
	U3Cpowrot_do_idleU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F41DE2279C2CDA1894F5AD423463BE47FC6B38B,
	U3Cpowrot_do_idleU3Ed__10_System_Collections_IEnumerator_Reset_m06D3A3A3C026141B5EBB96B3BD2BCAAA861CE22A,
	U3Cpowrot_do_idleU3Ed__10_System_Collections_IEnumerator_get_Current_m47744200FAD3336CEE8E36BD126F239BA8C84B9D,
	OdtworzAnimacje2_Start_mC082560229BCB25D7C5461F8FCCE81EA1D0D5A25,
	OdtworzAnimacje2_Update_m24BEBD55E0DF87B081DA41813CB674FAD610BBF5,
	OdtworzAnimacje2_OdtworzAnimacje_m7E1B5C0534D386489AD98CAFDAFDB4BBA5610788,
	OdtworzAnimacje2__ctor_mE0CF5BD3C0C0D3261DE29AA6DEB443E32550E3DC,
	okienka_Start_mA9DB420C76B0DBF9FB4B6764D824175863320F0B,
	okienka_Update_m5565A626C967B3BE8662C2C60D4BFE100654D658,
	okienka_Wlacz_okno_m6299C96405851DB683C2B7403E1105BA04536E4B,
	okienka__ctor_mCA6E61DBEE32CFF5F56264E1AD57F10993B9B36E,
	parental_gate_Start_m96E8320A1BD9D5AF5531DA5FD0C30EEDA635F2AC,
	parental_gate_Update_mAB8C66B2B514C1BE5F7E498B8DEA52000D1DFB96,
	parental_gate_Zamknij_mE594118D08C3EBD69DB07474987CAC6A1EE6500F,
	parental_gate_Otworz_mD7AB6FAB9EE97A52CF5ECE5B3B782AB27386448A,
	parental_gate_Sprawdz_wynik_m669DC6CD8B293F42BD173FE8BA1B7E7CB081406C,
	parental_gate__ctor_mB00F2D4CA08CA2E5B391279101E0D020DC9780A7,
	parental_gate_U3CStartU3Eb__22_0_m6B487015790BE472B0848A94C9C3660D347EF120,
	parental_gate_U3CStartU3Eb__22_1_m1D80F37BAC457C070A164484B1311765353B71BF,
	parental_gate_U3CStartU3Eb__22_2_mC848EAD591EDCF810559D3F9168C7E430985AB38,
	parental_gate_U3COtworzU3Eb__25_0_m3C33A3956E2B1F4A2E897C947AF647B5B2A02EC6,
	PlaySoundOnClick_Start_m6B8273A1DB35FA786419812A67A57444344D40C0,
	PlaySoundOnClick_Update_mC24C2DC8E5E79344214B1366617DDA182B013721,
	PlaySoundOnClick_PlaySound_m52AF3B9B5A9678202851B90C3CD50525A5B5C767,
	PlaySoundOnClick_PrzyciskPilota_lub_back_btn_mB881DEDB02FB66427DAC9575696A0CAB8B298030,
	PlaySoundOnClick__ctor_m7C021FAC19D375FBB148D9C8AD73686C1FCD430A,
	podnies_Start_m21CFFA4EE32C10476E3EDCAE367434A554D8DE9A,
	podnies_Update_mD87A8028FE4EC32A4F087E8CA835229B6F41AD14,
	podnies_OnTriggerEnter2D_mA6F02D218EF77C0F4092C136F994665E26C19CBE,
	podnies_Restart_warzywa_mE10955312F516A60988D9936C276BF437C17AC24,
	podnies__ctor_m3EEBD85DC44A123E0738C43D9113C3B3E26783D9,
	U3CRestart_warzywaU3Ed__12__ctor_mF1AB9E5C6DC52B68883CBBCAA408D6E1DF780457,
	U3CRestart_warzywaU3Ed__12_System_IDisposable_Dispose_m1D8CD81E218EDEE9B6F07DFA95F556D3ED2FF263,
	U3CRestart_warzywaU3Ed__12_MoveNext_mBB2F6CD7CFF1CA678119E615DDFC959833EB5B46,
	U3CRestart_warzywaU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m766E446A1CF6C35AC51B51985D90B73521834644,
	U3CRestart_warzywaU3Ed__12_System_Collections_IEnumerator_Reset_mBDEEFB3CA734E98B28939C798F28A4AA0DDD13D8,
	U3CRestart_warzywaU3Ed__12_System_Collections_IEnumerator_get_Current_m5000C188344D11B0DE46C54A46A6DA9A3146DF0F,
	pokazz_przyciskiOnStart_Start_m7A924BCF4289BECD4F6990B29F902447CCDC9625,
	pokazz_przyciskiOnStart_Update_m61195435C4DC26E60382D950768F69FCB0636636,
	pokazz_przyciskiOnStart_pokaz_m68DFA91ADA8A4A26B0DC63110C6F1BFC42A1B82A,
	pokazz_przyciskiOnStart_pokaz_raw_image_m6112D162DA29C9173F7CE2D62F640CB8EAFC3C88,
	pokazz_przyciskiOnStart__ctor_m45A621E0A9DF1DCEAC7EFD85EE44B6B1CEC594D5,
	pokaz_przyciski_Menu_Start_m5263EECD2A3DDFF5781A3015C41A27ED610BC5C6,
	pokaz_przyciski_Menu_Update_m6079C483A245726783F7458B2E0CFA879618752B,
	pokaz_przyciski_Menu__ctor_m3F144D8BB36E4606C5FC17CCF21A49D0312A4436,
	PressHandler_OnPointerDown_mE4A3AB7C45DF4246DD501D809A6EBB6AE2E41FD5,
	PressHandler__ctor_m0CD7508CE3B1FFF4EFF0733C5212B9B305274F95,
	ButtonPressEvent__ctor_m516CD67C95D008E473BDF03C87FB59B4A44ED431,
	przejscieV2_Animuj_m2C96E4CEA4F32BDC81C9B3B46B396F93B8EE61FA,
	przejscieV2_Start_mEB40F3B7E6C59FFF69DE12BE49E2B3FEF90D334F,
	przejscieV2_Update_mC6A5CC69DA9261742CEB1D0345C2A513D243FCA6,
	przejscieV2__ctor_m50E4A812D98AC7FCD9D9DEC2D45C45763DFA0B1A,
	przerwij_zzzz_Start_mAD1B60F89FA817C3F9730910E5DBC576D36A856D,
	przerwij_zzzz_Update_m08500400F96D7A46D7893BF2A6DF1BCC851238FC,
	przerwij_zzzz_ZwierzeJe_m2F0E8F4221EF6BFC88CD1C090E9CD7420615EDA1,
	przerwij_zzzz__ctor_m705011F5C1120DF9CD15834FDDB7C3526D9AA813,
	Przerwij_zzzz_dla_kaczek_Start_m7A39D7F6646ABC39E22EFA56741083738D2E70B7,
	Przerwij_zzzz_dla_kaczek_Update_m65A73BDD3C87D2EC7103FD3C762EA79047FB5ED0,
	Przerwij_zzzz_dla_kaczek_Kaczki_Wyszly_na_Zer_m777E9150AE287C2814857F95835DE98B6DD27804,
	Przerwij_zzzz_dla_kaczek_Kaczki_wrocily_spac_m580624FB77F787658A521F2C4D603B0E935CC227,
	Przerwij_zzzz_dla_kaczek_ZwierzeJe_m4EE985D0244809797BBAEFA107ACFFB971520BA7,
	Przerwij_zzzz_dla_kaczek__ctor_m2532B4FC4B674EE7F0038F49DE0F84951C374232,
	przesuwanie_suwaka_Start_m1CAA1F55F303576A4C3E1034CD8F567AA64A3629,
	przesuwanie_suwaka_Update_m6406A5E7D3A49B6A25EFC7879655801945C22547,
	przesuwanie_suwaka_OnTriggerEnter_m342A2246A14A30561E14B75EA0A4E117528866F2,
	przesuwanie_suwaka_OnTriggerExit_m0C5E6F6C6A1185D033B7D4213CAA9F45AA340715,
	przesuwanie_suwaka__ctor_m89C755292CD045B6E5AF21CFA666A9A4F884BAEA,
	przyciaganie_jedzenia_Start_m43407BC9D457AF23A0BE897FBEE8227DB815918E,
	przyciaganie_jedzenia_Pozycja_startowa_mD6CB6314ACA01403F369749CA4E631DA0D90BEDE,
	przyciaganie_jedzenia_Update_m0A1F301953FD7918D89F832C4EDBF00E86B8BEBE,
	przyciaganie_jedzenia_OnTriggerEnter2D_mA7EF16C871E568D73CBB09FB70D41516EED8361F,
	przyciaganie_jedzenia_Zniszcz_po_czasie_m82FB0FD1D93ED0CEAB424CC957AC2A6DD395FE7E,
	przyciaganie_jedzenia__ctor_m0036C2100756BD58ACD6B163C87DC3D1710B6934,
	U3CPozycja_startowaU3Ed__11__ctor_m201ECCAB449ABB89C7931F314DD1D6BCBD28F30E,
	U3CPozycja_startowaU3Ed__11_System_IDisposable_Dispose_m6C63FC14096F35CA6455BE94E2A0DDB664FB55C2,
	U3CPozycja_startowaU3Ed__11_MoveNext_mDDE893C2B1A9584F01A4109E08CE2471BED9A5DC,
	U3CPozycja_startowaU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DB4FB19D82AE415A9F5192AADB1D16CAE4601D3,
	U3CPozycja_startowaU3Ed__11_System_Collections_IEnumerator_Reset_m1485F63ACE5D941308FD469715D2A91681405A1B,
	U3CPozycja_startowaU3Ed__11_System_Collections_IEnumerator_get_Current_m853BD97B3D6FC858A2C4B5A0357FD9134B0AB30E,
	U3CZniszcz_po_czasieU3Ed__14__ctor_m1B56EC847353AA29A252B447926251A2CA09B9FE,
	U3CZniszcz_po_czasieU3Ed__14_System_IDisposable_Dispose_m5BE45F6D01D80EE040BEB6A79038639E58942156,
	U3CZniszcz_po_czasieU3Ed__14_MoveNext_m209AC42C341B4A20EC8A05CAE6458C5CD6EA3106,
	U3CZniszcz_po_czasieU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BC10E8ABCBA1701B7128D26756630D84CAA3940,
	U3CZniszcz_po_czasieU3Ed__14_System_Collections_IEnumerator_Reset_m0B0CFC02FE002B2B81806AC0EA61E4FC46DD0352,
	U3CZniszcz_po_czasieU3Ed__14_System_Collections_IEnumerator_get_Current_m120436A80C0C99D662F02B24DAF984C651E33645,
	przycisk_dla_rodzicow_Start_m1105D82BEC6D25E1BED410D40B523A6B090E1DB7,
	przycisk_dla_rodzicow_Update_m9F21BD4FC0E86FFA9853185B01B655BE8AE387E4,
	przycisk_dla_rodzicow_Ekran_dla_rodzicow_m3A3050B88584A51D853531707A983A79C3552BE8,
	przycisk_dla_rodzicow__ctor_m65C38A4ABE14040399D6E1AD7BC529F8B660133A,
	Raycast_Start_mE545E5462FCA8ED37857AAFB304E217D4D240D14,
	Raycast_Update_m9805032A4E75826B7AD913258F679C4B0DE38914,
	Raycast__ctor_m87D1EE0F8A0C8F66112EC24E52E8AD161EA6D132,
	RenderPieczatek_Start_mDE9BB65BA5099FDB49EC8800AA9848A1CB0B42FC,
	RenderPieczatek_Zmiana_pref_m1FF8F5E8E4172968EFCE4E9986C96C9327324615,
	RenderPieczatek_Update_m84F0790CF1B63300F04AF64E3DF717AB88D29BF3,
	RenderPieczatek__ctor_m6B16A63DE582E9DBEEC47F78361CC0BCC266C3B4,
	RobienieKupy_Start_m273DD513AFFB4CCCE4919A41019106F88B60087C,
	RobienieKupy_Update_m06D85811AFBCD2831A36FB5E66403B5F81650CFE,
	RobienieKupy_zrobKupe_mCBAEB4F7BEDA0B5EBB340D62C942F74E2C76A836,
	RobienieKupy__ctor_m10A4B4748FB7E60FB914EAFD227402961D612BB9,
	Rysowanie_poziom5_Start_m0FAEDB1DD7308E9FDD1FA3C0437B4F6E081C24F5,
	Rysowanie_poziom5_Update_m3EB7B53AB13FF136451B535512A744F7EB037886,
	Rysowanie_poziom5_Drawing_m19C391FC8409F27EEBCC96DEE06B3EAA1F10A636,
	Rysowanie_poziom5_getTouchPosition_m7E719A60DE29ECCF2C4D0E469EB63FFFC001B9D1,
	Rysowanie_poziom5_CreateBrush_m730BB2CB81D7D04802555CE7AF12188FE4BEAB6C,
	Rysowanie_poziom5_AddAPoint_m8DC88EB2331ADC16A0D08E84BD838412B1D5F4B8,
	Rysowanie_poziom5_PointToMousePos_m5DE581DD45B43DE98DFE33406F161EA32CD379AE,
	Rysowanie_poziom5__ctor_m02CC022753BB104B41B798A533C52B16DD540B85,
	Rysuj_Start_m210B6E4324C65F166944122E800B5D7579940E0C,
	Rysuj_Update_m28409F3D76D891626BF73E7B215BD94C12D7376E,
	Rysuj_Kredka_mF67EE5358C3AEF24052E8E298D93650CD3842CAF,
	Rysuj_Drawing_mD1F6D8E4E7F14FA7DCD592830C3E6577EB5AF01F,
	Rysuj_CreateBrush_mDA5FA0B4268B888B344756CC0BA6EA0D01269115,
	Rysuj_AddAPoint_m2240512A229DCDBCDC68A7832AB17863709CB229,
	Rysuj_PointToMousePos_m6070B7EED37A9FA7B61456ECFC8E69D4B4DA2E54,
	Rysuj__ctor_m6417571E25B522ADF6714CACBAE1839F015197F3,
	skaluj_Start_m003D25E3543CBBEA36EF2B07CEF91B98BB4B3C98,
	skaluj_Update_mBF4739894E7771428147DB23645F003E3C2CA361,
	skaluj_Skaluj_m59E3FF240A724D38E388FFD55E3533FBE5B23D32,
	skaluj__ctor_m2D7FBEA2118CB8FF9BF3A4AAB72A14E30C7BD12D,
	spawn_sianka_Start_mF2B11F9FFC60065E480E0B616EA2CB6ACA541495,
	spawn_sianka_Update_m61523C7443678D731A81770BD00BB4C50524E006,
	spawn_sianka__ctor_m060D0CCAE2AD96B78FD4E6C645CABB7E4681483D,
	spawn_zabawek_Start_m55BB95401BD69D9EC8123B8A6F6D6B2D237D863B,
	spawn_zabawek_Update_m713F08A133F1A3B7AB12137955F7CA0375C585C4,
	spawn_zabawek_blok_mE55B64FB44B1D7255C5CC2871DCDDB30CD0171B9,
	spawn_zabawek__ctor_m4176AC077DFF0860A2951701E0BC4A775B547BC6,
	U3CblokU3Ed__12__ctor_mEEF57704106999A5868EEB2081F4BC545D1D31DD,
	U3CblokU3Ed__12_System_IDisposable_Dispose_mCD97BD183F706538471BEFC07E50E9A0FE34F090,
	U3CblokU3Ed__12_MoveNext_m5AFC52827D0A1551A242F06E03E458FC35BA7B68,
	U3CblokU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EE1C377915AFD287F6AC9AE18AE09554C083796,
	U3CblokU3Ed__12_System_Collections_IEnumerator_Reset_mE00A4C890B5554FB12FE68213C0BECFC41ABC971,
	U3CblokU3Ed__12_System_Collections_IEnumerator_get_Current_m670445573D229171F539E1D44FBCD433F0508D53,
	sprawdz_punkt_Start_mD6EEF994281E5C9F56892AC48016B2D4697651F7,
	sprawdz_punkt_Update_m26FEAD7C1FD05DF5746C956F0175FB8CF0BAD5C4,
	sprawdz_punkt__ctor_mCEAFFEAD154CDA5483126BDF98A036A0D24D139F,
	szerokosc_suwaka_Start_mFB92E347350CDD5DCB3B1378CD03160E76627C41,
	szerokosc_suwaka_Dostosuj_suwak_m0EE7615816CE51C56C3D09CD5DEC91386A46A7D7,
	szerokosc_suwaka_Update_m363FA591CB89464773BA485449C722D7067E2033,
	szerokosc_suwaka__ctor_m5E5D127607852BA7C3C2B098DCCBC4A930292B43,
	U3CDostosuj_suwakU3Ed__3__ctor_m8D6BAFD5A6E536E4F0855C517C9CD20223F0B4A9,
	U3CDostosuj_suwakU3Ed__3_System_IDisposable_Dispose_mC854D49ADDE4FABEECC7BDB10D0380667921A811,
	U3CDostosuj_suwakU3Ed__3_MoveNext_m0E9E366EEF5B128B00DBC5F3BE0FE191634FEADE,
	U3CDostosuj_suwakU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA913F87A0A2AB2C76624A3DA597D2BF71360522,
	U3CDostosuj_suwakU3Ed__3_System_Collections_IEnumerator_Reset_mB729429FEB48F3309EA3A9AA74CF4F32134B7F59,
	U3CDostosuj_suwakU3Ed__3_System_Collections_IEnumerator_get_Current_m7DC24AA604A916BCDDE34757A97AB64B4DC8A7D8,
	TakeAScreenShot_Awake_m1088F0723B694ECB0648DC63A5B9C97B0801ABE5,
	TakeAScreenShot_OnPostRender_mCC9AF65CAB1F5F725A88636B870C4510324895D5,
	TakeAScreenShot_TakeScreen_m7420B106EAD5A17087DF534315597E256CE73186,
	TakeAScreenShot_TakeScreen_static_m1E6557581B62CAA4A2B1DC937A014A6BB8C46DAC,
	TakeAScreenShot__ctor_m9A7A4221124EFB3A34094B2F0D319D0AE958C0F0,
	Touch_location__ctor_m83019BE5FC11ACA4CC0832ECA5719AE8E7490E4C,
	traktor_dym_Start_m2D86E19283C223FAE9A083293FA64F0F38B6163F,
	traktor_dym_Update_m51BF7D024046ADDF8235AE6266A305FD2B53FBE6,
	traktor_dym_Pusci_dymek_mADF10ADA27BE81FD43D558241F2E5119A3084603,
	traktor_dym__ctor_mEF6E18F4BFA24D4DE8E85CC67A5FD0B4588A74F8,
	Transition_Start_m56FB6AAED27A2560AA91288897726B384F7E4F96,
	Transition_Update_m92BA0BCCA1DEB843C9840CFB87F43A9C59A9CEEA,
	Transition__ctor_m641CBEB2CAC3DD257BAC609DE294488F9BF64768,
	wroc_Powrot_m2B4B71ACA21995EEA4DDB2B922B5A7B16E6D5168,
	wroc_Odlicz_mEBE6125750C8C1400ABD2283EE5031CF0506B293,
	wroc__ctor_m92B904FB29036A448A2D9647BAF58EB50D9D94AD,
	U3COdliczU3Ed__1__ctor_m600C88D9950FE2F3214CC0EAE4221167E256D2A4,
	U3COdliczU3Ed__1_System_IDisposable_Dispose_m3619036035C71A8688B8C4FCCCEA57A58100EE1B,
	U3COdliczU3Ed__1_MoveNext_m32E60ECFB657D878E3674ABAECAADB6A7974CA2A,
	U3COdliczU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC64AE7DD542B6D7E938689FE085A3AC04FD71D54,
	U3COdliczU3Ed__1_System_Collections_IEnumerator_Reset_mB5218F820D5771067492D69800AA2DB828E5CA42,
	U3COdliczU3Ed__1_System_Collections_IEnumerator_get_Current_m6F8ACE4BD09D4FE82706F9B06A7D8A5E5AE1A2AE,
	wylacz_cien_Start_mBC4A433CE43FAA08A01E22FD08BFBEFAE59171A4,
	wylacz_cien_Update_m3D836B492D6BC54ADA1596920283DDBFF0B536D8,
	wylacz_cien_Graj_cien_mA07B2A1F074FCF3126D17CB6F6A59835A6531B1A,
	wylacz_cien_Wylacz_cien_m822FA9347DB569B1D0BDA27DF48DD33FF4EC75A3,
	wylacz_cien__ctor_m93B9674CC40322D5B1D588113EF9A0A10D2AA693,
	wylacz_lub_wlacz_buttony_Start_mDF2A5BEC7B73322FC71553215B546953C68BA6D5,
	wylacz_lub_wlacz_buttony_Update_m5FE20971DF75EDF2583445E460BD6EF23AEBC4A8,
	wylacz_lub_wlacz_buttony_Wyl_wla_button_mCCBA61197A59E9D88F739A49BCF299E447D18AFB,
	wylacz_lub_wlacz_buttony__ctor_m01EAAD91FD4F4F9DD95FA0AEAB92A8B81315DC35,
	zagraj_Start_m926BD229FA0C673DAABCA9AAC6A3D23E4642C36B,
	zagraj_Update_mBD83BA642B11DF0E42604311E816A0FE160E8CC6,
	zagraj_GrajIdle_m5007A58C4AA2EAEA6A6375FE836376D3B0282058,
	zagraj__ctor_mB311BEDFD4C9B0E61A573A62D0D7AB7AAB27BC8F,
	zejscie_Start_mFAEF3C6E9287809AE0EEE31BAADC43AF21C21A03,
	zejscie_Update_m1AA297BC8207A333DA8E593064142D24D42CB7E6,
	zejscie__ctor_m14C4081DCE699A86E6D8BE244DEDC68931FD14CB,
	ZmianaPrzycisku_Start_m86F749E809A7F35F2CAEC85A3C5CB754E2D8AEFE,
	ZmianaPrzycisku_Update_m8199F06F5CCECF695ADC21708E07B526EAFEC172,
	ZmianaPrzycisku_Zmiana_mA60241BFF61ED5568C80A35E8F091BA362B786D5,
	ZmianaPrzycisku_Odliczanie_mB2F8305180AA582DBBD72BE95D11DFAFEF83316F,
	ZmianaPrzycisku__ctor_m671B7C0EDFA1631DAE485FE6058E511C6153ED3D,
	U3COdliczanieU3Ed__11__ctor_mC0D9EA1295F165FA2D1E44723C2DBE8F8FFBA706,
	U3COdliczanieU3Ed__11_System_IDisposable_Dispose_m732660A62B8168391623594CD82C6853E47D70DA,
	U3COdliczanieU3Ed__11_MoveNext_m4F5A90664DE8472A1F70D254ADA4D35F9B5CBA3B,
	U3COdliczanieU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m999F90ED8624C784874695892F05DB704F545185,
	U3COdliczanieU3Ed__11_System_Collections_IEnumerator_Reset_m393805EB9CB81ED0838535B600510AE4FE0B9901,
	U3COdliczanieU3Ed__11_System_Collections_IEnumerator_get_Current_mFE9A283E8255893D062F5EE63F1DC34DC0396106,
	Znikani_Start_m00812A56028C48163DB66EA5B27C2B2CA267363F,
	Znikani_Update_mAB317D3CC7FE832254D141177CF3A24BF6951AE3,
	Znikani_przezroczystosc_m9E8D40473E6E36BFB114F54CF725EFF7C6A867B4,
	Znikani__ctor_m269A8A059734AF5A9E0D85F9A90DAE867497530F,
	U3CprzezroczystoscU3Ed__2__ctor_m32ECD20462DCE0CD1D15143A43014DBE7B5C5025,
	U3CprzezroczystoscU3Ed__2_System_IDisposable_Dispose_m81EFC0D8E0D3A18F78DB5EFFB0D92F3F7A629401,
	U3CprzezroczystoscU3Ed__2_MoveNext_mFFFAB37772D9E246F719E39E12042E02D439F556,
	U3CprzezroczystoscU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7117A9D0D326CD4CE242BA6C218D266681663C1,
	U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_Reset_m795D7A922600D17DE3132D0316F0FE2C0239D85F,
	U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_get_Current_m1F4EB8DDE83437BC7B277C7092A162373C3B5866,
	Znikanie_Start_mB2FB1E65F42A75B815B7BB2218CEC11F76A7EFA1,
	Znikanie_Update_m6E710580AEDF97228D6422A41F125ADFC08467B6,
	Znikanie_przezroczystosc_m6B297CAC7142D53A5A12D842744ADC2C02217915,
	Znikanie__ctor_m2FB112A57A993C9B6D1716DB55810E50EA461DA6,
	U3CprzezroczystoscU3Ed__2__ctor_mF16A534B6C0C6734C19A077669F63BBBEB8229D6,
	U3CprzezroczystoscU3Ed__2_System_IDisposable_Dispose_m5748E576F7C0979A909503A27B6BFB22F9751627,
	U3CprzezroczystoscU3Ed__2_MoveNext_m2CBF728B027D1FD0B6BFCF004BF81681480ACFCE,
	U3CprzezroczystoscU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19B571A6CAB62B237348DF9C682ED78CE0767D68,
	U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_Reset_m2C02BD0B6D460CC5352DD7D4AD35DA72C6945E16,
	U3CprzezroczystoscU3Ed__2_System_Collections_IEnumerator_get_Current_mF87BC6D8C828846D82C293637B08F30E33D289F6,
	zwierze_spi_Start_m4DA5D1D0EE4393BE70F0EB940B53676B26A6EEBB,
	zwierze_spi_Update_m3B30539EA9C931332E388A9BCF40B99120A2F592,
	zwierze_spi_Spanie_m26A0CB3C821699D960B9F499A7F0EC04062A9A70,
	zwierze_spi_sprawdz_emiter_mB6278DBAD89A731EDB1EFBADD6F12C78BCC1DB52,
	zwierze_spi_Wybudzanie_m95E6A8F5FF5E664C5AF174538CDE35FE7E4E48DA,
	zwierze_spi_Chrapanie_m9B5980A584118DCC680D9899A0DED142C5CB4F19,
	zwierze_spi_Sen_mE7778609E47C0CDE6E4DE0CD7C0F4FB88C335CE4,
	zwierze_spi_WybudzanieDlaKaczek_m5A73F354BA8AE354AF57D269711DC66B8019E3B4,
	zwierze_spi__ctor_mCD89971D947C414E2D71FA0ED65C59D0BD6CB8A8,
	Zyroskop_Start_m3A45A7CC7C643FE714DD8F454C486C00FEEB8B42,
	Zyroskop_Update_m66FDA0B3B2B0463AF0584FE59483ADF28E3599FE,
	Zyroskop__ctor_m3BF6F0A20643F1888337252F2B77967AD93DE9BF,
	Zzzzz_Start_m13CAE368DCB57CCD8E5654C4E4C89B2631D63C58,
	Zzzzz_Update_m388D91D4AE2D641BFBEDEE001B72D93EC5C25E3F,
	Zzzzz_Stworz_Zzzz_m156AEF224519D0703F46B8541CF3FE540193521B,
	Zzzzz_stworz_zzz_dla_swinki_mE73AB85C4ECFE8A731646EA3F3889D5E5FCED3CF,
	Zzzzz_Przerwij_zzzz_mE4416E25C6031A1815D0325E9511F862A3B44FC8,
	Zzzzz_Przerwij_zzzzV2_m6B66158B641F875FC8FC3FAFE657A907E90F8DCA,
	Zzzzz__ctor_mF7376CE734839F2188C23ECA4715E6F1D66EAD25,
	Zzzz_dla_kaczek_Start_m1A90BF0F56C205F71254280D4E281C056894F50E,
	Zzzz_dla_kaczek_Update_mA9AC9C39863330D378949FA3722F2F0502B22B60,
	Zzzz_dla_kaczek_Stworz_Zzzz_dla_kaczki_malej_mAA21425C3D9A9C9FA2B29337EBE4101A9D4E1CAA,
	Zzzz_dla_kaczek_Stworz_zzzzz_dla_kaczki_duzej_m83119E279C12514CFF01DFD04747A07109A0AB61,
	Zzzz_dla_kaczek_Przerwij_zzzz_kaczki_malej_m5195871D99E201AF06DC8064C2B42DD59BCC5CD3,
	Zzzz_dla_kaczek_Przerwij_zzzz_kaczki_duzej_m28499A8F7A9DEDB043460B20A6196EDE6F18A8F7,
	Zzzz_dla_kaczek__ctor_m6BF57E2DE7823B8858E476331E868A2173AEEABD,
	dzwieki_idle_fixed_Start_m179CC4E38AFEE78FE8719FEDF1B587D291E69FE2,
	dzwieki_idle_fixed_Update_m44E5F0F823005B509BB4F8EAE7839C9A3B7C9126,
	dzwieki_idle_fixed_Odtworz_idle_mD3736D2144695360B56A0A0CDF5F4C12F9543816,
	dzwieki_idle_fixed_OdtworzTrzaski_m30683568EC653A8D5D4F81E4DA2EB898AB6AFC13,
	dzwieki_idle_fixed_Wyldziw_m5CD57401F20FFE4BF3D37FA3C0967EC449F4BEC1,
	dzwieki_idle_fixed_WlaDziw_m5EBB05B1BDF6CA6708E815BD18DF286BFB654EA4,
	dzwieki_idle_fixed__ctor_mE7E61E42573BE0BA2E1AD21ECC796DFD5BE1CF1E,
	odtworzAnimacje_fixed_Update_m31DF34FCA105107F084559A232F89193EEA64427,
	odtworzAnimacje_fixed_Animacja_m2E4D8C71E7FDB943E81BEC919042B4BF9D233D6A,
	odtworzAnimacje_fixed_powrot_do_idle_m52B32050EE0D05096576178B7414852C5BC9ED5C,
	odtworzAnimacje_fixed__ctor_mB821E52581215DF9E46E50E491BAF9C86B38A911,
	U3Cpowrot_do_idleU3Ed__9__ctor_m73BEA6E206BF878AC9CBBB729D0CDCD7256D6C8C,
	U3Cpowrot_do_idleU3Ed__9_System_IDisposable_Dispose_mC004C3FCB7F217A052262DE89974A8EE80DBC965,
	U3Cpowrot_do_idleU3Ed__9_MoveNext_m42DF93AAB8620133118E369D32B2F0A2D812E09B,
	U3Cpowrot_do_idleU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4CBC54993A77BAB390622647BCBB591BC5CD62DC,
	U3Cpowrot_do_idleU3Ed__9_System_Collections_IEnumerator_Reset_m8E0935BF14395A32D56834F86E92216CF3DFEEBB,
	U3Cpowrot_do_idleU3Ed__9_System_Collections_IEnumerator_get_Current_mEF3553D76545FA212736C05978FD01400467E9B4,
	PlaySoundOnClick_fixed_Start_mC1E2C9CAB22E8253684B699C290527A0590A0234,
	PlaySoundOnClick_fixed_Update_mCC2853910E12605BDE866192D492C03656186699,
	PlaySoundOnClick_fixed_PlaySound_mECE51CDE6ED852F6B8601572F02C214C74489B98,
	PlaySoundOnClick_fixed_PrzyciskPilota_lub_back_btn_mCCF121997B1738C89DC4B2D6A379865AC609272C,
	PlaySoundOnClick_fixed__ctor_m906F3AE27CE88094A15BB8D462834C1F2939DC98,
	Spawnuj_chmury_Start_m3EA55A67E6A349C6A7F5A9F474C2EEFF92E813F6,
	Spawnuj_chmury_Update_mF1034A90847EA30876ED839DC0805F7D109F00C3,
	Spawnuj_chmury_Spawnuj_chmurki_m8B44BD0E14F18446C1BA5BB3B0D9B070875D8614,
	Spawnuj_chmury_Zablokuj_chwyt_mAEB6B79C210C23D7EEBFF4B53DC0047945343A7D,
	Spawnuj_chmury_Odblokuj_chwyt_m6B232A7A7160FC57094387480DCA3464808E87D4,
	Spawnuj_chmury_ZniszczonoChmurke_mFEE65A7FE4389EE964D97074FA3B75CF9620A322,
	Spawnuj_chmury__ctor_mE688EA3D0D04DB7D407507027491D982C7924AFE,
	U3CSpawnuj_chmurkiU3Ed__7__ctor_m81CB08DB0CBE3D56B532C1C354A71ED85CD8B091,
	U3CSpawnuj_chmurkiU3Ed__7_System_IDisposable_Dispose_mF0C85F588F4EA061591136B39E913032C0FE7242,
	U3CSpawnuj_chmurkiU3Ed__7_MoveNext_mAEF2ABB53FC502E877DABFD530D494632CAD6340,
	U3CSpawnuj_chmurkiU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E3803F6CC7750BBACAC301463BB4AD1CC72AFBC,
	U3CSpawnuj_chmurkiU3Ed__7_System_Collections_IEnumerator_Reset_m28243D9B29400E893CF1DD9E9F76C2EFB1182AFB,
	U3CSpawnuj_chmurkiU3Ed__7_System_Collections_IEnumerator_get_Current_m5B9719A6B90956FAD3BF7BFA42E471657D197018,
	sprawdz_punkt_gumka_Start_mD953E440045AAF24A3C86040EFEF071F72ED2297,
	sprawdz_punkt_gumka_Update_m41FED98C40E01051304A5930E974B9775969E1F3,
	sprawdz_punkt_gumka__ctor_mEBC08CC2E3FC517B543232ECA28DE5D2A6F61085,
	Sprawdz_punkt_improve_Start_mD475491C01A92EB1D25BD915B245499F6AE3A41F,
	Sprawdz_punkt_improve_Update_m53EDBC51C7EFB0E42382198DD73631947524D96C,
	Sprawdz_punkt_improve_WritePixels_mFA6DA5C64E21D227A8F755CDC0F9343B656623F1,
	Sprawdz_punkt_improve_GetLinearPositions_m504751DA30F8124B98CAB98BDB22EAF13050C731,
	Sprawdz_punkt_improve_GetNeighbouringPixels_mCABAED56FAA522F8C79018752C0D1399858EC0FF,
	Sprawdz_punkt_improve__ctor_m8CC57DD4458EDAC8C6022C5F5D3C5765D99C871E,
	stuknij_Start_m5D73E59C1FC20BEC5D239A38684A890AFF436C3B,
	stuknij_Update_m4374848631C43AD31D1356DCF41856755737A367,
	stuknij_Zgniec_m61A0F033FF57E9E8A45BA83B86DC119ABCFE86C4,
	stuknij_Stukniecie_mA8438645708C849BF62496E9709B2A0344477A64,
	stuknij__ctor_mBE3EDF9FD88A2D1862C0C07DE01CBA874C9A516C,
	U3CStukniecieU3Ed__8__ctor_mB548264EB2695355A669C0BA2CDAB9A68517776E,
	U3CStukniecieU3Ed__8_System_IDisposable_Dispose_mB61818D70BFDAD55CAA82570C29F41600C28EABE,
	U3CStukniecieU3Ed__8_MoveNext_mE46E4617B20C9D03745F987E2685F6C0066FF83C,
	U3CStukniecieU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47C7829991E6D9F0F8330780BDF1EEC15A617534,
	U3CStukniecieU3Ed__8_System_Collections_IEnumerator_Reset_m6501C7C0C12337CE0A8CB145757F66C50597B525,
	U3CStukniecieU3Ed__8_System_Collections_IEnumerator_get_Current_mCFD297B22EAA80D0C44C6FA251FA1C88156F278B,
	swinka_script_Start_mA70C1E2B94357CC3CCFEE5EB5A12A38825A46B91,
	swinka_script_Update_m7EDF8E5D41348BBB7A38FB0AD2493D81040E7925,
	swinka_script_Wylacz_swinie_m394F26D3C6E629D079C378BFD4C62DB6FA4A5EA1,
	swinka_script__ctor_mDBAD611E7FF09F11F1ECEAD5545190328A354FCD,
	traktor_jezdzi_Start_mDD20F518445B21915B77D7BB8534C01FE67C24D6,
	traktor_jezdzi_Update_m11930AB708CE5653EFBA0ABE3774AD0C12F57E04,
	traktor_jezdzi_Traktorek_Dzwiek_m1D0240067656723F147C02866FD36C2F3200AE34,
	traktor_jezdzi__ctor_m6BB21CA1DC9E503E0A2CF1907A76550AF49D4F57,
	wroc_z_reklama_Start_m05B041D05C372D967E2B00CD676518B77DDE8F62,
	wroc_z_reklama_Update_m07C231B4F117B822DB673F3DBC15C65FA9DB91F0,
	wroc_z_reklama_Wroc_z_reklama_mE80D53471FDF5B3ECF1A3E0CD632D0A45E027D3B,
	wroc_z_reklama_Powrot_m9F27345F71A99573806678AADC9DD2260B29BADF,
	wroc_z_reklama__ctor_mE35135F47CB8E75AF49C509DCCC21A61347004B8,
	U3CPowrotU3Ed__5__ctor_mC3EB9791D5BC6115D18C74C32B2025F0DC97219C,
	U3CPowrotU3Ed__5_System_IDisposable_Dispose_m0C3C7E51BB55E222299EC9C15143BBD40CACC31C,
	U3CPowrotU3Ed__5_MoveNext_m58446E1C88D3AC84DCEDB5CF4C2CFB9346A2FCA4,
	U3CPowrotU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF77C938FB3D991CF41694FD5C2D1EBD323D16A0F,
	U3CPowrotU3Ed__5_System_Collections_IEnumerator_Reset_mAC1F92D10DF7AEFBECFD3E4AF2BB5E2382B9813D,
	U3CPowrotU3Ed__5_System_Collections_IEnumerator_get_Current_m52FAD4421C3210F66E3F528C4BD787BDD7D5167B,
	wylacz_animator_Start_m4EE55120820BB556725A5E4BBF915475D54448F7,
	wylacz_animator_Update_m0007AB1B35CE35AA66DE4FC965926C81FBE5474D,
	wylacz_animator_Wylacz_mCC5B5B5E7167400CD500FA7867E67C91CFEA31D0,
	wylacz_animator__ctor_m95083924EAAD4876A4740A34F67D76092F1BF527,
	zwierze_spi_KaczkaDuza_Start_mFDDBA884ADE3697F0CF8A6BBDD33CB06056969CC,
	zwierze_spi_KaczkaDuza_Update_m8CA8F60D8FEA32FAB8AF783C65872A4D5D0FE8A6,
	zwierze_spi_KaczkaDuza_SpanieDlaKaczki_Duzej_mDD3E15A9E4043912B1CA1D025D77C72D88E8C96D,
	zwierze_spi_KaczkaDuza__ctor_m030D96580EBCCDD6678D35511E1F139510D033E4,
	zwierze_spi_kaczkaMala_Start_mEE76B18893B824B7D45399E8CD2201F9D3B52DF9,
	zwierze_spi_kaczkaMala_Update_m9780C896CF0E59FA414790342F81B75FA3CBAAE8,
	zwierze_spi_kaczkaMala_SpanieDlaKaczki_Malej_mC718E70B9AAAF2B38E35CC06C4963A7B99606E01,
	zwierze_spi_kaczkaMala__ctor_m8E00CE07B78A97DFCE81F5FB10F1CEAF74C9110E,
	zzz_turn_off_Start_m70350E2A9B70F4D1131159FD8353F4C127E00438,
	zzz_turn_off_Update_m4C21DA7B54C8026AC0F09E5DDD6A1ACDB02F5E21,
	zzz_turn_off_Wylacz_zzz_mBE14900961EF78575CAEBA4A4530973F85598658,
	zzz_turn_off__ctor_m3AA3157DC7C075DE406F28C2DB9C7EE2AAF078B3,
	INAP_Start_m458A93023033C82A65F4ABFEFD5AE4CFA7A827B5,
	INAP_Update_m172CFA4DD2CC4776952627D96AF11E9503005F93,
	INAP_InitializePurchasing_m5C36F0C1734D376E3FB23D308E2CF772F0CAD765,
	INAP_AddInitializationCallback_m30E33C6F21146CE6C849FB0996FFBA293E184B62,
	INAP_IsInitialized_mA1AFA91DBE86C5F14B51201A5C084560B390D113,
	INAP_BuyConsumable_m8D51D13539E23862ED9FF68D4AC17FAFD18C8B56,
	INAP_BuyNonConsumable_m01D7159CC2FA6CD3D37E782C4E3B35CCD00EBAFE,
	INAP_BuySubscription_m51CF307D523A9EC2722FE274E850F2E65D831F5B,
	INAP_BuyProductID_m55F570790AAB86E1F6463241106051D13076131C,
	INAP_RestorePurchases_mEF0D5E04B4B79F12ECB62734B1705E7DC18AB30C,
	INAP_OnInitialized_mB02D4AEB2132E4F9CC24D1DCC806CA1E540409EB,
	INAP_OnInitializeFailed_mD39D64E543F58C6133450338066AB8224E046245,
	INAP_ProcessPurchase_m50F99EEE6107A71BBD3EA0F7BDF0B98A1D1794FD,
	INAP_OnPurchaseFailed_m903C9AF7644D89E6EC9754C536AB3F279499DABB,
	INAP_LoadBack_mCD67344F7280D4B4336B136C410A0ACA5E3D0258,
	INAP_LoadKupno_m4FFF1DC22EBC6C335CD8DE489AAFCB9FBB914B43,
	INAP_ResetPlayerPrefs_mB94478DAA9F239D04550897ACBFBABF4CD0AE85B,
	INAP_Back_m8962B05F750B6F3BA6A75E964480917AAEC50703,
	INAP__ctor_mF5670AEAE40AFCF07412E7B9B61AB72E4C717873,
	INAP__cctor_mC8193CC81E3BB77BB2F0E94D9D4B4CB86857064E,
	U3CU3Ec__cctor_m9D744397B1D39386F10D0133E10E0D91B57A57E7,
	U3CU3Ec__ctor_m3C7D04ED84B3BE4EC6C635EA0BA0318C81AC0792,
	U3CU3Ec_U3CRestorePurchasesU3Eb__28_0_m688EBF758C1CF5775A36008B6EFA7BD599899577,
	U3CBackU3Ed__36__ctor_m983E4CDDC53D9212D9962D0DAF4376B81649CC9A,
	U3CBackU3Ed__36_System_IDisposable_Dispose_mA27D1ACEF0E1B01AFAAF29B75E441BDADF6912D1,
	U3CBackU3Ed__36_MoveNext_mD44802A47EF259FC9109C2B0505FBDD3C9F3ED54,
	U3CBackU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92249BBBE2458A0013DA464BCB108D8C713FD627,
	U3CBackU3Ed__36_System_Collections_IEnumerator_Reset_mCB694A95BB856557716A9D2FF8BBDA3E31195708,
	U3CBackU3Ed__36_System_Collections_IEnumerator_get_Current_mB447F3D30B6BA0BF98CE7D27A2C9CEE8C532C03B,
};
static const int32_t s_InvokerIndices[1000] = 
{
	7250,
	7250,
	7250,
	7250,
	3286,
	3286,
	3286,
	7101,
	7250,
	7250,
	12829,
	7250,
	5816,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7007,
	7003,
	7250,
	7250,
	7003,
	5714,
	7250,
	7250,
	7250,
	7250,
	5878,
	7250,
	7003,
	7250,
	3367,
	5878,
	7250,
	1565,
	1089,
	1565,
	5719,
	5785,
	7250,
	5714,
	5816,
	5816,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5183,
	5183,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7003,
	5714,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5947,
	7250,
	1565,
	1565,
	5719,
	5785,
	7250,
	5947,
	5714,
	5816,
	5816,
	3286,
	7250,
	7003,
	5714,
	7250,
	7250,
	7250,
	7250,
	7250,
	5947,
	7250,
	1565,
	1565,
	5719,
	5785,
	5785,
	5785,
	5947,
	5714,
	5402,
	5816,
	5816,
	7250,
	7003,
	5714,
	7250,
	7250,
	7250,
	7250,
	5947,
	7250,
	1565,
	1565,
	5719,
	5785,
	7250,
	5947,
	5714,
	5816,
	5816,
	3286,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5785,
	7101,
	7250,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7101,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7101,
	7101,
	7101,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7101,
	7101,
	7250,
	7250,
	5816,
	7250,
	7101,
	7101,
	7101,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	5785,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	12798,
	12829,
	12798,
	7250,
	7250,
	7250,
	7101,
	7101,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	7250,
	7250,
	7250,
	5816,
	7101,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	5785,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7101,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5188,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	1085,
	1558,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5183,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5785,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	7250,
	7250,
	7250,
	7250,
	5816,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	5816,
	7250,
	7250,
	7101,
	7250,
	5816,
	5188,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	3279,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	3346,
	5402,
	5205,
	3304,
	3304,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5949,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	2992,
	10454,
	7250,
	3020,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	5816,
	7250,
	7250,
	7250,
	7250,
	7101,
	5816,
	7250,
	5816,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	5947,
	1565,
	1565,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7101,
	7250,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	7250,
	5816,
	7003,
	7250,
	7250,
	7250,
	5816,
	7250,
	3286,
	5785,
	4888,
	3279,
	7250,
	7250,
	7250,
	7101,
	7250,
	12829,
	12829,
	7250,
	5714,
	5785,
	7250,
	7003,
	7101,
	7250,
	7101,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1000,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
