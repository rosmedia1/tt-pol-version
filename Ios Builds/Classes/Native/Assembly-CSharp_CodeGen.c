﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ADMOB::Start()
extern void ADMOB_Start_m31CB3BF932CDB229B063D2F588C3A0C125D371DD (void);
// 0x00000002 System.Void ADMOB::OnDisable()
extern void ADMOB_OnDisable_m2A5FB3D681D8F14A38D38BD48A2D49112279B324 (void);
// 0x00000003 System.Void ADMOB::RequestBanner()
extern void ADMOB_RequestBanner_m6FB0C993331D821C73667EB0BC7F0F8966B25093 (void);
// 0x00000004 System.Void ADMOB::.ctor()
extern void ADMOB__ctor_mA015BA67C8F8F8F1B2337F29BB1B28A2C1E3E525 (void);
// 0x00000005 System.Void ADMOBV2::Start()
extern void ADMOBV2_Start_m9ACBC2A6E58B7EF500CB4260294EF8DC79A9A126 (void);
// 0x00000006 System.Void ADMOBV2::RequestIntersitial()
extern void ADMOBV2_RequestIntersitial_m760124600C89A5A59852B5D65C44912757AE04A2 (void);
// 0x00000007 System.Void ADMOBV2::ShowIntersitial()
extern void ADMOBV2_ShowIntersitial_m8994051E600EBB169F4A6928FB50D611D09C5585 (void);
// 0x00000008 System.Void ADMOBV2::.ctor()
extern void ADMOBV2__ctor_mD341BA94F8451FB8B8CD87C1A5FFA37A33A01749 (void);
// 0x00000009 System.Void color_non_interactrive_index::.ctor()
extern void color_non_interactrive_index__ctor_m41CCD0C489083683E374075D9826932C57658933 (void);
// 0x0000000A System.Void dotyk_klawisza::Update()
extern void dotyk_klawisza_Update_mC965C3EB31E73192158236FBCF267C99402B2BB0 (void);
// 0x0000000B System.Void dotyk_klawisza::Odtworz_animacje()
extern void dotyk_klawisza_Odtworz_animacje_m33E3E5E75C46360CBBC99D0D74E5943719F4D731 (void);
// 0x0000000C System.Void dotyk_klawisza::Wylacz()
extern void dotyk_klawisza_Wylacz_m60908E5F2DA510BAF2FDA55A49C1C55B7795B558 (void);
// 0x0000000D System.Void dotyk_klawisza::Wlacz()
extern void dotyk_klawisza_Wlacz_m35D60CA9CF65E76C19AFE470D49505EB6EBED7AB (void);
// 0x0000000E System.Void dotyk_klawisza::Odtworz_muze()
extern void dotyk_klawisza_Odtworz_muze_mBACC52B817D6B9F57B1ADDF6EA6EE836649A733C (void);
// 0x0000000F System.Void dotyk_klawisza::Klik()
extern void dotyk_klawisza_Klik_m2DE3F9AFCC62C0058D8A7040F61BF0BF77219AA4 (void);
// 0x00000010 System.Void dotyk_klawisza::NoneKlik()
extern void dotyk_klawisza_NoneKlik_m4A91BA961E52D0D526467DEE15C844427CBED98B (void);
// 0x00000011 System.Void dotyk_klawisza::.ctor()
extern void dotyk_klawisza__ctor_mE5EE19B27F48B1CB9AC878FC8996445EC6937555 (void);
// 0x00000012 System.Void dotyk_klawisza_non_interactive_color::Update()
extern void dotyk_klawisza_non_interactive_color_Update_m05A24BD0567F54B42B68A2DE6FCF41076C65C11F (void);
// 0x00000013 System.Void dotyk_klawisza_non_interactive_color::.ctor()
extern void dotyk_klawisza_non_interactive_color__ctor_mADF618F432B03D42F0D08461E0D6F32F176C16FF (void);
// 0x00000014 System.Void Glowy_button::Start()
extern void Glowy_button_Start_m0F85EA6D93E451F75A9B5D1ED4CBB000348FEE09 (void);
// 0x00000015 System.Void Glowy_button::Update()
extern void Glowy_button_Update_mCEFA7E3C2C0C3DAE5C14386CC1CC91E649AC9D1F (void);
// 0x00000016 System.Void Glowy_button::Glowa()
extern void Glowy_button_Glowa_mE6D4DECC26FFFB0578D5880321ABC38FE7AA382D (void);
// 0x00000017 System.Void Glowy_button::.ctor()
extern void Glowy_button__ctor_mBF24C53DD2B6C01FBF3B6F6DF38EF1ED2E5B7F7D (void);
// 0x00000018 System.Void Graj_muzyke::Start()
extern void Graj_muzyke_Start_m19F5012F3197CE57642F72ACF9EF6858F8BDF29B (void);
// 0x00000019 System.Void Graj_muzyke::Update()
extern void Graj_muzyke_Update_m5FA614A47BC53C12820B4DE066A2323725DE6FFA (void);
// 0x0000001A System.Void Graj_muzyke::Odpal_klawisz1()
extern void Graj_muzyke_Odpal_klawisz1_m100E30ADA2B9876FF66A4BA137CD115BE4120EB6 (void);
// 0x0000001B System.Void Graj_muzyke::Odpal_klawisz2()
extern void Graj_muzyke_Odpal_klawisz2_m53EA91B3E48C950C716341CC03605193671BAD80 (void);
// 0x0000001C System.Void Graj_muzyke::Odpal_klawisz3()
extern void Graj_muzyke_Odpal_klawisz3_m72C0EE3CF8F1D442A1DC876D0A6BEBC2ED01D18C (void);
// 0x0000001D System.Void Graj_muzyke::Odpal_klawisz4()
extern void Graj_muzyke_Odpal_klawisz4_m4EBE93AC93CF547304E1FA88225FCE1ACAE5FAA5 (void);
// 0x0000001E System.Void Graj_muzyke::Odpal_klawisz5()
extern void Graj_muzyke_Odpal_klawisz5_mB49BFF35DB43D603E5FD807C9A9786402B9CC22F (void);
// 0x0000001F System.Void Graj_muzyke::Odpal_klawisz6()
extern void Graj_muzyke_Odpal_klawisz6_m7AB780D4EA62FDD319EC3883C0A1211B68A5524E (void);
// 0x00000020 System.Void Graj_muzyke::Odpal_klawisz7()
extern void Graj_muzyke_Odpal_klawisz7_mAC83CCE54D8A991223A145032759713EF40D461D (void);
// 0x00000021 System.Void Graj_muzyke::Odpal_klawisz8()
extern void Graj_muzyke_Odpal_klawisz8_m22575BE8A08BB4D54F29C2421A2442A7BF2D32C7 (void);
// 0x00000022 System.Void Graj_muzyke::.ctor()
extern void Graj_muzyke__ctor_m72F7D60A85591A134E7AB8691AA2281C218485D6 (void);
// 0x00000023 System.Void Heads_of_animals::Start()
extern void Heads_of_animals_Start_mC6B864944D9F6E95433CFA78C6CC718870264779 (void);
// 0x00000024 System.Void Heads_of_animals::Glowy(System.Int32)
extern void Heads_of_animals_Glowy_m8E75B0C94A145916B18ADC5BBF430E47549A1EEF (void);
// 0x00000025 System.Void Heads_of_animals::.ctor()
extern void Heads_of_animals__ctor_m35CAD4F729EA3C14D99A243D361D809212DDB870 (void);
// 0x00000026 System.Void klawisze_non_interactive_index::.ctor()
extern void klawisze_non_interactive_index__ctor_m848359B26F3FFF0648DE8A6AD557045E7139AA02 (void);
// 0x00000027 System.Void linker::Go_To_www()
extern void linker_Go_To_www_mEFDFE77C09CB51597EF558734CC7030CB7B0E687 (void);
// 0x00000028 System.Void linker::Go_To_youtube()
extern void linker_Go_To_youtube_m16DD4A856C1887FEE848CCBA84B14D20891EFC4C (void);
// 0x00000029 System.Void linker::Go_To_facebook()
extern void linker_Go_To_facebook_m6FC7EF256A3FAEE65C0E133301A4FF81628C8B5E (void);
// 0x0000002A System.Void linker::Go_To_email()
extern void linker_Go_To_email_m1D7E9720B21ACD19D9B5497EEC4B8EDB9EBC9FBF (void);
// 0x0000002B System.Void linker::Go_To_opinion()
extern void linker_Go_To_opinion_m00A63C6DC57A04651DD2A4FB9A4BFD3BF5856826 (void);
// 0x0000002C System.Void linker::SendEmail()
extern void linker_SendEmail_m820D144CA7CD37143FA04A0729BB532007CD2779 (void);
// 0x0000002D System.String linker::MyEscapeURL(System.String)
extern void linker_MyEscapeURL_mF41ED058D3577E1A876C0A4C2E152D486DAA98A3 (void);
// 0x0000002E System.Void linker::.ctor()
extern void linker__ctor_m7C67E14FE583F6B0D2045D805ABCE289B6BCD317 (void);
// 0x0000002F System.Void melodie_pokazowka::Switcher(System.Int32)
extern void melodie_pokazowka_Switcher_m7F71802B1A18FA5AAB400C3E47F9F4D4B19150B0 (void);
// 0x00000030 System.Void melodie_pokazowka::Zmien_layer()
extern void melodie_pokazowka_Zmien_layer_mE82699567B10AC0631F2D18D1263C085343A3EE5 (void);
// 0x00000031 System.Void melodie_pokazowka::AntySwitcher(System.Int32)
extern void melodie_pokazowka_AntySwitcher_m942749A1FFF695683E330B9C820D42C325F972BF (void);
// 0x00000032 System.Void melodie_pokazowka::.ctor()
extern void melodie_pokazowka__ctor_m2BC872E0D1558508325489AED814BFFA0E376F36 (void);
// 0x00000033 System.Void Muzyka_do_pianinka::Muza(System.Int32)
extern void Muzyka_do_pianinka_Muza_m2E53CB05115363C19C8A29DF3EE24E92375CE866 (void);
// 0x00000034 System.Void Muzyka_do_pianinka::.ctor()
extern void Muzyka_do_pianinka__ctor_mEF4EAC4EE6DCB21E052FD2F5D0B33E5753DF4BDC (void);
// 0x00000035 System.Void nutki_i_gwiazdki_index::Start()
extern void nutki_i_gwiazdki_index_Start_m59CE870CE653589ECF5C50A165FD0E9CEFFE6A2D (void);
// 0x00000036 System.Void nutki_i_gwiazdki_index::.ctor()
extern void nutki_i_gwiazdki_index__ctor_mB88C60A62A93BE72756B83566C9FAD7436E2D5BD (void);
// 0x00000037 System.Void Odpal_Parental_gata::Start()
extern void Odpal_Parental_gata_Start_mAE11E7BD971944EBB201753618332AD5F927321A (void);
// 0x00000038 System.Void Odpal_Parental_gata::Ekran_dla_rodzicow()
extern void Odpal_Parental_gata_Ekran_dla_rodzicow_mECBB3DD15AF5648F343E267C69C7492A1941F782 (void);
// 0x00000039 System.Void Odpal_Parental_gata::.ctor()
extern void Odpal_Parental_gata__ctor_mC4F82425E5605621966247D92346B8AE3E442AA9 (void);
// 0x0000003A System.Void Parental_Gate::Start()
extern void Parental_Gate_Start_m5A5D84A68FEE8AB0324D529005D1C2786D780CB1 (void);
// 0x0000003B System.Void Parental_Gate::Zamknij()
extern void Parental_Gate_Zamknij_m615BFD934CA75AE986485882F3A4864589ACC2EA (void);
// 0x0000003C System.Void Parental_Gate::Otworz()
extern void Parental_Gate_Otworz_mD1C9F8BD153F57BA0FE3963DED5F5565CC1D9F64 (void);
// 0x0000003D System.Collections.IEnumerator Parental_Gate::Backk()
extern void Parental_Gate_Backk_m878779F0F68680E0627A32B912F8CDBC146639F3 (void);
// 0x0000003E System.Void Parental_Gate::Wylacz_Emiterki()
extern void Parental_Gate_Wylacz_Emiterki_m8D5FB394E1B98D59CEAC40462E224E72A11986F2 (void);
// 0x0000003F System.Void Parental_Gate::Wylacz_tlo()
extern void Parental_Gate_Wylacz_tlo_mDFCEAEDABE0FADB912CBFB14F4B173E4D7F8C98E (void);
// 0x00000040 System.Void Parental_Gate::Wlacz_tlo()
extern void Parental_Gate_Wlacz_tlo_m5FBA13815E37513B8FF0316C0468190982BCB08A (void);
// 0x00000041 System.Void Parental_Gate::Wylacz_Menu()
extern void Parental_Gate_Wylacz_Menu_mE8C8A389A0E9D3F1C4FE5E1C3B2EE9C0CF2B81D5 (void);
// 0x00000042 System.Void Parental_Gate::Sprawdz_wynik(System.Int32)
extern void Parental_Gate_Sprawdz_wynik_m1F4D08D728262468C9F584DB43EDE21F6703AAD4 (void);
// 0x00000043 System.Void Parental_Gate::.ctor()
extern void Parental_Gate__ctor_m574FD22F9319C099CBF99B1F18F8351B7BD07905 (void);
// 0x00000044 System.Void Parental_Gate::<Start>b__21_0()
extern void Parental_Gate_U3CStartU3Eb__21_0_m4A61D4093B6904B5F2A36CDC125D7CD9D048A15B (void);
// 0x00000045 System.Void Parental_Gate::<Start>b__21_1()
extern void Parental_Gate_U3CStartU3Eb__21_1_mDACE490BDF5FFA909B625F73A72D2D409DB5E703 (void);
// 0x00000046 System.Void Parental_Gate::<Start>b__21_2()
extern void Parental_Gate_U3CStartU3Eb__21_2_m1121941CE6F5A52FAD3385B217E00B3E606A05E8 (void);
// 0x00000047 System.Void Parental_Gate::<Otworz>b__23_0()
extern void Parental_Gate_U3COtworzU3Eb__23_0_m262F7F7D4E3019EFBF0E000CA0D7CAB811704953 (void);
// 0x00000048 System.Void Parental_Gate/<Backk>d__24::.ctor(System.Int32)
extern void U3CBackkU3Ed__24__ctor_m9A7FD9193FAE5D3F5ACAA8F03F4E03D1E2D1386D (void);
// 0x00000049 System.Void Parental_Gate/<Backk>d__24::System.IDisposable.Dispose()
extern void U3CBackkU3Ed__24_System_IDisposable_Dispose_m1EC9B71FB0E407C113B90807FB438F4194B72E5A (void);
// 0x0000004A System.Boolean Parental_Gate/<Backk>d__24::MoveNext()
extern void U3CBackkU3Ed__24_MoveNext_m381F3F3A39EA82DD7C3658E120F07776DDA5858D (void);
// 0x0000004B System.Object Parental_Gate/<Backk>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackkU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7BD8E429F8CDB714B018B95628A2ED884361028 (void);
// 0x0000004C System.Void Parental_Gate/<Backk>d__24::System.Collections.IEnumerator.Reset()
extern void U3CBackkU3Ed__24_System_Collections_IEnumerator_Reset_mF331058D410E1705A9284A296D8C2B855A8B7AC6 (void);
// 0x0000004D System.Object Parental_Gate/<Backk>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CBackkU3Ed__24_System_Collections_IEnumerator_get_Current_mD1F11B63B34F3B37BD954D9AE8313B3B0C87A600 (void);
// 0x0000004E System.Collections.IEnumerator PowrotZReklama::Powrot()
extern void PowrotZReklama_Powrot_m9C1C7A7CCA75F440BF842123C80291CACC52B088 (void);
// 0x0000004F System.Void PowrotZReklama::Wroc_Z_Reklama()
extern void PowrotZReklama_Wroc_Z_Reklama_m8005F1A638A75ED0EC708FFF942569477DAE76A6 (void);
// 0x00000050 System.Void PowrotZReklama::.ctor()
extern void PowrotZReklama__ctor_mAFF7F54195558126644C5631BFB610F67902CE2D (void);
// 0x00000051 System.Void PowrotZReklama/<Powrot>d__2::.ctor(System.Int32)
extern void U3CPowrotU3Ed__2__ctor_mE6BB99029689F7048EAF01F4F39D6CEC852C1B53 (void);
// 0x00000052 System.Void PowrotZReklama/<Powrot>d__2::System.IDisposable.Dispose()
extern void U3CPowrotU3Ed__2_System_IDisposable_Dispose_m2FE01BC6EF12A7BC5CD5078159DBE78269F0327A (void);
// 0x00000053 System.Boolean PowrotZReklama/<Powrot>d__2::MoveNext()
extern void U3CPowrotU3Ed__2_MoveNext_mF0816455873D7F4CA42DD92E3DD3C142A05781BF (void);
// 0x00000054 System.Object PowrotZReklama/<Powrot>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPowrotU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88496B30CB71D3610BDBC163230EFD6BA81C69DC (void);
// 0x00000055 System.Void PowrotZReklama/<Powrot>d__2::System.Collections.IEnumerator.Reset()
extern void U3CPowrotU3Ed__2_System_Collections_IEnumerator_Reset_m70CF3251560ADADA3909F4A6F5BA7CEB87F8AE9B (void);
// 0x00000056 System.Object PowrotZReklama/<Powrot>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CPowrotU3Ed__2_System_Collections_IEnumerator_get_Current_m487DE1362E2BB648D97CA9E6AA719CFE799E28F9 (void);
// 0x00000057 System.Void zmiana_sprita_nutka_gwiazdka::Zmien_sprite_pomaranczowej()
extern void zmiana_sprita_nutka_gwiazdka_Zmien_sprite_pomaranczowej_m9DE675E3FA20DDF42EC8BE43CC56FFC210F5F451 (void);
// 0x00000058 System.Void zmiana_sprita_nutka_gwiazdka::Zmien_sprite_niebieskiej()
extern void zmiana_sprita_nutka_gwiazdka_Zmien_sprite_niebieskiej_m63BCDE22F80C5E7BA9A16C65AF49ABF0A9B6E766 (void);
// 0x00000059 System.Void zmiana_sprita_nutka_gwiazdka::Zmien_sprite_czerwonej()
extern void zmiana_sprita_nutka_gwiazdka_Zmien_sprite_czerwonej_mABF2B2268FE11AF7AF7894AFC9CD2236D0BB02A6 (void);
// 0x0000005A System.Void zmiana_sprita_nutka_gwiazdka::Zmien_sprite_pomaranczowej_gwiazdki()
extern void zmiana_sprita_nutka_gwiazdka_Zmien_sprite_pomaranczowej_gwiazdki_m8099425F24B37ADBAB4BF52D55827E5BDF071FC2 (void);
// 0x0000005B System.Void zmiana_sprita_nutka_gwiazdka::Zmien_sprite_niebeiskiej_gwiazdki()
extern void zmiana_sprita_nutka_gwiazdka_Zmien_sprite_niebeiskiej_gwiazdki_mDA83144940F6A8003B170CFC8D73EE524E6D5A3E (void);
// 0x0000005C System.Void zmiana_sprita_nutka_gwiazdka::Zmien_sprite_czerwonej_gwiazdki()
extern void zmiana_sprita_nutka_gwiazdka_Zmien_sprite_czerwonej_gwiazdki_m2F821EDEAE0899ACC6DF4E04A1527DBE1DE78E0A (void);
// 0x0000005D System.Void zmiana_sprita_nutka_gwiazdka::Wroc_do_podstawy()
extern void zmiana_sprita_nutka_gwiazdka_Wroc_do_podstawy_m4F40A2914F038C75448E9CD6168EF2FFADD688AA (void);
// 0x0000005E System.Void zmiana_sprita_nutka_gwiazdka::Kl1_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl1_up_mE39ADDD468A83B2F7049A7F291A33C4346C5C9A3 (void);
// 0x0000005F System.Void zmiana_sprita_nutka_gwiazdka::Kl2_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl2_up_m2CF97347514DC135C5524C3EBD4E1F62930E3041 (void);
// 0x00000060 System.Void zmiana_sprita_nutka_gwiazdka::Kl3_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl3_up_mE94F44C89F92B6E2A73173085A80CABB679360B9 (void);
// 0x00000061 System.Void zmiana_sprita_nutka_gwiazdka::Kl4_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl4_up_mD03AC09BB27F05E8D36FCABEC72CAB59DABAEEC5 (void);
// 0x00000062 System.Void zmiana_sprita_nutka_gwiazdka::Kl5_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl5_up_mE3ED93FD13E341B85CCE762B92F6B9898C0417E2 (void);
// 0x00000063 System.Void zmiana_sprita_nutka_gwiazdka::Kl6_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl6_up_m38926375186A2A06AD9B26518841D510CE7CBDF6 (void);
// 0x00000064 System.Void zmiana_sprita_nutka_gwiazdka::Kl7_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl7_up_m23769E4F82B462283F1DFC5A55C99A18C64426C6 (void);
// 0x00000065 System.Void zmiana_sprita_nutka_gwiazdka::Kl8_up()
extern void zmiana_sprita_nutka_gwiazdka_Kl8_up_m420B4A366C2299DD2E938D52C27C77B5D020E190 (void);
// 0x00000066 System.Void zmiana_sprita_nutka_gwiazdka::Kl1_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl1_down_m3F43204CAA0C8C287427D7D22702B84DB9C82F8C (void);
// 0x00000067 System.Void zmiana_sprita_nutka_gwiazdka::Kl2_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl2_down_m7B3E3705B5A8B803E7FA7BFD0DAE08736E20EC5E (void);
// 0x00000068 System.Void zmiana_sprita_nutka_gwiazdka::Kl3_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl3_down_m712D22CBC400333287269C5A02B7F87D1FFC7B59 (void);
// 0x00000069 System.Void zmiana_sprita_nutka_gwiazdka::Kl4_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl4_down_mC90E4F186E9DB1532BE399E489CDC2A2E3C9FE87 (void);
// 0x0000006A System.Void zmiana_sprita_nutka_gwiazdka::Kl5_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl5_down_m54F0992CE6F5AA1E8F81C318AC7ED4364200D982 (void);
// 0x0000006B System.Void zmiana_sprita_nutka_gwiazdka::Kl6_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl6_down_m412534128EA26C58344AE65349B7FB812CFBC1C7 (void);
// 0x0000006C System.Void zmiana_sprita_nutka_gwiazdka::Kl7_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl7_down_mB65BAD562F3E821DB73F4E1E1A5F0FEDA5156D35 (void);
// 0x0000006D System.Void zmiana_sprita_nutka_gwiazdka::Kl8_down()
extern void zmiana_sprita_nutka_gwiazdka_Kl8_down_m1085C14C888A5259499DA78AE0815EC828C5A40E (void);
// 0x0000006E System.Void zmiana_sprita_nutka_gwiazdka::.ctor()
extern void zmiana_sprita_nutka_gwiazdka__ctor_mE782EDB88B7ED51C4FF2762F571EC4943759A075 (void);
// 0x0000006F System.Void zmien_mape::Przejdz_Do_Kupna()
extern void zmien_mape_Przejdz_Do_Kupna_m3D2BD18A23532EC53D67A6D75110ED56E36E8ECB (void);
// 0x00000070 System.Void zmien_mape::Wroc_z_parentala()
extern void zmien_mape_Wroc_z_parentala_m612F95738816E3486CD92C3E52772AD6041C2999 (void);
// 0x00000071 System.Void zmien_mape::.ctor()
extern void zmien_mape__ctor_m84B930FB5995C7F50D2BFC9CB996E132FAD2C292 (void);
// 0x00000072 System.Void CompleteProject.Inap::Start()
extern void Inap_Start_m8BF698B7CB421F5CCAEA0FA4B530E12CFF6B586E (void);
// 0x00000073 System.Void CompleteProject.Inap::InitializePurchasing()
extern void Inap_InitializePurchasing_mA8E210145E2765430A2CF62D2AD40D4542336F97 (void);
// 0x00000074 System.Void CompleteProject.Inap::AddInitializationCallback(System.Action`1<System.Boolean>)
extern void Inap_AddInitializationCallback_m6A28C07248D7F8A99DCBDBF928E2E0F2D5389B60 (void);
// 0x00000075 System.Boolean CompleteProject.Inap::IsInitialized()
extern void Inap_IsInitialized_m9F1EC4F4EFA11221F82722E8D7911ADFDEB21A11 (void);
// 0x00000076 System.Void CompleteProject.Inap::BuyConsumable()
extern void Inap_BuyConsumable_m3B825A5D80766D9A427C14DEC63FC4E879D74A02 (void);
// 0x00000077 System.Void CompleteProject.Inap::BuyNonConsumable()
extern void Inap_BuyNonConsumable_m40EDFAF49DBEC97C8CCDB960277BA47AC1F15671 (void);
// 0x00000078 System.Void CompleteProject.Inap::BuySubscription()
extern void Inap_BuySubscription_mF31241EF020199CE33D8A4202B02FCEC7C59A211 (void);
// 0x00000079 System.Void CompleteProject.Inap::BuyProductID(System.String)
extern void Inap_BuyProductID_m83A38328E302E9D020FA878FD831B8476B76F6D6 (void);
// 0x0000007A System.Void CompleteProject.Inap::RestorePurchases()
extern void Inap_RestorePurchases_m043BE06530B83A3471B2489C9593CD287B2A25D5 (void);
// 0x0000007B System.Void CompleteProject.Inap::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void Inap_OnInitialized_mE916AB4327F2E5F7C82F30922A0EF3441B99B51F (void);
// 0x0000007C System.Void CompleteProject.Inap::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void Inap_OnInitializeFailed_m368018AEA4EB4880C1C7C1CE8EEE08DECC14680F (void);
// 0x0000007D UnityEngine.Purchasing.PurchaseProcessingResult CompleteProject.Inap::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void Inap_ProcessPurchase_m1D3E8D1453D5C7FF77FE1A37F038F9D4ACA2E7AD (void);
// 0x0000007E System.Void CompleteProject.Inap::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void Inap_OnPurchaseFailed_mCD6A2CB8AB53CADF1FD4372FFC5C8DDA946FD881 (void);
// 0x0000007F System.Void CompleteProject.Inap::LoadBack()
extern void Inap_LoadBack_mAA43D73485B40094848E734387D31B662410C873 (void);
// 0x00000080 System.Void CompleteProject.Inap::ResetPlayerPrefs()
extern void Inap_ResetPlayerPrefs_m7D70278B74C8B1935C175153A2ACE8A205B6442B (void);
// 0x00000081 System.Collections.IEnumerator CompleteProject.Inap::Back()
extern void Inap_Back_m689316849B251DD71710724F4B49EC779E3DEEB1 (void);
// 0x00000082 System.Void CompleteProject.Inap::.ctor()
extern void Inap__ctor_m36C578AF0EBD4828E4BB53E5599EE7BFE17FA5FA (void);
// 0x00000083 System.Void CompleteProject.Inap::.cctor()
extern void Inap__cctor_m450A7FBC3DBF44D3188B8B2C2EB45AE94CB9A97F (void);
// 0x00000084 System.Void CompleteProject.Inap::<RestorePurchases>b__19_0(System.Boolean)
extern void Inap_U3CRestorePurchasesU3Eb__19_0_m3D40E63A34A5B6E11DD20FE9AB8671B5F4F574D4 (void);
// 0x00000085 System.Void CompleteProject.Inap/<Back>d__26::.ctor(System.Int32)
extern void U3CBackU3Ed__26__ctor_m90EC6529D331CF4AF984DE9B99F8311F4723CA8E (void);
// 0x00000086 System.Void CompleteProject.Inap/<Back>d__26::System.IDisposable.Dispose()
extern void U3CBackU3Ed__26_System_IDisposable_Dispose_m2C550021C6C01FE410A872D784B77A3E188CEF74 (void);
// 0x00000087 System.Boolean CompleteProject.Inap/<Back>d__26::MoveNext()
extern void U3CBackU3Ed__26_MoveNext_m93E5DD73B09942EB694753DEA44CD2D60CF6FAEF (void);
// 0x00000088 System.Object CompleteProject.Inap/<Back>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86C8B8B7D03D09808C6E51193493115C46F4086B (void);
// 0x00000089 System.Void CompleteProject.Inap/<Back>d__26::System.Collections.IEnumerator.Reset()
extern void U3CBackU3Ed__26_System_Collections_IEnumerator_Reset_mA7EC0BF70C8BCD4CD4DC32E4124D6BDD0775BCB6 (void);
// 0x0000008A System.Object CompleteProject.Inap/<Back>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CBackU3Ed__26_System_Collections_IEnumerator_get_Current_m35B2CADDC156B6EC4B209C02F3C9B8D4FD4C1042 (void);
static Il2CppMethodPointer s_methodPointers[138] = 
{
	ADMOB_Start_m31CB3BF932CDB229B063D2F588C3A0C125D371DD,
	ADMOB_OnDisable_m2A5FB3D681D8F14A38D38BD48A2D49112279B324,
	ADMOB_RequestBanner_m6FB0C993331D821C73667EB0BC7F0F8966B25093,
	ADMOB__ctor_mA015BA67C8F8F8F1B2337F29BB1B28A2C1E3E525,
	ADMOBV2_Start_m9ACBC2A6E58B7EF500CB4260294EF8DC79A9A126,
	ADMOBV2_RequestIntersitial_m760124600C89A5A59852B5D65C44912757AE04A2,
	ADMOBV2_ShowIntersitial_m8994051E600EBB169F4A6928FB50D611D09C5585,
	ADMOBV2__ctor_mD341BA94F8451FB8B8CD87C1A5FFA37A33A01749,
	color_non_interactrive_index__ctor_m41CCD0C489083683E374075D9826932C57658933,
	dotyk_klawisza_Update_mC965C3EB31E73192158236FBCF267C99402B2BB0,
	dotyk_klawisza_Odtworz_animacje_m33E3E5E75C46360CBBC99D0D74E5943719F4D731,
	dotyk_klawisza_Wylacz_m60908E5F2DA510BAF2FDA55A49C1C55B7795B558,
	dotyk_klawisza_Wlacz_m35D60CA9CF65E76C19AFE470D49505EB6EBED7AB,
	dotyk_klawisza_Odtworz_muze_mBACC52B817D6B9F57B1ADDF6EA6EE836649A733C,
	dotyk_klawisza_Klik_m2DE3F9AFCC62C0058D8A7040F61BF0BF77219AA4,
	dotyk_klawisza_NoneKlik_m4A91BA961E52D0D526467DEE15C844427CBED98B,
	dotyk_klawisza__ctor_mE5EE19B27F48B1CB9AC878FC8996445EC6937555,
	dotyk_klawisza_non_interactive_color_Update_m05A24BD0567F54B42B68A2DE6FCF41076C65C11F,
	dotyk_klawisza_non_interactive_color__ctor_mADF618F432B03D42F0D08461E0D6F32F176C16FF,
	Glowy_button_Start_m0F85EA6D93E451F75A9B5D1ED4CBB000348FEE09,
	Glowy_button_Update_mCEFA7E3C2C0C3DAE5C14386CC1CC91E649AC9D1F,
	Glowy_button_Glowa_mE6D4DECC26FFFB0578D5880321ABC38FE7AA382D,
	Glowy_button__ctor_mBF24C53DD2B6C01FBF3B6F6DF38EF1ED2E5B7F7D,
	Graj_muzyke_Start_m19F5012F3197CE57642F72ACF9EF6858F8BDF29B,
	Graj_muzyke_Update_m5FA614A47BC53C12820B4DE066A2323725DE6FFA,
	Graj_muzyke_Odpal_klawisz1_m100E30ADA2B9876FF66A4BA137CD115BE4120EB6,
	Graj_muzyke_Odpal_klawisz2_m53EA91B3E48C950C716341CC03605193671BAD80,
	Graj_muzyke_Odpal_klawisz3_m72C0EE3CF8F1D442A1DC876D0A6BEBC2ED01D18C,
	Graj_muzyke_Odpal_klawisz4_m4EBE93AC93CF547304E1FA88225FCE1ACAE5FAA5,
	Graj_muzyke_Odpal_klawisz5_mB49BFF35DB43D603E5FD807C9A9786402B9CC22F,
	Graj_muzyke_Odpal_klawisz6_m7AB780D4EA62FDD319EC3883C0A1211B68A5524E,
	Graj_muzyke_Odpal_klawisz7_mAC83CCE54D8A991223A145032759713EF40D461D,
	Graj_muzyke_Odpal_klawisz8_m22575BE8A08BB4D54F29C2421A2442A7BF2D32C7,
	Graj_muzyke__ctor_m72F7D60A85591A134E7AB8691AA2281C218485D6,
	Heads_of_animals_Start_mC6B864944D9F6E95433CFA78C6CC718870264779,
	Heads_of_animals_Glowy_m8E75B0C94A145916B18ADC5BBF430E47549A1EEF,
	Heads_of_animals__ctor_m35CAD4F729EA3C14D99A243D361D809212DDB870,
	klawisze_non_interactive_index__ctor_m848359B26F3FFF0648DE8A6AD557045E7139AA02,
	linker_Go_To_www_mEFDFE77C09CB51597EF558734CC7030CB7B0E687,
	linker_Go_To_youtube_m16DD4A856C1887FEE848CCBA84B14D20891EFC4C,
	linker_Go_To_facebook_m6FC7EF256A3FAEE65C0E133301A4FF81628C8B5E,
	linker_Go_To_email_m1D7E9720B21ACD19D9B5497EEC4B8EDB9EBC9FBF,
	linker_Go_To_opinion_m00A63C6DC57A04651DD2A4FB9A4BFD3BF5856826,
	linker_SendEmail_m820D144CA7CD37143FA04A0729BB532007CD2779,
	linker_MyEscapeURL_mF41ED058D3577E1A876C0A4C2E152D486DAA98A3,
	linker__ctor_m7C67E14FE583F6B0D2045D805ABCE289B6BCD317,
	melodie_pokazowka_Switcher_m7F71802B1A18FA5AAB400C3E47F9F4D4B19150B0,
	melodie_pokazowka_Zmien_layer_mE82699567B10AC0631F2D18D1263C085343A3EE5,
	melodie_pokazowka_AntySwitcher_m942749A1FFF695683E330B9C820D42C325F972BF,
	melodie_pokazowka__ctor_m2BC872E0D1558508325489AED814BFFA0E376F36,
	Muzyka_do_pianinka_Muza_m2E53CB05115363C19C8A29DF3EE24E92375CE866,
	Muzyka_do_pianinka__ctor_mEF4EAC4EE6DCB21E052FD2F5D0B33E5753DF4BDC,
	nutki_i_gwiazdki_index_Start_m59CE870CE653589ECF5C50A165FD0E9CEFFE6A2D,
	nutki_i_gwiazdki_index__ctor_mB88C60A62A93BE72756B83566C9FAD7436E2D5BD,
	Odpal_Parental_gata_Start_mAE11E7BD971944EBB201753618332AD5F927321A,
	Odpal_Parental_gata_Ekran_dla_rodzicow_mECBB3DD15AF5648F343E267C69C7492A1941F782,
	Odpal_Parental_gata__ctor_mC4F82425E5605621966247D92346B8AE3E442AA9,
	Parental_Gate_Start_m5A5D84A68FEE8AB0324D529005D1C2786D780CB1,
	Parental_Gate_Zamknij_m615BFD934CA75AE986485882F3A4864589ACC2EA,
	Parental_Gate_Otworz_mD1C9F8BD153F57BA0FE3963DED5F5565CC1D9F64,
	Parental_Gate_Backk_m878779F0F68680E0627A32B912F8CDBC146639F3,
	Parental_Gate_Wylacz_Emiterki_m8D5FB394E1B98D59CEAC40462E224E72A11986F2,
	Parental_Gate_Wylacz_tlo_mDFCEAEDABE0FADB912CBFB14F4B173E4D7F8C98E,
	Parental_Gate_Wlacz_tlo_m5FBA13815E37513B8FF0316C0468190982BCB08A,
	Parental_Gate_Wylacz_Menu_mE8C8A389A0E9D3F1C4FE5E1C3B2EE9C0CF2B81D5,
	Parental_Gate_Sprawdz_wynik_m1F4D08D728262468C9F584DB43EDE21F6703AAD4,
	Parental_Gate__ctor_m574FD22F9319C099CBF99B1F18F8351B7BD07905,
	Parental_Gate_U3CStartU3Eb__21_0_m4A61D4093B6904B5F2A36CDC125D7CD9D048A15B,
	Parental_Gate_U3CStartU3Eb__21_1_mDACE490BDF5FFA909B625F73A72D2D409DB5E703,
	Parental_Gate_U3CStartU3Eb__21_2_m1121941CE6F5A52FAD3385B217E00B3E606A05E8,
	Parental_Gate_U3COtworzU3Eb__23_0_m262F7F7D4E3019EFBF0E000CA0D7CAB811704953,
	U3CBackkU3Ed__24__ctor_m9A7FD9193FAE5D3F5ACAA8F03F4E03D1E2D1386D,
	U3CBackkU3Ed__24_System_IDisposable_Dispose_m1EC9B71FB0E407C113B90807FB438F4194B72E5A,
	U3CBackkU3Ed__24_MoveNext_m381F3F3A39EA82DD7C3658E120F07776DDA5858D,
	U3CBackkU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7BD8E429F8CDB714B018B95628A2ED884361028,
	U3CBackkU3Ed__24_System_Collections_IEnumerator_Reset_mF331058D410E1705A9284A296D8C2B855A8B7AC6,
	U3CBackkU3Ed__24_System_Collections_IEnumerator_get_Current_mD1F11B63B34F3B37BD954D9AE8313B3B0C87A600,
	PowrotZReklama_Powrot_m9C1C7A7CCA75F440BF842123C80291CACC52B088,
	PowrotZReklama_Wroc_Z_Reklama_m8005F1A638A75ED0EC708FFF942569477DAE76A6,
	PowrotZReklama__ctor_mAFF7F54195558126644C5631BFB610F67902CE2D,
	U3CPowrotU3Ed__2__ctor_mE6BB99029689F7048EAF01F4F39D6CEC852C1B53,
	U3CPowrotU3Ed__2_System_IDisposable_Dispose_m2FE01BC6EF12A7BC5CD5078159DBE78269F0327A,
	U3CPowrotU3Ed__2_MoveNext_mF0816455873D7F4CA42DD92E3DD3C142A05781BF,
	U3CPowrotU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88496B30CB71D3610BDBC163230EFD6BA81C69DC,
	U3CPowrotU3Ed__2_System_Collections_IEnumerator_Reset_m70CF3251560ADADA3909F4A6F5BA7CEB87F8AE9B,
	U3CPowrotU3Ed__2_System_Collections_IEnumerator_get_Current_m487DE1362E2BB648D97CA9E6AA719CFE799E28F9,
	zmiana_sprita_nutka_gwiazdka_Zmien_sprite_pomaranczowej_m9DE675E3FA20DDF42EC8BE43CC56FFC210F5F451,
	zmiana_sprita_nutka_gwiazdka_Zmien_sprite_niebieskiej_m63BCDE22F80C5E7BA9A16C65AF49ABF0A9B6E766,
	zmiana_sprita_nutka_gwiazdka_Zmien_sprite_czerwonej_mABF2B2268FE11AF7AF7894AFC9CD2236D0BB02A6,
	zmiana_sprita_nutka_gwiazdka_Zmien_sprite_pomaranczowej_gwiazdki_m8099425F24B37ADBAB4BF52D55827E5BDF071FC2,
	zmiana_sprita_nutka_gwiazdka_Zmien_sprite_niebeiskiej_gwiazdki_mDA83144940F6A8003B170CFC8D73EE524E6D5A3E,
	zmiana_sprita_nutka_gwiazdka_Zmien_sprite_czerwonej_gwiazdki_m2F821EDEAE0899ACC6DF4E04A1527DBE1DE78E0A,
	zmiana_sprita_nutka_gwiazdka_Wroc_do_podstawy_m4F40A2914F038C75448E9CD6168EF2FFADD688AA,
	zmiana_sprita_nutka_gwiazdka_Kl1_up_mE39ADDD468A83B2F7049A7F291A33C4346C5C9A3,
	zmiana_sprita_nutka_gwiazdka_Kl2_up_m2CF97347514DC135C5524C3EBD4E1F62930E3041,
	zmiana_sprita_nutka_gwiazdka_Kl3_up_mE94F44C89F92B6E2A73173085A80CABB679360B9,
	zmiana_sprita_nutka_gwiazdka_Kl4_up_mD03AC09BB27F05E8D36FCABEC72CAB59DABAEEC5,
	zmiana_sprita_nutka_gwiazdka_Kl5_up_mE3ED93FD13E341B85CCE762B92F6B9898C0417E2,
	zmiana_sprita_nutka_gwiazdka_Kl6_up_m38926375186A2A06AD9B26518841D510CE7CBDF6,
	zmiana_sprita_nutka_gwiazdka_Kl7_up_m23769E4F82B462283F1DFC5A55C99A18C64426C6,
	zmiana_sprita_nutka_gwiazdka_Kl8_up_m420B4A366C2299DD2E938D52C27C77B5D020E190,
	zmiana_sprita_nutka_gwiazdka_Kl1_down_m3F43204CAA0C8C287427D7D22702B84DB9C82F8C,
	zmiana_sprita_nutka_gwiazdka_Kl2_down_m7B3E3705B5A8B803E7FA7BFD0DAE08736E20EC5E,
	zmiana_sprita_nutka_gwiazdka_Kl3_down_m712D22CBC400333287269C5A02B7F87D1FFC7B59,
	zmiana_sprita_nutka_gwiazdka_Kl4_down_mC90E4F186E9DB1532BE399E489CDC2A2E3C9FE87,
	zmiana_sprita_nutka_gwiazdka_Kl5_down_m54F0992CE6F5AA1E8F81C318AC7ED4364200D982,
	zmiana_sprita_nutka_gwiazdka_Kl6_down_m412534128EA26C58344AE65349B7FB812CFBC1C7,
	zmiana_sprita_nutka_gwiazdka_Kl7_down_mB65BAD562F3E821DB73F4E1E1A5F0FEDA5156D35,
	zmiana_sprita_nutka_gwiazdka_Kl8_down_m1085C14C888A5259499DA78AE0815EC828C5A40E,
	zmiana_sprita_nutka_gwiazdka__ctor_mE782EDB88B7ED51C4FF2762F571EC4943759A075,
	zmien_mape_Przejdz_Do_Kupna_m3D2BD18A23532EC53D67A6D75110ED56E36E8ECB,
	zmien_mape_Wroc_z_parentala_m612F95738816E3486CD92C3E52772AD6041C2999,
	zmien_mape__ctor_m84B930FB5995C7F50D2BFC9CB996E132FAD2C292,
	Inap_Start_m8BF698B7CB421F5CCAEA0FA4B530E12CFF6B586E,
	Inap_InitializePurchasing_mA8E210145E2765430A2CF62D2AD40D4542336F97,
	Inap_AddInitializationCallback_m6A28C07248D7F8A99DCBDBF928E2E0F2D5389B60,
	Inap_IsInitialized_m9F1EC4F4EFA11221F82722E8D7911ADFDEB21A11,
	Inap_BuyConsumable_m3B825A5D80766D9A427C14DEC63FC4E879D74A02,
	Inap_BuyNonConsumable_m40EDFAF49DBEC97C8CCDB960277BA47AC1F15671,
	Inap_BuySubscription_mF31241EF020199CE33D8A4202B02FCEC7C59A211,
	Inap_BuyProductID_m83A38328E302E9D020FA878FD831B8476B76F6D6,
	Inap_RestorePurchases_m043BE06530B83A3471B2489C9593CD287B2A25D5,
	Inap_OnInitialized_mE916AB4327F2E5F7C82F30922A0EF3441B99B51F,
	Inap_OnInitializeFailed_m368018AEA4EB4880C1C7C1CE8EEE08DECC14680F,
	Inap_ProcessPurchase_m1D3E8D1453D5C7FF77FE1A37F038F9D4ACA2E7AD,
	Inap_OnPurchaseFailed_mCD6A2CB8AB53CADF1FD4372FFC5C8DDA946FD881,
	Inap_LoadBack_mAA43D73485B40094848E734387D31B662410C873,
	Inap_ResetPlayerPrefs_m7D70278B74C8B1935C175153A2ACE8A205B6442B,
	Inap_Back_m689316849B251DD71710724F4B49EC779E3DEEB1,
	Inap__ctor_m36C578AF0EBD4828E4BB53E5599EE7BFE17FA5FA,
	Inap__cctor_m450A7FBC3DBF44D3188B8B2C2EB45AE94CB9A97F,
	Inap_U3CRestorePurchasesU3Eb__19_0_m3D40E63A34A5B6E11DD20FE9AB8671B5F4F574D4,
	U3CBackU3Ed__26__ctor_m90EC6529D331CF4AF984DE9B99F8311F4723CA8E,
	U3CBackU3Ed__26_System_IDisposable_Dispose_m2C550021C6C01FE410A872D784B77A3E188CEF74,
	U3CBackU3Ed__26_MoveNext_m93E5DD73B09942EB694753DEA44CD2D60CF6FAEF,
	U3CBackU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86C8B8B7D03D09808C6E51193493115C46F4086B,
	U3CBackU3Ed__26_System_Collections_IEnumerator_Reset_mA7EC0BF70C8BCD4CD4DC32E4124D6BDD0775BCB6,
	U3CBackU3Ed__26_System_Collections_IEnumerator_get_Current_m35B2CADDC156B6EC4B209C02F3C9B8D4FD4C1042,
};
static const int32_t s_InvokerIndices[138] = 
{
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	4124,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	3682,
	5109,
	4124,
	5109,
	4124,
	5109,
	4124,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	4989,
	5109,
	5109,
	5109,
	5109,
	4124,
	5109,
	5109,
	5109,
	5109,
	5109,
	4124,
	5109,
	4912,
	4989,
	5109,
	4989,
	4989,
	5109,
	5109,
	4124,
	5109,
	4912,
	4989,
	5109,
	4989,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	5109,
	4146,
	4912,
	5109,
	5109,
	5109,
	4146,
	5109,
	2376,
	4124,
	3459,
	2372,
	5109,
	5109,
	4989,
	5109,
	7656,
	4068,
	4124,
	5109,
	4912,
	4989,
	5109,
	4989,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	138,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
