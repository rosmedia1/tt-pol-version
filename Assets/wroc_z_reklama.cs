﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class wroc_z_reklama : MonoBehaviour
{
    //public static int counter;
    public GameObject reklama;
    public GameObject[] obiekty;
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("counter", counter);
       // Debug.Log(counter + "to counter");
    }

    // Update is called once per frame
    void Update()
    {
        //PlayerPrefs.GetInt("counter", counter);
    }

    public void Wroc_z_reklama()
    {
        StartCoroutine(Powrot());
    }

    public IEnumerator Powrot()
    {
        if (PlayerPrefs.HasKey("unlock") == false)
        {
            //counter += 41;
            //if (counter > 40)
            //{

                reklama.GetComponent<AdMob>().ShowInterstitialAd();
               // counter = 0;
                for (int i = 0; i < obiekty.Length; i++)
                {
                    obiekty[i].GetComponent<Button>().enabled = false;
                }
                yield return new WaitForSeconds(1f);
                SceneManager.LoadScene("Menu01");
            //}
        }

        if (PlayerPrefs.HasKey("unlock") == true)
        {
            //counter = 0;
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("Menu01");
        }

    }
}
