﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cymbalek : MonoBehaviour
{
    public AudioClip idleClip;
    private AudioSource cSource;
    public float randomCymbIdle;
    public bool cymbAlreadyPlaying;
    public bool startRandom;
    public bool bankaZbita;

    private void Start()
    {
        cSource = this.gameObject.GetComponent<AudioSource>();
        randomCymbIdle = Random.Range(15, 20);
    }
    private void Update()
    {
        if (startRandom == true)
        {
            CymbRandom();
            startRandom = false;
        }
    }
    public void IdleSound()
    {
        cSource.clip = idleClip;
        cSource.Play();
    }
    void CymbRandom()
    {
        randomCymbIdle = Random.Range(15, 20);
        //Debug.Log("losuje nowy delay" + randomCymbIdle);
    }
    public IEnumerator CymbIdleOn()
    {
        yield return new WaitForSeconds(5f);
        cymbAlreadyPlaying = false;

    }
}
