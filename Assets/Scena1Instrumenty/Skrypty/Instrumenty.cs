﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Instrumenty : MonoBehaviour
{

    // Linijka od 360 do 378 back button
    private Collider2D col;

    [SerializeField] private SphereCollider[] tS;

    private AudioSource src;
    private Animator anim;

    private bool cymbstart = false;

    public AudioClip[] audioClips;
    public AudioClip[] idleAudioC;
    public AnimationClip[] animClips;
    public AnimationClip[] idleAnimClips;
    public Collider2D[] BubbleOverlap;
    public ParticleSystem particles;

    public float idleGuard = 10f;
    public float animatorDelay = 10;
    private int randomIdle;
    private int randomSound;
    private bool isAlreadyPlaying;
    private bool stopIdleFromPlaying;
    private int randomAnim;

    public GameObject paleczka1GO;
    public GameObject paleczka2GO;
    private Animator paleczka1Anim;
    private Animator paleczka2Anim;

    public GameObject banka;
    private GameObject fabrykaBaniek;
    private Rigidbody2D sztywnaBanka;
    private Quaternion pozycjaPoczatkowaBanki;
    private int randomBanki;
    private int randomIloscBaniek;

    public GameObject dolnyCymb;
    public GameObject gornyCymb;
    public cymbalek cymbOjciec;

    //private Vector2 tenCymbalek;
    private GameObject tenCymbalek;
    private static Vector2 pozycjaPaleczki1;
    private static Vector2 pozycjaPaleczki2;
    private float offsetPaleczkiX = 1.5f;
    private float offsetPaleczkiY = 0.6f;
    public static string cymbPaleczki1;
    public static string cymbPaleczki2;
    public GameObject backButton;



    private static bool paleczka1Uzyta;
    private void Start()
    {
        col = this.gameObject.GetComponent<Collider2D>();
        anim = this.gameObject.GetComponent<Animator>();
        src = this.GetComponent<AudioSource>();
        cymbOjciec = GameObject.Find("cymbalki").GetComponent<cymbalek>();
        randomIdle = Random.Range(3, 12);
        StartCoroutine(IdleAnimations());
        UstawPaleczki();
        if (gameObject.tag == "dziecko")
        {

            StartCoroutine(CymbIdleAnimation());
            paleczka1Anim = paleczka1GO.GetComponent<Animator>();
            paleczka2Anim = paleczka2GO.GetComponent<Animator>();
        }
        if (gameObject.name == "trabka")
        {
            fabrykaBaniek = this.transform.GetChild(this.transform.childCount - 2).gameObject;
        }
        if (gameObject.name != "back_btn" && gameObject.tag != "banka")
        {
            particles = this.transform.GetChild(this.transform.childCount - 1).GetComponent<ParticleSystem>();
        }
        if (this.gameObject.tag == "banka")
        {
            pozycjaPoczatkowaBanki = Quaternion.Euler(0, 0, 0);
            sztywnaBanka = this.gameObject.GetComponent<Rigidbody2D>();
            UstawBanke();
        }
    }
    //Wstepne ustawianie paleczek wzgledem cymbalek
    void UstawPaleczki()
    {
        //yield return new WaitForEndOfFrame();
        if (gameObject == dolnyCymb ^ gameObject == gornyCymb)
        {
            pozycjaPaleczki1.x = dolnyCymb.transform.position.x + offsetPaleczkiX;
            pozycjaPaleczki1.y = dolnyCymb.transform.position.y + offsetPaleczkiY;
            pozycjaPaleczki2.x = gornyCymb.transform.position.x + offsetPaleczkiX;
            pozycjaPaleczki2.y = gornyCymb.transform.position.y + offsetPaleczkiY;
            paleczka1GO.transform.position = pozycjaPaleczki1;
            paleczka2GO.transform.position = pozycjaPaleczki2;
            cymbPaleczki1 = "cymb_1";
            cymbPaleczki2 = "cymb_2";
        }

    }
    void UstawBanke()
    {
        float randomsize = Random.Range(0.4f, 0.8f);
        this.gameObject.transform.localScale = new Vector2(randomsize, randomsize);
        Quaternion obrotBanki = Quaternion.Euler(0, 0, Random.Range(-60f, 60f));
        Quaternion obracanieBanki = Quaternion.LerpUnclamped(pozycjaPoczatkowaBanki, obrotBanki, 0.5f);
        this.gameObject.transform.rotation = obracanieBanki;
        Vector2 poczatkowaPredkosc = new Vector2(Random.Range(0.5f, 0.8f), Random.Range(-0.8f, 0.8f));
        Vector2 predkoscZero = new Vector2(0.6f, 0f);
        Vector2 predkoscBanki = Vector2.LerpUnclamped(poczatkowaPredkosc, predkoscZero, 0.5f);
        this.sztywnaBanka.velocity = predkoscBanki;
        //Debug.Log(this.sztywnaBanka.velocity);
        StartCoroutine(AutoZbijanieBaniek());

    }
    private void Update()
    {
        //Debug.Log(isAlreadyPlaying);
        //Skrypt sprawdzajacy nacisniecia i triggerujacy animacje i dzwieki
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector2 touchposition = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                if (col.OverlapPoint(touchposition))
                {
                    //Debug.Log("Touch");
                    if (touch.phase == TouchPhase.Began ^ touch.phase == TouchPhase.Moved && gameObject.tag != "noMove")
                    {
                        this.stopIdleFromPlaying = true;
                        Debug.Log(this.gameObject);
                        if (gameObject.tag == "dziecko")
                        {
                            StartCoroutine(PlaySoundAndAnimation());
                            //tenCymbalek = this.gameObject.transform.position;
                            tenCymbalek = this.gameObject;
                            StartCoroutine(cymbOjciec.CymbIdleOn());
                            cymbOjciec.cymbAlreadyPlaying = true;
                            //Debug.Log(this.gameObject.transform.position);
                        }
                        else
                        {
                            StartCoroutine(PlaySoundAndAnimation());
                        }
                    }
                    if (touch.phase == TouchPhase.Began ^ touch.phase == TouchPhase.Moved ^ touch.phase == TouchPhase.Stationary && gameObject.tag == "banka")
                    {
                        StartCoroutine(ZbijBanke());
                        StartCoroutine(PlaySoundAndAnimation());
                        cymbOjciec.bankaZbita = true;
                    }
                    if (touch.phase == TouchPhase.Began && gameObject.tag == "noMove")
                    {
                        this.stopIdleFromPlaying = true;
                        StartCoroutine(PlaySoundAndAnimation());
                        //Debug.Log(this.gameObject);
                    }
                }
                else if (col.OverlapPoint(touchposition) == false)
                {
                    SoundGlitchFix();
                }
            }
        }
        else
        {
            SoundGlitchFix();
        }
    }
    IEnumerator ZbijBanke()
    {
        if (cymbOjciec.bankaZbita == false)
        {
            cymbOjciec.bankaZbita = false;
            yield return new WaitForSeconds(audioClips[0].length);
            isAlreadyPlaying = true;
            Destroy(this.gameObject);
        }
    }
    IEnumerator AutoZbijanieBaniek()
    {
        yield return new WaitForSeconds(10 + Random.Range(0f, 1f));
        StartCoroutine(PlaySoundAndAnimation());
        yield return new WaitForSeconds(audioClips[0].length);
        Destroy(this.gameObject);
    }
    void SoundGlitchFix()
    {
        if (gameObject.tag == "dziecko")
        {
            isAlreadyPlaying = false;
            cymbOjciec.bankaZbita = false;
        }
        else

        {
            isAlreadyPlaying = false;
        }
    }
    //Rozpoczecie dzwieku i animacji
    IEnumerator PlaySoundAndAnimation()
    {
        if (isAlreadyPlaying == false)
        {
            this.isAlreadyPlaying = true;
            yield return new WaitForSeconds(0.00000005f);
            if (cymbOjciec.bankaZbita == false | gameObject.tag == "banka")
            {
                if (audioClips.Length > 0)
                {
                    if (gameObject.name == "trabka")
                    {
                        //Debug.Log("ilosc baniek " + GameObject.FindGameObjectsWithTag("banka").Length);
                        if (GameObject.FindGameObjectsWithTag("banka").Length < 35)
                        {
                            randomBanki = Random.Range(0, audioClips.Length + 1);
                        }
                        else
                        {
                            randomBanki = Random.Range(1, audioClips.Length + 1);
                        }
                        if (randomBanki != 0)
                        {
                            randomSound = Random.Range(0, audioClips.Length);
                            src.clip = audioClips[randomSound];
                            src.Play();
                        }
                    }
                    else
                    {
                        randomSound = Random.Range(0, audioClips.Length);
                        src.clip = audioClips[randomSound];
                        src.Play();
                    }
                }
                PlayAnimation();
                //backAnimEnabler = true;
            }
        }
        //string activeAnimation = animClips[randomSound].name;

    }
    //Skrypt od animacji
    void PlayAnimation()
    {
        // do animacji wszystkich obiektow
        if (audioClips.Length == animClips.Length && audioClips.Length > 0)
        {
            if (gameObject.name == "trabka")
            {
                if (randomBanki == 0)
                {
                    randomIloscBaniek = Random.Range(8, 15);
                    // StartCoroutine(SpawnerBaniek());
                }
                else
                {
                    anim.Play(animClips[randomSound].name, -1, 0f);
                }
            }
            else
            {
                anim.Play(animClips[randomSound].name, -1, 0f);
            }
        }
        else if (animClips.Length == 0)
        {

        }
        else
        {
            randomAnim = Random.Range(0, animClips.Length);
            anim.Play(animClips[randomAnim].name, -1, 0f);
        }
        //do animacji cymbalkow nie dotykac pod zadnym wzgledem
        if (gameObject.tag == "dziecko")
        {
            if (this.gameObject.name == "cymb_6" | paleczka1Uzyta == false && tenCymbalek.name != cymbPaleczki2)
            {
                pozycjaPaleczki1.x = this.gameObject.transform.position.x + offsetPaleczkiX;
                pozycjaPaleczki1.y = this.gameObject.transform.position.y + offsetPaleczkiY;
                paleczka1GO.transform.position = pozycjaPaleczki1;
                paleczka1Anim.Play("cymbalki_paleczka_click");
                cymbPaleczki1 = this.gameObject.name;
                paleczka1Uzyta = true;
                //Debug.Log("pozycja paleczki1: " + pozycjaPaleczki1.y);
            }
            else if (this.gameObject.name != "cymb_6" && paleczka1Uzyta == true && tenCymbalek.name != cymbPaleczki1)
            {
                pozycjaPaleczki2.x = this.gameObject.transform.position.x + offsetPaleczkiX;
                pozycjaPaleczki2.y = this.gameObject.transform.position.y + offsetPaleczkiY;
                paleczka2GO.transform.position = pozycjaPaleczki2;
                paleczka1Uzyta = false;
                cymbPaleczki2 = this.gameObject.name;
                paleczka2Anim.Play("cymbalki_paleczka_click");
                //Debug.Log("pozycja paleczki2: " + pozycjaPaleczki2.y);
            }
            if (tenCymbalek.name == cymbPaleczki1)
            {
                paleczka1Anim.Play("cymbalki_paleczka_click");
            }
            else if (tenCymbalek.name == cymbPaleczki2)
            {
                paleczka2Anim.Play("cymbalki_paleczka_click");
            }
        }
    }
    public void EmiterOn()
    {
        var main = particles.main;
        if (audioClips.Length == animClips.Length && audioClips.Length > 0)
        {
            particles.Stop();
            //main.duration = animClips[randomSound].length;
            particles.Play();
        }
        else
        {
            this.stopIdleFromPlaying = true;
            particles.Stop();
            //main.duration = 1;
            particles.Play();
        }
    }
    public void EmiterOff()
    {
        StartCoroutine(WylaczIdle());
        particles.Stop();
    }
    IEnumerator WylaczIdle()
    {
        yield return new WaitForSeconds(idleGuard);
        this.stopIdleFromPlaying = false;
    }
    IEnumerator SpawnerBaniek()
    {
        while (randomIloscBaniek > 0)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.7f));
            Vector2 losowyOffset = new Vector2(fabrykaBaniek.transform.position.x + Random.Range(0f, 0.5f), fabrykaBaniek.transform.position.y + Random.Range(-0.5f, 0.5f));
            Instantiate(banka, losowyOffset, pozycjaPoczatkowaBanki);
            randomIloscBaniek--;
        }
    }
    IEnumerator IdleAnimations()
    {
        if (this.stopIdleFromPlaying == false && this.gameObject.tag != "dziecko" && idleAnimClips.Length > 0)
        {
            randomIdle = Random.Range(3, 12);
            yield return new WaitForSeconds(animatorDelay + randomIdle);
            //if (backButton.wyjscie) yield break;
            if (idleAnimClips.Length > 0)
            {
                int randomIdleAnim = Random.Range(0, idleAnimClips.Length);
                this.anim.CrossFade(idleAnimClips[randomIdleAnim].name, 0.05f);
                StartCoroutine(IdleAnimations());
                /*if (gameObject.name == "trabka")
                {
                    src.clip = idleAudioC[0];
                    src.Play();
                } */
                //Debug.Log("Obiekt animowany" + gameObject.name);
                //Debug.Log("Animacja idle " + idleAnimClips[randomIdleAnim]);
            }
        }
        else if (this.stopIdleFromPlaying == true && this.gameObject.tag != "dziecko" && idleAnimClips.Length > 0)
        {
            yield return new WaitForSeconds(animatorDelay + randomIdle);
            //if (backButton.wyjscie) yield break;
            StartCoroutine(IdleAnimations());
        }
    }
    IEnumerator CymbIdleAnimation()
    {
        cymbOjciec = GameObject.Find("cymbalki").GetComponent<cymbalek>();
        if (this.gameObject.tag == "dziecko" && cymbOjciec.cymbAlreadyPlaying == false && cymbstart == true)
        {
            cymbOjciec.startRandom = true;
            //Debug.Log("Delay dla cymbalkow " + cymbOjciec.randomCymbIdle);
            yield return new WaitForSeconds(animatorDelay + cymbOjciec.randomCymbIdle); //bylo 25
            StartCoroutine(CymbIdleAnimation());
            cymbOjciec.IdleSound();
            yield return new WaitForSeconds(Random.Range(0, 0.15f));
            //Debug.Log("Idle cymbalka ");
            this.anim.CrossFade(idleAnimClips[0].name, 0.1f);
        }
        else if (this.gameObject.tag == "dziecko" && cymbOjciec.cymbAlreadyPlaying == true && cymbstart == false)
        {
            //Debug.Log("nie odtworzono animacji");
            yield return new WaitForSeconds(animatorDelay + cymbOjciec.randomCymbIdle);
            StartCoroutine(CymbIdleAnimation());
            cymbstart = true;
        }
    }
}
