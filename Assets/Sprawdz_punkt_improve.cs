﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Sprawdz_punkt_improve : MonoBehaviour
{
    private float distance;
    private Vector3 pozycja_autka;
    public GameObject samochodzik;
    public GameObject czujnik_ruchu;
    public int ukryte_punkty; // jako miejsce w world space do zniszczenia 
    public RawImage m_image;
    Color color = new Color(0, 0, 0, 255);
    Color penColour = new Color(0, 0, 0, 255);
    private Vector2? m_lastPos;
    private int radius;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        pozycja_autka = samochodzik.transform.TransformPoint(Vector3.zero);
        for (int i = 0; i < this.GetComponent<LineRenderer>().positionCount; i++)
        {



            distance = this.GetComponent<LineRenderer>().GetPosition(i).x - pozycja_autka.x;

            if (samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie == true)
            {

                if (distance > 0 && distance < 0.5f)
                {
                    //zmazywanie w prawo
                    WritePixels(czujnik_ruchu.transform.position);

                }
                else if (distance < 0 && distance > -0.5f)
                {
                    //zmazywanie w lewo
                    WritePixels(czujnik_ruchu.transform.position);

                }


            }




        }
        //if (ukryte_punkty >= this.GetComponent<LineRenderer>().positionCount)
        //{

        //    Destroy(this.gameObject);
        //}

    }


    private void WritePixels(Vector2 pos)
    {

        var mainTex = m_image.texture;
        var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);

        var curTex = RenderTexture.active;
        var renTex = new RenderTexture(mainTex.width, mainTex.height, 32);

        Graphics.Blit(mainTex, renTex);
        RenderTexture.active = renTex;

        tex2d.ReadPixels(new Rect(0, 0, mainTex.width, mainTex.height), 0, 0);

        var col = samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie ? color : penColour;
        var positions = m_lastPos.HasValue ? GetLinearPositions(m_lastPos.Value, pos) : new List<Vector2>() { pos };

        foreach (var position in positions)
        {
            var pixels = GetNeighbouringPixels(new Vector2(mainTex.width, mainTex.height), position, radius);

            if (pixels.Count > 0)
                foreach (var p in pixels)
                    tex2d.SetPixel((int)p.x, (int)p.y, col);
        }

        tex2d.Apply();

        RenderTexture.active = curTex;
        renTex.Release();
        Destroy(renTex);
        Destroy(mainTex);
        curTex = null;
        renTex = null;
        mainTex = null;

        m_image.texture = tex2d;
        m_lastPos = pos;
    }

    private List<Vector2> GetLinearPositions(Vector2 firstPos, Vector2 secondPos, int spacing = 2)
    {
        var positions = new List<Vector2>();

        var dir = secondPos - firstPos;

        if (dir.magnitude <= spacing)
        {
            positions.Add(secondPos);
            return positions;
        }

        for (int i = 0; i < dir.magnitude; i += spacing)
        {
            var v = Vector2.ClampMagnitude(dir, i);
            positions.Add(firstPos + v);
        }

        positions.Add(secondPos);
        return positions;
    }


    private List<Vector2> GetNeighbouringPixels(Vector2 textureSize, Vector2 position, int brushRadius)
    {
        var pixels = new List<Vector2>();

        for (int i = -brushRadius; i < brushRadius; i++)
        {
            for (int j = -brushRadius; j < brushRadius; j++)
            {
                var pxl = new Vector2(position.x + i, position.y + j);
                if (pxl.x > 0 && pxl.x < textureSize.x && pxl.y > 0 && pxl.y < textureSize.y)
                    pixels.Add(pxl);
            }
        }

        return pixels;
    }

}
