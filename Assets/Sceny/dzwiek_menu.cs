﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class dzwiek_menu : MonoBehaviour
{
    public Sprite spriteOn;
    public Sprite spriteOff;
    public AudioClip muzyka;
    //public GameObject nutka;
    public bool dzwiek;
    // Start is called before the first frame update
    void Start()
    {

        dzwiek = true;
        this.transform.GetChild(0).GetComponent<AudioSource>().Play();
        this.transform.GetChild(0).GetComponent<Image>().sprite = spriteOn;


    }


    // Update is called once per frame
    void Update()
    {

    }

    public void Dzwiek()
    {
        if (dzwiek == true)
        {
            this.transform.GetChild(0).GetComponent<AudioSource>().Pause();
            this.transform.GetChild(0).GetComponent<Image>().sprite = spriteOff;
            dzwiek = false;
        }
        else if (dzwiek == false)
        {
            this.transform.GetChild(0).GetComponent<AudioSource>().Play();
            this.transform.GetChild(0).GetComponent<Image>().sprite = spriteOn;
            dzwiek = true;
        }

    }



}
