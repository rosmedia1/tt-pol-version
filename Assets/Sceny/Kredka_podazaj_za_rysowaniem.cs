﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kredka_podazaj_za_rysowaniem : MonoBehaviour
{
    public GameObject rawimage;
    public Vector2 start_pos;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.localPosition = start_pos;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {


            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch1 = Input.GetTouch(i);
                Vector2 touch_pos = new Vector2(touch1.position.x, touch1.position.y);

                if (touch1.phase == TouchPhase.Began || touch1.phase == TouchPhase.Moved)
                {
                    if (rawimage.GetComponent<MouseDraw_ForMap3>().trzymanie == true)
                    {
                        this.transform.localPosition = touch_pos;
                    }

                }
                if (touch1.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Canceled)
                {
                    this.transform.localPosition = start_pos;

                }





            }

        }
    }
}
