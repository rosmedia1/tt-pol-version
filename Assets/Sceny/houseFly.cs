﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class houseFly : MonoBehaviour
{
    //public Vector3 currentPos;
    public Vector3 newPos;
    private Vector3 dir;
    private Vector3 screenBounds;
    private bool dead;
    public bool uderzenie;
    public bool spadanie;
    public GameObject Mucha;
    public GameObject granica;
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        float x = Random.Range(-screenBounds.x, screenBounds.x);
        float y = Random.Range(-screenBounds.y, screenBounds.y - 2f);
        newPos = new Vector3(x, y, this.transform.position.z);
    }


    void Update()
    {

        if (this.transform.position == newPos)
        {
            float x = Random.Range(-screenBounds.x, screenBounds.x);
            float y = Random.Range(-screenBounds.y, screenBounds.y - 2f);
            newPos = new Vector3(x, y, this.transform.position.z);
            this.transform.position = Vector3.MoveTowards(this.transform.position, newPos, 4 * Time.deltaTime);
        }
        dir = newPos - transform.position;
        float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        //this.transform.rotation = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
        Quaternion qRot = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, qRot, 4);



        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.name == "Mucha")
                {
                    if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                    {
                        if (touch.phase == TouchPhase.Began)
                        {

                            uderzenie = true;


                        }



                    }
                }

            }

        }

        if (uderzenie == true)
        {
            Vector3 spadnij = new Vector3(this.transform.position.x, granica.transform.position.y, this.transform.position.z);
            Mucha.GetComponent<Animator>().enabled = false;
            this.transform.position = Vector3.MoveTowards(this.transform.position, spadnij, 4 * Time.deltaTime);
            Mucha.GetComponent<PolygonCollider2D>().enabled = false;
            spadanie = true;

        }
        if (uderzenie == true && spadanie == true)
        {
            Respawn();
        }




    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(4f);
        Vector3 spadnij = new Vector3(this.transform.position.x, granica.transform.position.y, this.transform.position.z);
        Mucha.GetComponent<Animator>().enabled = false;
        this.transform.position = Vector3.MoveTowards(this.transform.position, -spadnij, 4 * Time.deltaTime);
        Mucha.GetComponent<PolygonCollider2D>().enabled = true;
        uderzenie = false;
        spadanie = false;

    }



}
