﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class stuknij : MonoBehaviour
{
    public bool wcisniete;
    public string nazwa_animacji;
    public Sprite sprite_wcisniety;
    public Sprite sprite_odcisniety;
    public AudioClip audio;


    // Start is called before the first frame update
    void Start()
    {
        wcisniete = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Zgniec()
    {
        if (wcisniete == false)
        {
            wcisniete = true;
            StartCoroutine(Stukniecie());
        }

    }


    IEnumerator Stukniecie()
    {
        //this.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 0);
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = audio;
        this.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<SpriteRenderer>().sprite = sprite_wcisniety;
        this.GetComponent<PolygonCollider2D>().enabled = false;
        yield return new WaitForSeconds(7f);
        //this.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
        this.GetComponent<SpriteRenderer>().sprite = sprite_odcisniety;
        this.GetComponent<Animator>().Play(nazwa_animacji);
        yield return new WaitForSeconds(3f);// czas bedzie zalezny od animacji
        this.GetComponent<PolygonCollider2D>().enabled = true;
        wcisniete = false;
    }








}
