﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class poziom5_przyciski : MonoBehaviour
{
    public GameObject[] przyciski;
    public bool[] przyciskOn;
    public GameObject slider;
    public GameObject Mouse_Draw;
    public Sprite zabawkiOff;
    public Sprite zabawkiOn;
    public Sprite drawOff;
    public Sprite drawOn;




    // Start is called before the first frame update
    void Start()
    {


        przyciski[0].GetComponent<Image>().sprite = zabawkiOn;
        przyciski[1].GetComponent<Image>().sprite = drawOff;
        //draw.GetComponent<Rysowanie_poziom5>().enabled = false;
        Mouse_Draw.GetComponent<MouseDraw>().enabled = false;
        slider.SetActive(false);
        przyciskOn[1] = false;
        przyciskOn[0] = true;
        przyciski[0].GetComponent<Button>().onClick.AddListener(() => PrzyciskClick(0));
        przyciski[1].GetComponent<Button>().onClick.AddListener(() => PrzyciskClick(1));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PrzyciskClick(int nr_przycisku)
    {

        if (nr_przycisku == 0)
        {
            if (przyciskOn[0] == false)
            {


                slider.SetActive(false);
                przyciski[0].GetComponent<Image>().sprite = zabawkiOn;
                przyciski[1].GetComponent<Image>().sprite = drawOff;
                Mouse_Draw.GetComponent<MouseDraw>().enabled = false;
                przyciskOn[0] = true;



                przyciskOn[1] = false;

            }
            else if (przyciskOn[0] == true)
            {
                przyciski[0].GetComponent<Image>().sprite = zabawkiOff;

                przyciskOn[0] = false;


            }
        }
        else if (nr_przycisku == 1)
        {
            if (przyciskOn[1] == false)
            {
                Mouse_Draw.GetComponent<MouseDraw>().enabled = true;
                slider.SetActive(true);
                przyciski[0].GetComponent<Image>().sprite = zabawkiOff;
                przyciski[1].GetComponent<Image>().sprite = drawOn;
                przyciskOn[1] = true;



                przyciskOn[0] = false;

            }
            else if (przyciskOn[1] == true)
            {
                Mouse_Draw.GetComponent<MouseDraw>().enabled = false;
                przyciski[1].GetComponent<Image>().sprite = drawOff;
                przyciskOn[1] = false;
                slider.SetActive(false);

            }




        }

    }

    IEnumerator blok()
    {
        yield return new WaitForSeconds(1f);
        przyciskOn[0] = true;
    }
}
