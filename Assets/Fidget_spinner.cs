﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fidget_spinner : MonoBehaviour
{
    public GameObject spinner;
    public GameObject zielony;
    public GameObject czerwony;
    public GameObject rozowy;
    public AudioClip audio;
    public GameObject blysk;
    public bool obracaj;
    btn_back btn;
    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(Zmien_Skina());
    }

    // Update is called once per frame
    void Update()
    {

        //if (obracaj == true)
        //{
        //    spinner.GetComponent<Animator>().enabled = false;
        //    float aktualny_obrot = this.transform.parent.rotation.z;
        //    float rotacja = Mathf.Lerp(this.transform.parent.rotation.z, aktualny_obrot + 320, 2.4f * Time.deltaTime);
        //    this.transform.parent.Rotate(0, 0, rotacja);
        //}


    }

    public void Odpal_Animacje()
    {
        //reszte odkomentuj wrazie czego
        //if (obracaj == false)
        //{
        //    StartCoroutine(Wylacz_obrot());
        //    obracaj = true;
        //    //float aktualny_obrot = this.transform.parent.rotation.z; to bylo zakomentowane
        //    //float rotacja = Mathf.Lerp(this.transform.parent.rotation.z, aktualny_obrot + 360, 5 * Time.deltaTime); to bylo zakomentowane
        //    //this.transform.parent.Rotate(0, 0, rotacja);  to bylo zakomentowane
        //    this.GetComponent<AudioSource>().clip = audio;
        //    this.GetComponent<AudioSource>().Play();
        //}

        spinner.GetComponent<Animator>().enabled = true;
        spinner.GetComponent<Animator>().Play("spinner_obrot");



    }

    public void Odpal_muze()
    {
        spinner.GetComponent<AudioSource>().enabled = true;
        spinner.GetComponent<AudioSource>().clip = audio;
        spinner.GetComponent<AudioSource>().Play();
    }

    public void blyskOn()
    {
        blysk.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void blyskOff()
    {
        blysk.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void Inny()
    {
        int choose = Random.Range(1, 4);
        if (choose == 1)
        {
            zielony.SetActive(true);
            czerwony.SetActive(false);
            rozowy.SetActive(false);
        }
        else if (choose == 2)
        {
            zielony.SetActive(false);
            czerwony.SetActive(true);
            rozowy.SetActive(false);
        }
        else if (choose == 3)
        {
            zielony.SetActive(false);
            czerwony.SetActive(false);
            rozowy.SetActive(true);
        }

    }

    IEnumerator Zmien_Skina()
    {
        yield return new WaitForSeconds(5f);
        int zmien_skina = Random.Range(1, 4);
        if (zmien_skina == 1)
        {
            zielony.SetActive(true);
            czerwony.SetActive(false);
            rozowy.SetActive(false);
        }
        else if (zmien_skina == 2)
        {
            zielony.SetActive(false);
            czerwony.SetActive(true);
            rozowy.SetActive(false);
        }
        else if (zmien_skina == 3)
        {
            zielony.SetActive(false);
            czerwony.SetActive(false);
            rozowy.SetActive(true);
        }
        yield return new WaitForSeconds(10f);
        yield return Zmien_Skina();



    }

    //IEnumerator Wylacz_obrot()
    //{
    //    yield return new WaitForSeconds(2f);
    //    obracaj = false;
    //}







}
