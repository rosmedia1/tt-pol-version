﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kwakaj : MonoBehaviour
{
    public AudioClip kwak;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Kwaki()
    {
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = kwak;
        this.GetComponent<AudioSource>().Play();
    }
}
