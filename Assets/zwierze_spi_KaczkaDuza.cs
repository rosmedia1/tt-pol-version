﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zwierze_spi_KaczkaDuza : MonoBehaviour
{
    public ParticleSystem zzz_kaczki_duzej;
    public GameObject kaczka_duza;
    public GameObject kaczka_duza_oko;
    public GameObject kaczka_mala_oko;
    public Sprite sprite_kaczka_mala;
    public Sprite sprite_kaczka_duza;
    public Sprite sprite_kaczka_mala1;
    public Sprite sprite_kaczka_duza1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpanieDlaKaczki_Duzej()
    {


        if (this.name == "Kaczor")
        {
            zzz_kaczki_duzej.enableEmission = true;
            zzz_kaczki_duzej.Play();
            kaczka_duza_oko.GetComponent<SpriteRenderer>().sprite = sprite_kaczka_duza;

        }


    }
}
