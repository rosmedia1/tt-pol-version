﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class MouseDraw : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Dictionary<int, Vector2?> drawing = new Dictionary<int, Vector2?>();  // int to id palca , Vector2? to pozycja do funkcji odpowiadajacej za rysowanie czyli writepixels nazwa drawing to nazwa tablicy 
    public GameObject samochodzik;
    [SerializeField]
    [Tooltip("The Canvas which is a parent to this Mouse Drawing Component")]
    private Canvas HostCanvas;
    public GameObject w;
    //public bool trzymanie;
    public bool dragging;
    [Range(2, 20)]
    [Tooltip("The Pens Radius")]
    public int penRadius = 10;
    public int carHigh = 100;
    public int carWidth = 1;

    [Tooltip("The Pens Colour.")]
    public Color32 penColour = new Color32(0, 0, 0, 255);

    [Tooltip("The Drawing Background Colour.")]
    public Color32 backroundColour = new Color32(0, 0, 0, 0);

    [SerializeField]
    [Tooltip("Pen Pointer Graphic GameObject")]
    private Image penPointer;


    [Tooltip("Toggles between Pen and Eraser.")]
    public bool IsEraser = false;

    private bool _isInFocus = false;
    /// <summary>
    /// Is this Component in focus.
    /// </summary>
    public bool IsInFocus
    {
        get => _isInFocus;
        private set
        {
            if (value != _isInFocus)
            {
                _isInFocus = value;
                TogglePenPointerVisibility(value);
            }
        }
    }

    private float m_scaleFactor = 10;
    private RawImage m_image;

    //private Vector2? m_lastPos;  // zakomentowane bo robimy to gdzie indziej 
    private Vector2? zmazywanie;

    public Transform granicaLewa;   // wazne
    public Transform granicaPrawa; // wazne

    void Start() // wazne
    {
        //trzymanie = false;
        Init();
    }

    public void Losuj_Kolorki_LR() // wazne
    {
        if (samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie == false)
        {
            float r = UnityEngine.Random.Range(0, 1f);
            float g = UnityEngine.Random.Range(0, 1f);
            float b = UnityEngine.Random.Range(0, 1f);
            Color kolor = new Color(r, g, b, 1f);
            penColour = kolor;
        }

    }

    public void Losuj_Rozmiar_Pena()
    {
        if (samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie == false)
        {
            penRadius = UnityEngine.Random.Range(3, 12);
        }

    }

    private void OnEnable()
    {
        m_image = transform.GetComponent<RawImage>();
        TogglePenPointerVisibility(false);
    }

    public void SliderValueDidChange(float x) // wazne
    {
        WritePixelsV2(x);
    }

    // TODO: Replace with IPointerDownHandler...
    void Update()
    {




        if (Input.touchCount > 0)
        {


            // Touch[] touch1 = Input.touches;
            //Debug.Log("dotyk" + touch1.Length);
            for (int i = 0; i < Input.touchCount; i++)
            {

                Touch touch = Input.GetTouch(i);
                int fingerIndex = touch.fingerId;   // deklarujemy to tutaj bo mamy touch i ogolnie bierzemy wartosc z toucha do finger index jako wartosci do dictionary w przyszlosci 
                var rt = this.GetComponent<RectTransform>();
                var camera = Camera.main;
                Vector2 worldPos = camera.ScreenToWorldPoint(touch.position);
                Vector2 anchoredPosition = rt.InverseTransformPoint(worldPos);
                bool inside = rt.rect.Contains(anchoredPosition);

                if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {

                    print("ended or cancelled");
                    //trzymanie = false;
                    //m_lastPos = null;
                    drawing.Remove(fingerIndex);  // tutaj usuwamy index z palca bo nie jest juz na ekranie wiec wartosc z dictionary poprostu usuwamy 
                    zmazywanie = null;
                    continue;
                }

                if (!inside)
                {
                    //m_lastPos = null;
                    drawing.Remove(fingerIndex);
                    continue;
                }
                if (samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie == true)
                {
                    IsEraser = true;
                }
                else
                {
                    IsEraser = false;
                }

                if (IsEraser != true)
                {
                    //trzymanie = true;


                    if (touch.phase == TouchPhase.Began)
                    {

                        drawing[fingerIndex] = null;  // zamiast uzywania m last pos uzywamy typowo wartosci z dictionary 
                        //trzymanie = true;
                        Losuj_Kolorki_LR();
                        Losuj_Rozmiar_Pena();

                    }

                    if (touch.phase == TouchPhase.Moved)
                    {

                        WritePixels(touch.position, fingerIndex);
                    }




                }



            }
        }


    }


    public bool CzyTrzyma()  // funkcja do sprawdzania boola if trzymanie == true 
    {
        return drawing.Count > 0;
    }


    private void Init() // wazne
    {

        m_scaleFactor = HostCanvas.scaleFactor * 2;

        var tex = new Texture2D(Convert.ToInt32(Screen.width / m_scaleFactor), Convert.ToInt32(Screen.height / m_scaleFactor), TextureFormat.RGBA32, false);
        for (int i = 0; i < tex.width; i++)
        {
            for (int j = 0; j < tex.height; j++)
            {
                tex.SetPixel(i, j, backroundColour);
            }
        }

        tex.Apply();
        m_image.texture = tex;
    }


    private void WritePixels(Vector2 pos, int fingerIndex)   // dodanie finger index do funkcji i wyjmowanie z dictionary do zmiennej last pos informacji
    {
        Vector2? m_lastPos = drawing[fingerIndex];
        pos /= m_scaleFactor;
        var mainTex = m_image.texture;
        var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);
        //Vector2? zmazywanie = czujnik.transform.position;
        var curTex = RenderTexture.active;
        var renTex = new RenderTexture(mainTex.width, mainTex.height, 32);

        Graphics.Blit(mainTex, renTex);
        RenderTexture.active = renTex;

        tex2d.ReadPixels(new Rect(0, 0, mainTex.width, mainTex.height), 0, 0);

        var col = IsEraser ? backroundColour : penColour;
        var positions = m_lastPos.HasValue ? GetLinearPositions(m_lastPos.Value, pos) : new List<Vector2>() { pos };
        var special_position = zmazywanie.HasValue ? GetLinearPositions(zmazywanie.Value, pos) : new List<Vector2>() { pos };
        if (IsEraser == false)
        {
            foreach (var position in positions)
            {
                var pixels = GetNeighbouringPixels(new Vector2(mainTex.width, mainTex.height), position, penRadius);

                if (pixels.Count > 0)
                    foreach (var p in pixels)
                        tex2d.SetPixel((int)p.x, (int)p.y, col);
            }
        }
        else if (IsEraser == true)
        {
            foreach (var position in special_position)
            {

                var pixels = GetNeighbouringPixelsV2(new Vector2(mainTex.width, mainTex.height), position, carHigh, carWidth);

                if (pixels.Count > 0)
                    foreach (var p in pixels)
                        tex2d.SetPixel((int)p.x, (int)p.y, col);
            }
        }

        tex2d.Apply();

        RenderTexture.active = curTex;
        renTex.Release();
        Destroy(renTex);
        Destroy(mainTex);
        curTex = null;
        renTex = null;
        mainTex = null;


        m_image.texture = tex2d;
        //m_lastPos = pos;
        drawing[fingerIndex] = pos;
    }

    private void WritePixelsV2(float x)
    {

        var mainTex = m_image.texture;
        //var gumTex = m_image.texture;
        var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);
        // Vector2? zmazywanie = czujnik.transform.position;
        var curTex = RenderTexture.active;
        var renTex = new RenderTexture(mainTex.width, mainTex.height, 32);

        Graphics.Blit(mainTex, renTex);
        RenderTexture.active = renTex;

        tex2d.ReadPixels(new Rect(0, 0, mainTex.width, mainTex.height), 0, 0);

        var col = backroundColour;

        int tx = (int)(x * mainTex.width);

        //Debug.Log("WritePixelsV2, x = " + x + ", tx = " + tx);

        for (int i = -5; i <= 5; i++)
        {
            for (int j = 0; j < mainTex.height; j++)
            {
                if (tx + i < 0 || tx + i >= mainTex.width) continue;
                tex2d.SetPixel(tx + i, j, col);
            }
        }

        //foreach (var position in special_position)
        //{

        //    var pixels = GetNeighbouringPixelsV2(new Vector2(mainTex.width, mainTex.height), position, carHigh, carWidth);

        //    if (pixels.Count > 0)
        //        foreach (var p in pixels)
        //            tex2d.SetPixel((int)p.x, (int)p.y, col);
        //}


        tex2d.Apply();

        RenderTexture.active = curTex;
        renTex.Release();
        Destroy(renTex);
        Destroy(mainTex);
        curTex = null;
        renTex = null;
        mainTex = null;

        m_image.texture = tex2d;
    }


    /// <summary>
    /// Clears the Texture.
    /// </summary>
    [ContextMenu("Clear Texture")]
    public void ClearTexture()
    {

        var mainTex = m_image.texture;
        var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);

        for (int i = 0; i < tex2d.width; i++)
        {
            for (int j = 0; j < tex2d.height; j++)
            {
                tex2d.SetPixel(i, j, backroundColour);
            }
        }

        tex2d.Apply();
        m_image.texture = tex2d;
    }

    /// <summary>
    /// Gets the neighbouring pixels at a given screenspace position.
    /// </summary>
    /// <param name="textureSize">The texture size or pixel domain.</param>
    /// <param name="position">The ScreenSpace position.</param>
    /// <param name="brushRadius">The Brush radius.</param>
    /// <returns>List of pixel positions.</returns>
    /// zmieniono int brushRadius na czujnik_radius
    private List<Vector2> GetNeighbouringPixels(Vector2 textureSize, Vector2 position, int czujnik_radius)
    {
        var pixels = new List<Vector2>();

        for (int i = -czujnik_radius; i < czujnik_radius; i++)
        {
            for (int j = -czujnik_radius; j < czujnik_radius; j++)
            {
                var pxl = new Vector2(position.x + i, position.y + j);
                if (pxl.x > 0 && pxl.x < textureSize.x && pxl.y > 0 && pxl.y < textureSize.y)
                    pixels.Add(pxl);
            }
        }

        return pixels;
    }


    private List<Vector2> GetNeighbouringPixelsV2(Vector2 textureSize, Vector2 position, int highSensor, int widthSensor)
    {
        var pixels = new List<Vector2>();

        for (int i = -widthSensor; i < widthSensor; i++)
        {
            for (int j = -highSensor; j < highSensor; j++)
            {
                var pxl = new Vector2(position.x + i, position.y + j);
                if (pxl.x > 0 && pxl.x < textureSize.x && pxl.y > 0 && pxl.y < textureSize.y)
                    pixels.Add(pxl);
            }
        }




        return pixels;
    }
    /// <summary>
    /// Interpolates between two positions with a spacing (default = 2)
    /// </summary>
    /// <param name="firstPos"></param>
    /// <param name="secondPos"></param>
    /// <param name="spacing"></param>
    /// <returns>List of interpońlated positions</returns>
    private List<Vector2> GetLinearPositions(Vector2 firstPos, Vector2 secondPos, int spacing = 2)
    {
        var positions = new List<Vector2>();

        var dir = secondPos - firstPos;

        if (dir.magnitude <= spacing)
        {
            positions.Add(secondPos);
            return positions;
        }

        for (int i = 0; i < dir.magnitude; i += spacing)
        {
            var v = Vector2.ClampMagnitude(dir, i);
            positions.Add(firstPos + v);
        }

        positions.Add(secondPos);
        return positions;
    }

    /// <summary>
    /// Sets the Pens Colour.
    /// </summary>
    /// <param name="color"></param>
    public void SetPenColour(Color32 color) => penColour = color;

    /// <summary>
    /// Sets the Radius of the Pen.
    /// </summary>
    /// <param name="radius"></param>
    public void SetPenRadius(int radius) => penRadius = radius;
    // public void SetCarSensor(int czujnik_radius) => carRadius = czujnik_radius;



    /// <summary>
    /// Sets the Size of the Pen Pointer.
    /// </summary>
    private void SetPenPointerSize()
    {
        var rt = penPointer.rectTransform;
        rt.sizeDelta = new Vector2(penRadius * 5, penRadius * 5);
    }

    /// <summary>
    /// Sets the position of the Pen Pointer Graphic.
    /// </summary>
    /// <param name="pos"></param>


    /// <summary>
    /// Toggles the visibility of the Pen Pointer Graphic.
    /// </summary>
    /// <param name="isVisible"></param>
    private void TogglePenPointerVisibility(bool isVisible)
    {
        if (isVisible)
            SetPenPointerSize();

        penPointer.gameObject.SetActive(isVisible);
        Cursor.visible = !isVisible;
    }

    /// <summary>
    /// On Mouse Pointer entering this Components Image Space.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData) => IsInFocus = true;

    /// <summary>
    /// On Mouse Pointer exiting this Components Image Space.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData) => IsInFocus = false;

}
