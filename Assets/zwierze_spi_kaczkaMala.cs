﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zwierze_spi_kaczkaMala : MonoBehaviour
{
    public GameObject kaczka_mala;
    public ParticleSystem zzz_kaczki_malej;
    public GameObject kaczka_duza_oko;
    public GameObject kaczka_mala_oko;
    public Sprite sprite_kaczka_mala;
    public Sprite sprite_kaczka_duza;
    public Sprite sprite_kaczka_mala1;
    public Sprite sprite_kaczka_duza1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpanieDlaKaczki_Malej()
    {


        if (this.name == "KaczkaMala")
        {
            zzz_kaczki_malej.enableEmission = true;
            zzz_kaczki_malej.Play();
            kaczka_mala_oko.GetComponent<SpriteRenderer>().sprite = sprite_kaczka_mala;

        }


    }
}
