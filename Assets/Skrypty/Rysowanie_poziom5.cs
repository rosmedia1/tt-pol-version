﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rysowanie_poziom5 : MonoBehaviour
{
    public GameObject samochodzik;
    public Camera m_camera;
    public GameObject brush;
    public int sortingLayerr;
    LineRenderer currentLineRenderer;
    Dictionary<int, LineRenderer> dotyki = new Dictionary<int, LineRenderer>();



    Vector3 lastPos;


    private void Start()
    {
        sortingLayerr = 0;
    }



    private void Update()
    {

        if (Input.touchCount > 0)
        {

            for (int i = 0; i < Input.touchCount; i++)
            {
                bool odpal_multi = Input.multiTouchEnabled;
                Touch touch1 = Input.GetTouch(i);
                Ray promien = Camera.main.ScreenPointToRay(touch1.position);
                RaycastHit rayhit;

                if (Physics.Raycast(promien, out rayhit))
                {

                    if (rayhit.collider.gameObject.tag == "powierzchnia_rysowania" || rayhit.collider.gameObject.tag == "zabawka")
                    {
                        Drawing(touch1, rayhit.point);
                    }
                }

            }

        }





    }



    void Drawing(Touch touch, Vector3 position)
    {

        if (touch.phase == TouchPhase.Began)
        {

            LineRenderer currentLineRenderer = CreateBrush(position);
            dotyki.Add(touch.fingerId, currentLineRenderer);

        }
        else if (touch.phase == TouchPhase.Moved)
        {
            PointToMousePos(dotyki[touch.fingerId], position);
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            dotyki.Remove(touch.fingerId);
        }
        else if (touch.phase == TouchPhase.Canceled)
        {
            dotyki.Remove(touch.fingerId);
        }

    }


    Vector2 getTouchPosition(Vector2 touchPosition)
    {
        return GetComponent<Camera>().ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, transform.position.z));
    }

    LineRenderer CreateBrush(Vector3 position)
    {


        sortingLayerr++;
        if (sortingLayerr >= 30000)
        {
            sortingLayerr = 0;
        }

        Vector3 create_pos = new Vector3(position.x, position.y, this.transform.position.z);                           // wrazie czego dodaj this.transform do x,y
        GameObject brushInstance = Instantiate(brush, create_pos, Quaternion.identity, this.transform) as GameObject;
        LineRenderer currentLineRenderer = brushInstance.GetComponent<LineRenderer>();
        brushInstance.GetComponent<Sprawdz_punkt_improve>().samochodzik = samochodzik;
        brushInstance.GetComponent<LineRenderer>().sortingOrder = sortingLayerr;
        Vector3 new_pos = new Vector3(position.x, position.y, this.transform.position.z - 3);


        currentLineRenderer.SetPosition(0, new_pos);
        currentLineRenderer.SetPosition(1, new_pos);
        float losowa_grubosc = Random.Range(0.1f, 0.4f);
        float r = Random.Range(0, 1f);
        float g = Random.Range(0, 1f);
        float b = Random.Range(0, 1f);

        Color kolor = new Color(r, g, b, 1f);

        currentLineRenderer.SetWidth(losowa_grubosc, losowa_grubosc);
        currentLineRenderer.material.color = kolor;
        return currentLineRenderer;
    }

    void AddAPoint(LineRenderer currentLineRenderer, Vector3 pointPos)
    {
        currentLineRenderer.positionCount++;
        int positionIndex = currentLineRenderer.positionCount - 1;
        currentLineRenderer.SetPosition(positionIndex, pointPos);

    }

    void PointToMousePos(LineRenderer currentLineRenderer, Vector3 position)
    {

        Vector3 new_pos = new Vector3(position.x, position.y, this.transform.position.z - 3);

        if (lastPos != new_pos)
        {
            AddAPoint(currentLineRenderer, new_pos);
            lastPos = new_pos;
        }
    }


    //private List<Vector2> GetLinearPositions(Vector2 firstPos, Vector2 secondPos, int spacing = 2)
    //{
    //    var positions = new List<Vector2>();

    //    var dir = secondPos - firstPos;

    //    if (dir.magnitude <= spacing)
    //    {
    //        positions.Add(secondPos);
    //        return positions;
    //    }

    //    for (int i = 0; i < dir.magnitude; i += spacing)
    //    {
    //        var v = Vector2.ClampMagnitude(dir, i);
    //        positions.Add(firstPos + v);
    //    }

    //    positions.Add(secondPos);
    //    return positions;
    //}


    //private List<Vector2> GetNeighbouringPixels(Vector2 textureSize, Vector2 position, int brushRadius)
    //{
    //    var pixels = new List<Vector2>();

    //    for (int i = -brushRadius; i < brushRadius; i++)
    //    {
    //        for (int j = -brushRadius; j < brushRadius; j++)
    //        {
    //            var pxl = new Vector2(position.x + i, position.y + j);
    //            if (pxl.x > 0 && pxl.x < textureSize.x && pxl.y > 0 && pxl.y < textureSize.y)
    //                pixels.Add(pxl);
    //        }
    //    }

    //    return pixels;
    //}

    //private void WritePixels(Vector2 pos)
    //{
    //    ////pos /= m_scaleFactor;
    //    ////var mainTex = m_image.texture;
    //    ////var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);

    //    //var curTex = RenderTexture.active;
    //    ////var renTex = new RenderTexture(mainTex.width, mainTex.height, 32);

    //    //Graphics.Blit(mainTex, renTex);
    //    //RenderTexture.active = renTex;

    //    //tex2d.ReadPixels(new Rect(0, 0, mainTex.width, mainTex.height), 0, 0);

    //    //var col = IsEraser ? backroundColour : penColour;
    //    //var positions = m_lastPos.HasValue ? GetLinearPositions(m_lastPos.Value, pos) : new List<Vector2>() { pos };

    //    //foreach (var position in positions)
    //    //{
    //    //    var pixels = GetNeighbouringPixels(new Vector2(mainTex.width, mainTex.height), position, penRadius);

    //    //    if (pixels.Count > 0)
    //    //        foreach (var p in pixels)
    //    //            tex2d.SetPixel((int)p.x, (int)p.y, col);
    //    //}

    //    //tex2d.Apply();

    //    //RenderTexture.active = curTex;
    //    //renTex.Release();
    //    //Destroy(renTex);
    //    //Destroy(mainTex);
    //    //curTex = null;
    //    //renTex = null;
    //    //mainTex = null;

    //    //m_image.texture = tex2d;
    //    //m_lastPos = pos;
    //}

}




