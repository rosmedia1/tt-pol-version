﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Odpal_planszeV2 : MonoBehaviour
{
    public GameObject[] przyciski_menu;
    public GameObject przejscie;
    public GameObject rodzice;
    public GameObject no_ads;
    // public Animation transition;
    public float transition_time;
    public int buildIndex;
    // Update is called once per frame
    void Update()
    {



    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        no_ads.GetComponent<Animator>().enabled = true;
        no_ads.GetComponent<Animator>().Play("no_ads_zejscie");
        przejscie.GetComponent<Animator>().Play("buttony_przejscie_menu_OnTransition");
        rodzice.GetComponent<Animator>().Play("rodzice_zejscie");
        yield return new WaitForSeconds(transition_time);
        SceneManager.LoadScene(buildIndex);
    }





}
