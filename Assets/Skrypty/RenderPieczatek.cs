﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderPieczatek : MonoBehaviour
{
    public AudioClip[] audioClip;
    public bool can_spawn;
    public GameObject aktualnyPrefab;
    private int numer_klocka;

    void Start()
    {
        can_spawn = true;


    }
    public void Zmiana_pref(GameObject prefab, int nr_klocka)
    {
        aktualnyPrefab = prefab;
        numer_klocka = nr_klocka;
    }



    void Update()
    {

        if (Input.touchCount > 0)
        {
            Ray promien = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit rayhit;
            can_spawn = true;
            if (Physics.Raycast(promien, out rayhit))
            {
                if (rayhit.collider.gameObject.tag == "klocek")
                {
                    can_spawn = false;
                }

            }
        }
        if (Input.touchCount > 0)
        {
            Touch dotyk = Input.GetTouch(0);
            Vector3 dotykPoss = Camera.main.ScreenToWorldPoint(dotyk.position);

            if (dotyk.phase == TouchPhase.Began && aktualnyPrefab != null && can_spawn == true)
            {
                Vector3 dotykPossnew = new Vector3(dotykPoss.x, dotykPoss.y, aktualnyPrefab.transform.position.z);
                Instantiate(aktualnyPrefab, dotykPossnew, Quaternion.identity, this.transform);
                this.GetComponent<AudioSource>().PlayOneShot(audioClip[numer_klocka]);
            }

        }

    }
}
