﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class parental_gate : MonoBehaviour
{
    public GameObject inap_screen;
    public int nr_planszy;
    public GameObject Parental_gate;
    public Sprite[] cyfry_niebieskie;
    public Sprite[] cyfry_biale;
    public GameObject cyfra1;
    public GameObject cyfra2;
    public GameObject[] wyniki;
    public Button exit;
    public bool aktywny;
    private int pierwsza_cyfra;
    private int druga_cyfra;
    public int[] liczby;
    public Button[] odpowiedzi;
    private int wynik;
    public AudioClip exitt;
    public AudioClip audio_error;
    public AudioClip audio_correct;
    public GameObject no_ads;
    public GameObject[] tlo_menu;
    public GameObject rodzicee;
    public GameObject btn_back;

    // Start is called before the first frame update
    void Start()
    {
        //Parental_gate.SetActive(false);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        odpowiedzi[0].onClick.AddListener(() => Sprawdz_wynik(0));
        odpowiedzi[1].onClick.AddListener(() => Sprawdz_wynik(1));
        odpowiedzi[2].onClick.AddListener(() => Sprawdz_wynik(2));

    }
    // Update is called once per frame
    void Update()
    {

    }

    public void Zamknij()
    {
        exit.GetComponent<AudioSource>().enabled = true;
        exit.GetComponent<AudioSource>().clip = exitt;
        exit.GetComponent<AudioSource>().Play();
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1200, this.transform.position.z);
        aktywny = false;
        if (tlo_menu != null)
        {
            for (int i = 0; i < tlo_menu.Length; i++)
            {
                if (tlo_menu[i].GetComponent<Button>() != null)
                {
                    tlo_menu[i].GetComponent<Button>().enabled = true;
                }
                if (tlo_menu[i].GetComponent<GraphicRaycaster>() != null)
                {
                    tlo_menu[i].GetComponent<GraphicRaycaster>().enabled = true;
                }

            }
            if (inap_screen != null)
            {
                inap_screen.GetComponent<Button>().enabled = true;
            }

            if (btn_back != null)
            {
                btn_back.GetComponent<Button>().enabled = true;
            }
        }


    }

    public void Otworz()
    {
        for (int i = 0; i < tlo_menu.Length; i++)
        {
            if (tlo_menu[i].GetComponent<Button>() != null)
            {
                tlo_menu[i].GetComponent<Button>().enabled = false;
            }
            if (tlo_menu[i].GetComponent<GraphicRaycaster>() != null)
            {
                tlo_menu[i].GetComponent<GraphicRaycaster>().enabled = false;
            }

        }

        if (btn_back != null)
        {
            btn_back.GetComponent<Button>().enabled = false;
        }
        if (inap_screen != null)
        {
            inap_screen.GetComponent<Button>().enabled = false;
        }

        this.transform.position = new Vector3(0, 0, this.transform.position.z);

        pierwsza_cyfra = Random.Range(1, 6);
        druga_cyfra = Random.Range(1, 6);
        liczby[0] = pierwsza_cyfra + druga_cyfra;
        wynik = liczby[0];
        liczby[1] = Random.Range(1, 11);
        liczby[2] = Random.Range(1, 11);
        while (liczby[1] == liczby[0])
        {
            liczby[1] = Random.Range(1, 11);
        }
        while (liczby[1] == liczby[2])
        {
            liczby[1] = Random.Range(1, 11);
        }
        while (liczby[2] == liczby[0])
        {
            liczby[2] = Random.Range(1, 11);
        }
        while (liczby[2] == liczby[1])
        {
            liczby[2] = Random.Range(1, 11);
        }
        cyfra1.GetComponent<SpriteRenderer>().sprite = cyfry_niebieskie[pierwsza_cyfra - 1];
        cyfra2.GetComponent<SpriteRenderer>().sprite = cyfry_niebieskie[druga_cyfra - 1];

        for (int i = 0; i < liczby.Length; i++)
        {
            int rnd = Random.Range(0, liczby.Length);
            int tempGO;


            tempGO = liczby[rnd];
            liczby[rnd] = liczby[i];
            liczby[i] = tempGO;
        }
        for (int i = 0; i < wyniki.Length; i++)
        {
            wyniki[i].GetComponent<SpriteRenderer>().sprite = cyfry_biale[liczby[i] - 1];
        }
        aktywny = true;
        exit.onClick.AddListener(() => Zamknij());
    }


    public void Sprawdz_wynik(int i)
    {
        string kliknieta_liczba = odpowiedzi[i].transform.GetChild(1).GetComponent<SpriteRenderer>().sprite.name;
        Debug.Log(i);



        if (kliknieta_liczba == wynik.ToString() + "_white")
        {
            Debug.Log("kliknieta");
            if (inap_screen != null && inap_screen.active == false && rodzicee.GetComponent<przycisk_dla_rodzicow>().rodzice == true)
            {

                Debug.Log("gga");
                this.GetComponent<AudioSource>().clip = audio_correct;
                this.GetComponent<AudioSource>().Play();
                SceneManager.LoadScene(nr_planszy);
            }
            else if (inap_screen != null && inap_screen.active == true)
            {
                Debug.Log("hihi");
                this.GetComponent<AudioSource>().clip = audio_correct;
                this.GetComponent<AudioSource>().Play();
                SceneManager.LoadScene("kupno");
            }
            else if (inap_screen != null && no_ads != null)
            {
                Debug.Log("gaff");
                this.GetComponent<AudioSource>().clip = audio_correct;
                this.GetComponent<AudioSource>().Play();
                SceneManager.LoadScene("kupno");
            }



        }
        else
        {

            this.GetComponent<AudioSource>().clip = audio_error;
            this.GetComponent<AudioSource>().Play();
            Zamknij();
        }




    }



}
