﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dzwieki_idle : MonoBehaviour
{
    public AudioClip[] odtworz_dzwieki_idle;
    private AnimatorClipInfo[] animator_info;
    public AudioClip[] odtworz_trzaski;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Odtworz_idle()
    {
        this.GetComponent<AudioSource>().volume = 1f;
        animator_info = this.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);
        if (animator_info.Length == 0) return;
        string nazwa_animacji = animator_info[0].clip.name;
        // Debug.Log(nazwa_animacji);
        if (nazwa_animacji.Contains("idle"))
        {
            if (nazwa_animacji.Contains("1"))
            {
                this.GetComponent<AudioSource>().clip = odtworz_dzwieki_idle[0];
                this.GetComponent<AudioSource>().Play();
            }
            if (nazwa_animacji.Contains("2"))
            {
                this.GetComponent<AudioSource>().clip = odtworz_dzwieki_idle[1];
                this.GetComponent<AudioSource>().Play();
            }
            if (nazwa_animacji.Contains("3"))
            {
                this.GetComponent<AudioSource>().clip = odtworz_dzwieki_idle[2];
                this.GetComponent<AudioSource>().Play();
            }
            if (nazwa_animacji.Contains("4"))
            {
                this.GetComponent<AudioSource>().clip = odtworz_dzwieki_idle[3];
                this.GetComponent<AudioSource>().Play();
            }
            if (nazwa_animacji.Contains("5"))
            {
                this.GetComponent<AudioSource>().clip = odtworz_dzwieki_idle[4];
                this.GetComponent<AudioSource>().Play();
            }

        }
    }
    public void OdtworzTrzaski()
    {
        this.GetComponent<AudioSource>().volume = 1f;
        this.GetComponent<AudioSource>().clip = odtworz_trzaski[Random.Range(0, odtworz_trzaski.Length)];
        this.GetComponent<AudioSource>().Play();

    }
    public void Wyldziw()
    {
        this.GetComponent<AudioSource>().volume = 0f;

    }
    public void WlaDziw()
    {
        this.GetComponent<AudioSource>().volume = 1f;
        this.GetComponent<AudioSource>().clip = odtworz_dzwieki_idle[0];
        this.GetComponent<AudioSource>().Play();
    }

}
