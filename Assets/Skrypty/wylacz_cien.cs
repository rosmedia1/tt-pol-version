﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wylacz_cien : MonoBehaviour
{
    public GameObject cien;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Graj_cien()
    {
        cien.GetComponent<Animator>().SetBool("pilka_cien", true);
        cien.GetComponent<Animator>().Play("pilka_idle_cien");
    }

    public void Wylacz_cien()
    {
        cien.GetComponent<Animator>().SetBool("pilka_cien", false);
        cien.GetComponent<Animator>().Play("zniknij2");
    }
}
