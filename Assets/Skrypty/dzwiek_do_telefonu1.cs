﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dzwiek_do_telefonu1 : MonoBehaviour
{
    public AudioClip idle1;
    public AudioClip idle2;
    public AudioClip idle3;
    public AudioClip idle4;
    public AudioClip idle5;
    public AudioClip idle6;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Odtworz_idla1()
    {

        this.GetComponent<AudioSource>().clip = idle1;
        this.GetComponent<AudioSource>().Play();


    }
    public void Odtworz_idla2()
    {

        this.GetComponent<AudioSource>().clip = idle2;
        this.GetComponent<AudioSource>().Play();


    }
    public void Odtworz_idla3()
    {

        this.GetComponent<AudioSource>().clip = idle3;
        this.GetComponent<AudioSource>().Play();


    }
    public void Odtworz_idla4()
    {

        this.GetComponent<AudioSource>().clip = idle4;
        this.GetComponent<AudioSource>().Play();

    }
    public void Odtworz_idla5()
    {

        this.GetComponent<AudioSource>().clip = idle5;
        this.GetComponent<AudioSource>().Play();

    }
    public void Odtworz_idla6()
    {

        this.GetComponent<AudioSource>().clip = idle6;
        this.GetComponent<AudioSource>().Play();

    }


}
