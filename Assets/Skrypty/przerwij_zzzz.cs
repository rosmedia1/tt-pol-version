﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class przerwij_zzzz : MonoBehaviour
{
    public GameObject swinka1;
    public GameObject swinka;
    public GameObject skrypt_zzzz;
    public ParticleSystem moje_zzzz;
    public GameObject oko_duzej;
    public GameObject oko_malej;
    public Sprite oko_duzejj;
    public Sprite oko_malejj;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        moje_zzzz.enableEmission = false;
                        moje_zzzz.Stop();
                        if (swinka1 != null)
                        {
                            this.GetComponent<zwierze_spi>().sen = false;
                            swinka1.SetActive(false);
                            swinka.SetActive(true);

                        }
                    }
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (this.name == "Kaczor")
                        {
                            oko_duzej.GetComponent<SpriteRenderer>().sprite = oko_duzejj;
                            moje_zzzz.enableEmission = false;
                            moje_zzzz.Stop();
                        }
                        if (this.transform.parent.name == "KaczkaMala")
                        {
                            oko_malej.GetComponent<SpriteRenderer>().sprite = oko_malejj;
                            moje_zzzz.enableEmission = false;
                            moje_zzzz.Stop();
                        }
                    }
                }
            }
        }


    }

    public void ZwierzeJe()
    {
        moje_zzzz.enableEmission = false;
        moje_zzzz.Stop();

    }



}
