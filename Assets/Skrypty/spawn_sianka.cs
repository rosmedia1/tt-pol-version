﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_sianka : MonoBehaviour
{
    public GameObject Sianko_pref;
    private GameObject _Sianko_pref;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            for(int i = 0; i< Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                
                if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                {
                    
                    if(touch.phase == TouchPhase.Began)
                    {
                       Vector3 spawn_pos = new Vector3(touch_pos.x, touch_pos.y, this.transform.position.z);
                      _Sianko_pref =  Instantiate(Sianko_pref, spawn_pos, Quaternion.identity, this.transform);
                    }
                    else if(touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {

                        Destroy(_Sianko_pref);
                    }
                }

            }
            
        }
    }
}
