﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    public string nazwaAnimacji;
    void Start()
    {

    }


    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch dotyk = Input.GetTouch(0);
            Vector3 dotykposs = Camera.main.ScreenToWorldPoint(dotyk.position);
            if (this.gameObject.GetComponent<PolygonCollider2D>().OverlapPoint(dotykposs))
            {
                this.transform.parent.GetComponent<Animator>().Play(nazwaAnimacji);

            }
        }
    }
}
