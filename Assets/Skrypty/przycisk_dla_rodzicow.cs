﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class przycisk_dla_rodzicow : MonoBehaviour
{
    public GameObject parental_gate;
    public AudioClip audio;
    public bool rodzice;
    // Start is called before the first frame update
    void Start()
    {
        parental_gate.SetActive(false);
        parental_gate.GetComponent<parental_gate>().Zamknij();
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void Ekran_dla_rodzicow()
    {
        rodzice = true;
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = audio;
        this.GetComponent<AudioSource>().Play();
        parental_gate.SetActive(true);
        parental_gate.GetComponent<parental_gate>().Otworz();

    }





}
