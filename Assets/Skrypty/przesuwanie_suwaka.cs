﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class przesuwanie_suwaka : MonoBehaviour
{
    public bool przeciaganie;
    public bool lewa_granica;
    public bool prawa_granica;
    public GameObject lewa;
    public GameObject prawa;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                Ray promien = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit ray_hit;
                if (Physics.Raycast(promien, out ray_hit))
                {

                    if (ray_hit.collider.gameObject == this.transform.gameObject)
                    {
                        if (touch.phase == TouchPhase.Began)
                        {
                            przeciaganie = true;
                        }
                    }
                }
                if (przeciaganie == true)
                {

                    if (touch.phase == TouchPhase.Moved)
                    {
                        float direction = touch_pos.x - this.transform.position.x;

                        this.transform.position = new Vector3(touch_pos.x, this.transform.position.y, this.transform.position.z);
                        if (lewa_granica == true)
                        {
                            if (direction < 0)
                            {
                                this.transform.position = new Vector3(touch_pos.x - direction, this.transform.position.y, this.transform.position.z);
                            }
                            else
                            {
                                this.transform.position = new Vector3(touch_pos.x, this.transform.position.y, this.transform.position.z);
                            }
                        }

                        if (prawa_granica == true)
                        {
                            if (direction > 0)
                            {
                                this.transform.position = new Vector3(touch_pos.x - direction, this.transform.position.y, this.transform.position.z);
                            }
                            else
                            {
                                this.transform.position = new Vector3(touch_pos.x, this.transform.position.y, this.transform.position.z);
                            }
                        }
                    }

                    else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        przeciaganie = false;
                    }
                }
            }



        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == lewa.gameObject)
        {

            lewa_granica = true;
        }

        if (other.gameObject == prawa.gameObject)
        {

            prawa_granica = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == lewa.gameObject)
        {

            lewa_granica = false;
        }

        if (other.gameObject == prawa.gameObject)
        {
            prawa_granica = false;
        }
    }

}
