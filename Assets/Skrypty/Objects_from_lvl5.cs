﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objects_from_lvl5 : MonoBehaviour
{

    [SerializeField]
    private float speed = 10f;
    [SerializeField]
    private float up_speed = 2f;
    [SerializeField]
    private float max_speed = 10f;

    Rigidbody rb;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion device_rotation = DeviceRotation.Get();
        transform.rotation = device_rotation;
        //rb.AddForce(transform.forward * speed, ForceMode.Acceleration);

        rb.AddForce(transform.up * up_speed, ForceMode.Acceleration);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, max_speed);


    }
}
