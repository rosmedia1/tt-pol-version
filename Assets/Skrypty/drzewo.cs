﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drzewo : MonoBehaviour
{
    public GameObject ApplPre;
    public int licznik;
    public int ilosc_jablek;
    public GameObject[] spawner_jablek;
    public GameObject swinka;
    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(Odlicz());
        Spawnuj_jablko();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Zabrano_jablko()
    {

        ilosc_jablek -= 1;
        if (ilosc_jablek < 0)
        {
            ilosc_jablek = 0;
        }
    }

    IEnumerator Odlicz()
    {
        if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("drzewo_zejscieV2") == true)
        {
            yield break;
        }
        else
        {
            yield return new WaitForSeconds(1);
            licznik++;
            if (licznik < 5)  //dodales rowna sie
            {
                this.GetComponent<Animator>().enabled = false;
                StartCoroutine(Odlicz());
            }
            else if (licznik >= 5)
            {
                if (ilosc_jablek <= 0)
                {
                    licznik = 0;
                    StartCoroutine(Animacja_drzewo());
                }
                else
                {
                    licznik = 0;
                    StartCoroutine(Odlicz());
                }
            }

        }

    }




    IEnumerator Animacja_drzewo()
    {


        if (this.GetComponent<Animator>().enabled != true)
        {
            this.GetComponent<Animator>().enabled = true;
            this.GetComponent<Animator>().Play("drzewo_animacja4");
            yield return new WaitForSeconds(18);
            this.GetComponent<Animator>().enabled = false;
            StartCoroutine(Odlicz());
        }
        else if (this.GetComponent<Animator>().enabled == true)
        {
            this.GetComponent<Animator>().enabled = false;
            yield break;
        }

    }

    public void Spawnuj_jablko()
    {
        for (int i = 0; i < spawner_jablek.Length; i++)
        {
            var Apples = Instantiate(ApplPre, spawner_jablek[i].gameObject.transform.position, spawner_jablek[i].gameObject.transform.rotation);
            Apples.GetComponent<przyciaganie_jedzenia>().drzewo = this.gameObject;
            Apples.GetComponent<przyciaganie_jedzenia>().swinka = swinka;
            Apples.transform.parent = spawner_jablek[i].gameObject.transform;
            ilosc_jablek++;
            //StartCoroutine(SpawnMoreApples());

        }

    }
    IEnumerator SpawnMoreApples()
    {
        for (int i = 0; i < spawner_jablek.Length; i++)
        {
            if (spawner_jablek[i].transform.childCount == 0)
            {
                yield return new WaitForSeconds(Random.Range(8f, 14f));
                var Apples = Instantiate(ApplPre, spawner_jablek[i].gameObject.transform.position, spawner_jablek[i].gameObject.transform.rotation);
                Apples.transform.parent = spawner_jablek[i].gameObject.transform;
                //Apples.GetComponent<Animator>().Play("jablko_pojaw_sie");
                //  Apples.GetComponent<Animator>().speed = Random.Range(0.5f, 1f);
                Apples.GetComponent<przyciaganie_jedzenia>().drzewo = this.gameObject;
                ilosc_jablek += 1;
            }
            else
            {
                yield return new WaitForSeconds(Random.Range(8f, 14f));
                StartCoroutine(SpawnMoreApples());
            }
        }
    }

}
