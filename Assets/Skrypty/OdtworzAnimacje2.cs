﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OdtworzAnimacje2 : MonoBehaviour
{

    public GameObject zabawka;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OdtworzAnimacje()
    {
        if (zabawka.name == "misiu")
        {
            int losowa_animacja = Random.Range(1, 6);
            zabawka.GetComponent<Animator>().Play("mis_animacja" + losowa_animacja);
            string nazwa_animacji = "mis_animacja" + losowa_animacja.ToString();
            //this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);
        }

        if (zabawka.name == "Pilka")
        {
            int losowa_animacja = Random.Range(1, 3);
            zabawka.GetComponent<Animator>().Play("pilka_animacja" + losowa_animacja);
            string nazwa_animacji = "pilka_animacja" + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);
        }

        if (zabawka.name == "back_btn_lvl0")
        {
            int losowa_animacja = Random.Range(1, 2);
            zabawka.GetComponent<Animator>().Play("back_btn_lvl_menu" + losowa_animacja);
            string nazwa_animacji = "back_btn_lvl_menu" + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);
        }
        if (zabawka.name == "back_btn_lvl1")
        {
            int losowa_animacja = Random.Range(1, 2);
            zabawka.GetComponent<Animator>().Play("back_btn_lvl1_animacja" + losowa_animacja);
            string nazwa_animacji = "back_btn_lvl1_animacja" + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);
        }
    }

}
