﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class szerokosc_suwaka : MonoBehaviour
{
    public GameObject suwak;
    private Vector3 krawedzie_ekranu;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Dostosuj_suwak());
    }

    IEnumerator Dostosuj_suwak()
    {
        yield return new WaitForSeconds(0.4f);
        float canvasx = this.GetComponent<RectTransform>().rect.size.x;
        suwak.GetComponent<RectTransform>().sizeDelta = new Vector2(canvasx, suwak.GetComponent<RectTransform>().rect.size.y);

        GameObject punktL = suwak.transform.GetChild(0).gameObject;
        GameObject punktP = suwak.transform.GetChild(1).gameObject;

        krawedzie_ekranu = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        punktL.transform.position = new Vector3(-krawedzie_ekranu.x - 2f, punktL.transform.position.y, punktL.transform.position.z);
        punktP.transform.position = new Vector3(krawedzie_ekranu.x + 2f, punktP.transform.position.y, punktP.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }



}
