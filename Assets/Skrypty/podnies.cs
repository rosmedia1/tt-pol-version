﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class podnies : MonoBehaviour
{
    private Vector3 start_pos;
    private bool przeciaganie;
    public bool powrot;
    public bool idle;
    private bool garnek;
    public string nazwaAnim;
    public int fingerIndex = 0;
    public GameObject garnekObj;
    public GameObject warzywo;
    // Start is called before the first frame update
    void Start()
    {
        idle = true;
        start_pos = warzywo.transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.GetComponent<BoxCollider2D>().OverlapPoint(touch_pos))
                {
                    if (fingerIndex >= 0 && fingerIndex != touch.fingerId) { continue; }
                    if (touch.phase == TouchPhase.Began)
                    {
                        fingerIndex = touch.fingerId;
                        warzywo.transform.GetChild(0).GetComponent<Image>().CrossFadeAlpha(0, 0.02f, false);
                        przeciaganie = true;
                        idle = false;
                        powrot = false;
                    }


                }

                if (przeciaganie == true && fingerIndex >= 0)
                {
                    if (touch.fingerId != fingerIndex) { continue; }
                    if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                    {
                        warzywo.transform.position = new Vector3(touch_pos.x, touch_pos.y, warzywo.transform.position.z);
                    }
                    if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        //warzywo.transform.GetChild(0).GetComponent<Image>().CrossFadeAlpha(1, 0.02f, false);
                        przeciaganie = false;
                        fingerIndex = -1;

                        if (garnek == false)
                        {

                            powrot = true;
                        }
                        else if (garnek == true)
                        {
                            powrot = false;
                        }

                    }

                }
            }

        }
        if (powrot == true && idle == false)
        {
            warzywo.transform.position = Vector3.Lerp(warzywo.transform.position, start_pos, 6 * Time.deltaTime);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == garnekObj.gameObject && przeciaganie == true)
        {
            przeciaganie = false;


            //wpisac odpalenie animacji
            if (warzywo.name == "pomidor")
            {

                int animacja = 5;
                garnekObj.GetComponent<Animator>().enabled = true;
                garnekObj.GetComponent<Animator>().Play("garnek2_animacja5");
                string nazwa_animacji = nazwaAnim + animacja.ToString();
                //garnekObj.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

            }
            if (warzywo.name == "ogorek")
            {

                int animacja = 4;
                garnekObj.GetComponent<Animator>().enabled = true;
                garnekObj.GetComponent<Animator>().Play("garnek2_animacja4");
                string nazwa_animacji = nazwaAnim + animacja.ToString();
                // garnekObj.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

            }


            garnek = true;
            powrot = true;
            StartCoroutine(Restart_warzywa());
        }
        else
        {
            garnek = false;
            powrot = true;
        }

    }
    IEnumerator Restart_warzywa()
    {
        garnekObj.GetComponent<BoxCollider2D>().enabled = false;
        warzywo.transform.GetChild(0).GetComponent<Image>().CrossFadeAlpha(0, 0.02f, false);
        warzywo.GetComponent<BoxCollider2D>().enabled = false;
        warzywo.GetComponent<Image>().CrossFadeAlpha(0f, 0.02f, false);
        fingerIndex -= 1;
        yield return new WaitForSeconds(2f);
        warzywo.GetComponent<Image>().CrossFadeAlpha(1f, 0.1f, false);
        warzywo.GetComponent<BoxCollider2D>().enabled = true;
        warzywo.transform.GetChild(0).GetComponent<Image>().CrossFadeAlpha(1, 0.02f, false);
        garnek = false;
        garnekObj.GetComponent<BoxCollider2D>().enabled = true;
    }

}
