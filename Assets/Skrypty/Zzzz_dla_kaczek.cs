﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zzzz_dla_kaczek : MonoBehaviour
{

    public ParticleSystem zzzz_kaczka_mala;
    public ParticleSystem zzzz_kaczka_duza;
    public GameObject kaczka_mala;
    public GameObject kaczka_duza;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Stworz_Zzzz_dla_kaczki_malej()
    {

        zzzz_kaczka_mala.enableEmission = true;
        zzzz_kaczka_mala.Play();

    }

    public void Stworz_zzzzz_dla_kaczki_duzej()
    {
        zzzz_kaczka_duza.enableEmission = true;
        zzzz_kaczka_duza.Play();
    }

    public void Przerwij_zzzz_kaczki_malej()
    {
        zzzz_kaczka_mala.enableEmission = false;
        zzzz_kaczka_mala.Stop();
    }

    public void Przerwij_zzzz_kaczki_duzej()
    {
        zzzz_kaczka_duza.enableEmission = false;
        zzzz_kaczka_duza.Stop();
    }




}
