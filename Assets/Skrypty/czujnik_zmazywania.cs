﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class czujnik_zmazywania : MonoBehaviour
{
    public GameObject samochodzik;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie == true)
        {
            Rusz_Sie();
        }

    }

    public void Rusz_Sie()
    {

        this.transform.position = new Vector2(samochodzik.transform.position.x, samochodzik.transform.position.y);
    }
}
