﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mucha2 : MonoBehaviour
{
    public Vector3 newPos;
    private Vector3 dir;
    private Vector3 screenBounds;
    public GameObject granica;
    public Quaternion angleAxis;
    public GameObject Mucha;
    public bool uderzenie;
    public bool spadanie;
    public Transform wskaznik;
    public bool restart;
    public AudioClip audio;
    // Start is called before the first frame update
    void Start()
    {
        Mucha.GetComponent<AudioSource>().enabled = true;
        Mucha.GetComponent<AudioSource>().clip = audio;
        Mucha.GetComponent<AudioSource>().Play();
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        float x = Random.Range(-screenBounds.x, screenBounds.x);
        float y = Random.Range(-screenBounds.y, screenBounds.y - 2f);
        newPos = new Vector3(x, y, this.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (uderzenie == false && spadanie == false && restart == false)
        {


            this.transform.position = Vector3.MoveTowards(this.transform.position, newPos, 4 * Time.deltaTime);


            if (this.transform.position == newPos)
            {
                float x = Random.Range(-screenBounds.x, screenBounds.x);
                float y = Random.Range(-screenBounds.y, screenBounds.y - 2f);
                newPos = new Vector3(x, y, this.transform.position.z);
                this.transform.position = Vector3.MoveTowards(this.transform.position, newPos, 4 * Time.deltaTime);
            }
            dir = newPos - transform.position;
            float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            //this.transform.rotation = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
            Quaternion qRot = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, qRot, 20);
        }

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.name == "Mucha")
                {
                    if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                    {
                        if (touch.phase == TouchPhase.Began)
                        {
                            Mucha.GetComponent<AudioSource>().Stop();
                            uderzenie = true;
                            spadanie = true;

                        }
                    }
                }

            }
        }

        if (uderzenie == true && spadanie == true)
        {
            Debug.Log("mucha_kliknieta");
            uderzenie = false;

            Vector3 spadnij = new Vector3(this.transform.position.x, granica.transform.position.y, this.transform.position.z);
            //this.transform.position = Vector3.MoveTowards(this.transform.position, spadnij, 4 * Time.deltaTime);
            //this.transform.position = new Vector3(this.transform.position.x, granica.transform.position.y, this.transform.position.z);
            //Vector3.Lerp(this.transform.position, granica.transform.position, this.transform.position.z);
            StartCoroutine(MoveObject(this.transform, this.transform.position, spadnij, 0.5f));
            Mucha.GetComponent<Animator>().enabled = false;
            Mucha.GetComponent<PolygonCollider2D>().enabled = false;
            StartCoroutine(Respawn());


        }

    }

    IEnumerator Respawn()
    {
        print("Respawn");
        yield return new WaitForSeconds(4f);
        dir = wskaznik.position - transform.position;

        //float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        //if (rotation < 0)
        //{
        //    Quaternion qRot = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, qRot, 20);
        //}
        //else if (rotation >= -40)
        //{
        //    Quaternion qRot = Quaternion.AngleAxis(rotation, Vector3.forward);
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, qRot, 20);
        //}
        //zmieniasz 90 kontrola obrotu muchy jest ustawiony na ujemny kiedy głowa muchy jest obrócona w kierunku lewej strony 
        //this.transform.position = Vector3.MoveTowards(this.transform.position, wskaznik.position, 4 * Time.deltaTime);


        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        angleAxis = Quaternion.AngleAxis(angle, Vector3.forward);
        StartCoroutine(RotateObjectWithAngle(0.50f, Mucha, angleAxis));
        StartCoroutine(MoveObject(this.transform, this.transform.position, wskaznik.position, 4f));
        uderzenie = false;
        spadanie = false;
        restart = true;
        this.GetComponent<PolygonCollider2D>().enabled = false;


        yield return new WaitForSeconds(5f);
        restart = false;
        Lataj_dalej_z_tym_tematem();
        this.GetComponent<PolygonCollider2D>().enabled = true;

    }

    public void Lataj_dalej_z_tym_tematem()
    {
        if (uderzenie == false && spadanie == false)
        {
            Mucha.GetComponent<AudioSource>().Play();
            this.transform.position = Vector3.MoveTowards(this.transform.position, newPos, 4 * Time.deltaTime);


            if (this.transform.position == newPos)
            {
                float x = Random.Range(-screenBounds.x, screenBounds.x);
                float y = Random.Range(-screenBounds.y, screenBounds.y - 2f);
                newPos = new Vector3(x, y, this.transform.position.z);
                this.transform.position = Vector3.MoveTowards(this.transform.position, newPos, 4 * Time.deltaTime);
            }
            dir = newPos - transform.position;
            float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            //this.transform.rotation = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
            Quaternion qRot = Quaternion.AngleAxis(rotation + 90, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, qRot, 20);
        }
    }

    public IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        //yield return new WaitForSeconds(0.0f);// first yield return after five seconds as you want
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }
    }


    public IEnumerator RotateObjectWithAngle(float time, GameObject obiektDoRotacji, Quaternion newRotation)
    {
        Quaternion originalRotation = obiektDoRotacji.transform.rotation;
        Quaternion destinationRotation = newRotation;



        float currentTime = 0.0f;



        do
        {
            obiektDoRotacji.transform.rotation = Quaternion.Lerp(originalRotation, destinationRotation, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);



        obiektDoRotacji.transform.rotation = destinationRotation;
    }

}

