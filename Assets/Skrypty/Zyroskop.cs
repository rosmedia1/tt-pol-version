﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zyroskop : MonoBehaviour
{
    private float predkosc = 10f;
    private Vector3 dir;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        dir = Input.acceleration;
        Physics.gravity = new Vector2(dir.x * predkosc, dir.y * predkosc);
    }
}
