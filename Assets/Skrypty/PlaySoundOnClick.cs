﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnClick : MonoBehaviour
{
    //public MainUI mainUi;

    public AudioClip[] dzwieki;
    public List<AudioClip> lista_dzwiekow;
    public AudioClip[] dzwieki_idle;
    //public bool muzykaOn;


    // Start is called before the first frame update
    void Start()
    {
        //  muzykaOn = true;

    }
    // Update is called once per frame
    void Update()
    {



    }

    public void PlaySound(string nazwa_animacji)
    {
        //Debug.Log(nazwa_animacji);
        if (nazwa_animacji.Contains("idle"))
        {
            string numer_idle = nazwa_animacji.Substring(nazwa_animacji.Length - 1, 1);
            int nr_idle = int.Parse(numer_idle);
            if (nr_idle > 1)
            {

            }
            this.GetComponent<AudioSource>().clip = dzwieki_idle[nr_idle];
            this.GetComponent<AudioSource>().playOnAwake = true;

        }
        else
        {
            // lista_dzwiekow.Clear();

            for (int i = 0; i < dzwieki.Length; i++)
            {
                if (dzwieki[i].name.StartsWith(nazwa_animacji))
                {
                    lista_dzwiekow.Add(dzwieki[i]);
                }
            }
            this.GetComponent<AudioSource>().clip = lista_dzwiekow[UnityEngine.Random.Range(0, lista_dzwiekow.Count)];
            this.GetComponent<AudioSource>().Play();

        }


    }

    public void PrzyciskPilota_lub_back_btn()
    {
        int losuj_clip = UnityEngine.Random.Range(0, dzwieki.Length);
        this.GetComponent<AudioSource>().clip = dzwieki[losuj_clip];
        this.GetComponent<AudioSource>().Play();
    }




}


