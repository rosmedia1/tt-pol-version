﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch_location
{
    public int touchId;
    public GameObject Linerenderer;

    public Touch_location(int newTouchId,GameObject newLinerenderer)
    {
        touchId = newTouchId;
        Linerenderer = newLinerenderer;
    }
    
}
