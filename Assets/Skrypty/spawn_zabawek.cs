﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class spawn_zabawek : MonoBehaviour
{
    public GameObject prefab_zabawka;
    public List<GameObject> lista_zabawek;
    public bool can_spawn;
    public GameObject Linerenderer;
    public float skala;
    public GameObject dolny_collider;
    private float duration = 3f;
    public bool idle;
    public bool not_idle;
    public float time_idle = 20f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(blok());
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<poziom5_przyciski>().przyciskOn[0] == true)
        {
            can_spawn = true;

        }
        else
        {

            for (int i = 0; i < lista_zabawek.Count; i++)
            {

                if (lista_zabawek[i] != null)
                {
                    lista_zabawek[i].GetComponent<klikanie_prefab>().Spadaj();
                }




            }


        }

        for (int i = 0; i < lista_zabawek.Count; i++)
        {
            if (lista_zabawek[i] == null)
            {
                lista_zabawek.Remove(lista_zabawek[i]);
            }
        }
        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);
            Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
            Ray promien = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit ray_hit;
            if (Physics.Raycast(promien, out ray_hit))
            {

                if (ray_hit.collider.gameObject.tag == "zabawka" || ray_hit.collider.gameObject.tag == "przycisk")
                {

                    can_spawn = false;
                }


            }
            if (touch.phase == TouchPhase.Began && can_spawn == true)
            {
                Vector3 spawn_pos = new Vector3(touch_pos.x, touch_pos.y, this.transform.position.z - 3);
                GameObject zabawka = Instantiate(prefab_zabawka, spawn_pos, Quaternion.identity, this.transform);
                lista_zabawek.Add(zabawka);
                zabawka.GetComponent<klikanie_prefab>().spawner = this.gameObject;
                if (lista_zabawek.Count > 14)
                {
                    Destroy(lista_zabawek[0]);
                    Destroy(lista_zabawek[1]);
                    Destroy(lista_zabawek[2]);


                }
                time_idle = 20f;
                idle = false;

            }
            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {

                idle = true;
            }

        }

        if (idle == true)
        {

            time_idle -= Time.deltaTime;
            if (time_idle < 0)
            {
                for (int i = 0; i < lista_zabawek.Count; i++)
                {

                    if (lista_zabawek[i] != null)
                    {
                        lista_zabawek[i].GetComponent<klikanie_prefab>().Spadaj();
                    }
                }
            }
        }



    }
    IEnumerator blok()
    {
        yield return new WaitForSeconds(1f);
        can_spawn = true;
    }


}

