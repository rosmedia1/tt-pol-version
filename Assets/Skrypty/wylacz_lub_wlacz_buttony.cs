﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class wylacz_lub_wlacz_buttony : MonoBehaviour
{
    
    public Button button;
    public GameObject Pg;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Wyl_wla_button()
    {
        if(Pg == isActiveAndEnabled)
        {
            button.GetComponent<Button>().interactable = false;
        }
        else if(Pg == enabled)
        {
            button.GetComponent<Button>().interactable = true;
        }
    }

}
