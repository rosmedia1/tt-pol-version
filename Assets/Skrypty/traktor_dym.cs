﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class traktor_dym : MonoBehaviour
{
    public ParticleSystem dymek;
    public AudioClip audio;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Pusci_dymek()
    {
        this.transform.parent.GetComponent<AudioSource>().enabled = true;
        this.transform.parent.GetComponent<AudioSource>().clip = audio;
        this.transform.parent.GetComponent<AudioSource>().Play();
        dymek.enableEmission = true;
        dymek.Play();
    }
}
