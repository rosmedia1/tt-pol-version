﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Przerwij_zzzz_dla_kaczek : MonoBehaviour
{
    public GameObject skrypt_zzzz;
    public ParticleSystem moje_zzzz;
    public GameObject oko_duzej;
    public GameObject oko_malej;
    public Sprite oko_duzejj;
    public Sprite oko_malejj;
    public GameObject SunMoon;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {

            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                {


                    if (touch.phase == TouchPhase.Began)
                    {
                        if (this.name == "Kaczor")
                        {

                            oko_duzej.GetComponent<SpriteRenderer>().sprite = oko_duzejj;
                            moje_zzzz.enableEmission = false;
                            moje_zzzz.Stop();
                        }
                        if (this.name == "KaczkaMala")
                        {
                            Debug.Log("mala_idz_spac");
                            oko_malej.GetComponent<SpriteRenderer>().sprite = oko_malejj;
                            moje_zzzz.enableEmission = false;
                            moje_zzzz.Stop();
                        }
                    }


                }
            }
        }
    }

    public void Kaczki_Wyszly_na_Zer()
    {
        SunMoon.GetComponent<Cykl_dnia_i_nocy_KD>().enabled = false;
        SunMoon.GetComponent<Cykl_dnia_i_nocy_KM>().enabled = false;
        this.GetComponent<zwierze_spi_KaczkaDuza>().enabled = false;
        this.GetComponent<zwierze_spi_kaczkaMala>().enabled = false;
        SunMoon.GetComponent<Zzzz_dla_kaczek>().enabled = false;

    }

    public void Kaczki_wrocily_spac()
    {
        SunMoon.GetComponent<Cykl_dnia_i_nocy_KD>().enabled = true;
        SunMoon.GetComponent<Cykl_dnia_i_nocy_KM>().enabled = true;
        this.GetComponent<zwierze_spi_KaczkaDuza>().enabled = true;
        this.GetComponent<zwierze_spi_kaczkaMala>().enabled = true;
        SunMoon.GetComponent<Zzzz_dla_kaczek>().enabled = true;
    }

    //public void KaczkiWyszly()
    //{
    //    if (anim.GetCurrentAnimatorStateInfo(1).IsName("KaczkaClick"))
    //   {
    //       Debug.Log("doszlo_tutaj");
    //      oko_duzej.GetComponent<SpriteRenderer>().sprite = oko_duzejj;
    //       moje_zzzz.enableEmission = false;
    //      moje_zzzz.Stop();
    //   }
    //      else if (animation.IsPlaying("KaczkaMalaClick") != true)
    //      {
    //         oko_malej.GetComponent<SpriteRenderer>().sprite = oko_malejj;
    //          moje_zzzz.enableEmission = false;
    //          moje_zzzz.Stop();
    //      }
    //}



    public void ZwierzeJe()
    {
        moje_zzzz.enableEmission = false;
        moje_zzzz.Stop();

    }




}

