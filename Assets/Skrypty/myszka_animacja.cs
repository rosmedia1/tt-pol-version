﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myszka_animacja : MonoBehaviour
{
    public string anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayAnim()
    {
        this.transform.parent.GetComponent<Animator>().enabled = true;
        this.transform.parent.GetComponent<Animator>().Play(anim);
    }
        
    
}
