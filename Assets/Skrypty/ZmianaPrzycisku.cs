﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZmianaPrzycisku : MonoBehaviour
{
    public Sprite turnOnTexture;
    public Sprite turnOffTexture;
    public bool turnOn;
    public bool przyciskCzasowy;
    public bool klocekOn;
    

    
    public bool klocek;
    public bool przyciskiNaPilocie;
    public bool touchpad;

    void Start()
    {
        turnOn = false;
    }


    void Update()
    {

    }

    public void Zmiana()
    {
        if (przyciskiNaPilocie == true)
        {
            if (turnOn == false)
            {
                turnOn = true;
            }
            else if (turnOn == true)
            {
                turnOn = false;
            }
            if (turnOn == false)
            {
                this.transform.parent.GetComponent<SpriteRenderer>().sprite = turnOffTexture;

            }
            if (turnOn == true)
            {
                this.transform.parent.GetComponent<SpriteRenderer>().sprite = turnOnTexture;

            }


        }


        if(klocek== true)
        {
            if(klocekOn == true)
        {

                Color color = this.transform.parent.GetComponent<SpriteRenderer>().color;
                color.a = 0.5f;
                this.transform.parent.GetComponent<SpriteRenderer>().color = color;
                klocekOn = false;
            }
        else if (klocekOn == false)
            {

                Color color = this.transform.parent.GetComponent<SpriteRenderer>().color;
                color.a = 1.0f;
                this.transform.parent.GetComponent<SpriteRenderer>().color = color;
                klocekOn = true;
            }
        }



        if (przyciskCzasowy == true)
        {
            StartCoroutine(Odliczanie());
        }

        if(touchpad == true)
        {
            if (turnOn == false)
            {
                turnOn = true;
            }
            else if (turnOn == true)
            {
                turnOn = false;
            }
            if (turnOn == false)
            {
                this.transform.parent.GetComponent<SpriteRenderer>().sprite = turnOffTexture;
            }
            if (turnOn == true)
            {
                this.transform.parent.GetComponent<SpriteRenderer>().sprite = turnOnTexture;

            }
        }
        




    }

    IEnumerator Odliczanie()
    {
        this.transform.parent.GetComponent<SpriteRenderer>().sprite = turnOnTexture;
        yield return new WaitForSeconds(8);
        this.transform.parent.GetComponent<SpriteRenderer>().sprite = turnOffTexture;

    }

}

