﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class odpal_plansze : MonoBehaviour
{
    public GameObject Parental_gate;
    public int nr_planszy;
    public GameObject przejscie;
    private Animation anim;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Parental_gate.GetComponent<parental_gate>().aktywny == true)
        {
            this.GetComponent<GraphicRaycaster>().enabled = false;
        }
        else
        {
            this.GetComponent<GraphicRaycaster>().enabled = true;
        }
    }
    public void Uruchom_plansze()
    {
        animator.SetTrigger("Start");
        przejscie.transform.parent.GetComponent<Animator>().Play("Touch");
        StartCoroutine(Odlicz(nr_planszy));
        SceneManager.LoadScene(nr_planszy);
    }


    IEnumerator Odlicz(int numer_poziomu)
    {
        przejscie.transform.parent.GetComponent<Animator>().Play("Touch");
        yield return new WaitForSeconds(10f);
        SceneManager.LoadScene(numer_poziomu);
    }

}
