﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class przyciaganie_jedzenia : MonoBehaviour
{
    private GameObject usta;
    public GameObject stos_siana;
    private Vector3 start_pos;
    private bool przeciaganie;
    public bool powrot;
    private bool garnek;
    public GameObject drzewo;
    private bool nakarm;
    public GameObject swinka;
    private int fingerIndex = -1;


    //public GameObject krowa;

    // Start is called before the first frame update
    void Start()
    {


        StartCoroutine(Pozycja_startowa());
    }

    IEnumerator Pozycja_startowa()
    {
        yield return new WaitForSeconds(0.4f);
        start_pos = transform.position;
    }


    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.name != "Sianko")
                {
                    if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                    {
                        if (fingerIndex >= 0 && fingerIndex != touch.fingerId) { continue; }
                        if (touch.phase == TouchPhase.Began)
                        {
                            fingerIndex = touch.fingerId;
                            przeciaganie = true;
                            powrot = false;

                        }
                    }
                }
                else
                {
                    if (stos_siana.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                    {
                        if (fingerIndex >= 0 && fingerIndex != touch.fingerId) { continue; }
                        if (touch.phase == TouchPhase.Began)
                        {
                            fingerIndex = touch.fingerId;
                            przeciaganie = true;
                            powrot = false;
                            this.GetComponent<SpriteRenderer>().sortingOrder = 500;
                            this.transform.position = new Vector3(touch_pos.x, touch_pos.y, this.transform.position.z);
                        }



                    }
                }
                if (przeciaganie == true && fingerIndex >= 0)
                {
                    if (touch.fingerId != fingerIndex) { continue; }
                    if (touch.phase == TouchPhase.Moved)
                    {
                        this.transform.position = new Vector3(touch_pos.x, touch_pos.y, this.transform.position.z);
                    }
                    else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        fingerIndex = -1;
                        if (this.name == "Sianko")
                        {
                            this.GetComponent<SpriteRenderer>().sortingOrder = 100;
                            this.transform.position = start_pos;
                        }

                        przeciaganie = false;

                        if (garnek == false)
                        {

                            powrot = true;
                        }
                        else if (garnek == true)
                        {
                            powrot = false;
                        }

                    }
                }
            }



        }
        if (powrot == true && this.name != "Sianko")
        {
            this.transform.position = Vector3.Lerp(this.transform.position, start_pos, 6 * Time.deltaTime);
        }


        if (nakarm == true)
        {
            this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            //this.transform.position = Vector3.Lerp(this.transform.position, usta.transform.position, 6 * Time.deltaTime);
        }

    }


    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (this.name == "Jablko(Clone)" && przeciaganie == true)
        {

            if (collision.gameObject.tag == "czujnik_jedzenia")
            {
                nakarm = true;
                usta = collision.gameObject;
                this.GetComponent<PolygonCollider2D>().enabled = false;
                przeciaganie = false;
                powrot = false;
                this.transform.position = start_pos;
                if (collision.gameObject.name == "czujnik_jedzenia1")
                {
                    collision.gameObject.transform.parent.GetComponent<Animator>().CrossFade("KrowaJe", 0.04f);


                }
                else if (collision.gameObject.name == "czujnik_jedzenia2")
                {
                    collision.gameObject.transform.parent.GetComponent<Animator>().CrossFade("swinka_je", 0.04f);


                }
                drzewo.GetComponent<drzewo>().Zabrano_jablko();
                StartCoroutine(Zniszcz_po_czasie(collision.gameObject));
            }



        }
        else if (this.name == "Sianko")
        {
            if (collision.gameObject.name == "czujnik_jedzenia3")
            {

                collision.gameObject.transform.parent.GetComponent<Animator>().CrossFade("KonJe", 0.04f);
                przeciaganie = false;
                this.transform.position = start_pos;
                this.GetComponent<SpriteRenderer>().sortingOrder = 100;

            }
        }
    }



    IEnumerator Zniszcz_po_czasie(GameObject kolizja)
    {
        kolizja.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(0.4f);
        kolizja.GetComponent<BoxCollider2D>().enabled = true;
        Destroy(this.gameObject);

    }







}

