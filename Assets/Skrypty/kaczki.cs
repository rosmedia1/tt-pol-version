﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kaczki : MonoBehaviour
{
    private PolygonCollider2D col;
    private GameObject kaczorObject;
    Vector2 touchposition;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);

                if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                {
                    if (touch.phase == TouchPhase.Began)
                    {

                        int losuj_animacje = Random.Range(1, 4);

                        if (losuj_animacje == 1)
                        {
                            if (this.name == "Kaczor")
                            {
                                this.GetComponent<Animator>().CrossFade("Quack", 0.4f);
                                this.GetComponent<Zzzz_dla_kaczek>().Przerwij_zzzz_kaczki_duzej();


                            }
                            else if (this.name == "KaczkaMala")
                            {

                                this.GetComponent<Animator>().CrossFade("KaczkaMalaQuack", 0.4f);

                            }
                        }
                        else if (losuj_animacje == 2)
                        {
                            if (this.name == "Kaczor")
                            {


                                this.GetComponent<Animator>().CrossFade("KaczorNurkowanie", 0.4f);


                            }
                            else if (this.name == "KaczkaMala")
                            {

                                this.GetComponent<Animator>().CrossFade("KaczkaMalaNurkowanie", 0.4f);

                            }
                        }
                        else if (losuj_animacje == 3)
                        {
                            if (this.name == "Kaczor")
                            {

                                this.GetComponent<Animator>().CrossFade("KaczkaClick", 0.4f);


                            }
                            else if (this.name == "KaczkaMala")
                            {

                                this.GetComponent<Animator>().CrossFade("KaczkaMalaClick", 0.4f);

                            }



                        }
                    }
                }
            }







        }


    }


    public void NieAktywny()
    {
        this.GetComponent<PolygonCollider2D>().enabled = false;
    }
    public void Aktywny()
    {
        this.GetComponent<PolygonCollider2D>().enabled = true;
    }



}



