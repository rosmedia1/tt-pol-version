﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Rysuj : MonoBehaviour
{
    public GameObject kredka;
    public Camera m_camera;
    public GameObject brush;
    public bool rysowanie;
    private Vector3 kredkaposs;
    public bool palec_na_kartce;
    public int sorting_layer;
    public GameObject gumka;

    LineRenderer currentLineRenderer;

    Vector3 lastPos;
    public GameObject kartka;

    private void Start()
    {
        kredkaposs = kredka.transform.position;

    }


    private void Update()
    {
        //kartka.transform.position = new Vector3(kartka.transform.position.x, kartka.transform.position.y, kartka.transform.position.z);
        if (Input.touchCount > 0)
        {
            palec_na_kartce = false;
            Vector3 touchPoss = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            if (kredka.GetComponent<PolygonCollider2D>().OverlapPoint(touchPoss))
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    kredka.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -70));
                    rysowanie = true;

                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    kredka.transform.rotation = Quaternion.Euler(new Vector3(0.32f, 0.44f, 0.2f));
                    rysowanie = false;
                }
            }

            if (kartka.GetComponent<PolygonCollider2D>().OverlapPoint(touchPoss))
            {

                if (rysowanie == true)
                {
                    palec_na_kartce = true;
                    Drawing();
                }
            }



            if (rysowanie == true)
            {
                if (palec_na_kartce == true)
                {
                    kredka.transform.position = new Vector3(touchPoss.x, touchPoss.y + 0.94f, kredka.transform.position.z);
                }
                else
                {
                    kredka.transform.position = kredkaposs;
                    rysowanie = false;
                    kredka.transform.rotation = Quaternion.Euler(new Vector3(0.32f, 0.44f, 0.2f));
                }
            }
            if (rysowanie == false)
            {
                kredka.transform.position = kredkaposs;
            }
        }
    }

    public void Kredka()
    {

    }

    void Drawing()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            CreateBrush();
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            PointToMousePos();
        }
        else
        {
            currentLineRenderer = null;
        }
    }

    void CreateBrush()
    {

        sorting_layer++;
        if (sorting_layer >= 30000)
        {
            sorting_layer = 0;
        }


        //Vector3 rysunek_pos = new Vector3(kartka.transform.position.x, kartka.transform.position.y, kartka.transform.parent.transform.position.z);
        GameObject brushInstance = Instantiate(brush, kartka.transform.parent.position, Quaternion.identity, kartka.transform.parent) as GameObject;
        currentLineRenderer = brushInstance.GetComponent<LineRenderer>();
        brushInstance.GetComponent<LineRenderer>().sortingOrder = sorting_layer; // dodane z Rysowanie_poziom5
        brushInstance.GetComponent<sprawdz_punkt_gumka>().gumka = gumka;    // dodane z Rysowanie_poziom5 jako gumka
        //because you gotta have 2 points to start a line renderer, 
        Vector3 touch_pos = m_camera.ScreenToWorldPoint(Input.GetTouch(0).position);
        touch_pos.z = 60;
        currentLineRenderer.SetPosition(0, touch_pos);
        currentLineRenderer.SetPosition(1, touch_pos);
        float r = Random.Range(0, 1f);
        float g = Random.Range(0, 1f);
        float b = Random.Range(0, 1f);
        Color kolor = new Color(r, g, b, 1f);
        currentLineRenderer.material.color = kolor;



    }

    void AddAPoint(Vector3 pointPos)
    {
        if (currentLineRenderer == null)
        {
            return;
        }
        currentLineRenderer.positionCount++;
        int positionIndex = currentLineRenderer.positionCount - 1;
        currentLineRenderer.SetPosition(positionIndex, pointPos);
    }



    void PointToMousePos()
    {
        Vector3 touch_pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        touch_pos.z = 60;
        Vector3 mousePos = m_camera.ScreenToWorldPoint(Input.mousePosition);
        if (lastPos != mousePos)
        {
            AddAPoint(touch_pos);
            lastPos = touch_pos;
        }
    }



}