﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zwierze_spi : MonoBehaviour
{
    public bool mozesz_spac;
    public string spanie;
    public string wybudzanie;
    public bool sen;
    public GameObject swinka;
    public GameObject krowka;
    public GameObject pajaczek;
    public GameObject kaczka_mala;
    public GameObject kaczka_duza;
    public GameObject konik;
    public GameObject kotek;
    public GameObject kaczka_duza_oko;
    public GameObject kaczka_mala_oko;
    public Sprite sprite_kaczka_mala;
    public Sprite sprite_kaczka_duza;
    public Sprite sprite_kaczka_mala1;
    public Sprite sprite_kaczka_duza1;
    public AudioClip chrapanie;
    public ParticleSystem zzz;
    // public Zzzzz zzzz;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Spanie()
    {

        sen = true;

        this.GetComponent<Animator>().Play(spanie);



    }

    public void sprawdz_emiter()
    {
        if (sen == true)
        {
            zzz.enableEmission = true;
            zzz.Play();
        }
        else
        {
            zzz.enableEmission = false;
            zzz.Stop();
        }
    }


    public void Wybudzanie()
    {
        this.GetComponent<AudioSource>().Pause();
        sen = false;
        this.GetComponent<Animator>().CrossFade(wybudzanie, 0.04f);
    }



    public void Chrapanie()
    {
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = chrapanie;
        this.GetComponent<AudioSource>().Play();
    }

    public void Sen()
    {
        swinka.GetComponent<Animator>().CrossFade(spanie, 0.04f);
    }



    public void WybudzanieDlaKaczek()
    {

        kaczka_mala_oko.GetComponent<SpriteRenderer>().sprite = sprite_kaczka_mala1;
        kaczka_duza_oko.GetComponent<SpriteRenderer>().sprite = sprite_kaczka_duza1;
        zzz.enableEmission = false;
        zzz.Stop();

    }




}
