﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class klikanie_prefab : MonoBehaviour
{

    public Sprite[] sprite_tab;
    //bool m_started;
    public LayerMask layerMask;
    public float sila;
    public bool dodaj_sile;
    private Vector3 losowy_kierunek;
    public float skala;
    public bool przeciaganie;
    public ParticleSystem gwiazdki;
    public AudioClip[] tablica_dzwiekow;
    private AudioClip ten_dzwiek;
    //public GameObject Linerenderer;
    public GameObject spawner;
    public bool spadaj;



    // Start is called before the first frame update
    void Start()
    {
        spadaj = false;
        dodaj_sile = true;
        sila = Random.Range(1, 3);
        float x = Random.Range(-4f, 4f);
        float y = Random.Range(-4f, 4f);
        float z = 0;
        losowy_kierunek = new Vector3(x, y, z);
        int random_sprite = Random.Range(0, sprite_tab.Length);
        this.GetComponent<Image>().sprite = sprite_tab[random_sprite];
        // StartCoroutine(Zniszcz_po_czasie());
        this.GetComponent<Rigidbody>().AddForce(losowy_kierunek * sila, ForceMode.Impulse);
        var img = this.GetComponent<Image>().sprite;
        if (img.name == "krowa")
        {

            Physics.IgnoreLayerCollision(0, 1);
            skala = 40;
            ten_dzwiek = tablica_dzwiekow[0];

        }
        if (img.name == "samochod2")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 23;
            ten_dzwiek = tablica_dzwiekow[1];

        }
        if (img.name == "czajnik")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 30;
            ten_dzwiek = tablica_dzwiekow[2];

        }
        if (img.name == "garnek")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 27;
            ten_dzwiek = tablica_dzwiekow[3];

        }
        if (img.name == "kaczka_1")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 15;
            ten_dzwiek = tablica_dzwiekow[4];

        }
        if (img.name == "kaczka_2")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 22;
            ten_dzwiek = tablica_dzwiekow[5];

        }
        if (img.name == "mis")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 30;
            ten_dzwiek = tablica_dzwiekow[6];

        }
        if (img.name == "swinka")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 38;
            ten_dzwiek = tablica_dzwiekow[7];

        }
        if (img.name == "samochod1")
        {

            Physics.IgnoreLayerCollision(0, 1);
            skala = 23;
            ten_dzwiek = tablica_dzwiekow[8];

        }
        if (img.name == "pilka")
        {
            Physics.IgnoreLayerCollision(0, 1);
            skala = 35;
            ten_dzwiek = tablica_dzwiekow[9];

        }
        this.GetComponent<RectTransform>().localScale = new Vector3(skala, skala, skala);
        this.GetComponent<AudioSource>().clip = ten_dzwiek;
        this.GetComponent<AudioSource>().Play();

    }

    // Update is called once per frame
    void Update()
    {

        Physics.IgnoreLayerCollision(0, 1);
        this.GetComponent<BoxCollider>().size = new Vector3(this.GetComponent<RectTransform>().rect.size.x, this.GetComponent<RectTransform>().rect.size.y, 20);
        //this.transform.parent.GetComponent<spawn_zabawek>().Can_spawn(true) ;
        if (Input.touchCount > 0 && spawner.GetComponent<poziom5_przyciski>().przyciskOn[0] == true)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                Ray promien = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit ray_hit;
                if (Physics.Raycast(promien, out ray_hit))
                {
                    if (ray_hit.collider.gameObject == this.gameObject)
                    {
                        if (touch.phase == TouchPhase.Began)
                        {
                            spawner.GetComponent<spawn_zabawek>().time_idle = 20;
                            przeciaganie = true;
                            this.GetComponent<Rigidbody>().useGravity = false;
                            this.GetComponent<AudioSource>().clip = ten_dzwiek;
                            this.GetComponent<AudioSource>().Play();

                        }

                    }
                }
                if (przeciaganie == true)
                {
                    if (touch.phase == TouchPhase.Moved)
                    {
                        spawner.GetComponent<spawn_zabawek>().time_idle = 20;
                        this.transform.position = new Vector3(touch_pos.x, touch_pos.y, this.transform.position.z);
                    }
                    else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        przeciaganie = false;
                        this.GetComponent<Rigidbody>().useGravity = true;


                    }
                }
            }

        }

        if (spadaj == true)
        {
            Vector3 dol = new Vector3(this.transform.position.x, -10, this.transform.position.z);
            this.transform.position = Vector3.MoveTowards(this.transform.position, dol, 8 * Time.deltaTime);
            this.GetComponent<BoxCollider>().enabled = false;
            if (this.transform.position.y <= -10)
            {
                Destroy(this.gameObject);
            }
        }

    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "zabawka" && przeciaganie == true)
        {
            gwiazdki.enableEmission = true;
            gwiazdki.Play();
        }
    }

    IEnumerator Zniszcz_po_czasie()
    {
        yield return new WaitForSeconds(8);
        Destroy(this.gameObject);
    }

    public void Spadaj()
    {
        spadaj = true;

    }





}




