﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sprawdz_punkt : MonoBehaviour
{
    public GameObject samochodzik;
    public GameObject gumka;
    private Vector3 pozycja_autka;
    private float distance;
    public int ukryte_punkty;
    private float granica_zmazywania;
    // Start is called before the first frame update
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {

        pozycja_autka = samochodzik.transform.TransformPoint(Vector3.zero);

        for (int i = 0; i < this.GetComponent<LineRenderer>().positionCount; i++)
        {
            distance = this.GetComponent<LineRenderer>().GetPosition(i).x - pozycja_autka.x;

            if (samochodzik.GetComponent<przesuwanie_suwaka>().przeciaganie == true)
            {

                if (distance > 0 && distance < 0.5f)
                {
                    //zmazywanie w prawo
                    var LR = this.GetComponent<LineRenderer>().GetPosition(i);

                    if (LR.z != -200)
                    {
                        Vector3 ukryta_pozycja = new Vector3(LR.x, LR.y, -200);
                        this.GetComponent<LineRenderer>().SetPosition(i, ukryta_pozycja);
                        ukryte_punkty++;
                    }

                }
                else if (distance < 0 && distance > -0.5f)
                {
                    //zmazywanie w lewo
                    var LR = this.GetComponent<LineRenderer>().GetPosition(i);


                    if (LR.z != -200)
                    {
                        Vector3 ukryta_pozycja = new Vector3(LR.x, LR.y, -200);
                        this.GetComponent<LineRenderer>().SetPosition(i, ukryta_pozycja);
                        ukryte_punkty++;
                    }

                }


            }




        }
        if (ukryte_punkty >= this.GetComponent<LineRenderer>().positionCount)
        {

            Destroy(this.gameObject);
        }

    }

}

