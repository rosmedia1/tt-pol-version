﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScale : MonoBehaviour
{
    public float resolutionX; //rozdzielczosc X urzadzenia
    public float resolutionY; //rozdzielczosc Y urzadzenia
    public float ratioDevice; //ratio urzadzenia

    public float defaultX = 960.0f; //domyslna rozdzielczosc X
    public float defaultY = 640.0f; //domyslna rozdzielczosc Y
    public float defaultRatio; //domyslne ratio

    public float factor;  //wspolczynnik odkryty przeze mnie :)

    public string deviceOrientation; //wartosc kontrolna, czy urzadzenie jest bardziej szerokie czy wysokie

    public float relativeRatio; //ratio obliczone przez podzielenie ratio urzadzenia przez ratio domyslne

    //nowe rozdzielczosci dla canvasa
    public static float canvasX;
    public static float canvasY;

    //SKRYPT PRZYPNIJ DO CANVASA!!!


    void Start()
    {
        factor = 1.375f; //bylo 1.375f
        resolutionX = Screen.width; //pobranie szerokosci X urzadzenia
        resolutionY = Screen.height; //pobranie wysokosci Y urzadzenia
        //Debug.Log(resolutionX + "," + resolutionY);
        //resolutionX = 960f;
        //resolutionY = 640f;
        ratioDevice = resolutionX / resolutionY; //obliczenie ratio urzadzenia

        defaultRatio = defaultX / defaultY; //okreslenie domyslnego ratio, w tym przypadku 1.5f

        float aspectRatio = Camera.main.aspect;
        //float size = Mathf.Sqrt(Mathf.Pow(Screen.width, 2) + Mathf.Pow(Screen.height, 2));
        if (aspectRatio > 1.6f)
        {
            HorizontalDevice();
            deviceOrientation = "iPhone/Smartphone";
        }

        else
        {
            VerticalDevice();
            deviceOrientation = "iPad/Tablet";
        }
    }


    void Update()
    {
        //Debug.Log("canvasX " + canvasX);
        //Debug.Log("canvasY " + canvasY);
    }

    public void HorizontalDevice() //urzadzenie szerokie
    {
        relativeRatio = ratioDevice / defaultRatio; //relativeRatio = ratioDevice / 1.5f

        float relativeResX = defaultX * relativeRatio; //nowa rozdzielczosc X dla urzadzenia na szablonie
        float relativeResY = relativeResX / ratioDevice; //nowa rozdzielczosc Y dla urzadzenia na szablonie

        canvasX = relativeResX * factor; //nowe wymiary X dla canvasa
        canvasY = relativeResY * factor; //nowe wymiary Y dla canvasa

        this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasX, canvasY);
        this.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.0f;
    }

    public void VerticalDevice()
    {
        //relativeRatio = defaultRatio / ratioDevice; //relativeRatio = 1.5f / ratioDevice

        //float relativeResY = defaultY * relativeRatio;
        //float relativeResX = relativeResY * ratioDevice;

        //float _relativeFactor = resolutionY / relativeResY;
        //float relativeFactor = _relativeFactor / 2;

        //float new_relativeResX = relativeResX * relativeFactor;
        //float new_relativeResY = new_relativeResX / ratioDevice;

        //canvasY = new_relativeResY * factor;
        //canvasX = new_relativeResX * factor;

        //this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasX, canvasY);

        //=====================================================//

        float relativeFactorY = resolutionY / 985f; // bylo 1053f
        //float relativeFactorY = resolutionY / 1053f; // bylo 1053f

        float _canvasY = resolutionY / relativeFactorY;
        float _canvasX = _canvasY * ratioDevice;

        canvasX = _canvasX;
        canvasY = _canvasY;

        this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasX, canvasY);

        this.GetComponent<CanvasScaler>().matchWidthOrHeight = 1f;
    }

}