﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;
//using UnityEngine.iOS;
public class Link : MonoBehaviour
{

    public void Go_To_www()
    {
        Application.OpenURL("https://proliberis.org/");
    }

    public void Go_To_youtube()
    {
        Application.OpenURL("https://www.youtube.com/channel/UCjJcloXrXkzDJqN0Q-km2vQ");
    }

    public void Go_To_facebook()
    {
        Application.OpenURL("https://www.facebook.com/FundacjaProLiberis/");
    }


 

    public void Go_To_email()
    {
        SendEmail();
    }

    public void Go_To_opinion()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Application.OpenURL("market://details?id=" + Application.identifier);
        }
        //if (Application.platform == RuntimePlatform.IPhonePlayer)
        //{
        //    Device.RequestStoreReview();
        //}
    }


    void SendEmail()

    {

        string email = "kontakt@proliberis.org";

        string subject = MyEscapeURL("My Subject");

        string body = MyEscapeURL("My Body\r\nFull of non-escaped chars");


        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);

    }

    string MyEscapeURL(string url)

    {

        return WWW.EscapeURL(url).Replace("+", "%20");

    }
}