﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gyroscope_to_lvl_5 : MonoBehaviour
{

    [SerializeField]
    private Vector3 axis;

    [SerializeField]
    private float spin_speed = 10f;
 

    void Update()
    {
        transform.Rotate(axis, spin_speed);    
    }


}
