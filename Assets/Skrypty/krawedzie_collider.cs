﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class krawedzie_collider : MonoBehaviour
{
    private Vector2 krawedzie_ekranu;
    public GameObject gorny_collider;
    public GameObject dolny_collider;
    public GameObject lewy_collider;
    public GameObject prawy_collider;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(opoznienie_krawedzi());
        
    }
    IEnumerator opoznienie_krawedzi()
    {
        yield return new WaitForSeconds(1);
        krawedzie_ekranu = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        gorny_collider.transform.position = new Vector3(gorny_collider.transform.position.x, krawedzie_ekranu.y, gorny_collider.transform.position.z);
        dolny_collider.transform.position = new Vector3(dolny_collider.transform.position.x, -krawedzie_ekranu.y, dolny_collider.transform.position.z);
        lewy_collider.transform.position = new Vector3(-krawedzie_ekranu.x, lewy_collider.transform.position.y, lewy_collider.transform.position.z);
        prawy_collider.transform.position = new Vector3(krawedzie_ekranu.x, prawy_collider.transform.position.y, prawy_collider.transform.position.z);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

}

