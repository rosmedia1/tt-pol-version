﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class Klocki : MonoBehaviour
{
    public GameObject[] klockiPref;
    public bool[] klocekOn;
    public GameObject[] klocki;
    public AudioClip audio1;
    public AudioClip audio2;
    void Start()
    {
        for (int i = 0; i < 2; i++)
        {
            Color color = klocki[i].GetComponent<SpriteRenderer>().color;
            color.a = 0.5f;
            klocki[i].GetComponent<SpriteRenderer>().color = color;
            klocekOn[i] = false;
        }

        klocki[0].GetComponent<Button>().onClick.AddListener(() => KlocekClick(0));
        klocki[1].GetComponent<Button>().onClick.AddListener(() => KlocekClick(1));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void KlocekClick(int nr_klocka)
    {

        if (nr_klocka == 0)
        {
            if (klocekOn[0] == false)
            {
                this.GetComponent<AudioSource>().enabled = true;
                this.GetComponent<AudioSource>().clip = audio2;
                this.GetComponent<AudioSource>().Play();
                //klocki[0].GetComponent<>
                Color color = klocki[0].GetComponent<SpriteRenderer>().color;
                color.a = 1.0f;
                klocki[0].GetComponent<SpriteRenderer>().color = color;
                klocekOn[0] = true;
                Color color2 = klocki[1].GetComponent<SpriteRenderer>().color;
                color2.a = 0.5f;
                klocki[1].GetComponent<SpriteRenderer>().color = color2;
                klocekOn[1] = false;

            }
            else if (klocekOn[0] == true)
            {
                //this.GetComponent<AudioSource>().enabled = true;
                //this.GetComponent<AudioSource>().clip = audio1;
                //this.GetComponent<AudioSource>().Play();
                Color color2 = klocki[0].GetComponent<SpriteRenderer>().color;
                color2.a = 0.5f;
                klocki[0].GetComponent<SpriteRenderer>().color = color2;
                klocekOn[0] = false;

                //this.GetComponent<RenderPieczatek>().Zmiana_pref(klockiPref[0]);
            }
        }
        else if (nr_klocka == 1)
        {
            if (klocekOn[1] == false)
            {
                this.GetComponent<AudioSource>().enabled = true;
                this.GetComponent<AudioSource>().clip = audio1;
                this.GetComponent<AudioSource>().Play();
                Color color = klocki[1].GetComponent<SpriteRenderer>().color;
                color.a = 1.0f;
                klocki[1].GetComponent<SpriteRenderer>().color = color;
                klocekOn[1] = true;
                Color color2 = klocki[0].GetComponent<SpriteRenderer>().color;
                color2.a = 0.5f;
                klocki[0].GetComponent<SpriteRenderer>().color = color2;
                klocekOn[0] = false;

            }
            else if (klocekOn[1] == true)
            {
                //this.GetComponent<AudioSource>().enabled = true;
                //this.GetComponent<AudioSource>().clip = audio2;
                //this.GetComponent<AudioSource>().Play();
                Color color2 = klocki[1].GetComponent<SpriteRenderer>().color;
                color2.a = 0.5f;
                klocki[1].GetComponent<SpriteRenderer>().color = color2;
                klocekOn[1] = false;

            }


        }



        if (klocekOn[0] == true)
        {
            this.GetComponent<RenderPieczatek>().Zmiana_pref(klockiPref[0], nr_klocka);
        }
        else if (klocekOn[1] == true)
        {
            this.GetComponent<RenderPieczatek>().Zmiana_pref(klockiPref[1], nr_klocka);
        }
        else if (klocekOn[0] == false && klocekOn[1] == false)
        {
            this.GetComponent<RenderPieczatek>().aktualnyPrefab = null;
        }


    }

}
