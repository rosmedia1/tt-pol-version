﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeAScreenShot : MonoBehaviour
{
    private static TakeAScreenShot instance;

    private Camera myCamera;
    private bool takescreenshotOnNextFrame;


    private void Awake()
    {
        instance = this;
        myCamera = gameObject.GetComponent<Camera>();
    }

    private void OnPostRender()
    {
        if(takescreenshotOnNextFrame)
        {
            takescreenshotOnNextFrame = false;
            RenderTexture renderTexture = myCamera.targetTexture;
            Texture2D renderResult = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
            renderResult.ReadPixels(rect, 0, 0);


            byte[] byteArray = renderResult.EncodeToPNG();
            System.IO.File.WriteAllBytes(Application.dataPath + "/CameraScreenshot-kopia3.png", byteArray);
            Debug.Log("Zapisalo ci screena");

            RenderTexture.ReleaseTemporary(renderTexture);
            myCamera.targetTexture = null;

        }
    }

    private void TakeScreen(int width, int height)
    {
        myCamera.targetTexture = RenderTexture.GetTemporary(width, height, 16);
        takescreenshotOnNextFrame = true;
    }

    public static void TakeScreen_static(int width, int height)
    {
        instance.TakeScreen(width, height);
    }

}
