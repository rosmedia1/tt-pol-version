﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zzzzz : MonoBehaviour
{
    public GameObject[] zwierzaki_spia;
    public ParticleSystem[] zzzz;
    public ParticleSystem swinia_zzz;
    public zwierze_spi spac;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {



    }



    public void Stworz_Zzzz()
    {
        for (int i = 0; i < zzzz.Length; i++)
        {
            if (zwierzaki_spia[i].GetComponent<zwierze_spi>().sen == true)
            {
                zzzz[i].enableEmission = true;
                zzzz[i].Play();
            }


        }
    }

    public void stworz_zzz_dla_swinki()
    {

        swinia_zzz.enableEmission = true;
        swinia_zzz.Play();
    }

    public void Przerwij_zzzz(GameObject zwierzak)
    {
        for (int i = 0; i < zwierzaki_spia.Length; i++)
        {
            if (zwierzaki_spia[i] == zwierzak)
            {
                zzzz[i].enableEmission = false;
                zzzz[i].Stop();
            }


        }
    }

    public void Przerwij_zzzzV2()
    {
        for (int i = 0; i < zwierzaki_spia.Length; i++)
        {

            zzzz[i].enableEmission = false;
            zzzz[i].Stop();

        }
    }



}
