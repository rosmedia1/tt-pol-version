﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cykl_dnia_i_nocy : MonoBehaviour
{
    public GameObject[] zwierzaki;
    public ParticleSystem[] emiterki;
    public GameObject swinka;
    public float[] czas_zasniecia;
    public GameObject btn_back;
    // Start is called before the first frame update
    void Start()
    {
        czas_zasniecia = new float[zwierzaki.Length];
        this.GetComponent<AudioSource>().Stop();
    }

    public void idz_spac()
    {
        if (btn_back.GetComponent<btn_back>().wyjscie == false)
        {
            StartCoroutine(idz_spac_enum());
        }
        else if (btn_back.GetComponent<btn_back>().wyjscie == true)
        {
            StopCoroutine(idz_spac_enum());
        }



    }


    IEnumerator idz_spac_enum()
    {
        for (int i = 0; i < zwierzaki.Length; i++)
        {
            if (btn_back.GetComponent<btn_back>().wyjscie == false)
            {
                czas_zasniecia[i] = Random.Range(0f, 6f);
                yield return new WaitForSeconds(czas_zasniecia[i]);
                zwierzaki[i].GetComponent<zwierze_spi>().Spanie();
                //zwierzaki[i].GetComponent<Zzzzz>().Stworz_Zzzz();
                Debug.Log(czas_zasniecia[i]);
            }
            else if (btn_back.GetComponent<btn_back>().wyjscie == true)
            {
                yield break;
            }

        }
    }


    public void swinka_idzie_spac_raz_jescze()
    {
        swinka.GetComponent<zwierze_spi>().Sen();
    }

    public void Wybudz_sie()
    {

        for (int i = 0; i < zwierzaki.Length; i++)
        {
            zwierzaki[i].GetComponent<zwierze_spi>().Wybudzanie();
        }

    }



    // Update is called once per frame
    void Update()
    {
        if (btn_back.GetComponent<btn_back>().wyjscie == true)
        {
            StopAllCoroutines();
            for (int i = 0; i < czas_zasniecia.Length; i++)
            {
                czas_zasniecia[i] = 200f;
            }
        }


    }
}


