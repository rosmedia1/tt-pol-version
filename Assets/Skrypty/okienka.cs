﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class okienka : MonoBehaviour
{
    public AudioClip audio;
    private bool wlaczone;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Wlacz_okno()
    {
        if (wlaczone == false)
        {
            this.transform.GetChild(0).gameObject.SetActive(true);
            wlaczone = true;
            this.GetComponent<AudioSource>().enabled = true;
            this.GetComponent<AudioSource>().clip = audio;
            this.GetComponent<AudioSource>().Play();
        }
        else if (wlaczone == true)
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
            wlaczone = false;
            this.GetComponent<AudioSource>().enabled = true;
            this.GetComponent<AudioSource>().clip = audio;
            this.GetComponent<AudioSource>().Play();
        }

    }
}
