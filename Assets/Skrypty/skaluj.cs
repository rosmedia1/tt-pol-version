﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class skaluj : MonoBehaviour
{
    public GameObject Canvas;
    public bool zeskaluj;
    // Start is called before the first frame update
    void Start()
    {
        zeskaluj = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(zeskaluj == true)
        {
            Vector2 skala1 = this.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
            Vector2 skala2 = Canvas.GetComponent<CanvasScaler>().referenceResolution;
            this.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = Vector2.MoveTowards(skala1, skala2, 1 * Time.deltaTime);
        }
    }

    public void Skaluj()
    {
        zeskaluj = true;
       
    }
}
