﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class btn_back : MonoBehaviour
{
    //public static int counter;
    public GameObject MouseDraw;
    public GameObject slider;
    public GameObject gitara;
    public GameObject[] elementy_planszy;
    public AudioClip audio;
    public GameObject swinka;
    public string[] nazwa_animacji_zejscia;
    public GameObject drzewo;
    public GameObject kartka;
    public GameObject kartka_przejscie;
    public GameObject suwakLvl5;
    public Instrumenty instrumenty;
    public GameObject chmurki;
    public drzewo dz;
    public ParticleSystem[] particleSystems;
    public GameObject canvas;
    public GameObject Draw;
    public GameObject mucha;
    public GameObject gumka;
    public GameObject blysk;
    public GameObject spinner;
    public Fidget_spinner F_s;
    public GameObject MouseDraw_lvl3;
    public GameObject button_spinner;
    public GameObject kartka_biala;
    public GameObject button_klikanie;
    klikanie_prefab klik;
    public GameObject reklama;
    public GameObject nutka_z_menu;
    public GameObject kartka_obecna_z_olowkiem;
    public GameObject inap_screen;
    //public GameObject Canvas;


    public float unlockAfter = 3f;


    // public Farma farma;
    //public Instrumenty instrumenty;
    //public Component farma;
    // public Component insturmenty;

    public bool wyjscie = false;

    //private void Awake()
    //{
    //    PlayerPrefs.SetInt("counter", counter);
    //    PlayerPrefs.Save();
    //}


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().interactable = false;
        StartCoroutine(UnlockBackButton());
        //PlayerPrefs.SetInt("counter", counter);
        //Debug.Log(counter + "to counter");

    }

    // Update is called once per frame
    void Update()
    {
        //PlayerPrefs.GetInt("counter", counter);
    }

    IEnumerator UnlockBackButton()
    {
        yield return new WaitForSeconds(unlockAfter);
        GetComponent<Button>().interactable = true;
    }

    public void Back()
    {
        if (wyjscie) return;
        wyjscie = true;
        if (wyjscie)
        {
            this.GetComponent<Button>().enabled = false;
        }
        StartCoroutine(Backk());


    }

    IEnumerator Backk()
    {

        for (int i = 0; i < elementy_planszy.Length; i++)
        {
            if (wyjscie)
            {
                elementy_planszy[i].GetComponent<Animator>().enabled = true;
                if (elementy_planszy[i].GetComponent<PolygonCollider2D>() != null)
                {
                    elementy_planszy[i].GetComponent<PolygonCollider2D>().enabled = false;

                }

                if (elementy_planszy[i].GetComponent<Farma>() != null)
                {
                    elementy_planszy[i].GetComponent<Farma>().enabled = false;
                }

                if(inap_screen != null)
                {
                    inap_screen.GetComponent<Button>().interactable = false;
                }
            }

            Animator animator = elementy_planszy[i].GetComponent<Animator>();

            if (instrumenty != null)
            {
                instrumenty.enabled = false;
            }
            if (dz != null)
            {
                dz.enabled = false;
            }

            if (kartka_obecna_z_olowkiem != null)
            {
                kartka_obecna_z_olowkiem.SetActive(false);
            }


            if (dz != null)
            {
                dz.enabled = false;
            }
            if (animator != null)
            {
                animator.Play(nazwa_animacji_zejscia[i]);
            }
            Button btn1 = elementy_planszy[i].GetComponentInChildren<Button>();
            if (btn1 != null)
            {
                btn1.interactable = false;



            }
            else
            {
                Button btn2 = elementy_planszy[i].GetComponent<Button>();
                if (btn2 != null)
                {
                    btn2.interactable = false;

                }
            }

            if (kartka != null)
            {
                if (kartka_przejscie != null)
                {
                    kartka_przejscie.GetComponent<Animator>().Play("kartka_przejscie");
                    kartka.SetActive(false);
                }


            }
            if (this != null)
            {
                for (int j = 0; j < particleSystems.Length; j++)
                {
                    particleSystems[j].enableEmission = false;
                    particleSystems[j].Stop();
                }
            }

            if (this.tag == "noMove")
            {
                this.tag = "wyrabane_w_to";
            }


            if (suwakLvl5 != null)
            {
                suwakLvl5.SetActive(false);
            }


            if (chmurki != null)
            {
                chmurki.SetActive(false);
            }
            if (mucha != null)
            {
                mucha.SetActive(false);
            }

            if (gumka != null)
            {
                gumka.GetComponent<Animator>().enabled = true;
            }

            if (blysk != null)
            {
                blysk.GetComponent<SpriteRenderer>().enabled = false;
            }

            if (drzewo != null)
            {
                drzewo.GetComponent<drzewo>().enabled = false;
            }


            if (Draw != null)
            {
                Draw.SetActive(false);
                GameObject[] zabawki;
                zabawki = GameObject.FindGameObjectsWithTag("zabawka");
                for (int j = 0; j < zabawki.Length; j++)
                {
                    zabawki[j].GetComponent<klikanie_prefab>().Spadaj();
                }
            }



            if (spinner != null)
            {

                spinner.GetComponent<Fidget_spinner>().enabled = false;
                button_spinner.GetComponent<Fidget_spinner>().enabled = false;
                spinner.GetComponent<Animator>().enabled = true;



            }

            if (MouseDraw != null && slider != null)
            {
                slider.SetActive(false);
                MouseDraw.GetComponent<RawImage>().enabled = false;
            }

            if (MouseDraw_lvl3 != null)
            {
                MouseDraw.SetActive(false);
                kartka_biala.SetActive(false);
            }




            if (swinka != null)
            {
                swinka.SetActive(false);
            }



            if (gitara != null)
            {
                gitara.SetActive(false);
            }


            //counter += 1;



            //if (counter > 32)
            //{
            //    counter = 0;
          
                
                if (nutka_z_menu != null)
                {
                    nutka_z_menu.GetComponent<AudioSource>().enabled = false;
                }
            //}
            else
            {
                if (nutka_z_menu != null)
                {
                    nutka_z_menu.GetComponent<AudioSource>().enabled = true;
                }
            }
            //if (PlayerPrefs.HasKey("unlock") == true)
            //{
            //    counter = 0;
            //}



        }

        if (this.GetComponent<Animator>() != null && this.GetComponent<AudioSource>() != null)
        {
            this.GetComponent<Animator>().Play("back_btn_lvl0_animacja1");
            this.GetComponent<AudioSource>().clip = audio;
            this.GetComponent<AudioSource>().Play();
        }
        if (PlayerPrefs.HasKey("unlock") == false && reklama != null)
        {
            reklama.GetComponent<AdMob>().ShowInterstitialAd();
        }
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("Menu01");



    }


}
