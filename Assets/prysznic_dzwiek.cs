﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prysznic_dzwiek : MonoBehaviour
{

    public AudioClip audio;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Prysznic()
    {
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = audio;
        this.GetComponent<AudioSource>().Play();
    }

}
