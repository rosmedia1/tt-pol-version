﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chmurka_skrypt : MonoBehaviour
{
    public bool lec;
    public bool przeciaganie;


    // Start is called before the first frame update
    void Start()
    {
        lec = true;
        int skala = 600;
        this.transform.localScale = new Vector3(skala, skala, skala);
    }

    // Update is called once per frame
    void Update()
    {
        if (lec == true)
        {
            Lec_chmuro();
        }
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        this.transform.parent.GetComponent<Spawnuj_chmury>().Zablokuj_chwyt(this.gameObject);
                        przeciaganie = true;
                        lec = false;
                    }
                    else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        this.transform.parent.GetComponent<Spawnuj_chmury>().Odblokuj_chwyt();
                        przeciaganie = false;
                        lec = true;
                    }
                }
                else
                {
                    this.transform.parent.GetComponent<Spawnuj_chmury>().Odblokuj_chwyt();
                    przeciaganie = false;
                    lec = true;
                }
                if (przeciaganie == true)
                {
                    if (touch.phase == TouchPhase.Moved)
                    {
                        Vector3 dotyk_pos = new Vector3(touch_pos.x, touch_pos.y, this.transform.position.z);
                        dotyk_pos.y = Mathf.Clamp(touch_pos.y, 0, 5);
                        this.transform.position = dotyk_pos;

                    }
                }

            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "CloudDestroyer")
        {
            this.transform.parent.GetComponent<Spawnuj_chmury>().ZniszczonoChmurke(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    public void Lec_chmuro()
    {
        this.transform.position = new Vector3(this.transform.position.x - 0.01f, this.transform.position.y, this.transform.position.z);
    }
}
