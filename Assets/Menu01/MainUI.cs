﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Animations;

public class MainUI : MonoBehaviour
{
    public GameObject Parental_gate;
    public AudioClip audioClip;
    public Sprite dzwiekOn;
    public Sprite dzwiekOff;

    public GameObject dzwiekObj;

    private bool dzwiek;

    private int ekran;

    public Sprite[] ekrany;

    public GameObject przejscie;

    private void Start()
    {
        dzwiek = false;
        DzwiekOnOff();
    }

    public void Update()
    {
        if (Parental_gate.GetComponent<parental_gate>().aktywny == true)
        {
            this.GetComponent<GraphicRaycaster>().enabled = false;
        }
        else
        {
            //   this.GetComponent<GraphicRaycaster>().enabled = true;
        }

    }


    public void PoziomInstrumenty()
    {
        ekran = 1;
        //przejscie.SetActive(true);
        // przejscie.GetComponent<Image>().sprite = ekrany[0];
        StartCoroutine(zaladuj_poziom(ekran));

        //SceneManager.LoadScene(2);
    }

    public void PoziomFarma()
    {

        ekran = 2;
        // przejscie.SetActive(true);
        // przejscie.GetComponent<Image>().sprite = ekrany[1];
        StartCoroutine(zaladuj_poziom(ekran));

        //SceneManager.LoadScene(3);
    }
    public void PoziomElektronika()
    {
        ekran = 3;

        // przejscie.SetActive(true);
        //  przejscie.GetComponent<Image>().sprite = ekrany[2];
        StartCoroutine(zaladuj_poziom(ekran));
        //SceneManager.LoadScene(4);
    }
    public void PoziomZabawki()
    {
        ekran = 4;

        SceneManager.LoadScene(5);
    }
    public void PoziomKolorowanka()
    {
        ekran = 5;

        //SceneManager.LoadScene(6);
    }
    public void PoziomCzajnik()
    {
        ekran = 6;
        StartCoroutine(zaladuj_poziom(ekran));
        SceneManager.LoadScene(6);
    }
    public void Ekran_dla_rodzicow()
    {
        ekran = 7;
        StartCoroutine(zaladuj_poziom_dla_rodzicow(ekran));

    }
    public void BackButton()
    {
        ekran = 0;

    }
    IEnumerator zaladuj_poziom(int numer_poziomu)
    {
        yield return new WaitForSeconds(0.24f);
        SceneManager.LoadScene(numer_poziomu);
        /*  if(ekran == 7)
          {
              yield return new WaitForSeconds(3);
              SceneManager.LoadScene(numer_poziomu);
          }*/
    }

    IEnumerator zaladuj_poziom_dla_rodzicow(int numer_poziomu)
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(numer_poziomu);
    }

    public void DzwiekOnOff()
    {
        if (this.gameObject == dzwiekObj)
        {
            if (dzwiek == false)
            {
                dzwiek = true;
                this.transform.GetChild(0).GetComponent<Image>().sprite = dzwiekOn;
                this.GetComponent<AudioSource>().clip = audioClip;
                this.GetComponent<AudioSource>().Play();
            }
            else
            {
                dzwiek = false;
                this.transform.GetChild(0).GetComponent<Image>().sprite = dzwiekOff;
                this.GetComponent<AudioSource>().Stop();
            }
        }
    }
}
