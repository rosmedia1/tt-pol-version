﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class odtworzAnimacje_fixed : MonoBehaviour
{
    public int ilosc_idle;
    public string nazwa_idle;
    public int numer_idle;
    public int stan_idle;
    public string nazwaAnim;
    public int stan_radia;
    private AnimatorClipInfo[] animatorinfo;


    void Update()
    {

    }

    public void Animacja()
    {



        if (this.transform.parent.name == "rakieta")
        {
            int losowa_animacja = Random.Range(1, 3);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "robot")
        {
            int losowa_animacja = Random.Range(1, 4);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }


        if (this.transform.parent.name == "radio")
        {
            if (stan_radia > 4)
            {
                stan_radia = 0;
            }

            stan_radia++;
            stan_idle++;
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play("radio_animacja" + stan_radia);
            string nazwa_animacji = "radio_animacja" + stan_radia.ToString();
            this.GetComponent<PlaySoundOnClick_fixed>().PlaySound(nazwa_animacji);

        }




        if (this.transform.parent.name == "ciezarowka")
        {
            int losowa_animacja = Random.Range(1, 4);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "samochodzik")
        {
            int losowa_animacja = Random.Range(1, 4);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "Pociag")
        {

            int losowa_animacja = Random.Range(1, 3);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        //plansza poziom 3 pod tym
        if (this.transform.parent.name == "komputer")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "telefon")
        {

            int losowa_animacja = Random.Range(1, 4);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "nozyczki")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "myszka")
        {


            this.transform.parent.GetComponent<Animator>().enabled = true;
            string nazwa_animacji = nazwaAnim;
            this.transform.parent.GetComponent<Animator>().Play(nazwa_animacji);


        }
        if (this.transform.parent.name == "kredka_czerwona")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "kredka_fioletowa")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "kredka_rozowa")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        //animacje do poziomu 6
        if (this.transform.parent.name == "garnek1")
        {

            int losowa_animacja = Random.Range(1, 4);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "garnek2")
        {

            int losowa_animacja = Random.Range(1, 4);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "okap")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "przybory_deseczka")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "zegar")
        {

            int losowa_animacja = Random.Range(1, 3);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "czajnik")
        {

            int losowa_animacja = Random.Range(1, 3);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }
        if (this.transform.parent.name == "mikser")
        {

            int losowa_animacja = Random.Range(1, 5);
            this.transform.parent.GetComponent<Animator>().enabled = true;
            this.transform.parent.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvlEDR")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "back_btn_lvl0")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvl1")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvl2")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvl3")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvl4")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvl5")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }

        if (this.transform.name == "btn_back_lvl6")
        {

            int losowa_animacja = Random.Range(1, 2);
            this.transform.GetComponent<Animator>().enabled = true;
            this.transform.GetComponent<Animator>().Play(nazwaAnim + losowa_animacja);
            string nazwa_animacji = nazwaAnim + losowa_animacja.ToString();
            this.GetComponent<PlaySoundOnClick>().PlaySound(nazwa_animacji);

        }







    }


    IEnumerator powrot_do_idle()
    {

        animatorinfo = this.transform.parent.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0);
        float animatorlenght = animatorinfo[0].clip.length;
        yield return new WaitForSeconds(animatorlenght + 1);
        string animatorname = animatorinfo[0].clip.name;
        this.GetComponent<PlaySoundOnClick>().PlaySound(animatorname);

    }


}




