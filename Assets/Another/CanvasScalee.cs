﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScalee : MonoBehaviour
{
    public float resolutionX; //rozdzielczosc X urzadzenia
    public float resolutionY; //rozdzielczosc Y urzadzenia
    public float ratioDevice; //ratio urzadzenia

    private float defaultX = 960f; //domyslna rozdzielczosc X
    private float defaultY = 640f; //domyslna rozdzielczosc Y
    private float defaultRatio; //domyslne ratio

    private float factor;  //wspolczynnik odkryty przeze mnie :)

    public string deviceOrientation; //wartosc kontrolna, czy urzadzenie jest bardziej szerokie czy wysokie

    private float relativeRatio; //ratio obliczone przez podzielenie ratio urzadzenia przez ratio domyslne

    //nowe rozdzielczosci dla canvasa
    public float canvasX;
    public float canvasY;

    //SKRYPT PRZYPNIJ DO CANVASA!!!


    void Awake()
    {
        factor = 1.375f;

        resolutionX = Screen.width; //pobranie szerokosci X urzadzenia
        resolutionY = Screen.height; //pobranie wysokosci Y urzadzenia
                                     //resolutionX = 960f;
                                     //resolutionY = 640f;
        ratioDevice = resolutionX / resolutionY; //obliczenie ratio urzadzenia

        defaultRatio = defaultX / defaultY; //okreslenie domyslnego ratio, w tym przypadku 1.5f

        float aspectRatio = Mathf.Max(resolutionX, resolutionY) / Mathf.Min(resolutionX, resolutionY);
        //float size = Mathf.Sqrt(Mathf.Pow(Screen.width, 2) + Mathf.Pow(Screen.height, 2));
        //size >= 6.5f

        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            if (ratioDevice < 1.5f)
            {
                VerticalDevice();
                deviceOrientation = "iPad/Tablet";
            }

            else
            {
                HorizontalDevice();
                deviceOrientation = "iPhone/Smartphone";
            }
        }

        else
        {
            if (ratioDevice > 0.6f)
            {
                VerticalDevice();
                deviceOrientation = "iPad/Tablet";
            }

            else
            {
                HorizontalDevice();
                deviceOrientation = "iPhone/Smartphone";
            }
        }
    }


    void Update()
    {

    }

    public void HorizontalDevice() //urzadzenie szerokie
    {
        relativeRatio = ratioDevice / defaultRatio; //relativeRatio = ratioDevice / 1.5f

        float relativeResX = defaultX * relativeRatio; //nowa rozdzielczosc X dla urzadzenia na szablonie
        float relativeResY = relativeResX / ratioDevice; //nowa rozdzielczosc Y dla urzadzenia na szablonie

        canvasX = relativeResX * factor; //nowe wymiary X dla canvasa
        canvasY = relativeResY * factor; //nowe wymiary Y dla canvasa

        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasX, canvasY);
            this.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.0f;
        }
        else
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasY, canvasX);
            this.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.0f;
        }
    }

    public void VerticalDevice()
    {
        float relativeFactorY = resolutionY / 1053f;

        float _canvasY = resolutionY / relativeFactorY;
        float _canvasX = _canvasY * ratioDevice;

        canvasX = _canvasX;
        canvasY = _canvasY;


        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasX, canvasY);
            this.GetComponent<CanvasScaler>().matchWidthOrHeight = 1.0f;
        }
        else
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(canvasY, canvasX);
            this.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.0f;
        }
    }

}