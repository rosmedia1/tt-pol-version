﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Ukryj_Przyciski : MonoBehaviour
{

    Animator anim;
    public GameObject[] przyciski_menu;
    public GameObject dzwiek;

    void Start()
    {
        Ukryj_Przyciskii();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Ukryj_Przyciskii()
    {
        //if (anim.GetCurrentAnimatorStateInfo(1) == true)


        for (int i = 0; i < przyciski_menu.Length; i++)
        {
            if (przyciski_menu[i].transform.childCount > 0)
            {
                for (int j = 0; j < przyciski_menu[i].transform.childCount; j++)
                {
                    przyciski_menu[i].transform.GetChild(j).gameObject.SetActive(false);
                }

            }
            przyciski_menu[i].GetComponent<Image>().enabled = false;
            if (przyciski_menu[i].GetComponent<GraphicRaycaster>() != null) przyciski_menu[i].GetComponent<GraphicRaycaster>().enabled = false;
        }
    }

    public void Pokaz_przyciski()
    {

        for (int i = 0; i < przyciski_menu.Length; i++)
        {
            if (przyciski_menu[i].transform.childCount > 0)
            {
                for (int j = 0; j < przyciski_menu[i].transform.childCount; j++)
                {
                    przyciski_menu[i].transform.GetChild(j).gameObject.SetActive(true);
                }



            }
            przyciski_menu[i].GetComponent<Image>().enabled = true;
            if (przyciski_menu[i].GetComponent<GraphicRaycaster>() != null) przyciski_menu[i].GetComponent<GraphicRaycaster>().enabled = true;
        }
    }

    public void Ukryj_buttony()
    {
        for (int i = 0; i < przyciski_menu.Length; i++)
        {




        }

    }

    public void Ukryj_dzwiek()
    {
        dzwiek.active = false;
    }





}
