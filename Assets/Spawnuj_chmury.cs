﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawnuj_chmury : MonoBehaviour
{
    public GameObject chmurka;
    public Sprite[] chmurki_sprajty;
    public List<GameObject> lista_chmurek;
    public bool przeciaganie;
    public bool zostaw;

    // Start is called before the first frame update
    void Start()
    {
        zostaw = true;
        przeciaganie = false;
        int losowa_chmurka = Random.Range(0, chmurki_sprajty.Length);
        Vector3 pozycja_spawnu = new Vector3(this.transform.position.x, Random.Range(0, 6), this.transform.position.z);
        GameObject chmurka_obj = Instantiate(chmurka, pozycja_spawnu, Quaternion.identity, this.transform);
        chmurka_obj.GetComponent<SpriteRenderer>().sprite = chmurki_sprajty[losowa_chmurka];
        lista_chmurek.Add(chmurka);
        StartCoroutine(Spawnuj_chmurki());
    }

    // Update is called once per frame
    void Update()
    {


    }


    IEnumerator Spawnuj_chmurki()
    {
        int losowy_czas_spawnu = Random.Range(3, 8);
        yield return new WaitForSeconds(losowy_czas_spawnu);
        int losowa_chmurka = Random.Range(0, chmurki_sprajty.Length);
        Vector3 pozycja_spawnu = new Vector3(this.transform.position.x, Random.Range(0, 5), this.transform.position.z);
        GameObject chmurka_obj = Instantiate(chmurka, pozycja_spawnu, Quaternion.identity, this.transform);
        chmurka_obj.GetComponent<SpriteRenderer>().sprite = chmurki_sprajty[losowa_chmurka];
        lista_chmurek.Add(chmurka);
        StartCoroutine(Spawnuj_chmurki());
    }

    public void Zablokuj_chwyt(GameObject chwycona_chmurka)
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i) != chwycona_chmurka)
            {
                this.transform.GetChild(i).GetComponent<PolygonCollider2D>().enabled = false;
            }
        }
    }

    public void Odblokuj_chwyt()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {


            this.transform.GetChild(i).GetComponent<PolygonCollider2D>().enabled = true;

        }
    }

    public void ZniszczonoChmurke(GameObject chmura_usun)
    {
        lista_chmurek.Remove(chmura_usun);
    }





}

