﻿using UnityEngine;
using GoogleMobileAds.Api;
using System;
using UnityEngine.UI;
//Interstitial ad

public class AdMob : MonoBehaviour
{
    
    private InterstitialAd adInterstitial;

    private string idApp, idInterstitial;

    //[SerializeField] Button BtnInterstitial;


    void Start()
    {
        //BtnInterstitial.interactable = false;


        idInterstitial = "ca-app-pub-2575683230512628/3686962224";

        MobileAds.Initialize(initStatus => { });

        RequestInterstitialAd();
    }

    #region Interstitial methods ---------------------------------------------

    public void RequestInterstitialAd()
    {
        adInterstitial = new InterstitialAd(idInterstitial);
        AdRequest request = AdRequestBuild();
        adInterstitial.LoadAd(request);

        //attach events
        adInterstitial.OnAdLoaded += this.HandleOnAdLoaded;
        adInterstitial.OnAdOpening += this.HandleOnAdOpening;
        adInterstitial.OnAdClosed += this.HandleOnAdClosed;
    }

    public void ShowInterstitialAd()
    {
        if (adInterstitial.IsLoaded())
            adInterstitial.Show();
    }

    public void DestroyInterstitialAd()
    {
        adInterstitial.Destroy();
    }

    //interstitial ad events
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        //this method executes when interstitial ad is Loaded and ready to show
       
       // BtnInterstitial.interactable = true; //button is ready to click (enabled)
    }

    public void HandleOnAdOpening(object sender, EventArgs args)
    {
        //this method executes when interstitial ad is shown
        MobileAds.SetiOSAppPauseOnBackground(true);
       // BtnInterstitial.interactable = false; //disable the button
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        //this method executes when interstitial ad is closed
        MobileAds.SetiOSAppPauseOnBackground(false);

        adInterstitial.OnAdLoaded -= this.HandleOnAdLoaded;
        adInterstitial.OnAdOpening -= this.HandleOnAdOpening;
        adInterstitial.OnAdClosed -= this.HandleOnAdClosed;

        RequestInterstitialAd(); //request new interstitial ad after close
    }

    #endregion


    //------------------------------------------------------------------------
    AdRequest AdRequestBuild()
    {
        return new AdRequest.Builder().Build();
    }

    void OnDestroy()
    {
        DestroyInterstitialAd();

        //dettach events
        adInterstitial.OnAdLoaded -= this.HandleOnAdLoaded;
        adInterstitial.OnAdOpening -= this.HandleOnAdOpening;
        adInterstitial.OnAdClosed -= this.HandleOnAdClosed;
    }

}