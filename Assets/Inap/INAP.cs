﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
namespace CompleteProject
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class INAP : MonoBehaviour, IStoreListener
    {
        //public static int counter;
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
        public GameObject rawimage;
        public GameObject canvas;
        public GameObject pomidor;
        public GameObject ogorek;
        public GameObject no_ads;
        public GameObject parental_gate;
        public GameObject gumka;
        public bool ads_on;
        public GameObject reklama;
        // Product identifiers for all products capable of being purchased: 
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values 
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.
        //public static string kProductIDConsumable = "consumable"; // odnawialny na przyklad do monet 
        //public static string kProductIDNonConsumable = "nonconsumable"; // tylko kupujesz raz na przyklad unlockall 
        //public static string kProductIDSubscription = "subscription"; // to subskrypcja 

        // Apple App Store-specific product identifier for the subscription product.
        //private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

        // Google Play Store-specific product identifier subscription product.
        //private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

        private List<Product> products = new List<Product>();
        private Action<bool> callback = null;
        public bool isLoading = true;
        [SerializeField] TMP_Text priceText;
        string _unlockAll = "unlock_all"; //nonconsumable
        public GameObject Inap_screen;
        private static string kProductNameAppleUnlockAll = "com.rosapp.mojepierwszedzwieki.InApp";
        private static string kProductNameGooglePlayUnlockAll = "full";

        void Start()
        {
            // If we haven't set up the Unity Purchasing reference
            //if (m_StoreController == null)
            //{
            //    // Begin to configure our connection to Purchasing
            //    InitializePurchasing();
            //}


            InitializePurchasing();
            if (PlayerPrefs.HasKey("unlock") == true)
            {
                if (no_ads != null)
                {
                    no_ads.SetActive(false); // odkomentuj jesli chcesz dac do sprawdzenia
                }
                if (Inap_screen != null)
                {
                    Inap_screen.SetActive(false);
                }
                if (rawimage != null)
                {
                    rawimage.SetActive(true);
                }
                if (canvas != null)
                {
                    canvas.GetComponent<spawn_zabawek>().enabled = true;
                }
                if (pomidor != null)
                {
                    pomidor.GetComponent<podnies>().enabled = true;
                }
                if (ogorek != null)
                {
                    ogorek.GetComponent<podnies>().enabled = true;
                }
                if (gumka != null)
                {
                    
                    gumka.GetComponent<gumka_do_mazania>().enabled = true;
                }


            }
            else if (PlayerPrefs.HasKey("unlock") == false)
            {
                if (no_ads != null)
                {
                    no_ads.SetActive(true); //odkomentuj jesli chcesz dac do sprawdzenia
                }
                if (Inap_screen != null)
                {
                    Inap_screen.SetActive(true);
                }
                if (rawimage != null)
                {
                    rawimage.SetActive(false);
                }
                if (canvas != null)
                {
                    canvas.GetComponent<spawn_zabawek>().enabled = false;
                }
                if (pomidor != null)
                {
                    pomidor.GetComponent<podnies>().enabled = false;
                }
                if (ogorek != null)
                {
                    ogorek.GetComponent<podnies>().enabled = false;
                }
                if (gumka != null)
                {
                    gumka.GetComponent<Animator>().enabled = false;
                    gumka.GetComponent<gumka_do_mazania>().enabled = false;
                }

            }


        }

        public void Update()
        {
            //            Debug.Log(counter);
        }

        public void InitializePurchasing()
        {
            // If we have already connected to Purchasing ...
            //if (IsInitialized())
            //{
            //    // ... we are done here.
            //    return;
            //}
            products.Clear();
            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Add a product to sell / restore by way of its identifier, associating the general identifier
            // with its store-specific identifiers.
            //builder.AddProduct(kProductIDConsumable, ProductType.Consumable);
            // Continue adding the non-consumable product.

            builder.AddProduct(_unlockAll, ProductType.NonConsumable, new IDs() {
            { kProductNameAppleUnlockAll, AppleAppStore.Name },
            { kProductNameGooglePlayUnlockAll, GooglePlay.Name },
        });

            // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
            // if the Product ID was configured differently between Apple and Google stores. Also note that
            // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
            // must only be referenced here. 
            //builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
            //    { kProductNameAppleSubscription, AppleAppStore.Name },
            //    { kProductNameGooglePlaySubscription, GooglePlay.Name },
            //});

            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            UnityPurchasing.Initialize(this, builder);
        }

        public void AddInitializationCallback(System.Action<bool> handler = null)
        {
            callback = handler;
        }


        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }


        public void BuyConsumable()
        {
            // Buy the consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            //BuyProductID(kProductIDConsumable);
        }


        public void BuyNonConsumable()
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            BuyProductID(_unlockAll);
        }


        public void BuySubscription()
        {
            // Buy the subscription product using its the general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            // Notice how we use the general product identifier in spite of this ID being mapped to
            // custom store-specific identifiers above.
            //BuyProductID(kProductIDSubscription);
        }


        void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                    // LoadBack();

                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");

                });
                LoadBack();
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


        //  
        // --- IStoreListener
        //

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;

            products.Clear();

            foreach (var product in controller.products.all)
            {
                Debug.Log(product.metadata.localizedTitle);
                Debug.Log(product.metadata.localizedDescription);
                Debug.Log(product.metadata.localizedPriceString);

                products.Add(product);
                Debug.Log("Ilość w tablicy: " + products.Count);

                if (product.definition.id == _unlockAll)
                {
                    if (priceText != null)
                    {
                        priceText.text = product.metadata.localizedPriceString;
                    }

                }

                if (product.receipt != null)
                {
                    if (product.definition.type == ProductType.Subscription)
                    {
                        //if (checkIfProductIsAvailableForSubscriptionManager(product.receipt))
                        //{
                        //    SubscriptionManager p = new SubscriptionManager(product, null);
                        //    SubscriptionInfo info = p.getSubscriptionInfo();
                        //    Debug.Log("product id is: " + info.getProductId());
                        //    Debug.Log("purchase date is: " + info.getPurchaseDate());
                        //    Debug.Log("subscription next billing date is: " + info.getExpireDate());
                        //    Debug.Log("is subscribed? " + info.isSubscribed().ToString());
                        //    Debug.Log("is expired? " + info.isExpired().ToString());
                        //    Debug.Log("is cancelled? " + info.isCancelled());
                        //    Debug.Log("product is in free trial period? " + info.isFreeTrial());
                        //    Debug.Log("product is auto renewing? " + info.isAutoRenewing());
                        //    Debug.Log("subscription remaining valid time until next billing date is: " + info.getRemainingTime());
                        //    Debug.Log("is this product in introductory price period? " + info.isIntroductoryPricePeriod());
                        //    Debug.Log("the product introductory localized price is: " + info.getIntroductoryPrice());
                        //    Debug.Log("the product introductory price period is: " + info.getIntroductoryPricePeriod());
                        //    Debug.Log("the number of product introductory price period cycles is: " + info.getIntroductoryPricePeriodCycles());

                        //}
                        //else
                        //{
                        //    Debug.Log("This product is not available for SubscriptionManager class, only products that are purchase by 1.19+ SDK can use this class.");
                        //}
                    }
                    else
                    {
                        Debug.Log("the product is not a subscription product");
                    }
                }
                else
                {
                    Debug.Log("the product should have a valid receipt");
                }
            }

            isLoading = false;

            //if (callback != null)
            //{
            //    callback(isSubscribed);
            //}

            // Overall Purchasing system, configured with products for this application.
            //m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            //m_StoreExtensionProvider = extensions;
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);

            isLoading = false;

            if (callback != null)
            {
                callback(false);
            }
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            // A consumable product has been purchased by this user.
            //if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
            //{
            //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            //    // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            //    //ScoreManager.score += 100;
            //}
            // Or ... a non-consumable product has been purchased by this user.
            if (String.Equals(args.purchasedProduct.definition.id, _unlockAll, StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                if (PlayerPrefs.HasKey("unlock") == false)
                {
                    PlayerPrefs.SetInt("unlock", 0);
                    //PlayerPrefs.Save();
                    Debug.Log("Dodany klucz unlockall");
                    LoadBack();
                }
                else
                {

                    Debug.Log("Klucz unlock istnieje");
                    LoadBack();
                }
            }
            // Or ... a subscription product has been purchased by this user.
            //else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
            //{
            //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            //    // TODO: The subscription item has been successfully purchased, grant this to the player.
            //}
            // Or ... an unknown product has been purchased by this user. Fill in additional products here....
            else
            {
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }

        public void LoadBack()
        {
            StartCoroutine(Back());
            ads_on = false;
        }

        public void LoadKupno()
        {
            if (parental_gate != null)
            {
                parental_gate.SetActive(true);
                ads_on = true;
                parental_gate.GetComponent<parental_gate>().Otworz();
            }



        }

        //public IEnumerator Wroc_z_reklama()
        //{
        //    if (PlayerPrefs.HasKey("unlock") == false)
        //    {

        //        counter += 40;
        //        if (counter > 40)
        //        {

        //            reklama.GetComponent<AdMob>().ShowInterstitial();
        //            counter = 0;
        //        }

        //    }
        //    yield return new WaitForSeconds(2.5f);
        //    string last_Scene = PlayerPrefs.GetString("last_scene", "Menu01");
        //    SceneManager.LoadScene(last_Scene);

        //}


        public void ResetPlayerPrefs() //TODO USUNĄĆ TO!!!
        {
            PlayerPrefs.DeleteAll();
        }

        IEnumerator Back()
        {
            yield return new WaitForSeconds(1f);
            string last_Scene = PlayerPrefs.GetString("last_scene", "Menu01");
            SceneManager.LoadScene(last_Scene);
        }
    }

}
