﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gumka_do_mazania : MonoBehaviour
{
    public GameObject rawimage;
    public bool mazanie;
    public GameObject kredka;
    public GameObject kartka;
    public Vector2 kredka_Start_pos;
    // public Quaternion kredka_rotate_pos;
    public GameObject kredka_zamiennik;
    public AudioClip audio;


    // Start is called before the first frame update
    void Start()
    {
        //Vector2 start_pos = this.transform.position;
        kredka_Start_pos = new Vector2(kredka.transform.position.x, kredka.transform.position.y);
        //kredka_rotate_pos = kredka.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {



            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);
                if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                {

                    Vector2 current_pos = this.transform.localPosition;
                    if (touch.phase == TouchPhase.Began)
                    {
                        rawimage.GetComponent<MouseDraw_ForMap3>().IsEraser = true;
                        rawimage.GetComponent<MouseDraw_ForMap3>().trzymanie = true;
                        //kredka.transform.position = kredka_Start_pos;
                        //  kredka.transform.rotation = kredka_rotate_pos;
                        kredka.SetActive(false);
                        kredka_zamiennik.SetActive(true);
                        mazanie = true;



                    }
                    else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                    {



                        if (mazanie == true)
                        {

                            rawimage.GetComponent<MouseDraw_ForMap3>().trzymanie = true;
                            Vector3 touchedPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));
                            this.transform.position = Vector3.Lerp(touchedPos, touchedPos, Time.deltaTime);
                            this.GetComponent<AudioSource>().enabled = true;
                            this.GetComponent<AudioSource>().clip = audio;
                            this.GetComponent<AudioSource>().Play();
                        }

                    }
                    else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        kredka.SetActive(true);
                        kredka_zamiennik.SetActive(false);
                        kredka.transform.position = kredka_Start_pos;
                        // kredka.transform.rotation = kredka_rotate_pos;
                        rawimage.GetComponent<MouseDraw_ForMap3>().trzymanie = false;
                        rawimage.GetComponent<MouseDraw_ForMap3>().IsEraser = false;
                        mazanie = false;

                    }
                }


            }

            if (mazanie == true)
            {
                for (int j = 0; j < Input.touchCount; j++)
                {
                    Touch touch = Input.GetTouch(j);

                    Vector3 touchedPos = Camera.main.ScreenToWorldPoint(touch.position);

                    if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                    {

                        this.transform.position = new Vector3(touchedPos.x, touchedPos.y, this.transform.position.z);
                    }
                }
            }


        }
    }



}
