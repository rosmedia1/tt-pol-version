﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swinka_script : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                Vector3 touch_pos = Camera.main.ScreenToWorldPoint(touch.position);

                if (this.name == "swinka")
                {
                    if (this.GetComponent<PolygonCollider2D>().OverlapPoint(touch_pos))
                    {
                        if (touch.phase == TouchPhase.Began)
                        {
                            this.GetComponent<Farma>().enabled = true;
                        }
                    }
                }



            }


        }
    }

    public void Wylacz_swinie()
    {
        this.GetComponent<Farma>().enabled = false;
    }

}

