﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kaczka_chodzi_dzwiek : MonoBehaviour
{
    public AudioClip kaczka_do_wody;
    public AudioClip kaczka_z_wody;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Kaczka_Do_Wody()
    {
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = kaczka_do_wody;
        this.GetComponent<AudioSource>().Play();
    }


    public void Kaczka_z_wody()
    {
        this.GetComponent<AudioSource>().enabled = true;
        this.GetComponent<AudioSource>().clip = kaczka_z_wody;
        this.GetComponent<AudioSource>().Play();
    }
}
