﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class odpal_czujnik : MonoBehaviour
{
    public GameObject czujnik;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Odpal_Off()
    {
        this.GetComponent<PolygonCollider2D>().enabled = false;
        czujnik.SetActive(false);
    }



    public void Odpal()
    {
        this.GetComponent<PolygonCollider2D>().enabled = true;
        czujnik.SetActive(true);
    }
}
