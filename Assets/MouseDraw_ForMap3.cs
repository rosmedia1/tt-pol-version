﻿using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class MouseDraw_ForMap3 : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public PolygonCollider2D poly;
    public Vector2 local_pos;
    RectTransform rectTransform;
    public GameObject gumka;
    public GameObject kredka;
    public GameObject kredka_zamiennik;
    public Vector2 gumkaStart_pos;
    public Vector2 kredka_pos;
    // public Quaternion kredka_rotation;
    //  public Quaternion kredka_zamiennik_rotation;
    public Vector2 kredka_zamiennik_pos;
    public Vector2 Gum_position;
    public GameObject PoleDoRysowania;
    public AudioClip audio1;
    public AudioClip audio2;
    public GameObject kartka;
    private bool dragging;
    private int fingerIndex;
    private int speed = 1;
    [SerializeField]
    [Tooltip("The Canvas which is a parent to this Mouse Drawing Component")]
    private Canvas HostCanvas;
    public bool trzymanie;

    [Range(2, 20)]
    [Tooltip("The Pens Radius")]
    public int penRadius = 10;
    public int gumHigh = 4; // wysokosc mazania
    public int gumWidth = 2; // szerokosc mazania
    [Tooltip("The Pens Colour.")]
    public Color32 penColour = new Color32(0, 0, 0, 255);

    [Tooltip("The Drawing Background Colour.")]
    public Color32 backroundColour = new Color32(0, 0, 0, 0);

    [SerializeField]
    [Tooltip("Pen Pointer Graphic GameObject")]
    private Image penPointer;


    [Tooltip("Toggles between Pen and Eraser.")]
    public bool IsEraser = false;

    private bool _isInFocus = false;
    /// <summary>
    /// Is this Component in focus.
    /// </summary>
    public bool IsInFocus
    {
        get => _isInFocus;
        private set
        {
            if (value != _isInFocus)
            {
                _isInFocus = value;
                TogglePenPointerVisibility(value);
            }
        }
    }

    private float m_scaleFactor = 10;
    private RawImage m_image;

    private Vector2? m_lastPos;
    private Vector2? zmazywanie;

    // public Transform granicaLewa;   // wazne
    // public Transform granicaPrawa; // wazne

    void Start() // wazne
    {
        kredka_zamiennik_pos = new Vector2(kredka_zamiennik.transform.position.x, kredka_zamiennik.transform.position.y);
        gumkaStart_pos = new Vector2(gumka.transform.position.x, gumka.transform.position.y);
        kredka_pos = new Vector2(kredka.transform.position.x, kredka.transform.position.y);
        //  kredka_rotation = kredka.transform.rotation;
        // kredka_zamiennik_rotation = kredka_zamiennik.transform.rotation;
        Init();
    }

    public void Losuj_Kolorki_LR() // wazne
    {

        if (kredka != null)
        {
            float r = UnityEngine.Random.Range(0, 1f);
            float g = UnityEngine.Random.Range(0, 1f);
            float b = UnityEngine.Random.Range(0, 1f);
            Color kolor = new Color(r, g, b, 1f);
            penColour = kolor;
        }

    }





    private void OnEnable()
    {
        m_image = transform.GetComponent<RawImage>();
        TogglePenPointerVisibility(false);
    }

    // TODO: Replace with IPointerDownHandler...
    void Update()
    {




        if (Input.touchCount > 0)
        {


            for (int i = 0; i < Input.touchCount; i++)
            {


                Touch touch1 = Input.GetTouch(0);
                Vector2 touch_pos = new Vector2(touch1.position.x, touch1.position.y);
                var camera = Camera.main;
                Vector2 worldPos = camera.ScreenToWorldPoint(touch_pos);
                Vector2 kredka_start_pos = kredka.transform.position;
                //Ray ray = Camera.main.ScreenPointToRay(touch1.position);
                var rt = kartka.GetComponent<RectTransform>();
                Vector2 anchoredPosition = rt.InverseTransformPoint(worldPos);
                bool inside = rt.rect.Contains(anchoredPosition);
                var degrees = rt.eulerAngles.z;
                var radians = Mathf.Deg2Rad * degrees;


                //print("inside: " + inside + ", anchoredPosition: " + anchoredPosition + ", rt: " + rt.rect + ", worldPos: " + worldPos + ", touchPos: " + touch_pos + ", degrees: " + degrees + ", radians: " + radians);

                if (!inside)
                {

                    m_lastPos = null;
                    kredka.transform.position = kredka_zamiennik_pos;
                    //kredka.transform.rotation = kredka_zamiennik_rotation;
                    trzymanie = false;
                    continue;
                }

                trzymanie = true;




                if (touch1.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Canceled)
                {
                    //kredka.transform.Rotate(Time.deltaTime * speed, kredka.transform.rotation.z, 50f);
                    //  kredka.transform.position = kredka_zamiennik_pos;
                    // kredka.transform.rotation = kredka_zamiennik_rotation;
                    trzymanie = false;
                    m_lastPos = null;
                    continue;
                }

                //if (fingerIndex >= 0 && fingerIndex != touch1.fingerId) { continue; }
                if (touch1.phase == TouchPhase.Began)
                {
                    //kredka.transform.Rotate(Time.deltaTime * speed, kredka.transform.rotation.z, -50f);
                    Vector2 good_pos = new Vector2(worldPos.x, worldPos.y + 1.3f);
                    kredka.transform.position = good_pos;
                    Losuj_Kolorki_LR();
                }

                if (touch1.phase == TouchPhase.Moved)
                {
                    if (gumka.GetComponent<gumka_do_mazania>().mazanie == true)
                    {
                        this.GetComponent<AudioSource>().enabled = true;
                        this.GetComponent<AudioSource>().clip = audio1;
                        this.GetComponent<AudioSource>().Play();
                    }
                    else if (gumka.GetComponent<gumka_do_mazania>().mazanie == false)
                    {
                        this.GetComponent<AudioSource>().enabled = true;
                        this.GetComponent<AudioSource>().clip = audio2;
                        this.GetComponent<AudioSource>().Play();
                    }
                    gumka.transform.position = gumkaStart_pos;
                    Vector2 good_pos = new Vector2(worldPos.x, worldPos.y + 1.3f);
                    kredka.transform.position = good_pos;
                    dragging = true;
                    WritePixels(touch1.position);
                }




                //}

                //if (trzymanie == false)
                //{
                //    m_lastPos = null;
                //}
                if (gumka.GetComponent<gumka_do_mazania>().mazanie == true)
                {



                    kredka.transform.position = kredka_zamiennik_pos;
                    m_lastPos = null;
                }



            }

        }



    }


    private void Init() // wazne
    {

        m_scaleFactor = HostCanvas.scaleFactor * 2;

        var tex = new Texture2D(Convert.ToInt32(Screen.width / m_scaleFactor), Convert.ToInt32(Screen.height / m_scaleFactor), TextureFormat.RGBA32, false);
        for (int i = 0; i < tex.width; i++)
        {
            for (int j = 0; j < tex.height; j++)
            {
                tex.SetPixel(i, j, backroundColour);
            }
        }

        tex.Apply();
        m_image.texture = tex;
    }


    public void WritePixels(Vector2 pos)
    {

        pos /= m_scaleFactor;
        var mainTex = m_image.texture;
        var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);

        var curTex = RenderTexture.active;
        var renTex = new RenderTexture(mainTex.width, mainTex.height, 32);

        Graphics.Blit(mainTex, renTex);
        RenderTexture.active = renTex;

        tex2d.ReadPixels(new Rect(0, 0, mainTex.width, mainTex.height), 0, 0);

        var col = IsEraser ? backroundColour : penColour;
        var positions = m_lastPos.HasValue ? GetLinearPositions(m_lastPos.Value, pos) : new List<Vector2>() { pos };

        foreach (var position in positions)
        {
            var pixels = GetNeighbouringPixels(new Vector2(mainTex.width, mainTex.height), position, IsEraser ? penRadius * 8 : penRadius);

            if (pixels.Count > 0)
                foreach (var p in pixels)
                    tex2d.SetPixel((int)p.x, (int)p.y, col);
        }

        tex2d.Apply();

        RenderTexture.active = curTex;
        renTex.Release();
        Destroy(renTex);
        Destroy(mainTex);
        curTex = null;
        renTex = null;
        mainTex = null;

        m_image.texture = tex2d;
        m_lastPos = pos;
    }

    [ContextMenu("Clear Texture")]
    public void ClearTexture()
    {

        var mainTex = m_image.texture;
        var tex2d = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, false);

        for (int i = 0; i < tex2d.width; i++)
        {
            for (int j = 0; j < tex2d.height; j++)
            {
                tex2d.SetPixel(i, j, backroundColour);
            }
        }

        tex2d.Apply();
        m_image.texture = tex2d;
    }

    private List<Vector2> GetNeighbouringPixels(Vector2 textureSize, Vector2 position, int czujnik_radius)
    {
        var pixels = new List<Vector2>();

        for (int i = -czujnik_radius; i < czujnik_radius; i++)
        {
            for (int j = -czujnik_radius; j < czujnik_radius; j++)
            {
                var pxl = new Vector2(position.x + i, position.y + j);
                if (pxl.x > 0 && pxl.x < textureSize.x && pxl.y > 0 && pxl.y < textureSize.y)
                    pixels.Add(pxl);
            }
        }

        return pixels;
    }



    private List<Vector2> GetLinearPositions(Vector2 firstPos, Vector2 secondPos, int spacing = 2)
    {
        var positions = new List<Vector2>();

        var dir = secondPos - firstPos;

        if (dir.magnitude <= spacing)
        {
            positions.Add(secondPos);
            return positions;
        }

        for (int i = 0; i < dir.magnitude; i += spacing)
        {
            var v = Vector2.ClampMagnitude(dir, i);
            positions.Add(firstPos + v);
        }

        positions.Add(secondPos);
        return positions;
    }


    public void SetPenColour(Color32 color) => penColour = color;


    public void SetPenRadius(int radius) => penRadius = radius;
    // public void SetCarSensor(int czujnik_radius) => carRadius = czujnik_radius;
    //public void SetCarSensor(int highSensor) => carHigh = highSensor;
    //public void SetCarSensorV2(int widthSensor) => carWidth = widthSensor;
    public void SetGumSensor(int highSensor) => gumHigh = highSensor;    // wysokosc przekazana do int high sensor
    public void SetGumSensorV2(int widthSensor) => gumWidth = widthSensor;   // szerokosc przekazana do int width sensor
    public void PositionOfGum(Vector2 GumPos) => Gum_position = GumPos;


    private void TogglePenPointerVisibility(bool isVisible)
    {
        if (isVisible)
            //SetPenPointerSize();

            penPointer.gameObject.SetActive(isVisible);
        Cursor.visible = !isVisible;
    }

    public Vector2 Correct(Vector2 pos)
    {
        //Vector2 kartka_pos = new Vector2(kartka.transform.position.x, kartka.transform.position.y);
        rectTransform = kartka.GetComponent<RectTransform>();
        Vector2 localPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, pos, Camera.current, out localPos);
        return localPos;

    }

    /// <summary>
    /// On Mouse Pointer entering this Components Image Space.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData) => IsInFocus = true;

    /// <summary>
    /// On Mouse Pointer exiting this Components Image Space.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData) => IsInFocus = false;

}