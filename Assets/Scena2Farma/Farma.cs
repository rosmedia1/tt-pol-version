﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farma : MonoBehaviour
{
    private Collider2D col;
    public GameObject drzewo;
    private AudioSource src;
    private Animator anim;
    private Rigidbody2D rb;
    private ParticleSystem particles;
    public GameObject shower;

    private GameObject poopPos;
    public AudioClip[] audioClips;
    public AudioClip[] idleAudioC;
    public AnimationClip[] animClips;
    public AnimationClip[] idleAnimClips;
    public AnimationClip[] idleAnimClipsWSound;
    public GameObject[] cloudPre;
    public GameObject[] poop;
    public GameObject ApplPre;
    public GameObject dymek;
    public GameObject siankoPre;
    public GameObject[] LeafPre;
    public GameObject swinka;


    Vector2 touchposition;
    float rCSi;
    Vector3 rCO;
    Touch touch;

    private int kaczeKlikniecie;
    private float randomSoundOrNo;
    private bool LocalWalkBool;
    private bool kaczyGuard;
    public float kaczyDelay = 10;
    public float idleGuardTime = 10;
    public float animatorDelay = 10;
    private int randomIdle;
    private int randomSound;
    [SerializeField] private bool stopIdleFromPlaying;
    private float zapiszVC;
    private bool cloud1;
    private int randomAnim;
    private bool isDragging;
    private bool isSleeping;
    private GameObject Sianko_prefab;
    private void Start()
    {
        col = this.GetComponent<Collider2D>();
        anim = this.GetComponent<Animator>();
        src = this.GetComponent<AudioSource>();
        randomIdle = Random.Range(3, 12);

        if (this.gameObject.tag == "CloudSpawner")
        {
            StartCoroutine(CloudSpawner());
        }
        else if (gameObject.tag == "banka")
        {
            rb = gameObject.GetComponent<Rigidbody2D>();
            Clouds();
        }
        else if (gameObject.tag == "Stolec")
        {
            rb = gameObject.GetComponent<Rigidbody2D>();
            PoopPhy();
        }
        else if (gameObject.name == "Kacza")
        {
            anim = GameObject.Find("KaczkaMala").GetComponent<Animator>();
        }
        else if (gameObject.name == "Kaczor" || this.gameObject.name == "Kacza")
        {
            StartCoroutine(KaczaAnimacja());
        }
        else if (gameObject.name == "swinka")
        {
            PigClickEnd();
            StartCoroutine(GetPigDirty());
            poopPos = GameObject.Find("GeneratorStolca");
        }
        else if (gameObject.tag == "SpawnerJablek")
        {

        }
        else if (gameObject.tag == "jablko")
        {
            float RAS = Random.Range(50f, 56f);
            transform.localScale = new Vector3(RAS, RAS, 1);
        }
        else if (gameObject.tag == "LeafMaker")
        {
            Quaternion rLeafr = Quaternion.Euler(0, 0, Random.Range(-60f, 60f));
            int rLeaf = Random.Range(0, LeafPre.Length);
            var Leafs = Instantiate(LeafPre[rLeaf], this.transform.position, rLeafr);
            Leafs.transform.parent = this.gameObject.transform;
        }
        else if (gameObject.tag == "Leaf")
        {
            //StartCoroutine(MakeLeavesFall());
        }
        else if (gameObject.name == "Traktor")
        {
            //  particles = transform.GetChild(transform.childCount - 1).GetComponent<ParticleSystem>();
        }
        else if (gameObject.tag == "Sianko")
        {
            isDragging = true;
        }
        if (this.gameObject.tag == "Zwierze" && gameObject.name != "swinka")
        {
            //StartCoroutine(IdleAnimations());
        }


    }


    public void Ubrudz_sie()
    {

        if (this.anim.GetCurrentAnimatorStateInfo(1).IsName("swinka_brud"))
        {
            this.GetComponent<Animator>().Play("swinka_brud");
        }
        else
        {
            this.GetComponent<Animator>().Play("swinka_brud_pojawianie");
        }





    }


    IEnumerator GetPigDirty()
    {


        yield return new WaitForSeconds(Random.Range(10f, 20f));
        newPigDirty();
        Debug.Log("GetPigDirty");


    }

    public void Odpal_coroutynke()
    {
        Debug.Log("startujń");
        StartCoroutine(GetPigDirty());
        isSleeping = false;
    }

    public void Zatrzymaj_coroutynke()
    {
        Debug.Log("zatrzymaj sie");
        StopCoroutine(GetPigDirty());
        isSleeping = true;


    }


    void newPigDirty()
    {
        if (shower.GetComponent<Shower>().isPigDirty == false && stopIdleFromPlaying == false && isSleeping == false)
        {
            int rDirty = Random.Range(0, 2);
            anim.SetBool("IsPigGettingDirty", true);
            //anim.SetInteger("ChooseAnimation", rDirty);
            shower.GetComponent<Shower>().isPigDirty = true;
            this.GetComponent<Animator>().Play("swinka_kladzenie_sie" + Random.Range(1, 3));
            // this.GetComponent<Animator>().Play("swinka_brud_pojawianie");
            Debug.Log("new_pig_dirty");
        }
        else
        {
            StartCoroutine(GetPigDirty());
        }
    }
    public void PigClickEnd()
    {

        anim.SetInteger("ChooseClick", 3);
        StartCoroutine(IdleGuard());
    }
    public void MakePigClean()
    {
        if (shower.GetComponent<Shower>().isPigDirty == true && shower.GetComponent<Shower>().isShowerOn == true)
        {
            anim.SetBool("IsPigGettingDirty", false);
            this.GetComponent<Animator>().Play("swinka_brud_znikanie");
            shower.GetComponent<Shower>().isPigDirty = false;
            stopIdleFromPlaying = true;
            StartCoroutine(GetPigDirty());
            Debug.Log("swinka_brudna_prysznic");
        }
        else if (shower.GetComponent<Shower>().isPigDirty == false && shower.GetComponent<Shower>().isShowerOn == true)
        {
            Debug.Log("swinka_czysta_prysznic");
            anim.SetBool("ShowerOn", true);
            stopIdleFromPlaying = true;
        }
    }
    public void ShowerOffPig()
    {
        anim.SetBool("ShowerOn", false);
    }
    void TurnShowerOn()
    {
        anim.SetBool("IsShowerOn", true);
    }
    public void TurnShowerOff()
    {
        anim.SetBool("IsShowerOn", false);
        shower.GetComponent<Shower>().isShowerOn = false;
    }
    public void TurnPigDirtOn()
    {
        anim.SetBool("IsPigDirty", true);
    }
    public void TurnPigDirtOff()
    {
        anim.SetBool("IsPigDirty", false);
    }


    IEnumerator KaczaAnimacja()
    {
        yield return new WaitForSeconds(Random.Range(5f, 8f));

        StartCoroutine(KaczkaQuack());

    }
    IEnumerator KaczkaQuack()
    {
        anim.SetBool("QuackBool", true);

        yield return new WaitForSeconds(0.4f);
        anim.SetBool("QuackBool", false);
        if (kaczyGuard == false)
        {

        }
    }
    IEnumerator KaczkaQuack2()
    {
        anim.SetBool("QuackBool", true);
        // Debug.Log(this.gameObject.name + " Quack");
        src.clip = audioClips[kaczeKlikniecie];
        src.Play();
        yield return new WaitForSeconds(0.4f);
        anim.SetBool("QuackBool", false);
    }
    void Clouds()
    {
        float rCS = Random.Range(0.5f, 0.75f);
        this.rb.velocity = new Vector2(-rCS, 0);
        rCSi = Random.Range(0.75f, 1f);
        this.gameObject.transform.localScale = new Vector2(rCSi, rCSi);
    }
    void PoopPhy()
    {
        this.rb.velocity = new Vector2(Random.Range(0.5f, 1f), 0);
        this.rb.angularVelocity = Random.Range(0f, 3f);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "CloudDestroyer" && gameObject.tag == "banka")
        {
            //Debug.Log("kolizja");
            Destroy(this.gameObject);
        }
        else if (other.name == "Podloga" && gameObject.tag == "Stolec")
        {
            col.isTrigger = false;
        }
    }
    private void Update()
    {
        if (isDragging == true)
        {
            if (gameObject.tag == "jablko")
            {
                this.gameObject.transform.position = touchposition;
            }
            else if (gameObject.tag == "banka")
            {
                if (zapiszVC == 0)
                {
                    zapiszVC = rb.velocity.x;
                }
                else
                {
                    rb.velocity = Vector3.zero;
                    this.gameObject.transform.position = new Vector3(touchposition.x, Mathf.Clamp(touchposition.y, -1f + (rCSi / 2), 4.75f + (rCSi)));
                }
            }
            else if (gameObject.tag == "Sianko")
            {
                this.gameObject.transform.position = touchposition;
            }

        }
        else
        {
            if (gameObject.tag == "banka")
            {
                //Debug.Log(this.gameObject.transform.position.y);
                if (zapiszVC != 0)
                {
                    rb.velocity = new Vector3(zapiszVC, 0);
                }
            }
            else if (gameObject.tag == "jablko")
            {

                // Vector2 posNow = this.gameObject.transform.position;
                //Vector2 returnObj = Vector2.Lerp(posNow, transform.parent.position, 4 * Time.deltaTime);
                //this.gameObject.transform.position = returnObj;
            }
            else if (gameObject.tag == "Sianko")
            {
                Destroy(this.gameObject);
            }
        }
        //Debug.Log(touch.phase);
        //Debug.Log(isAlreadyPlaying);
        //Skrypt sprawdzajacy nacisniecia i triggerujacy animacje i dzwieki
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                touch = Input.GetTouch(i);
                touchposition = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                if (col != null)
                {
                    if (col.OverlapPoint(touchposition))
                    {
                        //Debug.Log("Touch");
                        if (gameObject.tag == "jablko" || gameObject.tag == "banka" || gameObject.tag == "Stolec") // czemu tu jest jedno |
                        {
                            if (touch.phase == TouchPhase.Began)
                            {
                                isDragging = true;
                            }
                            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                            {
                                //col.isTrigger = false;
                                isDragging = false;
                            }
                        }
                        if (gameObject.tag == "Sianko" && touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary) // tu to samo
                        {
                            isDragging = true;
                        }
                        else if (gameObject.tag == "Sianko" && touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                        {
                            //col.isTrigger = false;
                            isDragging = false;
                        }
                        if (touch.phase == TouchPhase.Began && gameObject.tag == "Zwierze" && gameObject.name != "Kacza" && gameObject.name != "Kaczor")
                        {
                            PlayAnimationAndSound();
                            if (gameObject.name == "swinka")
                            {
                                anim.SetBool("IsPigGettingDirty", false);
                                //Debug.Log("rSwinia");
                                int rSwinia = Random.Range(0, 2);
                                int rSwiniaplus = rSwinia + 1;
                                randomSound = rSwinia;
                                // anim.SetInteger("ChooseClick", rSwinia);
                                anim.CrossFade("swinka_click" + rSwiniaplus, 0.04f);
                                Playsound();
                                stopIdleFromPlaying = true;
                                //StartCoroutine(Sranie());
                            }
                            /*  else if (gameObject.name == "Siano")
                              {
                                  Sianko_prefab = Instantiate(siankoPre, touchposition, Quaternion.identity, this.transform);
                                  if(touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                                  {
                                      Destroy(Sianko_prefab);
                                  }


                              }*/



                        }

                        if (gameObject.name == "Prysznic" && touch.phase == TouchPhase.Began)
                        {
                            shower.GetComponent<Shower>().isShowerOn = true;
                            TurnShowerOn();
                        }
                        else if (gameObject.name == "Traktor" && touch.phase == TouchPhase.Began)
                        {
                            //Debug.Log(this.gameObject.name);
                            //Dymienie();
                        }
                        else if (gameObject.name == "Kacza" || gameObject.name == "Kaczor") // tu tez 
                        {
                            kaczeKlikniecie = Random.Range(0, animClips.Length);
                            if (kaczeKlikniecie == animClips.Length)
                            {
                                StartCoroutine(KaczkaQuack());
                            }
                            else
                            {
                                randomAnim = kaczeKlikniecie;
                                if (kaczyGuard == false)
                                {
                                    stopIdleFromPlaying = true;
                                    PlayAnimationAndSound();
                                    //StartCoroutine(KaczkaQuack2());
                                    StartCoroutine(OnlyQuack());
                                }
                                else
                                {
                                    //StartCoroutine(KaczkaQuack());
                                }
                            }
                        }
                    }
                    else if (col.OverlapPoint(touchposition) == false)
                    {
                    }
                }
            }
        }
        else
        {
        }
    }
    IEnumerator OnlyQuack()
    {
        kaczyGuard = true;
        yield return new WaitForSeconds(kaczyDelay);
        kaczyGuard = false;
    }
    IEnumerator IdleGuard()
    {
        yield return new WaitForSeconds(idleGuardTime);
        stopIdleFromPlaying = false;

    }
    /*void Dymienie()
    {
        particles.Play();
    }*/
    IEnumerator Sranie()
    {
        int iR = Random.Range(0, 3);
        for (int i = iR; i > 0; i--)
        {
            if (GameObject.FindGameObjectsWithTag("Stolec").Length <= 9 - i)
            {
                yield return new WaitForSeconds(Random.Range(0.1f, 0.15f));
                int rPoop = Random.Range(0, 2);
                Quaternion q = Quaternion.Euler(0, 0, Random.Range(0, 360));
                Instantiate(poop[rPoop], poopPos.transform.position, q);
            }
            else
            {
                // Debug.Log("Stoperan");
            }
        }
    }
    //Skrypt od animacji
    public void DuckBeWalking()
    {
        this.GetComponent<zwierze_spi>().mozesz_spac = false;

        anim.SetBool("WalkBool", true);

    }
    public void StopWalking()
    {
        this.GetComponent<zwierze_spi>().mozesz_spac = true;

        anim.SetBool("WalkBool", false);
    }
    void PlayAnimationAndSound()
    {
        // do animacji wszystkich obiektow
        if (audioClips.Length == animClips.Length && audioClips.Length > 0)
        {
            randomSound = Random.Range(0, audioClips.Length);
            anim.Play(animClips[randomSound].name, -1, 0f);
            Playsound();
        }
        else if (animClips.Length == 0)
        { }
        else if (audioClips.Length > animClips.Length)
        {
            if (gameObject.name != "Kacza" || gameObject.name != "Kaczor") // tu to samo
            {
                randomAnim = Random.Range(0, animClips.Length);
                randomSound = Random.Range(0, audioClips.Length);
            }
            anim.Play(animClips[randomAnim].name, -1, 0f);
            if (gameObject.name != "krowa")
            {
                Playsound();
            }

        }
        else
        {
            randomAnim = Random.Range(0, animClips.Length);
            anim.Play(animClips[randomAnim].name, -1, 0f);
        }
    }
    void Playsound()
    {
        if (audioClips.Length == animClips.Length && audioClips.Length > 0)
        {
            src.clip = audioClips[randomSound];
            src.Play();
        }
        else if (audioClips.Length == 0 && animClips.Length > 0)
        {
            src.clip = audioClips[randomSound];
            src.Play();
        }
        else if (audioClips.Length > animClips.Length)
        {
            if (gameObject.name != "Kacza" && gameObject.name != "Kaczor")
            {
                src.clip = audioClips[randomSound];
                src.Play();
            }
        }
    }

    IEnumerator CloudSpawner()
    {
        if (cloud1 == false)
        {
            cloud1 = true;
        }
        else
        {
            yield return new WaitForSeconds(Random.Range(6f, 10f));
        }
        int rCA = Random.Range(0, 2);

        for (int i = rCA; i >= 0; i--)
        {
            rCO = new Vector3(Random.Range(0f, 1f), Random.Range(-1.5f, 1.5f), 0);
            yield return new WaitForSeconds(Random.Range(0.01f, 0.05f));

            int rC = Random.Range(0, 3);
            Vector3 rCP = this.transform.position + rCO;
            Instantiate(cloudPre[rC], rCP, this.gameObject.transform.rotation);
        }
        StartCoroutine(CloudSpawner());
        //cloud1 = true;
    }
}

