﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shower : MonoBehaviour
{
    private Farma swinka;
    private Farma kon;
    private Farma krowa;
    public bool isEating;

    public bool isPigDirty;
    public bool isShowerOn;
    // Start is called before the first frame update
    void Start()
    {
        swinka = GameObject.Find("swinka").GetComponent<Farma>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isShowerOn == true)
        {
            swinka.MakePigClean();
            isShowerOn = false;
        }

    }
}
