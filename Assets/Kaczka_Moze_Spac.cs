﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kaczka_Moze_Spac : MonoBehaviour
{
    public GameObject Kaczor;
    public GameObject KaczkaMala;
    public bool KaczkaMalaMozeSpacAnim;
    public bool KaczkaDuzaMozeSpacAnim;
    public bool KaczkaMozeSpacCykl_Dnia_i_nocy;
    public ParticleSystem zzzz_kaczka_mala;
    public ParticleSystem zzzz_kaczka_duza;
    public GameObject SunMoon;

    // Start is called before the first frame update
    void Start()
    {
        KaczkaMalaMozeSpacAnim = false;
        KaczkaDuzaMozeSpacAnim = false;
        KaczkaMozeSpacCykl_Dnia_i_nocy = false;
    }

    // Update is called once per frame
    void Update()
    {
        KaczkaMalaMozeSpacAnim = KaczkaMala.GetComponent<Kaczka_Moze_Spac>().KaczkaMalaMozeSpacAnim;
        KaczkaDuzaMozeSpacAnim = Kaczor.GetComponent<Kaczka_Moze_Spac>().KaczkaDuzaMozeSpacAnim;
        KaczkaMala.GetComponent<Kaczka_Moze_Spac>().KaczkaMozeSpacCykl_Dnia_i_nocy = KaczkaMozeSpacCykl_Dnia_i_nocy;
        Kaczor.GetComponent<Kaczka_Moze_Spac>().KaczkaMozeSpacCykl_Dnia_i_nocy = KaczkaMozeSpacCykl_Dnia_i_nocy;
    }
    public void MozeszSpacAnim_KaczkaMala()
    {
        KaczkaMalaMozeSpacAnim = true;

    }

    public void MozeszSpacAnim_KaczkaDuza()
    {
        KaczkaDuzaMozeSpacAnim = true;

    }




    public void NieMozeszSpacAnim_KaczkaMala()
    {
        KaczkaMalaMozeSpacAnim = false;
    }

    public void NieMozeszSpacAnim_KaczkaDuza()
    {
        KaczkaDuzaMozeSpacAnim = false;
    }


    public void KaczkiSen()
    {
        KaczkaMozeSpacCykl_Dnia_i_nocy = true;


    }



    public void KaczkaDuzaIdzieSpac()
    {
        if (KaczkaMozeSpacCykl_Dnia_i_nocy == true && KaczkaDuzaMozeSpacAnim == true)
        {
            SunMoon.GetComponent<Cykl_dnia_i_nocy_KD>().KaczkaDuza_spac();
            zzzz_kaczka_duza.enableEmission = true;
            zzzz_kaczka_duza.Play();
        }

    }

    public void KaczkaMalaIdzieSpac()
    {
        if (KaczkaMozeSpacCykl_Dnia_i_nocy == true && KaczkaMalaMozeSpacAnim == true)
        {
            SunMoon.GetComponent<Cykl_dnia_i_nocy_KM>().KaczkaMala_spac();

        }

    }







}
